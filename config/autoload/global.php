<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'rbplm'=>array(
        'path.wildspace' => "./data/wildspace",
    	'path.reposit.trash'=>"./data/trash",
        'path.reposit.import'=>"./data/import/packages",
        'path.reposit.importlog' => "./data/import/log",
        'path.reposit.unpack' => "./data/import/unpack",
        'path.reposit.delivred' => "./data/delivred",
        'path.scripts.doctype' => "./data/scripts/doctypeTriggers",
        'path.scripts.datatype' => "./data/scripts/datatypeTriggers",
        'path.scripts.container' => "./data/scripts/containersTriggers",
        'path.scripts.queries' => "./data/scripts/sqlqueries",
        'path.scripts.stats' => "./data/scripts/stats",
        'path.scripts.xlsrender' => "./data/excel_render",
        'path.mimesfile'=>"./config/mimes.csv",
        'visu.file_extension'=>array(".pdf", ".3dxml", ".wrl", ".igs", ".vda", ".stp"),
        'icons.doctype.compiled.path' => "./public/img/filetypes32/C",
        'icons.doctype.compiled.url' => "img/filetypes32/C",
        'icons.doctype.source.path' => "./public/img/filetypes32",
        'icons.doctype.source.url' => "img/filetypes32",
        'icons.doctype.type' => "gif",
        'mailbox.maxsize'=>500,
    ),
);
