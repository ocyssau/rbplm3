<?php 
return array(
    '569e918a134ca'=>array('', 'ged_category', '\Rbplm\Ged\Category'),
    '569e921e55b03'=>array('', 'ged_doctype', '\Rbplm\Ged\Doctype'),
    '569e91f77eb45'=>array('', 'ged_docfile', '\Rbplm\Ged\Docfile'),
    '569e92b86d248'=>array('', 'ged_docfile_version', '\Rbplm\Ged\Docfile\Version'),
    '569e924be82e7'=>array('', 'ged_document', '\Rbplm\Ged\Document'),
    '569e92709feb6'=>array('', 'ged_document_version', '\Rbplm\Ged\Document\Version'),
    '569e92f45d8ad'=>array('', 'ged_docfile_role', '\Rbplm\Ged\Docfile\Role'),
    '569e93ae7111f'=>array('', 'org_ou', '\Rbplm\Org\Mockup'),
    '569e93c6ee156'=>array('', 'org_ou', '\Rbplm\Org\Project'),
    '569e93dc9439c'=>array('', 'org_ou', '\Rbplm\Org\Root'),
    '569e93ef2cbbc'=>array('', 'org_ou', '\Rbplm\Org\Unit'),
    '569e94192201a'=>array('', 'org_ou', '\Rbplm\Org\Workitem'),
    '569e9459a1e71'=>array('', 'people_group', '\Rbplm\People\Group'),
    '569e94984ded3'=>array('', 'people_user', '\Rbplm\People\User'),
    '569b63eb7cce4'=>array('', 'people_user_preference', '\Rbplm\People\User\Preference'),
    '569e951e03bac'=>array('', 'vault_record', '\Rbplm\Vault\Record'),
    '569e955d1d1c4'=>array('', 'vault_reposit', '\Rbplm\Vault\Reposit'),
    '569e95a3ec264'=>array('', 'vault_reposit', '\Rbplm\Vault\Reposit\Read'),
    '569e95d92cc5a'=>array('', 'vault_reposit', '\Rbplm\Vault\Cache\Reposit'),
    '569e967f01ca6'=>array('', 'pdm_effectivity', '\Rbplm\Pdm\Effectivity'),
    '569e96b70f2a1'=>array('', 'pdm_product', '\Rbplm\Pdm\Product'),
    '569e972dd4c2c'=>array('', 'pdm_product_version', '\Rbplm\Pdm\Product\Version'),
    '569e96e35bab7'=>array('', 'pdm_usage', '\Rbplm\Pdm\Usage'),
    '569e970bdc932'=>array('', 'pdm_product_concept', '\Rbplm\Pdm\Product\Concept'),
    '569e9714da9aa'=>array('', 'pdm_product_context', '\Rbplm\Pdm\Product\Context'),
    '569e971bb57c0'=>array('', 'pdm_product_instance', '\Rbplm\Pdm\Product\Instance'),
    '569e9722bf42a'=>array('', 'pdm_product_material', '\Rbplm\Pdm\Product\Material'),
    '569e972851e2e'=>array('', 'pdm_product_physicalproperties', '\Rbplm\Pdm\Product\PhysicalProperties'),
);
