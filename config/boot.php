<?php
//%LICENCE_HEADER%

use Rbplm\Dao\Connexion;
use Rbplm\Dao\Factory as DaoFactory;


/**
 * Init constants for command line interface
 */
function rbinit_cli()
{
	define('CRLF',"\n");
	define('RBPLM_USESESSION', false );
}

/**
 * Init constants for soap interface
 */
function rbinit_soap()
{
	define('RBPLM_USESESSION', true );
	define('RBPLM_SESSION_NS', 'RBSERVICE' );
	rbinit_web();
}

/**
 * Init constants for sessions
 */
function rbinit_usesession()
{
	if( !defined('RBPLM_USESESSION') ){
		if( getenv('USESESSION') == 'On' ){
			define('RBPLM_USESESSION', true );
		}
		else{
			define('RBPLM_USESESSION', false );
		}
	}
	if( !defined('RBPLM_SESSION_NS') ){
		define('RBPLM_SESSION_NS', 'rbplm' );
	}
}

/**
 * Init include path
 */
function rbinit_includepath()
{
	// Define path to application directory
	if( !defined('RBPLM_APP_PATH') ){
		if( getenv('APPLICATION_PATH') ){
			define('RBPLM_APP_PATH', (getenv('APPLICATION_PATH') ) );
		}
		else{
			define('RBPLM_APP_PATH',
					str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/..' ) )
			);
		}
	}
	/*Alias for RBPLM_APP_PATH*/
	define('APPLICATION_PATH', RBPLM_APP_PATH );

	// Define path to application directory
	if( !defined('RBPLM_LIB_PATH') ){
		if( getenv('LIB_PATH') ){
			define('RBPLM_LIB_PATH', (getenv('LIB_PATH') ) );
		}
		else{
			define('RBPLM_LIB_PATH',
					str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../../library' ) )
			);
		}
	}

	// Define path to application directory
	if( !defined('RBPLM_EXTLIB_PATH') ){
		if( getenv('EXTLIB_PATH') ){
			define('RBPLM_EXTLIB_PATH', (getenv('EXTLIB_PATH') ) );
		}
		else{
			define('RBPLM_EXTLIB_PATH',
				str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../../external' ) )
			);
		}
	}

	// Ensure library/ is on include_path
	set_include_path ( implode ( PATH_SEPARATOR,
						array (
							realpath ( RBPLM_LIB_PATH ),
							realpath ( RBPLM_EXTLIB_PATH ),
							get_include_path ()
						)
					)
	);
}

/**
 * Init constants for define configs files and set config for Rbplm
 */
function rbinit_config()
{
	if( !defined('RB_INI_FILE') ){
		if( getenv('INI_FILE') ){
			define('RB_INI_FILE', getenv('INI_FILE') );
		}
		else{
			define('RB_INI_FILE', RBPLM_APP_PATH . '/configs/rbplm.ini');
		}
	}
	if( !defined('RB_DEF_INI_FILE') ){
		define('RB_DEF_INI_FILE', RBPLM_APP_PATH . '/configs/default/default.ini');
	}

	// Define application environment
	if( !defined('RBPLM_APP_ENV') ){
		define('RBPLM_APP_ENV',
				(getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
	}

	if( RBPLM_USESESSION == true ){
		//minimal options for session
		ini_set('session.use_cookies', 1);
		ini_set('session.use_only_cookies', 1);
		$session = Rbplm::getSession();

		if( $session->config ){
			$config = $session->config;
		}
		else{
			$config = _rbinit_configFactory();
			$session->config = $config;
		}
	}
	else{
		$config = _rbinit_configFactory();
	}

	Rbplm::setConfig( $config );
	\Rbplm\Sys\Config::setConfig ( $config['rbplm'] );

	date_default_timezone_set($config['phpSettings']['date']['timezone']);
}

/**
 * Init the DAO factory and loader
 */
function rbinit_dao()
{
	$config = Rbplm::getConfig();

	/* Configure the connexions */
	Connexion::setConfig( $config['rbplm']['db'] );

	/* Set the loader class to use for access to objects index */
	\Rbplm\Dao\Loader::setLoader('\Rbplm\Dao\Pg\Loader');

	/* Configure the factory.
	 * Config map buizness classes to dao classes and eventually,
	 * define the connexion to used for each buizness classes. */
	DaoFactory::setConfig(include(__DIR__.'/objectdaomap.dist.php'));

	/* Set the default suffix for DAO.
	 * Dao class name will use that format:
	 * [buizness class name] . 'Dao' . $defdaotype
	 */
	DaoFactory::setDefaultType( $config['rbplm']['defdaotype'] );

	/* Configure the Pg Dao */
	$conn = Connexion::get();
	if($conn){
		/* Set connexion to Pg_Loader*/
		\Rbplm\Dao\Pg\Loader::setConnexion( $conn );
		/*Init class/id mapping connexion (specific to Pg Dao)*/
		\Rbplm\Dao\Pg\ClassDao::singleton()->setConnexion( $conn );
	}
}

/**
 * Init file system protection and paths
 */
function rbinit_fs()
{
	$config = Rbplm::getConfig();
	\Rbplm\Sys\Trash::$path = $config['rbplm']['path']['trash'];
	\Rbplm\Sys\Filesystem::addAuthorized(realpath(\Rbplm\Sys\Trash::$path));
	\Rbplm\Sys\Filesystem::addAuthorized(realpath('/tmp/anonymous'));
	\Rbplm\Sys\Filesystem::isSecure(false);
}

/**
 * Init logger
 */
function rbinit_logger()
{
	Rbplm::setLogger( \Rbplm\Sys\Logger::singleton() );
}

/**
 * Load meta-model
 */
function rbinit_metamodel()
{
	$config = Rbplm::getConfig();
	if( RBPLM_USESESSION == 1 ){
		$session = Rbplm::getSession();
		if( $session->metamodel ){
			\Rbplm\Sys\Meta\Model::setSingleton($session->metamodel);
		}
		else{
			rbinit_loadMetamodel($config);
			$session->metamodel = \Rbplm\Sys\Meta\Model::singleton();
		}
	}
	else{
		rbinit_loadMetamodel($config);
	}
}

/**
 * Load meta-model
 */
function rbinit_loadMetamodel($config)
{
	if($config['rbplm']['metamodel']['default']['loader'] == 'ods'){
		$Loader = new \Rbplm\Sys\Meta\Loader\Ods( array('filename'=>$config['rbplm']['metamodel']['default']['filename']) );
	}
	else if($config->rbplm->metamodel->default->loader == 'csv'){
		$Loader = new \Rbplm\Sys\Meta\Loader\Csv( array('filename'=>$config['rbplm']['metamodel']['default']['filename']) );
	}
	else{
		throw new Exception('None valid meta-model loader is set. Check your config key rbplm.metamodel.default.loader');
	}
	$Loader->load( \Rbplm\Sys\Meta\Model::singleton() );
	return $Loader;
}

/**
 * Init a autoloader.
 * Not use in application, prefer Zend_Autoload.
 * If $zend = true, init a Zend_Loader_Autoloader
 * else add _rbinit_loader in spl_autoload_register()
 *
 * @param boolean $zend	[OPTIONAL]
 */
function rbinit_autoloader($zend = true)
{
	$zend=false;
	if($zend){
		require_once ('Zend/Loader/Autoloader.php');
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Rbplm_');
		$autoloader->registerNamespace('Rbservice_');
		$autoloader->registerNamespace('Rba_');
		$autoloader->registerNamespace('Rbview_');
		$autoloader->registerNamespace('ZendRb_');
		$autoloader->registerNamespace('Zend_');
		$autoloader->registerNamespace('HTML_');
		$autoloader->registerNamespace('modules_');
		return $autoloader;
	}
	else{
		spl_autoload_register('_rbinit_loader', true);
	}
}

/**
 * Loader Callback funtion
 *
 * @param string	$className
 * @return void
 */
function _rbinit_loader($className)
{
	$className = str_replace('\\', '_', $className);
	$file = str_replace('_', '/', $className) . '.php';
	include($file);
}

