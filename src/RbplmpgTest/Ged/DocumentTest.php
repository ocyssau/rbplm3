<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Ged;

use \Rbplm\Ged\Document;

/**
 * @brief Test class for \Rbplm\Ged\Document.
 * @include Rbplm/Ged/DocumentTest.php
 * 
 */
class DocumentTest extends \Rbplm\Test\Test
{
    /**
     * @var    \Rbplm\Ged\Document
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	$this->object = new \Rbplm\Ged\Document($properties, \Rbplm\Org\Root::singleton());
    }
	
	function testSetParent(){
		//require_once 'Rbplm/Org/Unit.php';
		$ou = new \Rbplm\Org\Unit();
		$ret = $this->object->setParent($ou);
		assert($ret === $this->object);
	}
	
	function testGetParent(){
		//require_once 'Rbplm/Org/Unit.php';
		$ou = new \Rbplm\Org\Unit();
		$this->object->setParent($ou);
		
		$ret = $this->object->getParent();
		assert( $ret === $ou);
	}
	
	function testGetVersions(){
		//require_once 'Rbplm/Ged/Document/Version.php';
		
		$versions = $this->object->getVersions();
		$versions->add( new \Rbplm\Ged\Document\Version() );
		$versions->add( new \Rbplm\Ged\Document\Version() );
		$versions->add( new \Rbplm\Ged\Document\Version() );

		assert( is_a($versions, '\Rbplm\Model\Collection') );
		assert( $versions->count() == 3 );
		
		foreach($versions as $version){
			var_dump( $version->getName() );
		}
	}
	
	function testNewVersion(){
		$this->setUp();
		
		//require_once 'Rbplm/Org/Unit.php';
		$ou = new \Rbplm\Org\Unit();
		
		$ret = $this->object->newVersion( 'my_version_name1', $ou );
		$ret = $this->object->newVersion( 'my_version_name2', $ou );
		$ret = $this->object->newVersion( 'my_version_name3', $ou );
		
		assert( is_a($ret, '\Rbplm\Ged\Document\Version') );
		
		$versions = $this->object->getVersions();
		assert( $versions->count() === 3 );
		
		$versions->rewind();
		$versions->current()->getName == 'my_version_name1';
		$versions->next();
		$versions->current()->getName == 'my_version_name2';
		
		foreach($versions as $version){
			$parent = $version->getParent();
			assert( $parent === $ou );
			
			//require_once('Rbplm/Org/Root.php');
			$ou2 = new \Rbplm\Org\Unit();
			$ou2->setParent( \Rbplm\Org\Root::singleton() );
			$parent->setParent( $ou2 );
			
			//Recalculate the path after add a parent in chain of dependencies
			$path = $version->getPath(true);
			$path2 = '/' . \Rbplm\Org\Root::singleton()->getLabel() . '/' . $ou2->getLabel().'/'.$parent->getLabel().'/'.$version->getLabel();
			assert( $path === $path2 );
			
			assert($version->getBase() === $this->object);
		}
	}
	
	
	function testSetterGetter(){
		$this->setUp();
		
		$this->object->description = 'My test base document';
		assert( $this->object->description === 'My test base document');
		
		//With unexisting property
		$this->object->any = 'A not define property';
		assert( $this->object->any === 'A not define property');
		
		//A protected property
		try{
			$this->object->_document_version = 'BadType';
		}
		catch(\Exception $e){
			assert( is_a($e, '\Rbplm\Ged\Exception') );
			var_dump( $e->getMessage() );
			$faulty = true;
		}
		assert($faulty);
	}
	
	
	function testDaoSaveLoad(){
		
		$ou = new \Rbplm\Org\Unit( array('name'=>uniqid('myOu')), \Rbplm\Org\Root::singleton() );
		$DaoOu = new \Rbplm\Org\UnitDaoPg( array() );
		$DaoOu->setConnexion( \Rbplm\Dao\Connexion::get() );
		$DaoOu->save($ou);
		
		$Document1 = new \Rbplm\Ged\Document( array(), $ou );
		$Document1->description = 'My test base document';
		$Document1->setName('My_Object');
		$Document1->setNumber( uniqid('My_Object001') );
		$Document1->version = 3;
		$Document1->iteration = 5;
		$Document1->init();
		
        $Dao = new \Rbplm\Ged\DocumentDaoPg( array() );
        $Dao->setConnexion( \Rbplm\Dao\Connexion::get() );
        $Dao->save($Document1);
        
        $Document2 = new \Rbplm\Ged\Document(null, $this->object->getParent());
        $Dao->loadFromUid( $Document2, $Document1->getUid() );
        
        assert( $Document1->description == $Document2->description );
        assert( $Document1->name == $Document2->getName() );
        assert( $Document1->number == $Document2->getNumber() );
        assert( \Rbplm\Uuid::compare($Document1->getUid(), $Document2->getUid()) );
        assert( \Rbplm\Uuid::compare($Document1->updateById, $Document2->updateById) );
        assert( $Document1->updated == $Document2->updated );
        assert( \Rbplm\Uuid::compare($Document1->createById, $Document2->createById) );
        assert( $Document1->created == $Document2->created );
        assert( \Rbplm\Uuid::compare($Document1->ownerId, $Document2->ownerId) );
	}
	
}

