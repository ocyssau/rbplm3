<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Ged\Docfile;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Proxy\Generic;
use Rbplm\Dao\Connexion;
use Rbplm\Dao\Pg\DaoList;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\AccessCode;
use Rbplm\Ged\Historize;

use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsata;


/**
 * @brief Test class for \Rbplm\Ged\Docfile_Version.
 * @include Rbplm/Ged/Docfile/VersionTest.php
 */
class VersionTest extends \Rbplm\Test\Test
{
	
	/**
	 * @var    \Rbplm\Ged\Docfile_Version
	 * @access protected
	 */
	protected $object;
	
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$ou = \Rbplm\Org\Unit::init(uniqid('myOu'));
		$ou->setParent(\Rbplm\Org\Root::singleton());
		
		$Base = Docfile::init(uniqid('baseDocfile'));
		$Base->setParent($ou);
		
		$this->object = DocfileVersion::init('docfileV1.'.uniqid());
		$this->object->setBase($Base);
		$this->object->setNumber(uniqid());
		
		if($this->_withDao){
			DaoFactory::getDao($ou)->save($ou);
			DaoFactory::getDao($Base)->save($Base);
		}
	}
	
	/**
	 * From class: \Rbplm\Ged\Docfile_Version
	 */
	function Test_DaoIterationAssoc()
	{
	
		/*Create new records require for tests*/
		$record1 = \Rbplm\Vault\Record::init();
		$record2 = \Rbplm\Vault\Record::init();
		$record3 = \Rbplm\Vault\Record::init();
		
		/*Save the records*/
		$rDao = DaoFactory::getDao('Rbplm\Vault\Record');
		$rDao->save($record1)
			 ->save($record2)
			 ->save($record3);
		
		/*Add new records to Docfile*/
		$this->object->getLinks()->add($record1, 1);
		$this->object->getLinks()->add($record2, 2);
		$this->object->getLinks()->add($record3, 3);
        
		var_dump(CurrentUser::get()->getUid());
		
		/*Save*/
		$Dao = DaoFactory::getDao('\Rbplm\Ged\Docfile\Version');
		$Dao->save( $this->object );
		
		/*Assertions*/
		$FilteredCollection = new \Rbplm\Model\CollectionClassFilter($this->object->getLinks(), '\Rbplm\Vault\Record');
		foreach($FilteredCollection as $r){
			var_dump( get_class($r) );
			assert( is_a($r, '\Rbplm\Vault\Record') );
		}
		var_dump('Count:', count($FilteredCollection));
		var_dump($FilteredCollection[0]->getUid(), $FilteredCollection->key());
		var_dump($FilteredCollection[1]->getUid(), $FilteredCollection->key());
		var_dump($FilteredCollection[2]->getUid(), $FilteredCollection->key());
		
		assert( $FilteredCollection[0]->getUid() ==  $record1->getUid() );
		
		assert( $this->object->getLinks()->offsetGet($record1) ==  1 );
		assert( $this->object->getLinks()->offsetGet($record2) ==  2 );
		
		/*Reset the records associations*/
		$this->object->getLinks()->offsetUnset($record1);
		$this->object->getLinks()->offsetUnset($record2);
		$this->object->getLinks()->offsetUnset($record3);
		
		foreach($FilteredCollection as $r){
			var_dump(get_class($r));
			assert( is_a($r, '\Rbplm\Vault\Record') );
		}
		
		var_dump('Count:', count($FilteredCollection));
		
		/*
		 * Load from a list
		 */
		/*
		$ListDao = new DaoList( array('vtable'=>'view_vault_record_links') );
		$ListDao->setConnexion( Connexion::get() );
		$uid = $this->object->getUid();
		$filter = "docfileId='$uid' ORDER BY iteration ASC";
		$ListDao->load($filter);
		
		$t = $ListDao->current();
		assert( \Rbplm\Uuid::compare($t['uid'], $record1->getUid() ) );
		assert( $t['iteration'] == 1 );
		$ListDao->next();
		$t = $ListDao->current();
		assert( \Rbplm\Uuid::compare($t['uid'], $record2->getUid() ) );
		assert( $t['iteration'] == 2 );
		$ListDao->next();
		$t = $ListDao->current();
		assert( \Rbplm\Uuid::compare($t['uid'], $record3->getUid() ) );
		assert( $t['iteration'] == 3 );
		*/
		
		/*
		 * Load record in collection.
		 * Query the view view_docfile_data_record_assoc to get properties of records.
		 */
		/*
		$ListDao = new DaoList( array('table'=>'view_vault_record_links') );
		$ListDao->setConnexion( Connexion::get() );
		$ListDao->loadInCollection($this->object->getLinks(), $filter);
		*/
		
		//Reload object
		$uid = $this->object->getUid();
		$Docfile = new \Rbplm\Ged\Docfile\Version();
		$Dao = DaoFactory::getDao($Docfile);
		$Dao->loadFromUid($Docfile, $uid);
		
		var_dump($Docfile->name);
		var_dump($Docfile->uid);
		
		assert($Docfile->uid==$uid);
		
		/*
		$Dao->loadLinks($Docfile, '\Rbplm\Vault\Record');
		$FilteredCollection = new \Rbplm\Model\CollectionClassFilter($Docfile->getLinks(), '\Rbplm\Vault\Record');
		foreach($FilteredCollection as $r){
			var_dump( get_class($r), $r->getUid() );
			assert( is_a($r, '\Rbplm\Vault\Record') );
		}
		assert( count($FilteredCollection) == 3 );
		*/
	}
	
	
	/**
	 * From class: \Rbplm\Ged\Docfile_Version
	 */
	function Test_DaoCheckInCheckOut()
	{
		$this->setUp();
		\Rbplm\Sys\Filesystem::isSecure(false);
		
		$dataPath=realpath('../../data');
		var_dump($dataPath);
		
		//Vault file
		echo '--CREATE RECORD IN VAULT--'.CRLF;
		$file1 = $dataPath.'/testfile1.txt';
		touch($file1);
		file_put_contents($file1, 'version001' . "\n");
		
		$reposit=Reposit::init($dataPath.'/repositTest/'.uniqId());
		$vault = new \Rbplm\Vault\Vault($reposit, DaoFactory::getDao('\Rbplm\Vault\Record'));
		$record=Record::init();
		$vaultfilename = './' . $this->object->getName() . '.txt';
		$vault->record( $record, $file1, 'file', $vaultfilename );
		
		var_dump($reposit->getUrl());
		var_dump($vault->getReposit()->getUrl());
		var_dump($vault->getLastRecord()->fullpath);
		
		$this->object->setData( $record );
		
		echo '--SAVE DOCFILE--'.CRLF;
		DaoFactory::getDao($this->object)->save($this->object);
		
		echo '--CHECKOUT--'.CRLF;
		$this->object->lock(AccessCode::CHECKOUT);
		
		assert( $this->object->checkAccess() == AccessCode::CHECKOUT );
		assert( $this->object->getLockBy()->getUid() == CurrentUser::get()->getUid() );
		/*Or is valid too :*/
		assert( $this->object->lockById == CurrentUser::get()->getUid() );
		
		$it = $this->object->iteration;
		var_dump($it);
		
		//echo '--PUSH RECORD IN ITERATION REPOSIT--'.CRLF;
		//$vault->depriveToIteration($record, $it);
		
		echo '--HISTORIZE DOCFILE--'.CRLF;
		Historize::depriveRecordToIteration($reposit, $this->object->getData(), $this->object->iteration);
		$iterationDocfile = Historize::depriveDocfileToIteration($this->object);
		
		echo '--UPDATE AND HISTORIZE--'.CRLF;
		$releasing = false;
		$checkAccess = true;

		echo '--CREATE UPDATE RECORD IN VAULT--'.CRLF;
		$file2 = $dataPath.'/testfile2.txt';
		touch($file2);
		file_put_contents($file2, 'version002' . "\n");
		
		$record2=Record::init('2nd record');
		$vault->record($record2, $file2, 'file', $vaultfilename);
		$ret = $this->object->checkIn($record2, $releasing, $checkAccess);
		DaoFactory::getDao($this->object)->save($this->object);
		
		assert( $this->object->checkAccess() == AccessCode::CHECKOUT );
		assert( $this->object->getLockBy() == CurrentUser::get() );
		assert( $this->object->iteration == $it + 1 );
		assert( $this->object->getData() == $record2 );
		assert( $this->object->getPreviousData() == $record );
		
		$this->reposit=$reposit;
		$this->vault=$vault;
		
	}
	
}


