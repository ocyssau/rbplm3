<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Ged;

use Rbplm\Ged\Docfile;
use Rbplm\Dao\Factory as DaoFactory;


/**
 * @brief Test class for \Rbplm\Ged\Docfile.
 * @include Rbplm/Ged/DocfileTest.php
 * 
 */
class DocfileTest extends \Rbplm\Test\Test
{
    /**
     * @var    \Rbplm\Ged\Docfile
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	$this->object = new \Rbplm\Ged\Docfile( $properties, \Rbplm\Org\Root::singleton() );
    }
	
	function Test_SetParent(){
		//require_once 'Rbplm/Org/Unit.php';
		$ou = new \Rbplm\Org\Unit();
		$ret = $this->object->setParent($ou);
		assert($ret === $this->object);
	}
	
	function Test_GetParent(){
		//require_once 'Rbplm/Org/Unit.php';
		$ou = new \Rbplm\Org\Unit();
		$this->object->setParent($ou);
		
		$ret = $this->object->getParent();
		assert( $ret === $ou);
	}
	
	function Test_GetVersions(){
		//require_once 'Rbplm/Ged/Docfile/Version.php';
		
		$versions = $this->object->getVersions();
		$versions->add( new \Rbplm\Ged\Docfile\Version() );
		$versions->add( new \Rbplm\Ged\Docfile\Version() );
		$versions->add( new \Rbplm\Ged\Docfile\Version() );

		assert( is_a($versions, '\Rbplm\Model\Collection') );
		assert( $versions->count() == 3 );
		
		foreach($versions as $version){
			var_dump( $version->getName() );
		}
	}
	
	function Test_NewVersion(){
		$this->setUp();
		
		//require_once 'Rbplm/Org/Unit.php';
		$ou = new \Rbplm\Org\Unit();
		
		$ret = $this->object->newVersion( 'my_version_name1', $ou );
		$ret = $this->object->newVersion( 'my_version_name2', $ou );
		$ret = $this->object->newVersion( 'my_version_name3', $ou );
		
		assert( is_a($ret, '\Rbplm\Ged\Docfile\Version') );
		
		$versions = $this->object->getVersions();
		var_dump( $versions->count() );
		assert( $versions->count() === 3 );
		
		$versions->rewind();
		$versions->current()->getName == 'my_version_name1';
		$versions->next();
		$versions->current()->getName == 'my_version_name2';
		
		foreach($versions as $version){
			$parent = $version->getParent();
			assert( $parent === $ou );
			
			//require_once('Rbplm/Org/Root.php');
			$ou2 = new \Rbplm\Org\Unit();
			$ou2->setParent( \Rbplm\Org\Root::singleton() );
			$parent->setParent( $ou2 );
			
			//Recalculate the path after add a parent in chain of dependencies
//			$path = $version->getPath(true);
//			$path2 = '/' . \Rbplm\Org\Root::singleton()->getLabel() . '/' . $ou2->getLabel().'/'.$parent->getLabel().'/'.$version->getLabel();
//			assert( $path === $path2 );
			
//			assert($version->getBase() === $this->object);
		}
	}
	
	
	function Test_SetterGetter(){
		$this->setUp();
		
		$this->object->description = 'My test base document';
		assert( $this->object->description === 'My test base document');
		
		//With unexisting property
		$this->object->any = 'A not define property';
		assert( $this->object->any === 'A not define property');
		
		//A protected property
		try{
			$this->object->_document_version = 'BadType';
		}
		catch(\Exception $e){
//			assert( is_a($e, '\Rbplm\Ged\Exception') );
			var_dump( $e->getMessage() );
			$faulty = true;
		}
		assert($faulty);
	}
	
	
	function Test_DaoSaveLoad(){
		
		$ou = new \Rbplm\Org\Unit( array('name'=>uniqid('myOu')), \Rbplm\Org\Root::singleton() );
		$DaoOu = new \Rbplm\Org\UnitDaoPg( array() );
		$DaoOu->setConnexion( \Rbplm\Dao\Connexion::get() );
		$DaoOu->save($ou);
		
		$Docfile1 = new \Rbplm\Ged\Docfile( array(), $ou );
		$Docfile1->description = 'My test base document';
		$Docfile1->setName('My_Object');
		$Docfile1->setNumber( uniqid('My_Object001') );
		$Docfile1->version = 3;
		$Docfile1->iteration = 5;
		$Docfile1->state = 'asBuild';
		$Docfile1->init();
		
        //$config = \Rbplm\Sys\Config::getConfig();
        //$DaoConfig = \Rbplm\Sys\Config::getDaoConfiguration('default');
        $Dao = new \Rbplm\Ged\DocfileDaoPg( array() );
        $Dao->setConnexion( \Rbplm\Dao\Connexion::get() );
        $Dao->save($Docfile1);
        
        $Docfile2 = new \Rbplm\Ged\Docfile( null, $this->object->getParent() );
        $Dao->loadFromUid( $Docfile2, $Docfile1->getUid() );
        
        assert( $Docfile1->description == $Docfile2->description );
        assert( $Docfile1->name == $Docfile2->name );
        assert( $Docfile1->number == $Docfile2->number );
        assert( $Docfile1->updated == $Docfile2->updated );
        assert( $Docfile1->created == $Docfile2->created );
        assert( \Rbplm\Uuid::compare($Docfile1->getUid(), $Docfile2->getUid()) );
        assert( \Rbplm\Uuid::compare($Docfile1->createById, $Docfile2->createById) );
        assert( \Rbplm\Uuid::compare($Docfile1->updateById, $Docfile2->updateById) );
        assert( \Rbplm\Uuid::compare($Docfile1->ownerId, $Docfile2->ownerId) );
	}
	
}

