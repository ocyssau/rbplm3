<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Acl;

use Rbplm\Acl\Acl;
use Rbplm\Acl\AclDaoPg as AclDao;
use Rbplm\Acl\Initializer As AclInitializer;
use Rbplm\People;
use Rbplm\People\CurrentUser;
use Rbplm\Org;
use Rbplm\Dao;
use Rbplm\Dao\Connexion;
use Rbplm\Dao\Pg\Loader;
use Rbplm\Dao\Factory as DaoFactory;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 * 
 * @include Rbplm/Acl/Test.php
 */
class Test extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\Acl\Acl
	 * @access protected
	 */
	protected $object;
	
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		Dao\Registry::singleton()->reset();
		
		$this->object = Acl::newSingleton();
		
		/*Reset Org_Root*/
		$rootOu = Org\Root::singleton();
		
		/*Reset current user*/
		$CurrentUser = CurrentUser::get();
		$CurrentUser->setParent( Org\Root::singleton() );
		
		echo "Get current user \n";
		$CurrentUser = CurrentUser::get();
		$CurrentUser->setParent( Org\Root::singleton() );
		
		/* First you must create groups tree definition and assign groups to user. */
		
		/**/
		echo "Create some groups \n";
		$this->group00 = People\Group::init( uniqid('group00') )->setParent($CurrentUser->getParent());
		$this->group01 = People\Group::init( uniqid('group01') )->setParent($CurrentUser->getParent());
		$this->group02 = People\Group::init( uniqid('group02') )->setParent($CurrentUser->getParent());
		$this->group10 = People\Group::init( uniqid('group10') )->setParent($CurrentUser->getParent());
		
		echo "Create a groups trees where group00 and group10 are roots \n";
		/*
		 * In functional view, the tree is like next, but note that the logical tree is reverse.
		* Functional view:
		* $group00
		* 		|$group01
		* 		|$group02
		* $group10
		* 		|$group01
		* 		|$group02
		* 	$group01
		* 		|$CurrentUser
		* 	$group02
		* 		|$CurrentUser
		*
		* Logical view, as tree is set in model:
		* $CurrentUser
		* 		|$group01
		* 			|$group00
		* 			|$group10
		* 		|$group02
		* 			|$group00
		* 			|$group10
		*/
		$this->group01->getGroups()->add( $this->group00 );
		$this->group01->getGroups()->add( $this->group10 );
		$this->group02->getGroups()->add( $this->group00 );
		$this->group02->getGroups()->add( $this->group10 );
		
		/**/
		echo "Assign groups to currrent user \n";
		$CurrentUser->getGroups()->add($this->group01);
		$CurrentUser->getGroups()->add($this->group02);
		
		/*
		 * Now we must create resources.
		* In this example, create a set of Org_Unit with parent relationships:
		* $RootOu
		* 		|$this->Ou001
		* 			|$this->Ou002
		* 				|$this->Ou003
		* 				|$this->Ou004
		*/
		echo "Create ressources \n";
		$RootOu = Org\Root::singleton();
		$this->Ou001 = Org\Unit::init('Ou001')->setParent($RootOu);
		$this->Ou002 = Org\Unit::init('Ou002')->setParent($this->Ou001);
		$this->Ou003 = Org\Unit::init('Ou003')->setParent($this->Ou002);
		$this->Ou004 = Org\Unit::init('Ou004')->setParent($this->Ou002);
	}
	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		unset($CurrentUser->groups);
		unset($CurrentUser->links);
		unset($CurrentUser->children);
		
		$Acl = Acl::newSingleton();
	}
	
	/**
	 * 
	 * Example to show how use the initializer.
	 */
	function Test_Tutorial()
	{
	    $this->setUp();

	    $CurrentUser = CurrentUser::get();
	    
		/*
		 * When user and his groups are set you may call Initializer:initRole() on User to init.
		 * InitRole create Role for $CurrentUser and for each of his parent groups.
		 * As it is a recusrsive method, create too Role for each groups of groups.
		 * Add each role to acl and set correctly parent in accordance to groups links relations.
		 */
		echo "Init roles \n";
		AclInitializer::initRole( $CurrentUser );
		
		/*
		 * The initResource method must be call on leafs anyobjects.
		 * This method create resources in a \Zend\Acl instance for $this->Ou004, $this->Ou003 and each of their parents.
		 */
		echo "Init ressources \n";
    	AclInitializer::initResource( $this->Ou004 );
		AclInitializer::initResource( $this->Ou003 );
		
		/*Get ACL object from singleton to get rules define internaly to People objects*/
		/*
		 * The \Zend\Acl instance initialize by initRole and initResource is reachable by the singleton
		 * of \Rbplm\Acl\Acl.
		 */
		$Acl = Acl::singleton();
		
		/* Now define some rules */
		
		/*
		 * $group00 is able to read $this->Ou003 and $this->Ou004
		 */
		$Acl->allow($this->group00->getUid(), array($this->Ou003->getUid(), $this->Ou004->getUid()), 'read');
		
		/*
		 * Has group00 is able to read $this->Ou003, current user is able too.
		 */
		assert($Acl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') == true);
		
		/*Prority rule illustration*/
		/*
		 * If a rule is define twice in inheritance tree, priority is given to the lower level as
		 * priority rule define in \Zend_Acl.
		 * In this example, for $this->Ou003 'read' is deny to currentUser, but is allow on group00, and currentUser is belong to group00.
		 * Result of Acl calculation is a deny of currentUser on resource $this->Ou003 for 'read'.
		 */
		$Acl->deny($CurrentUser->getUid(), $this->Ou003->getUid(), 'read');
		assert($Acl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') == false);
		
		/**/
		echo "To save the acl, first all anyobject involved in ACL must be saved \n";
		echo "Save roles \n";
		DaoFactory::getDao($this->group00)->save($this->group00);
		DaoFactory::getDao($this->group01)->save($this->group01);
		DaoFactory::getDao($this->group02)->save($this->group02);
		DaoFactory::getDao($this->group10)->save($this->group10);

		echo "Save ressources \n";
		DaoFactory::getDao($this->Ou001)->save($this->Ou001);
		DaoFactory::getDao($this->Ou002)->save($this->Ou002);
		DaoFactory::getDao($this->Ou003)->save($this->Ou003);
		DaoFactory::getDao($this->Ou004)->save($this->Ou004);
		
		/**/
		echo "Save the rules \n";
		DaoFactory::getDao($Acl)->save($Acl);
		
		/**/
		//echo "Save the links of user \n";
		//DaoFactory::getDao($CurrentUser)->save($CurrentUser);
		
		echo 'Current user uid :' . $CurrentUser->getUid() . CRLF;
		echo 'Resource uid :' . $this->Ou003->getUid() . CRLF;
		var_dump( 'has links:' . $CurrentUser->getName(), $CurrentUser->hasLinks() );
	}

	function _Test_DaoLoadMethode2()
	{
	    /* How to load ACLs. METHODE 2*/
	    /*
	     * Use the loadAclFromRoleResource method.
	    */
	    echo 'How to load ACLs. METHODE 2' . CRLF;
	    /* Re-init ACLs */
	    echo 'Re-init ACLs' . CRLF;
	    $Acl = null;
	    $Acl = Acl::newSingleton();
	     
	    /* Load acls */
	    $RoleId = CurrentUser::get()->getUid();
	    $ResourcePath = $this->Ou003->getPath();
	    echo 'Use the loadAclFromRoleResource method.' . CRLF;
	    $AclList = DaoFactory::get($Acl)->loadAclFromRoleResource($RoleId, $ResourcePath, $Acl);
	    
	    /* Check */
	    assert($Acl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') == false);
	}
	
	function _Test_DaoDelete()
	{
	    echo '****************************Read, suppress ACL*********************************************' . CRLF;
	    /*
	     * Load rule in list to read acl about a resource and role.
	    */
	    echo 'Load rule in list to read acl about a resource and role.' . CRLF;
	    $rsrcId = $this->Ou003->getUid();
	    $roleId = $CurrentUser->getUid();
	    $List = DaoFactory::get($Acl)->newList();
	    $List->load("resource_id='$rsrcId' AND role_id='$roleId'");
	    foreach($List as $rule){
	        var_dump($List->toApp($AclDao));
	    }
	     
	    /*
	     * Suppress some rule
	    */
	    $List->suppress("resource_id='$rsrcId' AND role_id='$roleId'");
	     
	    /*
	     * To suppress all rules, you must create a condition always true, because filter string can not be empty in suppress method
	    */
	    $List->suppress("1=1");
	}
	
	
	/**
	 * @return void
	 */
	function Test_General()
	{
		$this->setUp();

		/*Get current user*/
		$CurrentUser = CurrentUser::get();
		
		echo "construct a new Acl definition \n";
		$User001Role = new \Zend\Permissions\Acl\Role\GenericRole( $CurrentUser->getUid() );
		$Group00Role = new \Zend\Permissions\Acl\Role\GenericRole( $this->group00->getUid() );
		$Group01Role = new \Zend\Permissions\Acl\Role\GenericRole( $this->group01->getUid() );
		$Group02Role = new \Zend\Permissions\Acl\Role\GenericRole( $this->group02->getUid() );
		$Group10Role = new \Zend\Permissions\Acl\Role\GenericRole( $this->group10->getUid() );
		
		$Acl = new Acl();
		$Acl->addRole($Group00Role);
		$Acl->addRole($Group10Role);
		$Acl->addRole( $Group01Role, array($this->group00->getUid(), $this->group10->getUid()) );
		$Acl->addRole( $Group02Role, array($this->group00->getUid(), $this->group10->getUid()) );
		$Acl->addRole( $User001Role, array($this->group01->getUid(), $this->group02->getUid()) );
		
		echo "Parse tree from bottom to top \n";
		$it = new \RecursiveIteratorIterator( $CurrentUser->getGroups(), \RecursiveIteratorIterator::SELF_FIRST);
		foreach( $it as $grp ){
			var_dump( $grp->getName() );
		}
		
		echo "Init all Roles \n";
		$Acl->removeRoleAll();
		
		//==============================================
		//use \Rbplm\Acl\Initializer
		//==============================================
		/*
		 * initRole create Role for $CurrentUser and for each of his parent groups.
		 * As it is a recusrsive method, create too Role for each groups of groups.
		 * Add each role to acl and set correctly parent in accordance to groups links relations.
		 */
		echo "initRole create Role for CurrentUser and for each of his parent groups \n";		
		AclInitializer::initRole( $CurrentUser );
		
		/**/
		echo "Create resources \n";		
		$RootResource = new \Zend\Permissions\Acl\Resource\GenericResource( Org\Root::singleton()->getUid() );
		$Resource001 = new \Zend\Permissions\Acl\Resource\GenericResource( $this->Ou001->getUid() );
		$Resource002 = new \Zend\Permissions\Acl\Resource\GenericResource( $this->Ou002->getUid() );
		$Resource003 = new \Zend\Permissions\Acl\Resource\GenericResource( $this->Ou003->getUid() );
		$Resource004 = new \Zend\Permissions\Acl\Resource\GenericResource( $this->Ou004->getUid() );
		
		/**/
		echo "Init resources \n";		
		AclInitializer::initResource( $this->Ou004 );
		AclInitializer::initResource( $this->Ou003 );
		
		//
		echo "Get ACL object from singleton to get rules define internaly to People objects \n";		
		$Acl = Acl::singleton();
		
		echo "Test acl on inherit \n";		
		$Acl->allow($this->group00->getUid(), array($this->Ou003->getUid(), $this->Ou004->getUid()), 'read');
		var_dump( $Acl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') );
		assert($Acl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') == true);
		
		//Prority rule illustration
		$Acl->deny($CurrentUser->getUid(), $this->Ou003->getUid(), 'read');
		$Acl->allow($CurrentUser->getUid(), array($this->Ou003->getUid(),$this->Ou004->getUid()), 'write');
		$Acl->allow($CurrentUser->getUid(), array($this->Ou003->getUid(),$this->Ou004->getUid()), 'execute');
		$Acl->allow($CurrentUser->getUid(), array($this->Ou003->getUid(),$this->Ou004->getUid()), 'suppress');
		assert($Acl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') == false);
		
		/**/
		echo "Serialize \n";
		$serialized = serialize( $Acl );
		$uAcl = unserialize( $serialized );
		assert($uAcl->isAllowed($CurrentUser->getUid(), $this->Ou003->getUid(), 'read') == false);
	}
	
	
	/**
	 */
	function _Test_Dao()
	{
		$AclDao = new AclDao();
		$AclDao->setConnexion( Connexion::get() );
		$GroupDao = new \Rbplm\People\GroupDaoPg( array() );
		$GroupDao->setConnexion( Connexion::get() );

		$Acl = Acl::singleton();
		
		$CurrentUser = \Rbplm\People\CurrentUser::get();
		$Groups = $CurrentUser->getGroups();
		foreach($Groups as $group){
			$GroupDao->save($group);
		}
		
		$Groups->rewind();
		$Groups->next();
		$CurrentUserrole = $Acl->getRole( $CurrentUser->getUid() );
		$Group01Role = $Acl->getRole( $Groups->current()->getUid() );
		
		$RootOu = \Rbplm\Org\Root::singleton();
		$childs = $RootOu->getChild();
		$this->Ou001 = $childs->getByName('Ou001');
		$this->Ou002 = $this->Ou001->getChild()->getByName('Ou002');
		$this->Ou003 = $this->Ou002->getChild()->getByName('Ou003');
		$this->Ou004 = $this->Ou002->getChild()->getByName('Ou004');
		
		$ouDao = new \Rbplm\Org\UnitDaoPg( array(), Connexion::get() );
		$ouDao->save($this->Ou001);
		$ouDao->save($this->Ou002);
		$ouDao->save($this->Ou003);
		$ouDao->save($this->Ou004);
		
		var_dump($this->Ou001->getuid(), $this->Ou001->getName());
		var_dump($this->Ou002->getuid(), $this->Ou002->getName());
		var_dump($this->Ou003->getuid(), $this->Ou003->getName());
		var_dump($this->Ou004->getuid(), $this->Ou004->getName());
		
		$this->Ou001Resource = $Acl->get( $this->Ou001->getUid() );
		$this->Ou002Resource = $Acl->get( $this->Ou002->getUid() );
		$this->Ou003Resource = $Acl->get( $this->Ou003->getUid() );
		$this->Ou004Resource = $Acl->get( $this->Ou004->getUid() );
		
		$rules = $Acl->getRules( $this->Ou003->getUid(), $CurrentUser->getUid() );
		var_dump( $rules );
		var_dump( $Acl->getResources() );
		var_dump( $Acl->getRoles() );
		
		//SAVE
		$AclDao->save( $Acl );
		
		//LOAD
		$AclDao->load( $Acl, "resource_id='" . $this->Ou001->getUid() . "'" );
	}
	
} //End of class

