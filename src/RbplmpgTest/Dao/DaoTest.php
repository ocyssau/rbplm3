<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Dao;

use Rbplm\Test\Test;
use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Org\Unit as OrgUnit;
use \Rbplm\Org\Root as Root;

/**
 * @brief Generic Test of Dao package
 * 
 * @include Rbplm/Dao/Test.php
 */
class DaoTest extends Test
{
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	\Rbplm\Dao\Registry::singleton()->reset();
    }
    
    
	function Test_DaoMoveNode()
	{
		/*Create a node at root*/
	    $ou1 = OrgUnit::init( get_class($this) . uniqid('ou1') );
		$Dao = DaoFactory::getDao($ou1);
		$Dao->save($ou1);
		
		/*Change parent*/
		$ouParent = OrgUnit::init();
		$ou1->setParent($ouParent);
		$Dao->save($ou1);
	}
	
	
    /**
     * 
     */
	function Test_DaoGeneral()
	{
		/*
		 * Desactivate the registry for test
		 */
		\Rbplm\Dao\Registry::singleton()->isActive(false);
		
		/****  Create some example buizness objects ****/
		$ou1 = OrgUnit::init( uniqid('ou1') );
		$ou2 = OrgUnit::init( uniqid('ou2') );
		$ou3 = OrgUnit::init( uniqid('ou3') );
		$ou4 = OrgUnit::init( uniqid('ou4') );
		
		DaoFactory::getDao($ou1)->save($ou1);
		DaoFactory::getDao($ou1)->save($ou2);
		
		/*Get dao from the factory*/
		$DaoOu = DaoFactory::getDao($ou1);
		
		/**** Some others ways to instanciate a DAO *****/
		$DaoOu = new \Rbplm\Org\UnitDaoPg( );
		$DaoOu->setConnexion( Connexion::get() );
		/* or */
		$DaoOu = new \Rbplm\Org\UnitDaoPg( Connexion::get() );
		
		/**** Save objects ****/
		$DaoOu->save($ou1);
		$DaoOu->save($ou2);
		$DaoOu->save($ou3);
		$DaoOu->save($ou4);
		
		$uid1 = $ou1->getUid();
		$uid2 = $ou2->getUid();
		$uid3 = $ou3->getUid();
		$uid4 = $ou4->getUid();
		
		/**** LOAD OBJECTS ****/
		$ou1_1 = new OrgUnit();
		$DaoOu->loadFromUid($ou1_1, $uid1);
		
		assert( $ou1_1->getName() == $ou1->getName() );
		assert( \Rbplm\Uuid::compare($ou1_1->getUid() , $ou1->getUid()) );
	}
	
	
	public function Test_DaoLoader()
	{
	    /**** LOAD WITH THE LOADER CLASS ****/
	    $connexion = Connexion::get();
	    $ou1_2 = \Rbplm\Dao\Pg\Loader::load($uid1);
	    assert( $ou1_2->getName() == $ou1->getName() );
	    assert( \Rbplm\Uuid::compare($ou1_2->getUid() , $ou1->getUid()) );
	    
	    /*****************************************************************************************
	     /* LOADER
	    */
	    /*
	     * The loader instanciate and load the object from database.
	    * This is very useful. However, keep in mind that the loader do 2 queries.
	    * First to get the class of object and second to get the object.
	    */
	    \Rbplm\Dao\Pg\Loader::setConnexion( \Rbplm\Dao\Connexion::get() );
	    $ou1_1 = \Rbplm\Dao\Pg\Loader::load( $uid1 );
	    assert( \Rbplm\Uuid::compare($ou1_1->getuid(), $ou1->getuid()) );
	    
	    $ou2_1 = \Rbplm\Dao\Pg\Loader::load( $uid2 );
	    assert( \Rbplm\Uuid::compare($ou2_1->getuid(), $ou2->getuid()) );
	}
	
	
	public function Test_DaoRegistry()
	{
	    /*****************************************************************************************
	     * REGISTRY
	    */
	    /*
	     * Registry is useful to create a storage and retrieve instanciated and loaded objects.
	    * Its a very good alternative to global var, abolutly banned in Rbplm
	    */
	    \Rbplm\Dao\Registry::singleton()->isActive(true);
	    \Rbplm\Dao\Registry::singleton()->reset();
	    \Rbplm\Dao\Registry::singleton()->add($ou1);
	    \Rbplm\Dao\Registry::singleton()->add($ou2);
	    /*or call directly \ArrayObject method*/
	    \Rbplm\Dao\Registry::singleton()->offsetSet($ou3->getUid(), $ou3);
	    
	    $ou1_3 = \Rbplm\Dao\Registry::singleton()->get( $uid1 );
	    assert($ou1_3 === $ou1 );
	    
	    assert( \Rbplm\Dao\Registry::singleton()->exist($uid1) === true );
	    /*or call directly \ArrayObject method*/
	    assert( \Rbplm\Dao\Registry::singleton()->offsetExists($uid4) === false );
	}
	
	
	public function Test_DaoList()
	{
	    $List = new \Rbplm\Dao\Pg\DaoList();
	    $List->load( "parent='$uid1'" );
	    
	    /*
	     * List contains a iterator pointed on db. It return only array of scalar.
	    * To convert a list to object collection, may use method \Rbplm\Dao\Pg\DaoList::loadInCollection
	    * or use the external iterator \Rbplm\Model\CollectionListBridge.
	    *
	    * loadInCollection return a full loaded collection of object.
	    *
	    * \Rbplm\Model\CollectionListBridge iterate on list and populate Collection when pass through by call
	    * \Rbplm\Dao\Pg\DaoList::toObject method.
	    *
	    */
	    
	    
	    /**** LIST TO COLLECTION ****/
	    
	    /**** with \Rbplm\Model\CollectionListBridge ****/
	    $CollectionLoader = new \Rbplm\Model\CollectionListBridge($ou1_1->getChild(), $List);
	    $ou1->getChild()->rewind();
	    foreach($CollectionLoader as $t){
	        //var_dump( $ou1->getChild()->current()->getUid() , $t->getuid() );
	        assert( \Rbplm\Uuid::compare($ou1->getChild()->current()->getUid() , $t->getuid() ) );
	        var_dump( $t->getName() , $ou1->getChild()->current()->getName() );
	        $ou1->getChild()->next();
	    }
	    assert( $ou1_1->getChild()->count() == $ou1->getChild()->count() );
	    
	    /**** with \Rbplm\Dao\Pg\DaoList::loadInCollection ****/
	    $Collection = new \Rbplm\Model\Collection();
	    $List = new \Rbplm\Dao\Pg\DaoList( array('table'=>'org_ou') , $connexion );
	    $List->loadInCollection( $Collection, "parent='$uid1'" );
	    
	    $ou1->getChild()->rewind();
	    foreach($Collection as $t){
	        assert( \Rbplm\Uuid::compare($ou1->getChild()->current()->getUid() , $t->getuid() ) );
	        var_dump( $t->getName() , $ou1->getChild()->current()->getName() );
	        $ou1->getChild()->next();
	    }
	    
	    /**** Other way: Explicitly *****/
	    $List = new \Rbplm\Dao\Pg\DaoList( array('table'=>'org_ou') , $connexion );
	    $List->load( "parent='$uid1'" );
	    $List->rewind();
	    $ou1->getChild()->rewind();
	    assert( \Rbplm\Uuid::compare( $ou1->getChild()->current()->getUid() , $List->toObject()->getUid() ) );
	    $List->next();
	    $ou1->getChild()->next();
	    assert( \Rbplm\Uuid::compare( $ou1->getChild()->current()->getUid() , $List->toObject()->getUid() ) );
	}
	
	
}

