<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Vault;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Sys\Filesystem;
use Rbplm\Vault\Vault;
use Rbplm\Vault\Record;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Cache;

/**
 * @brief Test class for \Rbplm\AnyObject.
 * 
 * @include Rbplm/Vault/VaultTest.php
 * 
 */
class VaultTest extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\AnyObject
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		Filesystem::isSecure(false);
	    
		$this->tid = uniqid();
	    $this->datapath = realpath('/tmp');
	     
	    var_dump($data);
	    
	    echo '--PREPARE FILES ON DISK--'.CRLF;
	    
		$i = 0;
		while($i < 5){
			$var = 'file'.$i;
			$this->$var = $this->datapath.'/tmpTestfile'.$i.'.txt';
			touch($this->$var);
			file_put_contents($this->$var, 'version001' . "\n");
			$i++;
		}
		
		$reposit = Reposit::init($this->datapath.'/repositTest/'.$this->tid);
		$vault = new Vault($reposit, DaoFactory::get()->getDao(Record::$classId));
		
		$this->reposit = $reposit;
		$this->vault = $vault;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		//suppress samples files
		$i = 0;
		while($i < 5){
			$var = 'file'.$i;
			var_dump($this->$var);
			unlink( $this->$var );
			$i++;
		}
	}
	
	function Test_DaoRepositSave()
	{
		DaoFactory::getDao($this->reposit)->save($this->reposit);
		$url = $this->reposit->getUrl();
		$number = $this->reposit->getNumber();
		
		$Dao = DaoFactory::getDao($this->reposit);
		
		$Reposit = new Reposit();
		$Dao->loadActive($Reposit);
		var_dump( $Reposit->getUrl(), $url, $Reposit );
		
		$Reposit = new Reposit();
		$Dao->loadFromNumber($Reposit, $number);
		assert( $Reposit->getUrl() == $url );
		assert( $Reposit->getNumber() == $number );
		
		var_dump( $Reposit->getUid() );
		var_dump( $Reposit->id );
	}
	
	

	function Test_GeneralUse()
	{
	    echo '--SAVE RECORDS IN VAULT--'.CRLF;
	    $record1=$this->vault->record( Record::init(), $this->file1, 'file', basename($this->file1) );
	    $record2=$this->vault->record( Record::init(), $this->file2, 'file', basename($this->file2) );
	    
		assert( $record1 instanceof Record );
		assert( $record2 instanceof Record );
		
		$data = $record1->getFsdata()->getData();
		assert( $data instanceof \Rbplm\Sys\Datatype\File );
	}
	
	
	function _Test_Cache()
	{
	    //@todo: revoir completement mecanisme des types et modes
	    
	    //Cache:
	    //create a new cache
	    $Reposit = new Cache\Reposit();
	    //set type to READ
	    $Reposit->setType(Reposit::TYPE_READ);
	    //Set mode to copy, to create copy of data in cache
	    $Reposit->setMode(Reposit::MODE_COPY);
	    $Reposit->setName('Cache001');
	    //Create reposit directory if necessary
	    $Reposit->init('./tmp/repositCache'.uniqid());
	    
	    try{
	        $builder = new Cache\Builder\Localfile();
	        Cache\Builder\Localfile::build($data->getFullpath(), 'file', $name, $Reposit);
	    }
	    catch(\Exception $e){
	        var_dump( $e->getMessage() );
	    }
	}
}
