<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Org;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Pg\ClassDao;
use Rbplm\Test\Test;
use Rbplm\Dao\Connexion;
use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Uuid;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 * 
 * @include Rbplm/Org/UnitTest.php
 * 
 */
class UnitTest extends Test
{
    
	/**
	 * @var    \Rbplm\Org\Unit
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$parent = Unit::init(uniqid('myParentOu'))->setParent(Root::singleton());
		$this->object = Unit::init(uniqid('myOu'))->setParent($parent);
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	function Test_Instanciate()
	{
		//Create instances of ou
		$ou1 = Root::init(get_class($this) . uniqid('ou1'));
	    $ou1 = Unit::init(get_class($this) . uniqid('ou1'))->setParent(Root::singleton());
		$ou1 = Project::init(get_class($this) . uniqid('ou1'))->setParent(Root::singleton());
		$ou1 = Mockup::init(get_class($this) . uniqid('ou1'))->setParent(Root::singleton());
		$ou1 = Workitem::init(get_class($this) . uniqid('ou1'))->setParent(Root::singleton());
	}
	
	
	function Test_ComponentTree()
	{
		//Create instances of ou
		$ou1 = Unit::init(get_class($this) . uniqid('ou1'))->setParent(Root::singleton());
		$ou2 = Unit::init(get_class($this) . uniqid('ou2'))->setParent($ou1);
		$ou3 = Unit::init(get_class($this) . uniqid('ou3'))->setParent($ou1);
		$ou4 = Unit::init(get_class($this) . uniqid('ou4'))->setParent($ou1);
		
		//Test parent getter
		assert( Root::singleton()->getParent() === null );
		assert( $ou1->getParent() === Root::singleton() );
		assert( $ou2->getParent() === $ou1 );
		assert( $ou3->getParent() === $ou1 );
		assert( $ou4->getParent() === $ou1 );
	}
	
	
	/**
	 */
	function Test_DaoSaveLoad()
	{
		/*init loader and factories*/
    	ClassDao::singleton()->setConnexion( Connexion::get() );
		
		$Dao = DaoFactory::getDao($this->object);
		
		echo "SAVE PARENT\n";
		$Dao->save( $this->object->getParent() );
		
		echo "SAVE OBJECT\n";
		$Dao->save( $this->object );
		
		$uid = $this->object->getUid();
		
		echo "LOAD \n";
		$object = new \Rbplm\Org\Unit();
		$Dao->loadFromUid( $object, $this->object->getUid() );
		
		assert( $object->getName() ==  $this->object->getName() );
		assert( Uuid::compare($object->getUid(), $uid) );
		
		var_dump($object->id);
		
		echo "UPDATE AND SAVE \n";
		$object->setName('modified');
		$Dao->save( $object );
		
		echo "UPDATE AND SAVE 2 \n";
		$object = $Dao->loadFromUid( new Unit(), $this->object->getUid() );
		assert( $object->getName() ==  'modified' );
		assert( Uuid::compare($object->getUid(), $uid) );
	}
	
}


