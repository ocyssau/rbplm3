<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Sys;

use Rbplm\Rbplm;
use Rbplm\Any;
use Rbplm\AnyObject;
use Rbplm\AnyPermanent;
use Rbplm\Sys\Logger;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 *
 * @include Rbplm/Org/UnitTest.php
 *
 */
class LoggerTest extends \Rbplm\Test\Test
{
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
        var_dump(Rbplm::getApiFullVersion());
        $logger = Logger::singleton();
        $logger->syslog->setApplicationName('RbplmpgTest');
        
        $message = 'DEBUG TEST MESSAGE';
        $priority = Logger::DEBUG;
        $logger->log($message, $priority, array('extra1','extra2'));
        $logger->log('INFO TEST MESSAGE');
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }
    
}


