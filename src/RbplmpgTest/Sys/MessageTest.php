<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Sys;

use Rbplm\Sys\Message;
use Rbplm\People\CurrentUser;
use Rbplm\Uuid;

/**
 * @brief Test class for \Rbplm\Sys\Message.
 * 
 * @include Rbplm/Sys/MessageTest.php
 * 
 */
class MessageTest extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\Sys\Message
	 */
	protected $object;
	
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->object = new Message(Message::TYPE_SEND);
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 */
	function testBase()
	{
		$body = 'Body text or <b>Html body text</b>';
		$subject = 'A test message';
		$from = CurrentUser::get()->getUid();
		$to = Uuid::newUid();
		$cc = Uuid::newUid();
		$bcc = Uuid::newUid();
		
		$this->object->setFrom($from, 'me');
		$this->object->owner = $from;
		$this->object->addTo($to, 'to user');
		$this->object->addCc($cc, 'CC user');
		$this->object->addBcc($bcc, 'BCC user');
		$this->object->setBodyHtml($body);
		//$this->object->setBodyText('Body text or <b>Html body text</b>');
		$this->object->setSubject($subject);
		
		$uid = $this->object->getUid();
		assert( !empty( $uid ) );
		assert( $this->object->getSubject() == $subject );
		assert( $this->object->getBodyHtml()->getContent() == $body );
		assert( $this->object->getFrom() == $from);
		$recipients = $this->object->getRecipients();
		assert( $recipients[0] == $to);
		assert( $recipients[1] == $cc);
		assert( $recipients[2] == $bcc);
		
		//DEFAULTS VALUES
		assert( $this->object->isFlagged === false );
		assert( $this->object->isReplied === false );
		assert( $this->object->isRead === false );
		assert( $this->object->priority === 3 );
		
		//CAST TO BOOL
		$this->object->isFlagged = 1;
		$this->object->isReplied = 1;
		$this->object->isRead = 1;
		assert( $this->object->isFlagged === true );
		assert( $this->object->isReplied === true );
		assert( $this->object->isRead === true );
		
		//CAST TO INTEGER
		$this->object->priority = '4';
		assert( $this->object->priority === 4 );
		
		$this->object->isFlagged = false;
		$this->object->isReplied = false;
		$this->object->isRead = false;
	}
	
	
	
	/**
	 */
	function testDao()
	{
		//SEND A MESSAGE:
		//First construct a message with type SEND
		$SentMessage = new Message(Message::TYPE_SEND);
		
		//create a new dao to configure access to sent message table.
		$config['table'] = \Rbplm\Sys\MessageDaoPg::$sentbox_table;
		//OR, is the same, configure the type:
		$config['type'] = Message::TYPE_SEND;
		//dont forget connexion
		$config['connex'] = '';
		$SentMessageDao = new \Rbplm\Sys\MessageDaoPg( $config );
		// Or is valid too to replace $config['connex']:
		$SentMessageDao->setConnexion( \Rbplm\Dao\Connexion::get() );
		
		//set message content and recipients
		$body = 'Body text or <b>Html body text</b>';
		$subject = 'A test message';
		$from = CurrentUser::get()->getUid();
		$to = Uuid::newUid();
		$cc = Uuid::newUid();
		$bcc = Uuid::newUid();
		
		
		$SentMessage->setFrom($from, 'me');
		$SentMessage->owner = $from;
		$SentMessage->addTo($to, 'to user');
		$SentMessage->addCc($cc, 'CC user');
		$SentMessage->addBcc($bcc, 'BCC user');
		$SentMessage->setBodyHtml($body);
		$SentMessage->setSubject($subject);
		
		//And send the message
		$SentMessage->send( $SentMessageDao );
		//And send the message many time
		$SentMessage->send( $SentMessageDao );
		$SentMessage->send( $SentMessageDao );
		
		//GET THE TO USER:
		//Get all recipents in a array
		$recipients = $SentMessage->getRecipients();
		//This array contains list of recipients user in ordre as inputs. Here, to user user is in key 0.
		$to = $recipients[0];
		
		//LOAD:
		//Load and construct the message from the mailbox of the to user
		$Message = new Message( Message::TYPE_MAILBOX );
		//Add a filter to get messages from owner and hash id
		$messageId = $SentMessage->getMessageId(); //A hash id to retrieve message. See \Zend_Mail
		$filter = "message_id = '$messageId' AND mailbox_owner= '$to'";
		//Construct the Dao
		$config = array('connex'=>'', 'type'=>\Rbplm\Sys\Message::TYPE_MAILBOX);
		$MessageDao = new \Rbplm\Sys\MessageDaoPg( $config );
		//var_dump($MessageDao->getConnexion()->getAttribute(\PDO::ATTR_SERVER_INFO));
		//Load the message
		$MessageDao->load( $Message, $filter );
		//The loaded message is identical to initial message
		assert( $Message->getBodyHtml()->getContent() == $SentMessage->getBodyHtml()->getContent() );
		assert( Uuid::format($Message->owner) == Uuid::format($to) );
		
		//LISTS:
		//Construct a new list from the mailbox
		$config = array('table'=>\Rbplm\Sys\MessageDaoPg::$mailbox_table);
		$mailboxList = new \Rbplm\Dao\Pg\DaoList($config);
		//load message of the user to
		$filter = "mailbox_owner='$to'";
		$mailboxList->load($filter);
		//get the first message
		$current = $mailboxList->current();
		$firstUid = $current['uid'];
		
		
		//LOAD NEXT MESSAGE FROM A MESSAGE UID
		//First message uid is $firstUid
		//Construct a new message
		$Message = new Message(Message::TYPE_MAILBOX);
		$filter = "mailbox_owner='$to'";
		$MessageDao->loadNextMessage( $Message, $firstUid, $filter);
		//message is other than the first
		assert($Message->getUid() != $firstUid);
		
		//SAVE THE READ MESSAGE
		$Message->isRead = true;
		$Message->isReplied = true;
		$MessageDao->save($Message);
		
		//COUNT ALL ELEMENT IN MAILBOX
		$filter = 'true = true';
		$count = $mailboxList->countAll($filter);
		var_dump($count);
		
		//COUNT MESSAGE OF USER $to
		$filter = "mailbox_owner='$to'";
		$count = $mailboxList->countAll($filter);
		var_dump($count);
		
		//SUPPRESS ALL MESSAGE OLDER THAN $days
		$days = 0.001;
		$age = date("U") - ($days * 3600 * 24);
		$age = (int) $age;
		$filter = "date<'$age'";
		$mailboxList->suppress($filter);
	}

}


