<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Model;

/**
 * @brief Test class for \Rbplm\Model\CollectionListBridge
 * 
 * @include Rbplm/Model/CollectionListBridgeTest.php
 */
class CollectionListBridgeTest extends \Rbplm\Test\Test
{
    /**
     * @var    \Rbplm\Model\CollectionListBridge
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }

	function testDao(){
		$Collection = new \Rbplm\Model\Collection( array('name'=>uniqId()) );
		$config = array(
			'table'=>'anyobject'
		);
		$List = new \Rbplm\Dao\Pg\DaoList($config);
		$List->setConnexion( \Rbplm\Dao\Connexion::get() );
		$List->load();
		
		$CollectionDao = new \Rbplm\Model\CollectionListBridge($Collection, $List);
		
		$i = 0;
		foreach($CollectionDao as $key=>$current){
			if( $i > 10){
				break;
			}
			var_dump( $key, $current->getUid() );
			$i++;
		}
	}
}
