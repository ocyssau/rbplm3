<?php
//%LICENCE_HEADER%

namespace RbplmpgTest\Model;


//require_once 'Rbplm/Model/ReferenceComponent.php';

/**
 * @brief Test class for ReferenceComponent.
 * 
 * @include Rbplm/Model/ComponentTest.php
 */
class ReferenceComponentTest extends \Rbplm\Test\Test
{
	protected $object;
	protected $parent;
	protected $reference;
	protected $name;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->name = 'ReferenceComponentTested';
		$this->reference = new \Rbplm\Any(array('name'=>'\Rbplm\Any_Refered'));
		$this->parent = new \Rbplm\AnyObject(array('name'=>'\Rbplm\AnyObject_Parent'));
		$this->object = new ReferenceComponent();
		$this->object->setName($this->name)->setReference($this->reference)->setParent($this->parent);
	}



	/**
	 * From class: ReferenceComponent
	 */
	function Test_GetReference(){
		$name = 'ReferenceComponentTested';
		
		$reference = new \Rbplm\Any(array('name'=>'\Rbplm\Any_Refered'));
		$parent = new \Rbplm\AnyObject(array('name'=>'\Rbplm\AnyObject_Parent'));
		$object = new ReferenceComponent();
		$object->setName($name)->setReference($reference)->setParent($parent);
		
		$ret = $object->getReference();
		assert($ret === $reference );
	}

	/**
	 * From class: ReferenceComponent
	 */
	function Test_SetReference(){
		$reference2 = new \Rbplm\Any(array('name'=>'\Rbplm\Any_Refered2'));
		
		$ret = $this->object->setReference($reference2);
		assert($ret === $this->object );
		$ret = $this->object->getReference();
		assert($ret === $reference2);
	}

	/**
	 * From class: \Rbplm\AnyObject
	 */
	function __Test_GetBasePath(){
		$this->setUp();

		$ret = $this->object->getBasePath();
		var_dump( $ret );
		assert( $ret == '/\Rbplm\AnyObject_Parent');
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function __Test_GetPath(){
		$ret = $this->object->getPath();
		var_dump( $ret );
		assert( $ret == '/\Rbplm\AnyObject_Parent/' . $this->name);
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function __Test_SetBasePath(){
		$basePath = '/Root/Object001';
		$ret = $this->object->setBasePath($basePath);
		assert($ret === $this->object);
		$ret = $this->object->getBasePath();
		var_dump( $ret );
		assert($ret === $basePath);
	}

	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_GetChild(){
		$ret = $this->object->getChildren();
		var_dump( $ret );
		assert( is_array($ret) );
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_GetParent(){
		$ret = $this->object->getParent();
		assert( $ret === $this->parent);
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_SetParent(){
		$parent = new \Rbplm\AnyObject(array('name'=>'\Rbplm\AnyObject_Parent2'));
		
		$ret = $this->object->setParent($parent);
		assert( $ret === $this->object);
		
		$ret = $this->object->getParent();
		assert( $ret === $parent);
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_SetChild(){
	    $child1 = new \Rbplm\AnyObject(array('name'=>'\Rbplm\AnyObject_child1'));

		$ret = $this->object->addChild($child1);
		assert( $ret === $this->object);
		
		$ret = $this->object->getChildren();
		assert( $ret[$child1->getUid()] === $child1 );
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_HasChild(){
		$this->setUp();
		
		$this->object->addChild( new \Rbplm\AnyObject(array('name'=>'\Rbplm\Any_Refered2')) );
		$ret = count($this->object->getChildren());
		assert( $ret == true);
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_GetUid(){
		$ret = $this->object->getUid();
		var_dump($ret);
		assert( empty($ret) );
		
		$reference = new \Rbplm\Any(array('name'=>'\Rbplm\Any_Refered'));
		$parent = new \Rbplm\AnyObject(array('name'=>'\Rbplm\AnyObject_Parent'));
		$object = ReferenceComponent::init($name, $parent)->setReference($reference);
		$ret = $object->getUid();
		var_dump($ret);
		assert( !empty($ret) );
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_GetName(){
		$this->setUp();
		$ret = $this->object->getName();
		assert( $ret === $this->name );
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_SetName(){
		$name = 'Name2';
		$ret = $this->object->setName($name);
		assert( $ret === $this->object );
		
		$ret = $this->object->getName();
		var_dump($ret, $name);
		assert( $ret === $name );
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_SetUid(){
		$uid = 'myuid';
		$ret = $this->object->setUid($uid);
		assert( $ret === $this->object );
		$ret = $this->object->getUid();
		var_dump($ret, $uid);
		assert( $ret === $uid );
	}

}


