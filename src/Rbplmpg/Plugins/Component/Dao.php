<?php 


namespace Rbplmpg\Plugins\Component;

//require_once('Rbplm/Plugins/Interface.php');

class Dao implements \Rbplm\Plugins\PluginsInterface
{
	
	private $_anyobject = null;
	private $_dao = null;
	
	public function dao(){
		if( !$this->_dao ){
			$this->_dao = \Rbplm\Dao\Factory::getDao($this->_anyobject);
		}
		return $this->_dao;
	}
	
	public function setanyobject(\Rbplm\AnyObject $anyobject){
		$this->_anyobject = $anyobject;
	}
}
