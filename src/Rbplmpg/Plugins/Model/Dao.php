<?php 

namespace Rbplmpg\Plugins\Model;

use \Rbplm\Plugins\PluginsInterface;

class Dao implements PluginsInterface
{
	
	private $_anyobject = null;
	private $_dao = null;
	
	public function dao(){
		if( !$this->_dao ){
			$this->_dao = \Rbplm\Dao\Factory::getDao($this->_anyobject);
		}
		return $this->_dao;
	}
	
	public function setanyobject(\Rbplm\AnyObject $anyobject){
		$this->_anyobject = $anyobject;
	}
}
