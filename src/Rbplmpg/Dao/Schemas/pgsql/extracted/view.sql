-- ======================================================================
-- From file Pg.php
-- ======================================================================
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
CREATE OR REPLACE VIEW view_fullpathname_anyobject AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.label FROM anyobject AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathname
FROM anyobject As o 
WHERE path IS NOT NULL
ORDER BY fullpathname;

CREATE OR REPLACE VIEW view_fullpathuid_anyobject AS
SELECT o.id, o.name, o.label, o.cid, o.parent, o.path, array_to_string(
	ARRAY(SELECT a.uid FROM anyobject AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM anyobject As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;


CREATE OR REPLACE VIEW view_fullpathuid_anyobject_2 AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.uid FROM anyobject AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM anyobject As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;

-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
CREATE OR REPLACE VIEW view_wf_activity_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_activity AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_wf_activity_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_activity AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_acl_resource_tree AS
	SELECT comp.name, comp.label, comp.path, comp.parent, acl.*
	FROM anyobject AS comp
	JOIN acl_rules AS acl ON comp.uid = acl.resource_id;
-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
CREATE OR REPLACE VIEW view_people_user_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_user AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_category_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_category AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_docfile_version_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_docfile_version AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_document_version_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_document_version AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_docfile_role_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_docfile_role AS r
	JOIN anyobject_links AS l ON r.file_uid = l.lchild;
	
-- CREATE OR REPLACE VIEW view_vault_record_links AS
-- 	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
-- 	FROM vault_record AS r
-- 	JOIN anyobject_links AS l ON r.uid = l.lchild;

CREATE OR REPLACE VIEW view_vault_record_links AS
	SELECT l.docfile AS docfile, l.iteration AS iteration, r.*
	FROM vault_record AS r
	JOIN ged_docfile_data_links AS l ON r.uid = l.record;

-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_propset_property AS
SELECT l.property AS property, l.lparent AS lparent, l.cid AS cid, r.sysname AS sysname, r.appname AS appname
FROM propset AS l
JOIN meta_type AS r ON l.property = r.id;
-- ======================================================================
-- From file AnyObjectDaoPg.php
-- ======================================================================
