-- ======================================================================
-- From file Pg.php
-- ======================================================================
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
ALTER TABLE wf_instance ADD FOREIGN KEY (process) REFERENCES wf_process (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_instance ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_instance ADD FOREIGN KEY (next_activity) REFERENCES wf_activity (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_instance ADD FOREIGN KEY (next_user) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
ALTER TABLE wf_instance_activity ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
ALTER TABLE wf_activity ADD FOREIGN KEY (cid) REFERENCES classes (id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_activity ADD FOREIGN KEY (process) REFERENCES wf_process (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
--- ALTER TABLE acl_rules ADD FOREIGN KEY (resource_id) REFERENCES anyobject (uid);
-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
ALTER TABLE people_user_preference ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
ALTER TABLE ged_category ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
ALTER TABLE ged_doctype ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
ALTER TABLE ged_docfile ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
ALTER TABLE ged_document ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
 ALTER TABLE ged_document_version ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (lock_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (base) REFERENCES ged_document (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (from_anyobject) REFERENCES ged_document_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (doctype) REFERENCES ged_doctype (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
ALTER TABLE ged_docfile_role ADD FOREIGN KEY (file_uid) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (lock_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (base) REFERENCES ged_docfile (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (from_anyobject) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (data) REFERENCES vault_record (uid) ON UPDATE CASCADE ON DELETE SET NULL;
--
-- For ged_docfile_data_links
--
ALTER TABLE ged_docfile_data_links ADD FOREIGN KEY (docfile) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ged_docfile_data_links ADD FOREIGN KEY (record) REFERENCES vault_record (uid) ON UPDATE CASCADE ON DELETE CASCADE;
-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
 ALTER TABLE org_ou ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file AnyObjectDaoPg.php
-- ======================================================================
