 CREATE TABLE anyobject (
 id bigint NOT NULL,
 uid uuid NOT NULL,
 cid integer NOT NULL,
 name varchar(256),
 label varchar(256),
 owner uuid,
 parent uuid,
 path ltree
 );
 CREATE TABLE anyobject_links (
 luid uuid NOT NULL,
 lparent bigint NOT NULL,
 lchild bigint NOT NULL,
 lname varchar(256),
 ldata text NULL,
 lindex integer NULL,
 level int DEFAULT 0,
 lpath ltree,
 is_leaf boolean NOT NULL DEFAULT false
 );
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
CREATE TABLE wf_instance(
	description varchar(256), 
	process uuid, 
	properties text, 
	status varchar(64), 
	next_activity uuid, 
	next_user uuid, 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (anyobject);
CREATE TABLE wf_instance_activity(
	status varchar(64), 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (anyobject);
CREATE TABLE wf_process(
	description varchar(256), 
	version varchar(16), 
	normalized_name varchar(128), 
	is_valid boolean, 
	is_active boolean
) INHERITS (anyobject);
CREATE TABLE wf_activity(
	description varchar(256), 
	normalized_name varchar(128), 
	process uuid, 
	is_interactive boolean DEFAULT false, 
	is_autoRouted boolean DEFAULT false, 
	is_automatic boolean DEFAULT false, 
	is_comment boolean DEFAULT false, 
	type varchar(32), 
	expiration_time integer
)INHERITS (anyobject);
CREATE TABLE acl_rules (
  id bigint NOT NULL,
  resource_id uuid NOT NULL,
  role_id uuid  NOT NULL,
  privilege varchar(32)  NOT NULL,
  rule varchar(32)  NOT NULL
);
CREATE TABLE people_user_preference(
	owner uuid, 
	cid integer, 
	preferences text, 
	enable boolean
);
CREATE TABLE people_user(
	is_active boolean, 
	last_login integer, 
	login varchar(255) NOT NULL, 
	firstname varchar(255), 
	lastname varchar(255), 
	password varchar(255), 
	mail varchar(255), 
	wildspace varchar(255)
) INHERITS (anyobject);
CREATE TABLE people_group(
	is_active boolean, 
	description varchar(255)
) INHERITS (anyobject);
CREATE TABLE meta_type(
	id integer, 
	uid uuid NOT NULL, 
	appname varchar(128) NOT NULL, 
	sysname varchar(128) NOT NULL, 
	description varchar(255), 
	type varchar(32) NOT NULL,
	length integer, 
	regex varchar(255),
	is_require boolean, 
	is_hide boolean,
	return_name boolean, 
	is_multiple boolean, 
	display_both boolean, 
	is_tiltinglist boolean, 
	is_autocomplete boolean,
	select_list varchar(255), 
	selectdb_where varchar(255), 
	selectdb_table varchar(255), 
	selectdb_field_for_value varchar(255), 
	selectdb_field_for_display varchar(255)
);
CREATE TABLE messages_mailbox (
	id bigint NOT NULL,
	uid character(13) NOT NULL,
    owner uuid NOT NULL,
    subject text,
    body text,
    date date,
    fromuser uuid,
    touser text,
    is_read boolean,
	is_replied boolean,
  	is_flagged boolean,
  	priority integer,
  	type integer,
    PRIMARY KEY  (id)
);
CREATE TABLE messages_sent ( 
	LIKE messages_mailbox INCLUDING INDEXES 
);
CREATE TABLE messages_archive ( 
	LIKE messages_mailbox INCLUDING INDEXES 
);
CREATE TABLE comments(
	uid uuid, 
	commented_id uuid, 
	title varchar(128), 
	body varchar(512)
);
 CREATE TABLE vault_record (
 id integer NOT NULL PRIMARY KEY,
 uid uuid NOT NULL UNIQUE,
 cid integer DEFAULT 600,
 name varchar(256),
 created integer NOT NULL,
 extension char(16),
 path varchar(256),
 rootname varchar(256),
 fullpath varchar(256),
 size integer,
 mtime integer,
 type varchar(32),
 md5 char(38)
 );
CREATE TABLE vault_reposit(
	id integer NOT NULL,
	uid uuid NOT NULL,
	name varchar(255),
	number varchar(255),
	description varchar(255),
	url varchar(255),
	type integer,
	mode integer,
	state integer,
	priority integer,
	maxsize integer,
	maxcount integer,
	create_by varchar(255),
	created integer
);
CREATE TABLE ged_category(
	description varchar(255), 
	icon varchar(255)
) INHERITS (anyobject);
CREATE TABLE ged_doctype(
	description text, 
	file_extension varchar(16), 
	file_type varchar(16), 
	icon varchar(255), 
	script_post_store varchar(255), 
	script_pre_store varchar(255), 
	script_post_update varchar(255), 
	script_pre_update varchar(255), 
	recognition_regexp text, 
	visu_file_extension varchar(16), 
	may_be_composite boolean
) INHERITS (anyobject);
CREATE TABLE ged_docfile(
	number varchar NOT NULL, 
	description text, 
	last_version_id integer, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	update_by uuid, 
	create_by uuid NOT NULL
) INHERITS (anyobject);
CREATE TABLE ged_document(
	number varchar NOT NULL, 
	description text, 
	last_version_id integer, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	update_by uuid, 
	create_by uuid NOT NULL
) INHERITS (anyobject);
 CREATE TABLE ged_document_version(
 number varchar NOT NULL,
 description text,
 created integer NOT NULL,
 updated integer,
 closed integer,
 locked integer,
 owner uuid NOT NULL,
 update_by uuid,
 create_by uuid NOT NULL,
 lock_by uuid,
 base uuid NOT NULL,
 from_anyobject uuid,
 doctype uuid NOT NULL,
 access_code integer NOT NULL,
 iteration integer NOT NULL,
 version integer NOT NULL,
 life_stage varchar
 ) INHERITS (anyobject);
CREATE TABLE ged_docfile_role(
	name varchar(255), 
	file_uid uuid NOT NULL, 
	role_id integer, 
	description text
);
CREATE TABLE ged_docfile_version(
	number varchar NOT NULL, 
	description text, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	locked integer, 
	owner uuid NOT NULL, 
	update_by uuid, 
	create_by uuid NOT NULL, 
	lock_by uuid, 
	base uuid NOT NULL, 
	from_anyobject uuid, 
	data uuid, 
	access_code integer NOT NULL, 
	iteration integer NOT NULL, 
	version integer NOT NULL, 
	life_stage varchar
) INHERITS (anyobject);
CREATE TABLE ged_docfile_data_links(
	docfile uuid NOT NULL, 
	record uuid NOT NULL, 
	iteration integer NOT NULL DEFAULT 1
);
 CREATE TABLE org_ou(
 description varchar(255),
 created date,
 updated date,
 update_by uuid,
 create_by uuid NOT NULL
 ) INHERITS (anyobject);
CREATE TABLE propset (
	lparent uuid NOT NULL,
	property integer NOT NULL,
	cid integer NOT NULL,
	PRIMARY KEY (lparent, property, cid)
);
