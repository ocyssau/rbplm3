-- ======================================================================
-- From file Pg.php
-- ======================================================================
 -------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_anyobject_update_path
--
DROP TRIGGER IF EXISTS rb_trig01_anyobject_update_path ON anyobject;
CREATE TRIGGER rb_trig01_anyobject_update_path AFTER INSERT OR UPDATE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_anyobject_delete
--
DROP TRIGGER IF EXISTS rb_trig02_anyobject_delete ON anyobject;
CREATE TRIGGER rb_trig02_anyobject_delete AFTER DELETE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_wf_instance AFTER INSERT OR UPDATE 
		   ON wf_instance FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_wf_instance AFTER DELETE 
		   ON wf_instance FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_wf_instance_activity AFTER INSERT OR UPDATE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_wf_instance_activity AFTER DELETE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_wf_process AFTER INSERT OR UPDATE 
		   ON wf_process FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_wf_process AFTER DELETE 
		   ON wf_process FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
DROP TRIGGER IF EXISTS trig01_wf_activity ON wf_activity;
CREATE TRIGGER trig01_wf_activity AFTER INSERT OR UPDATE 
		   ON wf_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
DROP TRIGGER IF EXISTS trig02_wf_activity ON wf_activity;
CREATE TRIGGER trig02_wf_activity AFTER DELETE 
		   ON wf_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
CREATE OR REPLACE FUNCTION rb_replace_aclrule(in_resource_id UUID, in_role_id UUID, in_privilege TEXT, in_rule TEXT) RETURNS VOID AS
$$
BEGIN
    LOOP
		UPDATE acl_rules SET rule=in_rule  WHERE resource_id=in_resource_id AND role_id=in_role_id AND privilege=in_privilege;
        IF found THEN
            RETURN;
        END IF;

        BEGIN
			INSERT INTO anyobject_links (resource_id,role_id,privilege,rule) VALUES (in_resource_id, in_role_id, in_privilege, in_rule);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;



-- http://wiki.postgresql.org/wiki/Return_more_than_one_row_of_data_from_PL/pgSQL_functions

CREATE TYPE aclrule AS (resource_id uuid, role_id uuid, privilege varchar(32), rule  varchar(32), resource_path text, role_path text);
CREATE OR REPLACE FUNCTION rb_get_aclrule(in_resource_path ltree, in_role_id UUID) RETURNS SETOF aclrule AS
$$
	DECLARE
	aclrule aclrule%ROWTYPE;
	BEGIN
	FOR aclrule IN
	SELECT DISTINCT acl.resource_id, acl.role_id, acl.privilege, acl.rule, resource.uidpath AS resource_path, role.path AS role_path FROM 
	(
		WITH RECURSIVE tree_role(lparent, lchild, data, depth, path, lindex) AS (
			SELECT lnk.related, lnk.lchild, lnk.data, 1, (lnk.lparent || '.' || lnk.lchild)::text, lnk.lindex
				FROM anyobject_links lnk
				WHERE lnk.lparent=in_role_id
			UNION ALL
				SELECT lnk.lparent, lnk.lchild, lnk.data, tr.depth + 1, (tr.path || '.' || lnk.lchild)::text, lnk.lindex
				FROM anyobject_links lnk, tree_role tr
				WHERE tr.lchild = lnk.lparent)
		SELECT DISTINCT tr.*, 
				child.uid AS childuid, child.name AS childname, child.path AS childpath, child.cid AS childclassid,
				parent.uid AS parentuid, parent.name AS parentname, parent.path AS parentpath, parent.cid AS parentclassid
		FROM tree_role AS tr
			JOIN anyobject AS child ON child.uid = tr.lchild
			JOIN anyobject AS parent ON parent.uid = tr.lparent
			ORDER BY path ASC
	) AS role
	JOIN acl_rules AS acl ON role.lchild = acl.role_id
	JOIN
	(
		SELECT uid, path, array_to_string(
				ARRAY(SELECT bcomp.uid FROM anyobject AS bcomp WHERE bcomp.path @> comp.path ORDER BY bcomp.path), 
			'.') As uidpath
		FROM 
		anyobject AS comp
		WHERE comp.path @> in_resource_path
	) AS resource ON resource.uid = acl.resource_id
	LOOP
	RETURN NEXT aclrule;
	END LOOP;
	END
$$
LANGUAGE plpgsql;


-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_user AFTER DELETE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();

-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_comments AFTER INSERT OR UPDATE 
		   ON comments FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_comments AFTER DELETE 
		   ON comments FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_category AFTER INSERT OR UPDATE 
		   ON ged_category FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_category AFTER DELETE 
		   ON ged_category FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_doctype AFTER INSERT OR UPDATE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_doctype AFTER DELETE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_docfile AFTER INSERT OR UPDATE 
		   ON ged_docfile FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_docfile AFTER DELETE 
		   ON ged_docfile FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();

-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_document AFTER INSERT OR UPDATE 
		   ON ged_document FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_document AFTER DELETE 
		   ON ged_document FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
 CREATE TRIGGER trig01_ged_document_version AFTER INSERT OR UPDATE
 ON ged_document_version FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_ged_document_version AFTER DELETE
 ON ged_document_version FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_docfile_version AFTER INSERT OR UPDATE 
		   ON ged_docfile_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_docfile_version AFTER DELETE 
		   ON ged_docfile_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
 CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_org_ou AFTER DELETE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file AnyObjectDaoPg.php
-- ======================================================================
