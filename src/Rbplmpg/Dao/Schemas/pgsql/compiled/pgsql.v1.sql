 CREATE SEQUENCE anyobject_id_seq
 START WITH 100
 INCREMENT BY 1
 NO MAXVALUE
 NO MINVALUE
 CACHE 1;
CREATE SEQUENCE acl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
CREATE SEQUENCE meta_type_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
CREATE SEQUENCE messages_mailbox_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
CREATE SEQUENCE messages_sent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
CREATE SEQUENCE messages_archive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 CREATE SEQUENCE vault_id_seq
 START WITH 1
 INCREMENT BY 1
 NO MAXVALUE
 NO MINVALUE
 CACHE 1;
CREATE SEQUENCE vault_reposit_seq
    START WITH 100
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 CREATE TABLE anyobject (
 id bigint NOT NULL,
 uid uuid NOT NULL,
 cid integer NOT NULL,
 name varchar(256),
 label varchar(256),
 owner uuid,
 parent uuid,
 path ltree
 );
 CREATE TABLE anyobject_links (
 luid uuid NOT NULL,
 lparent bigint NOT NULL,
 lchild bigint NOT NULL,
 lname varchar(256),
 ldata text NULL,
 lindex integer NULL,
 level int DEFAULT 0,
 lpath ltree,
 is_leaf boolean NOT NULL DEFAULT false
 );
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
CREATE TABLE wf_instance(
	description varchar(256), 
	process uuid, 
	properties text, 
	status varchar(64), 
	next_activity uuid, 
	next_user uuid, 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (anyobject);
CREATE TABLE wf_instance_activity(
	status varchar(64), 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (anyobject);
CREATE TABLE wf_process(
	description varchar(256), 
	version varchar(16), 
	normalized_name varchar(128), 
	is_valid boolean, 
	is_active boolean
) INHERITS (anyobject);
CREATE TABLE wf_activity(
	description varchar(256), 
	normalized_name varchar(128), 
	process uuid, 
	is_interactive boolean DEFAULT false, 
	is_autoRouted boolean DEFAULT false, 
	is_automatic boolean DEFAULT false, 
	is_comment boolean DEFAULT false, 
	type varchar(32), 
	expiration_time integer
)INHERITS (anyobject);
CREATE TABLE acl_rules (
  id bigint NOT NULL,
  resource_id uuid NOT NULL,
  role_id uuid  NOT NULL,
  privilege varchar(32)  NOT NULL,
  rule varchar(32)  NOT NULL
);
CREATE TABLE people_user_preference(
	owner uuid, 
	cid integer, 
	preferences text, 
	enable boolean
);
CREATE TABLE people_user(
	is_active boolean, 
	last_login integer, 
	login varchar(255) NOT NULL, 
	firstname varchar(255), 
	lastname varchar(255), 
	password varchar(255), 
	mail varchar(255), 
	wildspace varchar(255)
) INHERITS (anyobject);
CREATE TABLE people_group(
	is_active boolean, 
	description varchar(255)
) INHERITS (anyobject);
CREATE TABLE meta_type(
	id integer, 
	uid uuid NOT NULL, 
	appname varchar(128) NOT NULL, 
	sysname varchar(128) NOT NULL, 
	description varchar(255), 
	type varchar(32) NOT NULL,
	length integer, 
	regex varchar(255),
	is_require boolean, 
	is_hide boolean,
	return_name boolean, 
	is_multiple boolean, 
	display_both boolean, 
	is_tiltinglist boolean, 
	is_autocomplete boolean,
	select_list varchar(255), 
	selectdb_where varchar(255), 
	selectdb_table varchar(255), 
	selectdb_field_for_value varchar(255), 
	selectdb_field_for_display varchar(255)
);
CREATE TABLE messages_mailbox (
	id bigint NOT NULL,
	uid character(13) NOT NULL,
    owner uuid NOT NULL,
    subject text,
    body text,
    date date,
    fromuser uuid,
    touser text,
    is_read boolean,
	is_replied boolean,
  	is_flagged boolean,
  	priority integer,
  	type integer,
    PRIMARY KEY  (id)
);
CREATE TABLE messages_sent ( 
	LIKE messages_mailbox INCLUDING INDEXES 
);
CREATE TABLE messages_archive ( 
	LIKE messages_mailbox INCLUDING INDEXES 
);
CREATE TABLE comments(
	uid uuid, 
	commented_id uuid, 
	title varchar(128), 
	body varchar(512)
);
 CREATE TABLE vault_record (
 id integer NOT NULL PRIMARY KEY,
 uid uuid NOT NULL UNIQUE,
 cid integer DEFAULT 600,
 name varchar(256),
 created integer NOT NULL,
 extension char(16),
 path varchar(256),
 rootname varchar(256),
 fullpath varchar(256),
 size integer,
 mtime integer,
 type varchar(32),
 md5 char(38)
 );
CREATE TABLE vault_reposit(
	id integer NOT NULL,
	uid uuid NOT NULL,
	name varchar(255),
	number varchar(255),
	description varchar(255),
	url varchar(255),
	type integer,
	mode integer,
	state integer,
	priority integer,
	maxsize integer,
	maxcount integer,
	create_by varchar(255),
	created integer
);
CREATE TABLE ged_category(
	description varchar(255), 
	icon varchar(255)
) INHERITS (anyobject);
CREATE TABLE ged_doctype(
	description text, 
	file_extension varchar(16), 
	file_type varchar(16), 
	icon varchar(255), 
	script_post_store varchar(255), 
	script_pre_store varchar(255), 
	script_post_update varchar(255), 
	script_pre_update varchar(255), 
	recognition_regexp text, 
	visu_file_extension varchar(16), 
	may_be_composite boolean
) INHERITS (anyobject);
CREATE TABLE ged_docfile(
	number varchar NOT NULL, 
	description text, 
	last_version_id integer, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	update_by uuid, 
	create_by uuid NOT NULL
) INHERITS (anyobject);
CREATE TABLE ged_document(
	number varchar NOT NULL, 
	description text, 
	last_version_id integer, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	update_by uuid, 
	create_by uuid NOT NULL
) INHERITS (anyobject);
 CREATE TABLE ged_document_version(
 number varchar NOT NULL,
 description text,
 created integer NOT NULL,
 updated integer,
 closed integer,
 locked integer,
 owner uuid NOT NULL,
 update_by uuid,
 create_by uuid NOT NULL,
 lock_by uuid,
 base uuid NOT NULL,
 from_anyobject uuid,
 doctype uuid NOT NULL,
 access_code integer NOT NULL,
 iteration integer NOT NULL,
 version integer NOT NULL,
 life_stage varchar
 ) INHERITS (anyobject);
CREATE TABLE ged_docfile_role(
	name varchar(255), 
	file_uid uuid NOT NULL, 
	role_id integer, 
	description text
);
CREATE TABLE ged_docfile_version(
	number varchar NOT NULL, 
	description text, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	locked integer, 
	owner uuid NOT NULL, 
	update_by uuid, 
	create_by uuid NOT NULL, 
	lock_by uuid, 
	base uuid NOT NULL, 
	from_anyobject uuid, 
	data uuid, 
	access_code integer NOT NULL, 
	iteration integer NOT NULL, 
	version integer NOT NULL, 
	life_stage varchar
) INHERITS (anyobject);
CREATE TABLE ged_docfile_data_links(
	docfile uuid NOT NULL, 
	record uuid NOT NULL, 
	iteration integer NOT NULL DEFAULT 1
);
 CREATE TABLE org_ou(
 description varchar(255),
 created date,
 updated date,
 update_by uuid,
 create_by uuid NOT NULL
 ) INHERITS (anyobject);
CREATE TABLE propset (
	lparent uuid NOT NULL,
	property integer NOT NULL,
	cid integer NOT NULL,
	PRIMARY KEY (lparent, property, cid)
);
-- ======================================================================
-- From file Pg.php
-- ======================================================================
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (430, '\Rbplm\Wf\Instance', 'wf_instance'); 
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (431, '\Rbplm\Wf\Instance_Activity', 'wf_instance_activity');
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (420, '\Rbplm\Wf\Process', 'wf_process'); 
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (421, '\Rbplm\Wf\Activity_Activity', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (422, '\Rbplm\Wf\Activity_Start', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (423, '\Rbplm\Wf\Activity_End', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (424, '\Rbplm\Wf\Activity_Join', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (425, '\Rbplm\Wf\Activity_Split', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (426, '\Rbplm\Wf\Activity_Switch', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (427, '\Rbplm\Wf\Activity_Standalone', 'wf_activity');
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (111, '\Rbplm\People\User\Preference', 'people_user_preference');
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (110, '\Rbplm\People\User', 'people_user');
INSERT INTO people_user (id, uid, cid, name, label, is_active, login)
                VALUES (1, '99999999-9999-9999-9999-00000000abcd', 110, 'admin', 'admin', true, 'admin');
INSERT INTO people_user (id, uid, cid, name, label, is_active, login, parent)
                VALUES (4, '99999999-9999-9999-9999-99999999ffff', 110, 'anonymous', 'anonymous', true, 'anonymous', '01234567-0123-0123-0123-0123456789ac');
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (120, '\Rbplm\People\Group', 'people_group'); 
-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (500, '\Rbplm\Sys\Comment', 'comments'); 
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (600, '\Rbplm\Vault\Record', 'vault_record');
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (60, '\Rbplm\Ged\Category', 'ged_category'); 
-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (61, '\Rbplm\Ged\Doctype', 'ged_doctype');
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (50, '\Rbplm\Ged\Docfile', 'ged_docfile'); 
-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (30, '\Rbplm\Ged\Document', 'ged_document'); 
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (31, '\Rbplm\Ged\Document\Version', 'ged_document_version');
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (52, '\Rbplm\Ged\Docfile_Role', 'ged_docfile_role');
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (51, '\Rbplm\Ged\Docfile_Version', 'ged_docfile_version');
-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (1, '\Rbplm\Org\Root', 'org_ou');
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (21, '\Rbplm\Org\Project', 'org_project');
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (22, '\Rbplm\Org\Workitem', 'org_ou');
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (23, '\Rbplm\Org\Mockup', 'org_mockup');
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (20, '\Rbplm\Org\Unit', 'org_ou');
INSERT INTO org_ou (id, uid, cid, name, label, description, created, create_by, owner)
                VALUES (2, '01234567-0123-0123-0123-0123456789ab', 20, 'RanchbePlm', 'RanchbePlm', 'Root node', '05-05-2014', '99999999-9999-9999-9999-00000000abcd', '99999999-9999-9999-9999-00000000abcd');
INSERT INTO org_ou (id, uid, cid, name, label, description, created, create_by, owner, parent)
                VALUES (3, '01234567-0123-0123-0123-0123456789ac', 20, 'Users', 'Users', 'Users OU', '05-05-2014', '99999999-9999-9999-9999-00000000abcd', '99999999-9999-9999-9999-00000000abcd', '01234567-0123-0123-0123-0123456789ab');
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file AnyObjectDaoPg.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (10, '\Rbplm\AnyObject', 'anyobject');
