<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;

use Rbplm\Dao\Mysql;

use Zend\Ldap\Filter\StringFilter;

use Rbplm\Dao\DaoInterface;
use Rbplm\Dao\MappedInterface;

use Rbplm\Dao\Connexion;
use Rbplm\Dao\Exception;
use Rbplm\Sys\Error;
use Rbplm\Signal;
use Rbplmpg\Dao\ClassDao;
use Rbplmpg\Dao\DaoList;
use Rbplm\Dao\Filter\FilterInterface;

/** SQL_SCRIPT>>
 -- SEQUENCE
 -- anyobject_id_seq
 --
 CREATE SEQUENCE anyobject_id_seq
 START WITH 100
 INCREMENT BY 1
 NO MAXVALUE
 NO MINVALUE
 CACHE 1;

 -------------------------------------------------------------------------------------------------------------------
 -- anyobject
 --
 CREATE TABLE anyobject (
 id bigint NOT NULL,
 uid uuid NOT NULL,
 cid integer NOT NULL,
 name varchar(256),
 label varchar(256),
 owner uuid,
 parent uuid,
 path ltree
 );

 -------------------------------------------------------------------------------------------------------------------
 -- anyobject_links
 --
 CREATE TABLE anyobject_links (
 luid uuid NOT NULL,
 lparent bigint NOT NULL,
 lchild bigint NOT NULL,
 lname varchar(256),
 ldata text NULL,
 lindex integer NULL,
 level int DEFAULT 0,
 lpath ltree,
 is_leaf boolean NOT NULL DEFAULT false
 );
 <<*/

/** SQL_ALTER>>
 -------------------------------------------------------------------------------------------------------------------
 -- anyobject
 --
 ALTER TABLE anyobject ALTER COLUMN id SET DEFAULT nextval('anyobject_id_seq'::regclass);
 ALTER TABLE anyobject ADD PRIMARY KEY (id);
 ALTER TABLE anyobject ADD UNIQUE (path);
 ALTER TABLE anyobject ADD FOREIGN KEY (cid) REFERENCES classes (id);

 CREATE INDEX INDEX_anyobject_name ON anyobject USING btree (name);
 CREATE INDEX INDEX_anyobject_label ON anyobject USING btree (label);
 CREATE INDEX INDEX_anyobject_uid ON anyobject USING btree (uid);
 CREATE INDEX INDEX_anyobject_cid ON anyobject USING btree (cid);
 CREATE UNIQUE INDEX INDEX_anyobject_path_btree ON anyobject USING btree(path);
 CREATE INDEX INDEX_anyobject_path_gist ON anyobject USING gist(path);

 -------------------------------------------------------------------------------------------------------------------
 -- anyobject_links
 --
 ALTER TABLE anyobject_links ADD UNIQUE (lparent, lchild);
 CREATE INDEX INDEX_anyobject_links_parent ON anyobject_links USING btree (lparent);
 CREATE INDEX INDEX_anyobject_links_child ON anyobject_links USING btree (lchild);
 <<*/

/** SQL_PROC>>

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
--
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_id bigint) RETURNS ltree AS
$$
SELECT
CASE WHEN tobj.lparent IS NULL
THEN
tobj.id::text::ltree
ELSE
rb_get_object_path(tobj.lparent)  || tobj.id::text
END
FROM anyobject_links As tobj
WHERE tobj.luid=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
--
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE
anyobject_links As tobj
SET
lpath = rb_get_object_path(tobj.parent) || tobj.uid::text,
level = nlevel(path);
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
--
CREATE OR REPLACE FUNCTION rb_replace_links(in_uuid uuid,in_parent bigint,in_child bigint,in_data TEXT,in_lindex INT,in_name TEXT) RETURNS VOID AS
$$
BEGIN
LOOP
UPDATE anyobject_links SET ldata = in_data, lindex = in_lindex, lname = in_name WHERE lparent = in_parent AND lchild = in_child;
IF found THEN
RETURN;
END IF;
BEGIN
--loc_lui = uuid();
INSERT INTO anyobject_links (luid,lparent,lchild,ldata,lindex,lname) VALUES (in_uuid,in_parent,in_child,in_data,in_lindex,in_name);
RETURN;
EXCEPTION WHEN unique_violation THEN
-- do nothing
END;
END LOOP;
END;
$$
LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Proc rb_trig_anyobject_update_path
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_update_path() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'UPDATE' THEN
IF (OLD.lpath IS NULL) THEN
UPDATE anyobject_links AS lt SET
lpath = rb_get_object_path(anyobject.id),
level = nlevel(NEW.lpath)
WHERE
lt.lparent = NEW.lparent AND lt.lchild = NEW.lchild
OR
lt.parent = NEW.id
;
END IF;
IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
-- update all nodes that are children of this one including this one
UPDATE anyobject SET
path = rb_get_object_path(anyobject.id),
depth = nlevel(NEW.path)
WHERE
OLD.path  @> anyobject.path;
END IF;

ELSIF TG_OP = 'INSERT' THEN
UPDATE anyobject SET
path = rb_get_object_path(NEW.id),
depth = nlevel(NEW.path)
WHERE
anyobject.id = NEW.id;
END IF;

RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_anyobject_delete
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_delete() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'DELETE' THEN
DELETE FROM anyobject_links WHERE parent = OLD.id OR child = OLD.id;
END IF;
RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

<<*/


/** SQL_TRIGGER>>
 -------------------------------------------------------------------------------------------------------------------
 -- Trigger rb_trig01_anyobject_update_path
 --
 DROP TRIGGER IF EXISTS rb_trig01_anyobject_update_path ON anyobject;
 CREATE TRIGGER rb_trig01_anyobject_update_path AFTER INSERT OR UPDATE
 ON anyobject FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();

 -------------------------------------------------------------------------------------------------------------------
 -- Trigger rb_trig02_anyobject_delete
 --
 DROP TRIGGER IF EXISTS rb_trig02_anyobject_delete ON anyobject;
 CREATE TRIGGER rb_trig02_anyobject_delete AFTER DELETE
 ON anyobject FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_DROP>>
 DROP SEQUENCE IF EXISTS anyobject_id_seq;
 DROP TABLE IF EXISTS anyobject CASCADE;
 DROP TABLE IF EXISTS anyobject_links CASCADE;
 <<*/


/** SQL_DROP>>
 DROP SEQUENCE IF EXISTS anyobject_id_seq;
 DROP TABLE IF EXISTS anyobject CASCADE;
 DROP TABLE IF EXISTS anyobject_links CASCADE;
 <<*/


/**
 * @brief Abstract Dao for pgsql database with shemas using ltree to manage trees.
 *
 * NEED TO INSTALL LTREE.
 * ON UBUNTU:
 * apt-get install postgresql-contrib
 *
 * psql -d bdd_name -f SHAREDIR/contrib/ltree.sql
 * where SHAREDIR = /usr/share/postgresql/8.4 (example)
 *
 *
 * Extract from http://docs.postgresql.fr/8.4/contrib.html:
 * Le fichier .sql doit être exécuté dans chaque base de données où le module doit être disponible.
 * Il peut également être exécuté dans la base template1 pour que le module soit automatiquement copié dans toute
 * nouvelle base de données créée.
 * La première commande du fichier .sql peut être modifiée pour déterminer le schéma de la base où sont créés les objets.
 * Par défaut, ils sont placés dans public.
 * Après une mise à jour majeure de PostgreSQL™, le script d'installation doit être réexécuté,
 * même si les objets du module sont éventuellement créés par une sauvegarde de l'ancienne installation.
 * Cela assure que toute nouvelle fonction est disponible et tout correction nécessaire appliquée.
 *
 *
 *  To create converter between sys attributs and application properties you may add static methods with name [appPropertyName]'ToSys'($value)
 *  and  [sysAttributName]'ToApp'($value). This methods return values in appropriate format.
 *  You may see examples as methods pathToApp and pathToSys.
 *
 * @link http://docs.postgresql.fr/8.4/contrib.html
 * @link http://docs.postgresql.fr/8.4/ltree.html
 *
 */
abstract class Pg extends Mysql
{
    /**
     *
     * @var unknown_type
     */
    public static $sysToApp = array(
        'id'=>'id',
        'uid'=>'uid',
        'name'=>'name',
        'parent'=>'parentId',
        'cid'=>'classId'
    );
    
    /**
     *
     * @var unknown_type
     */
    public static $sysToAppFilter = array(
    );
    
    /**
     * Class id use by postgresql dao to map rbplm class to entry in db.
     * @var integer
     */
    protected static $classId;

    /**
     * Constructor
     *
     * @param \PDO
     */
    public function __construct($conn = null)
    {
    	if( $conn ){
    		$this->connexion = $conn;
    	}
    	else{
    		$this->connexion = Connexion::get();
    	}
    
    	$this->metaModel = static::$sysToApp;
    	$this->metaModelFilters = static::$sysToAppFilter;
    
    	$this->_table = static::$table;
    	$this->_vtable = static::$vtable;
    	$this->_ltable = static::$ltable;
    	$this->_lvtable = static::$lvtable;
    } //End of function
    
    /**
     * @param AnyObject   	$mapped
     * @param array				$bind		Bind definition to add to generic bind construct from $sysToApp
     * @param array				$select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
     * @return void
     * @throws Exception
     *
     */
    protected function _insert($mapped, $select=null)
    {
        $sysToApp=$this->metaModel;
        $bind = array();
        $table = static::$table;
        $mapped->classId = static::$classId;
        unset($sysToApp['id']);
        
        //Init statement
        if(!$this->insertStmt){
            foreach($sysToApp as $asSys=>$asApp)
            {
                $sysSet[] = $asSys;
                $appSet[] = ':'.$asApp;
            }
            
            //object insert
            $sql = 'INSERT INTO ' . $table . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ') RETURNING id;';
            $this->insertStmt = $this->connexion->prepare($sql);
        
            //links insert
            $lsql = 'INSERT INTO ' . static::$ltable . ' (luid, lparent, lchild, lname, lindex, ldata)
                                                  VALUES (:uid, :parentId, :childId, :name, :lindex, :data)';
            $this->linsertStmt = $this->connexion->prepare($lsql);
        }
        
        foreach($sysToApp as $asSys=>$asApp)
        {
            if(is_bool($mapped->$asApp)){
                $bind[':'.$asApp] = (int)$mapped->$asApp;
            }
            elseif($mapped->$asApp instanceof \DateTime){
                $bind[':'.$asApp] = $mapped->$asApp->__toString();
            }
            else{
                $bind[':'.$asApp] = $mapped->$asApp;
            }
        }
        //var_dump($bind);
        
        $this->insertStmt->execute($bind);
        $mapped->id = $this->insertStmt->fetch(\PDO::FETCH_COLUMN);
        
        if(is_array($mapped->links)){
            foreach($mapped->links as $link){
                if($link->rule == \Rbh\Link::RULE_PARENT){
                    $link->childId = $mapped->id;
                }
                else{
                    $link->parentId = $mapped->id;
                }
                $this->linsertStmt->execute($link->bind());
            }
        }
    }

    /**
     * @param Rbh\AnyObject   $mapped
     * @param array				  $bind		Bind definition to add to generic bind construct from $sysToApp
     * @param array				  $select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
     * @return void
     * @throws \Rbh\Sys\Exception
     *
     */
    protected function _update($mapped, $select=null)
    {
        $sysToApp=$this->metaModel;
        $bind = array();
        $table = static::$table;     //objects table
        $ltable = static::$ltable;   //links table
        unset($sysToApp['id']);
        
        if(!$this->updateStmt){
            if(is_array($select)){
                $sysToApp = array_intersect($sysToApp, $select);
            }
            foreach($sysToApp as $asSys=>$asApp){
                $set[] = $asSys . '=:' . $asApp;
            }
            $sql = 'UPDATE '.$table.' SET ' . implode(',', $set) . ' WHERE id=:id;';
            $this->updateStmt = $this->connexion->prepare($sql);
        
            $sql = "SELECT rb_replace_links(:uid, :parentId, :childId, :data, :lindex, :name)";
            $this->lupdateStmt = $this->connexion->prepare($sql);
        }
        
        foreach($sysToApp as $asSys=>$asApp)
        {
            $bind[':'.$asApp] = $mapped->$asApp;
        }
        
        if(is_array($select)){
            array_walk($select, function(&$val,$key){
                $val=':'.$val;
            });
            $bind = array_intersect_key($bind, array_flip($select));
        }
        
        $bind[':id'] = $mapped->id;
        $this->updateStmt->execute($bind);

        //suppress links
        if(isset($this->linksToSuppress)){
            $sql = "DELETE FROM $ltable WHERE lparent = :parentId AND lchild = :childId";
            $stmt = $this->connexion->prepare($sql);
            foreach($this->linksToSuppress as $bind)
            {
                $stmt->execute($bind);
            }
            $this->linksToSuppress = array();
        }

        if(is_array($mapped->links)){
            foreach($mapped->links as $lnk){
                $this->lupdateStmt->execute($lnk->bind);
            }
        }
    }


    /**
     * Alias for hydrate
     */
    public function loadFromArray( $mapped, array $row, $fromApp = false )
    {
        return $this->hydrate($mapped, $row, $fromApp);
    } //End of function


    /**
     * @see \Rbplm\Dao\DaoInterface::loadLinks()
     * @return Pg
     *
     */
    public function loadLinks( MappedInterface $mapped, $class, $filter = null, $options = array() )
    {
        /* Load in list
         $List = new Pg_List(array('table'=>$this->_lvtable));
        $List->setConnexion( $this->connexion );
        $f00 = 'related = \'' . $mapped->getUid() . '\'';
        if(is_a($filter, '\Rbplm\Dao\Filter\FilterInterface')){
        $filter = $filter->__toString();
        }
        if($filter){
        $f00 .= ' AND ' . $filter;
        }
        $List->load($f00, $options);

        return $List;

        //autre exemple:
        $filter = "docfile='$uid' ORDER BY iteration ASC";
        $ListDao = new Pg_List( array('table'=>$this->_lvtable) );
        $ListDao->setConnexion( $this->connexion );
        $ListDao->loadInCollection($mapped->getLinks(), $filter);
        */
        $method = '_load_' . $class;
        if( method_exists($this, $method) ){
            return call_user_func( array($this, $method), $mapped, $filter, $options);
        }

        $table = ClassDao::singleton()->getTable($class);
        $classId = ClassDao::singleton()->toId($class);
        $relatedUid = $mapped->getUid();
        $select = '*';

        if(is_a($filter, '\Rbplm\Dao\Filter\FilterInterface')){
            $filter = $filter->__toString();
        }

        $sql = "SELECT $select FROM $table AS r JOIN $this->_ltable AS l ON l.lchild = r.uid";

        if($filter){
            $sql .= " WHERE $filter AND cid='$classId' AND related='$relatedUid'";
        }
        else{
            $sql .= " WHERE cid='$classId' AND related='$relatedUid'";
        }
        $sql .= ' ORDER BY lindex ASC';

        $stmt = $this->connexion->prepare($sql);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $stmt->execute();

        $Dao = \Rbplm\Dao\Factory::getDao($class);
        while ($row = $stmt->fetch()) {
            $uid = $row['uid'];
            $LinkObj = \Rbplm\Dao\Registry::singleton()->get($uid);
            if(!$LinkObj){
                $LinkObj = new $class();
                $Dao->loadFromArray($LinkObj, $row, false);
            }
            $mapped->getLinks()->add($LinkObj, $row['data']);
            \Rbplm\Dao\Registry::singleton()->add($LinkObj);
        }
        return $mapped;
    } //End of method


    /**
     *
     * @see \Rbplm\Dao\DaoInterface::loadParent()
     * @return Pg
     */
    public function loadParent( MappedInterface $mapped, $expectedProperties = array(), $force = false, $locks = true )
    {
        $mapped->setParent( Loader::load( $mapped->parentId, $expectedProperties, $force, $locks ) );
        return $mapped;
    } //End of method


    /**
     *
     * @return Pg
     */
    public function getChildren( MappedInterface $mapped, $options = array() )
    {
        $F = $DocVersionDao->newFilter();
        $F->children( $mapped->getPath() );
        $F->andfind($class, 'class', \Rbplm\Dao\Filter\Op::OP_EQUAL);
        return $this->newList()->load($F);
        //return $this;
    } //End of method


    /**
     * Getter for links. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @param string|Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Pg_Filter.
     * @param array
     * @return DaoList
     */
    public function getLinksRecursively($mapped, $filter = null, $options = array() )
    {
        $uid = $mapped->getUid();
        $table = 'anyobject';
         
        $sql = "WITH RECURSIVE graphe(name, related, lchild, data, depth, path, lindex) AS (
        SELECT g.name, g.related, g.lchild, g.data, 1, '/' || g.related, g.lindex
        FROM anyobject_links g WHERE g.related = '$uid'
        UNION ALL
        SELECT g.name, g.related, g.lchild, g.data, sg.depth + 1, path || '/' || g.related, g.lindex
        FROM anyobject_links g, graphe sg
        WHERE sg.lchild = g.related )
        SELECT ct.*, graphe.depth AS ldepth, graphe.path AS lpath, graphe.lindex, graphe.related AS lparent, graphe.data AS ldata FROM graphe
        JOIN $table AS ct ON ct.uid = graphe.lchild";

        if($filter instanceof FilterInterface){
            $filter = $filter->__toString();
        }
        if($filter){
            $sql .= ' WHERE ' . $filter;
        }

        $sql .= ' ORDER BY lpath ASC, lindex ASC;';

        $List = new DaoList( $table, $this->connexion );
        $List->loadFromSql($sql, $options);

        return $List;
    }

    /**
     * Save links in anyobject_links table if links is a anyobject.
     * Else call method "_savelinks_[Link class]" of current DAO.
     *
     * Note that a method must be write for each link class that is not a subtype of \Rbplm\AnyObject.
     * Take example on \Rbplm\Ged\Docfile_VersionDaoPg::_savelinks_\Rbplm\Vault\Record method.
     *
     * @param \Rbplm\Dao\MappedInterface	$mapped
     */
    protected function _saveLinks($mapped)
    {
        $table = static::$ltable;

        //suppress links
        if($this->linksToSuppress){
            $sql = "DELETE FROM $table WHERE related = :puid AND lchild = :cuid";
            $stmt = $this->connexion->prepare($sql);
            foreach($this->linksToSuppress as $bind){
                $stmt->execute($bind);
            }
            $this->linksToSuppress = array();
        }

        //replace links
        if( $mapped->hasLinks() ){
            $related = $mapped->getUid();
            $i=0;
            foreach( $mapped->getLinks() as $link){
                if( is_a($link, '\Rbplm\Model\Collection') ){
                    $j = 0;
                    foreach( $link as $anyobject){
                        $lchild = $anyobject->getUid();
                        $data = $link->getInfo();
                        $sql = "SELECT rb_replace_links('$related', '$lchild', '$data', $j)";
                        $this->connexion->exec($sql);
                        $j++;
                    }
                }
                else if( is_a($link, '\Rbplm\AnyObject') ){
                    $lchild = $link->getUid();
                    $data = $mapped->getLinks()->getInfo();
                    $sql = "SELECT rb_replace_links('$related', '$lchild', '$data', $i)";
                    $this->connexion->exec($sql);
                }
                else{
                    $method = '_savelinks_' . get_class($link);
                    if(method_exists($this, $method)){
                        call_user_func( array($this, $method), $mapped, $mapped->getLinks() );
                    }
                }
                $i++;
            }
        }
    } //End of function


    /**
     * Write all other properties in table properties
     *
     * @param MappedInterface	$mapped
     * @throws Exception
     *
     */
    protected function _saveProperties($mapped)
    {
        $table = static::$table . '_properties';

        //suppress properties
        if($this->_propsToSuppress){
            $sql = "DELETE FROM $table WHERE extpname = :pname AND related = :pkey";
            $stmt1 = $this->connexion->prepare($sql);
            $sql = "DELETE FROM $table WHERE extpname = :pname AND extpvalue = :pvalue AND related = :pkey";
            $stmt2 = $this->connexion->prepare($sql);
            foreach($this->_propsToSuppress as $bind){
                if(count($bind) == 2){
                    $stmt1->execute($bind);
                }else{
                    $stmt2->execute($bind);
                }
            }
            $this->_propsToSuppress = array();
        }

        //insert/replace properties
        $ptype_id = 1;

        /* methode 1 :
         $sql = "INSERT INTO object_properties (object_id, ptype_id, pname, pvalue) VALUES(?,?,?,?);";
        $stmt = $this->connexion->prepare($sql);
        foreach( get_class_vars($mapped) as $propName=>$propValues){
        foreach( $propValues as $index=>$value){
        $in = array($mapped->pkey, $ptype_id, $propName, $value, $index);
        $stmt->execute($in);
        }
        }
        */

        /* methode 2 :avec insertion multiple dans une requete, c'est pas plus rapide
         $sql_base = "REPLACE INTO object_properties (object_id, ptype_id, pname, pvalue, pindex)";
        foreach( get_class_vars($mapped) as $propName=>$propValues){
        $sql_values = array();
        $in = array();
        foreach( $propValues as $index=>$value){
        $sql_values[] = ' (?,?,?,?,?) ';
        $in = array_merge($in, array($mapped->pkey, $ptype_id, $propName, $value, $index));
        }
        $sql = $sql_base .' VALUES '. implode(',', $sql_values) . ';';
        $stmt = $this->connexion->prepare($sql);
        $stmt->execute($in);
        }
        */

        /* ca c'est plus rapide que methode 1 et 2 */
        if( count(get_class_vars($mapped)) > 0 ){
            $sql_base = "REPLACE INTO $table (object_id, ptype_id, pname, pvalue, pindex)";
            $sql_values = array();
            $in = array();
            foreach( get_class_vars($mapped) as $propName=>$propValues){
                if( in_array($propName, static::$_sysToApp ) ){
                    continue;
                }
                if($propValues == $this->_loadedProperties[$propName]){
                    continue;
                }
                if( !is_array($propValues) ){
                    $propValues = array($propValues);
                }
                foreach( $propValues as $index=>$value){
                    $sql_values[] = ' (?,?,?,?,?) ';
                    $in = array_merge($in, array($mapped->pkey, $ptype_id, $propName, $value, $index));
                }
            }
            if( $sql_values ){
                try{
                    $sql = $sql_base .' VALUES '. implode(',', $sql_values) . ';';
                    $stmt = $this->connexion->prepare($sql);
                    $stmt->execute($in);
                    $mapped->_propertiesToSave = array();
                    $this->_loadedProperties = get_class_vars($mapped);
                }catch(\PDO\Exception $e){
                    throw new Exception($e->getMessage(), $e->getCode());
                    return false;
                }
            }
        }
        return true;
    } //End of function



    /**
     * @see library/Rbplm/Dao/\Rbplm\Dao\DaoInterface#suppress($mapped)
     * @return Pg
     *
     */
    public function delete( $mapped, $withChilds = false )
    {
        $this->connexion->beginTransaction();

        if( $withChilds ){
            $path = $mapped->getPath();
            $path = self::pathToSys($path);
            $sql = "DELETE FROM anyobject WHERE path::ltree ~ '$path.*{0,}'";
            $this->connexion->exec($sql);
        }
        else{
            $uid = $mapped->getUid();
            $parentId = $mapped->parentId;
            if($parentId){
                $sql = "UPDATE anyobject SET parent='$parentId' WHERE parent='$uid'";
                $this->connexion->exec($sql);
            }
        }

        /*
         if( !$this->connexion->exec($sql) ){
        throw new Exception( \Rbplm\Sys\Error::SUPPRESSION_FAILED, \Rbplm\Sys\Error::ERROR, array($bind, $sql) );
        }
        */

        $sql = 'DELETE FROM anyobject WHERE uid=:uid';
        $bind = array( ':uid'=>$mapped->getUid() );
        $stmt = $this->connexion->prepare($sql);
        $stmt->execute($bind);

        $this->connexion->commit();
        return $this;

        /*
         if( ! ){
        throw new Exception( \Rbplm\Sys\Error::SUPPRESSION_FAILED, \Rbplm\Sys\Error::ERROR, array($bind, $sql));
        }
        else{
        $this->connexion->commit();
        }
        */
    } //End of method


    /**
     * Get the id of the class.
     *
     * @return integer
     */
    public function getClassId()
    {
        return static::$classId;
    } //End of function

    /**
     * Translate path from application to system.
     *
     * @param string
     * @return string
     */
    public static function pathToSys( $path )
    {
        $path = trim($path, '/');
        $path = str_replace('/', '.', $path);
        return $path;
    }

    /**
     * Translate path from system to application.
     *
     * @param string
     * @return string
     */
    public static function pathToApp( $path )
    {
        $path = '/' . str_replace('.', '/', $path);
        return $path;
    }

    /**
     * Translate class name from application to system.
     *
     * @param string
     * @return integer
     */
    public static function classToSys( $class )
    {
        return ClassDao::singleton()->toId($class);
    }

    /**
     * Translate class name from system to application.
     *
     * @param string
     * @return string
     */
    public static function classToApp( $class )
    {
        return ClassDao::singleton()->toName($class);
    }

    /**
     * Convert a array in a postgres array synthax.
     * The keys names are stored in the second array strucuture:
     *
     * {{value,...}{key,...}}
     *
     * @link http://docs.postgresqlfr.org/8.4/arrays.html
     *
     * @param array	$phpArray
     * @return string
     */
    protected static function arrayToSys( $phpArray )
    {
        $strValues = '{';
        $strKeys = '{';
        $delim = '';
        foreach($phpArray as $key=>$value){
            if(is_array($value)){
                $value = self::_toPgArray($value);
            }
            $strValues .= $delim . '"' . $value . '"';
            $strKeys .= $delim . '"' . $key . '"';
            $delim = ',';
        }
        $strValues .= '}';
        $strKeys .= '}';

        return '{' . $strValues . ',' . $strKeys . '}';
    }

    /**
     * Convert a postgres array strucutre synthax to php array.
     *
     * @link http://docs.postgresqlfr.org/8.4/arrays.html
     *
     * From veggivore at yahoo dot com on : @link http://php.net/manual/en/ref.pgsql.php
     *
     * @param string	$pgArray
     * @return array
     */
    protected static function arrayToApp( $pgArray )
    {
        $s = $pgArray;
        $s = str_replace("{", "array(", $s);
        $s = str_replace("}", ")", $s);
        $s = "\$retval = $s;";
        eval( $s );
        return $retval;
    }

    /**
     * Convert a array in a postgres xml structure.
     *
     * @param array		$phpArray
     * @param boolean	$asString	If true, return a string, else return a \SimpleXMLElement object
     * @return string XML | \SimpleXMLElement
     */
    protected static function xmlarrayToSys( $phpArray, $asString = true )
    {
        $xml = new \SimpleXMLElement("<array></array>");
        array_walk_recursive($phpArray, array ($xml, 'addChild'));
        if($asString){
            return $xml->asXML();
        }
        else{
            return $xml;
        }
    }

    /**
     * Convert a xml structure synthax to php array.
     *
     * @param string	$xml
     * @return array
     */
    protected static function xmlarrayToApp( $xml )
    {
        $f = function($iter) {
            foreach($iter as $key=>$val)
                $arr[$key][] = ($iter->hasChildren())?
                call_user_func (__FUNCTION__, $val)
                : strval($val);
            return $arr;
        };
        return $f( new \SimpleXMLElement($xml) );
    }

    /**
     * Encode multi-value property to inject in postgresql
     * Use json encoding, but may be other like xml or postgres array.
     *
     * @param array		$phpArray
     * @return string
     */
    public static function jsonToSys( array $phpArray )
    {
        return json_encode($phpArray);
    }

    /**
     * Decode multi-value property to inject in application.
     * Ensure that method multivalToPg is coherent with this converter method.
     *
     * @param string	$json
     * @return array
     */
    public static function jsonToApp( $json )
    {
        return json_decode($json, true);
    }
} //End of class
