<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;

use Rbplm\Dao\LoaderInterface;
use Rbplm\Dao\MappedInterface;

use Rbplm\Dao\Exception;
use Rbplm\Sys\Error;
use Rbplm\Dao\Factory as DaoFactory;

/** SQL_SCRIPT>>
 CREATE TABLE index (
 uid uuid NOT NULL,
 dao varchar(256) DEFAULT NULL,
 connexion varchar(256) DEFAULT NULL
 );
 ALTER TABLE index ADD PRIMARY KEY (uid);
 <<*/


/** SQL_VIEW>>
 CREATE OR REPLACE VIEW view_anyobject_index AS
 SELECT
 anyobject.id,
 anyobject.uid,
 anyobject.name,
 anyobject.cid,
 anyobject.label,
 anyobject.parent,
 anyobject.path,
 classes.name AS class_name,
 index.dao,
 index.connexion
 FROM
 anyobject
 JOIN classes ON (anyobject.cid = classes.id)
 LEFT OUTER JOIN index ON (anyobject.uid = index.uid);

 CREATE OR REPLACE VIEW view_anyobject_class AS
 SELECT
 anyobject.id,
 anyobject.uid,
 anyobject.name,
 anyobject.label,
 anyobject.parent,
 anyobject.path,
 classes.id AS cid,
 classes.name AS class_name
 FROM
 anyobject
 JOIN classes ON (anyobject.cid = classes.id);

 <<*/



/**
 * @brief Instanciate a anyobject and load properties in from the given Uuid.
 *
 * It is a builder class to instanciate the Rbplm objects from a Uuid. instanciate the corresponding class
 * and load properties from the pg database.
 *
 * @see \Rbplm\Dao\LoaderInterface
 *
 * @example
 * @code
 * $myanyobject = Loader::load($uid);
 * @endcode
 *
 *
 *
 */
class Loader implements LoaderInterface
{
	private static $options = array(
		'force'=>false,
		'locks'=>false,
		'useRegistry'=>true,
		'uidkeyname'=>'uid',
		'idkeyname'=>'id',
		'parentkeyname'=>'parentId',
		'childkeyname'=>'childId',
		'dao'=>null,
		'classname'=>null
	);

	/**
	 * The table to query for get the class name from uid.
	 * @var string
	 */
	static $_master_index_table = 'view_anyobject_index';

	/**
	 * Connexion to pgsql server where is the master index table
	 * @var \PDO
	 */
	private static $_connexion = null;

	/**
	 * Set the connexion to Postgresql db where are store the index.
	 *
	 * @param \PDO
	 * @return void
	 */
	public static function setConnexion( \PDO $connexion )
	{
		self::$_connexion = $connexion;
	}

	/**
	 * Set a option
	 * Option key may be :
	 * 		force
	 * 		lock
	 * 		useregistry
	 * 		uidkeyname
	 * 		idkeyname
	 * 		parentidkeyname
	 * 		childidkeyname
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public static function setOptions( $key, $value )
	{
		$key = strtolower($key);
		static::$options[$key]=$value;
	}


	/**
	 * @see \Rbplm\Dao\LoaderInterface::load()
	 *
	 * @param string				$uid	Uuid of the object to load.
	 * @param array		$options = array(
	 * 						boolean	'force'	[OPTIONAL] If true, force to query db to reload links.
	 * 						boolean	'locks'  [OPTIONAL] If true, lock the db records in accordance to the use db system.
	 * 						boolean	'useRegistry' [OPTIONAL] If true, get object from registry, default is true
	 * 						string	'classname'	  [OPTIONAL]	Name of class of object to load
	 * 						\Rbplmpg\Dao	'dao' [OPTIONAL]	The dao used to load
	 * )
	 * @return \Rbplm\AnyObject
	 *
	 * @throws \Rbplm\Sys\Exception
	 */
	public static function loadFromUid($uid, $classId)
	{
		if(!$uid){
			throw new Exception( Error::BAD_PARAMETER_OR_EMPTY, Error::WARNING, array('uid') );
		}

		if($uid == \Rbplm\Org\Root::UUID){
			return \Rbplm\Org\Root::singleton();
		}

		$options = self::$options;

		$className = $options['classname'];
		$dao = $options['dao'];
		$force = $options['force'];
		$locks = $options['locks'];
		$useRegistry = $options['useRegistry'];

		if($useRegistry){
			$Object = \Rbplm\Dao\Registry::singleton()->get($uid);
			if($Object){
				return $Object;
			}
		}

		if( $className == null && self::$_connexion){
			//Get class from master index
			$sql = "SELECT class_name FROM " . self::$_master_index_table . ' WHERE uid=:uid';
			$stmt = self::$_connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$stmt->execute( array(':uid'=>$uid) );
			$row = $stmt->fetch();
			//$className = $row['class_name'];
			$className = str_replace('_', '\\', $row['class_name']);
			//$daoClassname = $row['dao'];
			//$connexionName = $row['connexion'];
		}

		if( !$className ){
			throw new Exception( 'UNABLE_TO_FIND_OBJECT_UID_%0%_IN_%1%', Error::WARNING, array($uid, self::$_master_index_table) );
		}

		//use the factory for get instance of dao from registry:
		$Object = new $className();

		if( !is_a($Dao, '\Rbplmpg\Dao') ){
			$dao = DaoFactory::getDao( $className );
		}

		$options = array('locks'=>$locks, 'force'=>$force);
		$dao->loadFromUid( $Object, $uid, $options );

		if($useRegistry){
			\Rbplm\Dao\Registry::singleton()->add($Object);
		}

		return $Object;
	}

	/**
	 *
	 */
	public static function loadFromId($id, $classId)
	{
		return;
	}


	/**
	 * Recursive function
	 * Load from Uid
	 */
	public static function loadChildren($parent, $filter=null)
	{
		$DaoFactory = static::_getDaoFactory();
		$ModelFactory = static::_getModelFactory();

		$List = $DaoFactory::getList($Parent::$classId);
		$ParentUid = $Parent->getUid();
		if($filter){
			$filter = "$filter AND parentUid='$ParentUid' ORDER BY `index` ASC";
		}
		else{
			$filter = "parentUid='$ParentUid' ORDER BY `index` ASC";
		}
		$List->load($filter);

		foreach($List as $item){
			$uid = $item['uid'];
			$Child = $ModelFactory::get($item['cid']);
			$DaoFactory::getDao($item['cid'])->loadFromUid($Child, $uid);
			$Parent->addChild($Child);
			self::loadChildren($Child);
		}
		return $Parent;
	}



	/**
	 *
	 * @param MappedInterface    	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParent( MappedInterface $mapped, $expectedProperties = array(), $force = false, $locks = true )
	{
		$mapped->setParent( self::load( $mapped->parentId, $expectedProperties, $force, $locks ) );
	} //End of method


	/**
	 *
	 * @param \Rbplm\AnyObject	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParentRecursively( MappedInterface $mapped, $force = false, $locks = true )
	{
		if($mapped->parentId){
			$parent = Loader::load( $mapped->parentId, array(), $force, $locks );
			if($parent){
				self::loadParentRecursively( $parent, $force, $locks );
				$mapped->setParent( $parent );
			}
		}
	} //End of method

} //End of class
