<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;


/**
 * $Id: Model.php 454 2011-06-10 16:26:26Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Model.php $
 * $LastChangedDate: 2011-06-10 18:26:26 +0200 (ven., 10 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 454 $
 */


/** @brief This class manage metadatas definition in extend tables.
 * 
 *
 */
class Propset{

	/**
	 * 
	 * Definition of propsets.
	 * Its array where first keys are uid of the related anyobjects, 
	 * second keys are id of the related class,
	 * and value are array of extended properties name
	 * @code
	 * 	$this->_propset = array( 
	 * 		uuid=>array(integer=>array(string))
	 *  );
	 * @endcode 
	 * 
	 * @var array
	 */
	protected $_propsets = array();
	
	/**
	 * Implements \Rbplm\Dao\MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	/**
	 * Singleton instance.
	 * @var Rbplm_Dao_Propset
	 */
	protected static $_instance = null;
	
	/** 
	 *
	 */
	private function __construct()
	{
	}//End of method
	
	
	/** 
	 * @return Rbplm_Dao_Propset
	 */
	public function singleton()
	{
		if( !self::$_instance ){
			self::$_instance = new self();
		}
		return self::$_instance;
	}//End of method
	
	
	/**
	 * Implements \Rbplm\Dao\MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/\Rbplm\Dao\MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->loaded;
		}
		else{
			return $this->loaded = (boolean) $bool;
		}
	}
	
	
	/** 
	 * Setter for propset of the anyobject $uid and class $classId
	 *
	 */
	public function setPropset( array $propsets, $uid, $classId )
	{
		$this->_propsets[$uid][$classId] = $propsets;
	}//End of method
	
	
	/** 
	 * Getter for propset of the anyobject $uid and class $classId.
	 * 
	 * - You can set only $uid to get all propset for this object.
	 * - You can let both parameter to default to get all defined propset.
	 * - You can not use only $classId.
	 * 
	 * @param string	$uid
	 * @param integer	$classId
	 */
	public function getPropset( $uid=null, $classId=null )
	{
		if($uid && $classId){
			return $this->_propsets[$uid][$classId];
		}
		else if($uid){
			return $this->_propsets[$uid];
		}
		else if(!$uid && !$classId){
			return $this->_propsets;
		}
		else{
			throw new \Exception('$classid parameter must be use with $uid parameter');
		}
	}//End of method
	
	
	/** 
	 * Has propsets for the anyobject $uid and class $classId?
	 * 
	 * 
	 * @param string	$uid
	 * @param integer	OPTIONAL $classId
	 *
	 */
	public function hasPropset( $uid, $classId = null )
	{
		if($uid && $classId){
			return isset($this->_propsets[$uid][$classId]);
		}
		else if($uid){
			return isset($this->_propsets[$uid]);
		}
		else{
			throw new \Exception('You must set at least uid parameter');
		}
	}//End of method
	
	
	/**
	 * 
	 * @param $uid
	 * @param $classId
	 * @return array
	 */
	public function toSysSelect( $uid, $classId )
	{
		$select = array();
		foreach( $this->_propsets[$uid][$classId] as $prop ){
			$select[] = $prop['sysname'];
		}
		return $select;
	}
	
	/**
	 * 
	 * @param $uid
	 * @param $classId
	 * @return array
	 */
	public function toAppSelect( $uid, $classId )
	{
		$select = array();
		foreach( $this->_propsets[$uid][$classId] as $prop ){
			$select[] = $prop['appname'];
		}
		return $select;
	}
	
	
	
	/** 
	 * Reset all propset.
	 * 
	 * - You can set only $uid to reset all propset for this object.
	 * - You can let both parameter to default to reset all defined propset.
	 * - You can not use only $classId.
	 * 
	 * @param string	$uid
	 * @param integer	$classId
	 *
	 */
	public function reset( $uid=null, $classId=null )
	{
		if($uid && $classId){
			$this->_propsets[$uid][$classId] = array();
		}
		else if($uid){
			$this->_propsets[$uid] = array();
		}
		else if(!$uid && !$classId){
			$this->_propsets = array();
		}
		else{
			throw new \Exception('$classid parameter must be use with $uid parameter');
		}
		$this->loaded = false;
	}//End of method
	
	
	
} //End of class
