<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;
use Rbplmpg\Dao\ClassDao;
use Rbplm\Test\Test;
use Rbplm\Dao\Connexion;

/**
 * @brief Test class for \Rbplmpg\Dao\Class
 * 
 */
class ClassDaoTest extends Test
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }
	
    
    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    	echo '/////////////////////////////////////' . CRLF;
    	echo 'End of ' . get_class($this) . CRLF;
    	echo '/////////////////////////////////////' . CRLF;
    }
	
	/**
	 * 
	 */
	function Test_Dao()
	{
		$Class = ClassDao::singleton()->setConnexion(Connexion::get());
		
		var_dump( $Class->toName(10) );
		var_dump( $Class->toId('\Rbplm\AnyObject') );
		var_dump( $Class->getTable('\Rbplm\AnyObject') );
		var_dump( $Class->getTable(10) );
		
		assert( $Class->toName(10) == '\Rbplm\AnyObject');
		assert( $Class->toId('\Rbplm\AnyObject') == 10);
		assert( $Class->getTable('\Rbplm\AnyObject') == 'anyobject');
		assert( $Class->getTable(10) == 'anyobject');
	}
}


