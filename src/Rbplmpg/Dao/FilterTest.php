<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;


    /**
     * Assume that tests datas are loaded in db with this structure:
     * 
     *		INSERT INTO test VALUES ('Top');
     *		INSERT INTO test VALUES ('Top.Science');
     *		INSERT INTO test VALUES ('Top.Science.Astronomy');
     *		INSERT INTO test VALUES ('Top.Science.Astronomy.Astrophysics');
     *		INSERT INTO test VALUES ('Top.Science.Astronomy.Cosmology');
     *		INSERT INTO test VALUES ('Top.Hobbies');
     *		INSERT INTO test VALUES ('Top.Hobbies.Amateurs_Astronomy');
     *		INSERT INTO test VALUES ('Top.Collections');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Stars');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Galaxies');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Astronauts');
     * 
     */


/**
 * @brief Test class for \Rbplmpg\Dao\Filter
 * 
 */
class FilterTest extends \Rbplm\Test\Test
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	\Rbplm\Dao\Registry::singleton()->reset();
    }
	
    
    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    	echo '/////////////////////////////////////' . CRLF;
    	echo 'End of ' . get_class($this) . CRLF;
    	echo '/////////////////////////////////////' . CRLF;
    }
	
	/**
	 * 
	 */
	function testLiteral()
	{
		$Filter = \Rbplm\Dao\Factory::getFilter('\Rbplm\Org\Unit');
		
		//many ancestors condition
		$Filter->children('RanchbePlm/Top/Science', array('level'=>1, 'depth'=>0, 'boolOp'=>'OR'));
		$Filter->children('RanchbePlm/Top/Collections', array('level'=>1, 'depth'=>0, 'boolOp'=>'OR'));
		echo $Filter . CRLF;
		
		//with sub filter
		$SubFilter = \Rbplm\Dao\Factory::getFilter('\Rbplm\Org\Unit');
		$SubFilter->andfind('qqueschose', 'name', \Rbplm\Dao\Filter\Op::OP_CONTAINS);
		$SubFilter->orfind('autrechose', 'name', \Rbplm\Dao\Filter\Op::OP_CONTAINS);
		$SubFilter->orfind('encoreautrechose', 'name', \Rbplm\Dao\Filter\Op::OP_CONTAINS);
		$Filter->subor($SubFilter);
		echo $Filter . CRLF;;
	}
    
    
	/**
	 * 
	 */
	function testFilter()
	{
		$Filter = \Rbplm\Dao\Factory::getFilter('\Rbplm\Org\Unit');
		$List = \Rbplm\Dao\Factory::getList('\Rbplm\Org\Unit');
		
		echo 'Get first level after RanchbePlm/Top' . CRLF;
		$Filter->children('RanchbePlm/Top', array('level'=>1, 'depth'=>0))
				->sort('path', 'ASC')
				->page($page, $pageLimit);
		$List->load($Filter);
		
		echo 'list raw data in system semantic' . CRLF;
		foreach($List as $e){
			var_dump($e['path']);
		}
		
		echo 'list converted to application properties' . CRLF;
		$List->rewind();
		while($List->valid()){
			$List->next();
			$e = $List->toApp();
			var_dump($e['path']);
		}
		
		echo 'Get 2 levels after RanchbePlm/Top' . CRLF;
		$Filter = \Rbplm\Dao\Factory::getFilter('\Rbplm\Org\Unit')
				->children('RanchbePlm/Top', array('level'=>1, 'depth'=>1))
				->sort('path', 'ASC')
				->page($page, $pageLimit);
		$List->load($Filter, array('force'=>true));
		while($List->valid()){
			$List->next();
			$e = $List->toApp();
			var_dump($e['path']);
		}
		
		echo 'Get the 2nd level after RanchbePlm/Top' . CRLF;
		$Filter = \Rbplm\Dao\Factory::getFilter('\Rbplm\Org\Unit')
				->children('RanchbePlm/Top', array('level'=>2, 'depth'=>0))
				->sort('path', 'ASC')
				->page($page, $pageLimit);
		$List->load($Filter, array('force'=>true));
		while($List->valid()){
			$List->next();
			$e = $List->toApp();
			var_dump($e['path']);
		}
	}
	
}


