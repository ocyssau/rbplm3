<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;

use Rbplm\Dao\Exception;
use Rbplm\Sys\Error;

/** SQL_SCRIPT>>
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
<<*/

/**
 * Dao to access to class identification in db.
 * Use by Postgres Dao class only.
 * 
 * This class load class definition from a table in db, to map class name to a integer id.
 * 
 * To use it, first, you must set a connexion. When setConnexion is call, class definition are loaded,
 * so, just do in bootstrap of application:
 * @code
 * Class::singleton()->setConnexion( $myConnexion );
 * //when set connexion is set, class is ready to use:
 * $myClassName = Class::singleton()->toName(100);
 * $myClassId = Class::singleton()->toId($myClassName);
 * @endcode
 * 
 */
class ClassDao
{
	
	/**
	 *
	 * @var \PDO
	 */
	protected $connexion = null;
	
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'classes';
	
	/**
	 * 
	 * @var array
	 */
	protected $_classes = null;
	
	/**
	 * @var array
	 */
	protected $_idIndex = null;
	
	/**
	 * @var array
	 */
	protected $_classIndex = null;
	
	/**
	 * 
	 * Singleton pattern
	 * @var Class
	 */
	protected static $_instance = null;
	
	/**
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	/**
	 * Constructor
	 */
	private function __construct()
	{
	} //End of function
	
	
	/**
	 * Singleton pattern
	 * @return Class
	 */
	public static function singleton()
	{
		if( !self::$_instance ){
			self::$_instance = new ClassDao();
		}
		return self::$_instance;
	} //End of function
	
	
	/**
	 *
	 * @return \PDO
	 */
	public function getConnexion()
	{
		if( !$this->connexion ){
			throw new Exception('CONNEXION_IS_NOT_SET', Error::ERROR);
		}
		return $this->connexion;
	} //End of method


	/**
	 *
	 * @param \PDO
	 * @return Class
	 */
	public function setConnexion( $conn )
	{
		$this->connexion = $conn;
		$this->_load();
		return $this;
	} //End of method
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Org\Unit	$mapped
	 * @param array $row	\PDO fetch result to load
	 * @return void
	 */
	protected function _load()
	{
	    $table = static::$table;
		$sql = "SELECT id, name, tablename FROM $table";
		$stmt = $this->connexion->query($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$this->_classes = $stmt->fetchAll();

		//$this->_idIndex = new \SplFixedArray( count($this->_classes) );
		//$this->_classIndex = new \SplFixedArray( count($this->_classes) );
		
		$this->_idIndex 	= array();
		$this->_classIndex 	= array();
		
		foreach($this->_classes as $k=>$def){
			$this->_idIndex[$k] = $def['id'];
			$this->_classIndex[$k] = $def['name'];
		}
		$this->loaded = true;
	}
	
	/**
	 * Return the id of a class from his name.
	 * 
	 * @param string $name
	 * @return integer
	 * @throws Exception
	 */
	public function toId($name)
	{
		$k = array_search($name, $this->_classIndex);
		if( $k === false ){
			throw new Exception('CLASSNAME_NOT_FOUND_%0%', Error::WARNING, array($name));
		}
		return $this->_classes[$k]['id'];
	}
	
	/**
	 * Return name of a class from his id.
	 * 
	 * @param integer 		$id
	 * @return string
	 * @throws Exception
	 */
	public function toName($id)
	{
		$k = array_search($id, $this->_idIndex);
		if( $k === false ){
			throw new Exception('CLASSID_NOT_FOUND_%0%', Error::WARNING, array($id));
		}
		return $this->_classes[$k]['name'];
	}
	
	/**
	 * Return table used to store objects of class $id
	 * 
	 * @param integer|string		$id
	 * @return string
	 * @throws Exception
	 */
	public function getTable($id)
	{
		if(is_integer($id)){
			$k = array_search($id, $this->_idIndex);
			if( $k === false ){
				throw new Exception('CLASSID_NOT_FOUND_%0%', Error::WARNING, array($id));
			}
		}
		else{
			$k = array_search($id, $this->_classIndex);
			if( $k === false ){
				throw new Exception('CLASSNAME_NOT_FOUND_%0%', Error::WARNING, array($id));
			}
		}
		return $this->_classes[$k]['tablename'];
	}
	
	
} //End of class
