<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader as DaoLoader;
use Rbplmpg\Dao\ClassDao;
use Rbplmpg\Dao\Propset;
use Rbplm\Dao\Connexion;
use Rbplm\Org;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 * 
 * @include Rbplm/Org/UnitTest.php
 * 
 */
class PropsetTest extends \Rbplm\Test\Test
{
    
	/**
	 * @var    \Rbplm\Org\Unit
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->object = Org\Unit::init(uniqid('myOu'))->setParent(Org\Root::singleton());
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	/**
	 */
	function Test_Dao()
	{
		DaoFactory::getDao($this->object)->save($this->object);
		
		echo "EXTENDS PROPERTIES \n";
		$uid = $this->object->getUid();
		$classId = $this->object->classId;
		
		$Propset = Propset::singleton();
		assert( $Propset->isLoaded() === false );
		
		$Propset->setPropset(array(109,110,111), $uid, $classId);
		DaoFactory::getDao($Propset)->savePropset($Propset);
		
		/* Explicit style: */
		/*
		$config = array('table'=>'propset');
		$PropsetDao = new \Rbplmpg\Dao\DaoList($config);
		$PropsetDao->setConnexion( Connexion::get() );
		
		$listToSave = array();
		$listToSave[] = array('related'=>$uid, 'cid'=>$classId, 'property'=>1 );
		$listToSave[] = array('related'=>$uid, 'cid'=>$classId, 'property'=>2 );
		$listToSave[] = array('related'=>$uid, 'cid'=>$classId, 'property'=>3 );
		$PropsetDao->save($listToSave);
		*/
		
		/*Load propset definition*/
		Propset::singleton()->reset();
		$Propset = Propset::singleton();
		DaoFactory::getDao($Propset)->loadPropset( $Propset, $uid, $classId );
		
		
		/* Explicit style: */
		/*
		$filter = "related='$uid' AND cid=$classId";
		$PropsetDao->load( $filter );
		$Propset->setPropset($PropsetDao->toArray(), $uid, $classId);
		$Propset->isLoaded(true);
		*/
		
		$properties = $Propset->getPropset($uid, $classId);
		assert( is_array($properties) );
		$isLoaded = $Propset->isLoaded();
		assert( $isLoaded === true );
		$has = $Propset->hasPropset($uid, $classId);
		assert( $has === true );
		
		assert( is_array( $Propset->toSysSelect($uid, $classId) ) );
		assert( is_array( $Propset->toAppSelect($uid, $classId) ) );
		
		var_dump($properties);
		var_dump( $Propset->toSysSelect($uid, $classId) );
		var_dump( $Propset->toAppSelect($uid, $classId) );
		
		return;
		
		//TO CREATE PROPSET FOR DOCUMENTS CHILDREN OF THE OU
		$classId = ClassDao::singleton()->toId( '\Rbplm\Ged\Document\Version' );
		$Propset = new \Rbplmpg\Dao\Propset();
		$filter = "related='$uid' AND class=$classId";
		$PropsetDao->load($Propset, $filter);
		
		//TO CREATE PROPSET FROM THE DOCTYPE BUT ONLY RELATED TO OU
		$classId = ClassDao::singleton()->toId( '\Rbplm\Ged\Doctype' );
		$Propset = new \Rbplmpg\Dao\Propset();
		$filter = "related='$uid' AND class=$classId";
		$PropsetDao->load($Propset, $filter);
		
		//TO CREATE PROPSET FOR ALL DOCUMENT WITH SPECIFIED DOCTYPE
		$classId = ClassDao::singleton()->toId( '\Rbplm\Ged\Document\Version' );
		$Doctype = new \Rbplm\Ged\Doctype();
		$uid = $Doctype->getUid();
		$Propset = new \Rbplmpg\Dao\Propset();
		$filter = "related='$uid' AND class='$classId'";
		$PropsetDao->load($Propset, $filter);
	}
}
