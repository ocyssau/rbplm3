<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;

use Rbplm\Dao\DaoListAbstract;

use Rbplm\Dao\Registry;
use Rbplm\Dao\Exception As Exception;
use Rbplm\Sys\Error As Error;
use Rbplm\Model\Collection;
use Rbplmpg\Dao\ClassDao;
use Rbplm\Dao\Factory as DaoFactory;

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_fullpathname_anyobject AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.label FROM anyobject AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathname
FROM anyobject As o 
WHERE path IS NOT NULL
ORDER BY fullpathname;

CREATE OR REPLACE VIEW view_fullpathuid_anyobject AS
SELECT o.id, o.name, o.label, o.cid, o.parent, o.path, array_to_string(
	ARRAY(SELECT a.uid FROM anyobject AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM anyobject As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;


CREATE OR REPLACE VIEW view_fullpathuid_anyobject_2 AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.uid FROM anyobject AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM anyobject As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;
<<*/

/**
 * A list of records in db.
 * This class implements a iterator.
 * Query postgresql database to create a list of scalar values in a iterator class.
 * Example and tests: Rbplm/Dao/Pg/ListTest.php
 * 
 * @todo Check and improve update list procedures.
 * 
 */
class DaoList implements ListInterface
{
	
	/**
	 * @see Rbplm\Dao.ListInterface::toApp()
	 *
	 * Translate current row from system notation to application notation.
	 *
	 * Class variable $this->dao may be set the dao object.
	 * If $dao is a object, assume that is a type with a method toApp().
	 * If $dao is null, try to use the cid property from record to get the dao
	 * In this last case, require an attribut cid in current statement.
	 *
	 * @param $dao	[OPTIONAL]	Set a Dao class or any other class with a method toApp(), to use for convert attributs.
	 * @return array
	 */
	public function toApp( $daoClassOrObjectOrCid=null )
	{
		$current = $this->current();
	
		if( is_string($daoClassOrObjectOrCid) || is_object($daoClassOrObjectOrCid) ){
			$dao = $daoClassOrObject;
		}
		else if( is_integer($daoClassOrObjectOrCid) ){
			$classId = $daoClassOrObject;
			$dao = DaoFactory::getDaoClass($classId);
		}
		else if( isset($current['cid']) ){
			$classId = $current['cid'];
			$dao = DaoFactory::getDaoClass($classId);
		}
		else{
			throw new Exception('cid_IS_NOT_SET', Error::WARNING, $current);
		}
	
		return call_user_func(array($dao, 'toApp'), $current);
	}
	
	
	
	/**
	 * Translate current row to Rbplm object.
	 * If $useRegistry = true, get object from registry if exist and add it if not.
	 *
	 * @param bool $useRegistry
	 * @return AnyObject
	 *
	 */
	public function toObject($useRegistry = true)
	{
		$current = $this->current();
	
		if($useRegistry){
			$Mapped = Registry::singleton()->get($current['uid']);
		}
	
		if( !$Mapped ){
			if( isset($current['cid']) ){
				$className = ClassDao::singleton()->toName( $current['cid'] );
				$Dao = DaoFactory::getDao($className);
				$Mapped = new $className();
				$Dao->loadFromArray($Mapped, $current, false);
				if($useRegistry){
					Registry::singleton()->add($Mapped);
				}
			}
			else{
				throw new Exception('cid_IS_NOT_REACHABLE', Error::ERROR, $current);
			}
		}
	
		return $Mapped;
	}
	
	/**
	 * Translate current row from system notation to application notation.
	 * 
	 * @return array
	 */
	public function toSys()
	{
		$current = $this->current();
		if( isset($current['classId']) ){
			$class = ClassDao::singleton()->toName($current['classId']);
		}
		else{
			throw new Exception('cid_IS_NOT_SET', Error::WARNING, $current);
		}
		$DaoClass = DaoFactory::getDaoClass($class);
		return call_user_func( array($DaoClass, 'toSys'), array($current) );
	}
	
	/**
	 * Return application class for current entry.
	 * 
	 * @return array
	 */
	public function getAppClass()
	{
		$current = $this->current();
		if( isset($current['cid']) ){
			return ClassDao::singleton()->toName($current['cid']);
		}
		else{
			throw new Exception('cid_IS_NOT_SET', Error::WARNING, $current);
		}
	}
	
} //End of class
