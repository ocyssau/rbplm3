<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;


/** SQL_SCRIPT>>
CREATE TABLE propset (
	lparent uuid NOT NULL,
	property integer NOT NULL,
	cid integer NOT NULL,
	PRIMARY KEY (lparent, property, cid)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_propset_property AS
SELECT l.property AS property, l.lparent AS lparent, l.cid AS cid, r.sysname AS sysname, r.appname AS appname
FROM propset AS l
JOIN meta_type AS r ON l.property = r.id;
 <<*/

/** SQL_DROP>>
DROP TABLE IF EXISTS propset CASCADE;
 <<*/

//require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for \Rbplmpg\Dao\Propset
 * 
 */
class PropsetDaoPg extends \Rbplmpg\Dao\DaoList
{
	
	public static $table = 'propset';
	public static $vtable = 'view_propset_property';
	
	/**
	 * 
	 * @param $mapped
	 * @param $uid
	 * @param $classId
	 * @param array			$options	Is array of options.
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return void
	 */
	public function loadPropset( $mapped, $uid, $classId, $options = array() )
	{
		parent::load( "related='$uid' AND cid=$classId", $options );
		$mapped->setPropset($this->_stmt->fetchAll(), $uid, $classId);
		$mapped->isLoaded(true);
	} //End of function
	
	
	/**
	 * 
	 * @param $mapped
	 * @param array			$options	Is array of options.
	 * 							'unlock'	=>boolean 	If true, unlock records, default is false
	 * @return void
	 */
	public function savePropset( $mapped, $options = array() )
	{
		$listToSave = array();
		foreach( $mapped->getPropset() as $uid=>$objectPropsets ){
			foreach($objectPropsets as $classId=>$classPropsets){
				foreach($classPropsets as $propertyId){
					$listToSave[] = array('related'=>$uid, 'cid'=>$classId, 'property'=>$propertyId );
				}
			}
		}
		return parent::save($listToSave, $options);
	} //End of function

} //End of class

