<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Dao;

/**
 * Generate Dao, SQL scripts for a model.
 * 
 * Assume that model define this DTYP:
 * appName,appGetter, appSetter, appType, toSysFilter,toAppFilter,sysName,sysType,sysTypeParam,sysDefault,isnull,isindex,isuniq,foreignKey,foreignTable,linkTable
 *
 */
class Generator{
	
	
	/**
	 * 
	 * Sql tables defintions
	 * @var array
	 */
	private $_sqls = array();
	
	/**
	 * 
	 * The last model use to generate dao class
	 * @var \Rbplm\Sys\Meta\Model
	 */
	private $_model = null;
	
	/**
	 * Id of current application class
	 * @var integer
	 */
	protected static $classId;
	
	/**
	 * Name of current application class
	 * @var string
	 */
	protected $_className;
	
	/**
	 * Sql table definition.
	 * @var array
	 * 
	 * array(
	 * 	'table'=>string,
	 * 	'inherits'=>string,
	 *  'fields'=>array(0=>array(
	 *  	'name'=>string
	 *  	'type'=>string
	 *  	'isnull'=>boolean
	 *  	'length'=>integer
	 *  	'constraint'=>string
	 *  	'index'=>boolen
	 *  	'foreignKey'=>string
	 *  	'foreignTable'=>string
	 *  'inheritsfields'=>array(0=>array()) //as fields
	 *  )))
	 */
	private $_table = array(
					  	'name'=>'',
					  	'inherits'=>'',
						'inheritsfields'=>array(),
					    'fields'=>array());
	
	/**
	 * 
	 * @param string	$class
	 * @param string	$outputDir
	 * @param string	$templateFile
	 * @param \Rbplm\Sys\Meta\Model	$Model
	 * @return void
	 * @throws Exception
	 */
    public function generate($class, $outputDir, $templateFile, $Model){
    	$this->_model = $Model;
    	
    	/*Get application class name*/
    	$class = trim($class);
    	
    	/*declarations*/
		$A_UPDATE_SET = array();
		$A_UPDATE_BIND_AFFECTATION = array();
		$A_INSERT_BIND_AFFECTATION = array();
		$A_ADDITIONALS_METHODS = array();
		//$A_LOAD_SELECT = array();
		$A_LOAD_FROM_APP_GETTER = array();
		$A_LOAD_FROM_SYS_GETTER = array();
		$A_SYS_TO_APP = array();
		
		/*Add common select for all classes*/
		//$A_LOAD_SELECT[] = 'id';
		//$A_LOAD_SELECT[] = 'cid';
		
    	/*Create output dir*/
    	if(!is_dir($outputDir)){
    		mkdir($outputDir, 0777, true);
    	}
    	/*Build path to files*/
    	$classFile = str_replace('_', '/', $class) . '.php';
    	require_once($classFile);
    	
        $daoClassName = $class . 'DaoPg';
        $myDaoClassFile = str_replace('_', '/', $daoClassName) . '.php';
        
        $myTestClass = $class . 'Test';
        $myTestFile = str_replace('_', '/', $myTestClass) . '.php';
        
        $output = $outputDir . '/' . basename($myDaoClassFile);
        
        $templateContent = file_get_contents($templateFile);
        $templateContent = str_replace('%DAO_CLASS_NAME%', $daoClassName, $templateContent);
        $templateContent = str_replace('%MAPPED_CLASS_NAME%', $class, $templateContent);
        $templateContent = str_replace('%EXAMPLE_PATH%', $myTestFile, $templateContent);
        $templateContent = str_replace('%EXAMPLE_CLASS%', $myTestClass, $templateContent);
        $templateContent = str_replace('%VAR_DECLARATION%', '', $templateContent);
        
        /*The table name*/
        $tableName = $Model->sysClasses[$class]['map'];
        $templateContent = str_replace('%TABLE_NAME%', $tableName, $templateContent);
        $tableInherits = $Model->sysClasses[$class]['inherits'];
        
        $classId = $Model->classeId[$class];
        $templateContent = str_replace('%cid%', $classId, $templateContent);
        
        $reflexionClass = new \ReflectionClass($class);
        //$properties = $reflexionClass->getProperties();
        
        /*Populate _table property to generate SQL creation scripts*/
        $this->_table['name'] = $tableName;
        $this->_table['inherits'] = $tableInherits;
        
        /*set class signature*/
    	$this->_className = $class;
    	$this->_classId = $classId;
        $properties = $Model->toSys[$class];
        
        $inheritsProperties = array();
        if( $Model->getAppInherits($class) ){
        	$properties = array_merge( $Model->toSys[$Model->getAppInherits($class)] , $properties );
        }
        //var_dump( $Model->sysClasses[$class]['map'] );die;
        //var_dump( $Model->getAppInherits($class) );
        //var_dump( $Model->toSys[$Model->getAppInherits($class)] );
        
        foreach($properties as $appPropname=>$sysProp){
        	$isPhatomAppProp = false;
			if( $appPropname[0] == '@' ){
				$isPhatomAppProp = true;
        		$appPropname = str_replace('@', '', $appPropname);
			}
			
        	$sysPropname = $sysProp['sysName'];
        	$propTyp = $sysProp['sysType'];
        	$appPropTyp = $sysProp['appType'];
        	$propParam = $sysProp['sysTypeParam'];
        	$propConstraint = $sysProp['constraint'];
        	$propDefault = $sysProp['sysDefault'];
        	$propFkey = $sysProp['foreignKey'];
        	$propFtable = $sysProp['foreignTable'];
        	$isCollection = (boolean) $sysProp['iscollection'];
        	$isIndex = (boolean) $sysProp['isindex'];
        	$isUniq = (boolean) $sysProp['isuniq'];
        	$isNull = (boolean) $sysProp['isnull'];
        	$isIndex = (boolean) $sysProp['isindex'];
        	$isPkey = (boolean) $sysProp['ispkey'];
        	$isAuto = (boolean) $sysProp['isautoincrement'];
        	$toSysFilter = $sysProp['toSysFilter'];
        	$toAppFilter = $sysProp['toAppFilter'];
        	$toAppFilterFromApp = null;
        	$toAppFilterFromSys = null;
        	
        	if( $isCollection ){
        		$A_ADDITIONALS_METHODS[] =  $this->_getCollectionLoaderMethod( $sysProp );
        		continue;
        	}
        	else if($sysPropname[0] == '@'){
        		continue;
        	}
        	
        	/*set table properties to generate SQL scripts*/
        	$sysClass = $Model->sysClasses[$class]['map'];
        	$sysInheritClass = $Model->getSysInherits($sysClass);
        	if( $sysInheritClass ){
	        	if( !in_array( $sysPropname, array_keys($Model->toApp[$sysInheritClass]) ) ){
		        	$this->_table['fields'][] = $sysProp;
	        	}
	        	else{
	        		$this->_table['inheritsfields'][] = $sysProp;
	        	}
        	}else{
	        	$this->_table['fields'][] = $sysProp;
        	}
        	
        	/*Extract type of var from comments
        	$docComment = $reflectionProp->getDocComment();
			strtok( strstr($docComment, '@var'), ' ' );
			$propTyp1 = strtolower(trim(strtok(' ')));
			$propTyp2 = strtolower(trim(strtok(' ')));
			*/
			if($toAppFilter){
        		$toAppFilter = str_replace('%appvalue%', $appPropname, $toAppFilter);
        		$toAppFilter = str_replace('%mapped%', '$mapped', $toAppFilter);
				
				$toAppFilterFromSys = str_replace('%sysvalue%', '$row[\''.$sysPropname.'\']', $toAppFilter);
        		$toAppFilterFromApp = str_replace('%sysvalue%', '$row[\''.$appPropname.'\']', $toAppFilter);
			}
			else{
				$toAppFilterFromSys = "\$row['$sysPropname']";
				$toAppFilterFromApp = "\$row['$appPropname']";
			}
			
        	$bindPropname = null;
        	$getter = null; /*Getter as : $mapped->getName(); or $mapped->name;*/
        	$fromSysSetter = null; /*Setter as: $mapped->ownerId = $row['owner_id'];*/
        	$fromAppSetter = null; /*Setter as: $mapped->ownerId = $row['ownerId'];*/
        	
        	if($appPropTyp == 'boolean'){
        		$getter = $this->_getBooleanLoaderGetter($reflexionClass, $sysPropname, $appPropname);
        		$fromSysSetter = $this->_getBooleanLoaderSetter($reflexionClass, $sysPropname, $appPropname, $toAppFilterFromSys);
        		$fromAppSetter = $this->_getBooleanLoaderSetter($reflexionClass, $appPropname, $appPropname, $toAppFilterFromApp);
        		$bindPropname = ':' . $appPropname;
        	}
        	else if( $isCollection ){
        		$getter = $this->_getCollectionLoaderMethod($sysProp);
        	}
        	else if( $isAuto ){
        		$getter = null;
        		$fromSysSetter = '$mapped->' . $appPropname . ' = ' . $toAppFilterFromSys;
        		$fromAppSetter = '$mapped->' . $appPropname . ' = ' . $toAppFilterFromApp;
        		$bindPropname = null;
        	}
        	else{
		        $bindPropname = ':' . $appPropname;
        		/* If property is phantom in app, set it in dao */
				if( $isPhatomAppProp ){
					$fromSysSetter = "\$mapped->$appPropname = $toAppFilterFromSys";
					$getter = "\$mapped->$appPropname";
					$fromAppSetter = "\$mapped->$appPropname = $toAppFilterFromApp";
				}
				else{
					if( $reflexionClass->hasMethod( $setterMethod = 'set' . ucfirst($appPropname) ) ){
						$fromSysSetter = "\$mapped->$setterMethod($toAppFilterFromSys)";
						$fromAppSetter = "\$mapped->$setterMethod($toAppFilterFromApp)";
					}
					else{
						$fromSysSetter = "\$mapped->$appPropname = $toAppFilterFromSys";
						$fromAppSetter = "\$mapped->$appPropname = $toAppFilterFromApp";
					}
					if( $reflexionClass->hasMethod( $getterMethod = 'get' . ucfirst($appPropname) ) ){
						$getter = "\$mapped->$getterMethod()";
					}
					else{
        				$getter = "\$mapped->$appPropname";
					}
				}
        	}
        	
			/*In filter definition, replace sysPropname bind by var name to read*/
			if($toSysFilter){
        		$toSysFilter = str_replace('%appvalue%', $getter, $toSysFilter);
        		$toSysFilter = str_replace('%mapped%', '$mapped', $toSysFilter);
			}
        	else{
				$toSysFilter = $getter;
        	}
        	
        	
			/*populate _stdInsertSelect dao property
			 *  e.g on final result:
			 * 	$this->_stdInsertSelect = 'uid, cid, name, parent';
			 *	$this->_stdInsertValues = ':uid,:cid,:name,:parentUid';
			 *	$this->_stdLoadSelect = 'id,' . $this->_stdInsertSelect;
			 */
        	if($sysPropname && $bindPropname && $sysPropname != 'path'){
				$A_INSERT_SELECT[] = $sysPropname;
				$A_INSERT_VALUES[] = $bindPropname;
        	}
        	if($sysPropname){
				$A_SYS_TO_APP[] = "'$sysPropname'" . '=>' . "'$appPropname'";
				
		        $selectAlias = $sysPropname;
        		if($appPropname){
	        		if($appPropname != $sysPropname){
		        		$selectAlias = $sysPropname . ' AS "' . $appPropname . '"';
	        		}
        		}
	        	$A_LOAD_SELECT[] = $selectAlias;
        	}
        	
			/*
			 * populate the loader.
			 * e.g on final result
			 * $mapped->setUid( \Rbplm\Uuid::format($row['uid']) );
			 * $mapped->setName($row['name']);\n$mapped->setLabel($row['label'], false);
			 * $mapped->owner = \Rbplm\Uuid::format($row['owner']);
			 * $mapped->parentId = $row['parent'];
			 * $mapped->setParent( \Rbplmpg\Dao\Loader::load($this->getConnexion(), $row['parent']) );
			 */
        	if($fromAppSetter){
				$A_LOAD_FROM_APP_GETTER[] = $fromAppSetter;
				$A_LOAD_FROM_SYS_GETTER[] = $fromSysSetter;
        	}
			
			
			/*
			 * populate the update binding set
			 * e.g on final result
			 * uid=:uid,
			 * cid=:cid, 
			 * name=:name, 
			 * parent=:parentUid, 
			 */
        	if( $sysPropname && $bindPropname && $sysPropname != 'path' ){
				$A_UPDATE_SET[] = "$sysPropname = $bindPropname";
        	}
			
			/* populate affectation of values to bind when update
			 * e.g on final result
			 * ':uid'=>$mapped->getUid(),
			 * ':cid'=>$this->_classId,
			 * ':name'=>$mapped->getName(), ':label'=>$mapped->getLabel(),
			 * ':owner'=>$mapped->owner,
			 * ':parentUid'=>$mapped->getParent()->getUid(),
			 */
			if( $getter &&  $bindPropname && $sysPropname != 'path' ){
				$A_UPDATE_BIND_AFFECTATION[] = "'$bindPropname'=>$toSysFilter";
				/*populate affectation of values to bind when insert*/
				$A_INSERT_BIND_AFFECTATION[] = "'$bindPropname'=>$toSysFilter";
			}
        }
        
        /*remove path of the select to insert and update. Path is create by trigger in postgresql*/
        //unset($A_INSERT_VALUES[array_search(':path', $A_INSERT_VALUES)], $A_INSERT_SELECT[array_search('path', $A_INSERT_VALUES)], $A_UPDATE_BIND_AFFECTATION['path'], $A_INSERT_BIND_AFFECTATION['path']);
        
        
        $templateContent = str_replace('%SYS_TO_APP%', implode(', ', $A_SYS_TO_APP), $templateContent);
        $templateContent = str_replace('%INSERT_VALUES%', implode(',' , $A_INSERT_VALUES), $templateContent);
        $templateContent = str_replace('%INSERT_SELECT%', implode(',' , $A_INSERT_SELECT), $templateContent);
        //$templateContent = str_replace('%LOAD_SELECT%', implode(', ', $A_LOAD_SELECT), $templateContent);
        $templateContent = str_replace('%LOAD_FROM_APP_GETTER%', implode(";\n\t\t\t", $A_LOAD_FROM_APP_GETTER) . ";", $templateContent);
        $templateContent = str_replace('%LOAD_FROM_SYS_GETTER%', implode(";\n\t\t\t", $A_LOAD_FROM_SYS_GETTER) . ";", $templateContent);
        $templateContent = str_replace('%UPDATE_SET%', implode(",\n\t\t\t\t\t", $A_UPDATE_SET), $templateContent);
        $templateContent = str_replace('%UPDATE_BIND_AFFECTATION%', implode(",\n\t\t\t\t\t", $A_UPDATE_BIND_AFFECTATION), $templateContent);
        $templateContent = str_replace('%INSERT_BIND_AFFECTATION%', implode(",\n\t\t\t\t\t", $A_INSERT_BIND_AFFECTATION), $templateContent);
        $templateContent = str_replace('%ADDITIONALS_METHODS%', implode("\n\n", $A_ADDITIONALS_METHODS), $templateContent);
		
        /*Generate SQL script*/
        $templateContent = str_replace('%SQL%', $this->_generateSql($Model), $templateContent);
        
        $out .= $templateContent;

        //Put result in file
        if( is_file($output) ){
        	rename($output, $output . uniqid() . '.bck' );
        	echo 'Output file '.$output.' is existing!' . CRLF;
        }
        
        file_put_contents($output, $out);
    }
    
    
    /**
     * @param \Rbplm\Sys\Meta\Model
     */
    private function _generateSql($Model){
    	
    	/*Record class id in class table*/
    	$cid = $this->_classId;
    	$class_name = $this->_className;
    	$table_name = $this->_table['name'];
    	$sqlInsert = 'INSERT INTO classes (id, name, tablename) VALUES ' . "($cid, '$class_name', '$table_name');";
    	
    	$sql .= 'CREATE TABLE ' . $this->_table['name'] . "(\n";
    	if( $this->_table['inherits'] ){
    		$inheritsTableName = $this->_table['inherits'];
    		$inheritsFields = $Model->toApp[$inheritsTableName];
    	}
    	
    	/* Generate columns
    	 */
    	$cols = array();
    	$defaults = array();
    	foreach($this->_table['fields'] as $field){
    		$name = $field['sysName'];
    		$fSql = $name;
    		
    		if( $field['sysType'] == 'string' ){
    			$typ = sprintf( 'varchar(%s)', $field['sysTypeParam'] );
    		}
    		else if( $field['sysType'] == 'char' ){
    			$typ = sprintf( 'char(%s)', $field['sysTypeParam'] );
    		}
    		else if( $field['sysType'] == 'varchar' ){
    			$typ = sprintf( 'varchar(%s)', $field['sysTypeParam'] );
    		}
    		else{
    			$typ = $field['sysType'];
    		}
    		$fSql .= ' ' . $typ;
    		
    		if( is_bool($field['sysDefault']) || !empty($field['sysDefault']) ){
    			if( $field['sysDefault'] === true){
    				$defaultVal = 'true';
    			}
    			else if( $field['sysDefault'] === false){
    				$defaultVal = 'false';
    			}
    			else{
    				$defaultVal = $field['sysDefault'];
    			}
    			$defaults[] = sprintf('ALTER TABLE %s ALTER COLUMN %s SET DEFAULT %s;', $this->_table['name'], $name, $defaultVal);
    		}
    		
    		if( $field['notnull'] ){
    			$fSql .= ' NOT NULL';
    		}
    		
    		if( $field['foreignKey'] ){
    			$fks[] = sprintf('ALTER TABLE %s ADD FOREIGN KEY (%s) REFERENCES %s (%s) ON UPDATE CASCADE ON DELETE SET NULL;', $this->_table['name'], $name, $field['foreignTable'], $field['foreignKey']);
    		}
    		
    		if( $field['isindex'] ){
    			$findexes[] = sprintf('CREATE INDEX INDEX_%s_%s ON %s USING btree (%s);', $this->_table['name'], $name, $this->_table['name'], $name );
    		}
    		
    		if( $field['isuniq'] ){
				$fUniqIndexes[] = sprintf( 'ALTER TABLE %s ADD UNIQUE (%s);', $this->_table['name'], $name );
    			//$findexes[] = sprintf('CREATE UNIQUE INDEX INDEX_%s_%s ON %s USING btree (%s);', $this->_table['name'], $name, $this->_table['name'], $name );
    		}
 	   		$cols[] = $fSql;
    		
    		/*If field is yet define in parent table, ignore his definition but add constraints and foreign keys*/
    		/*
    		if( !$inheritsFields[$name] ){
 		   		$cols[] = $fSql;
    		}
    		*/
    	}
    	
    	foreach($this->_table['inheritsfields'] as $field){
    		$name = trim($field['sysName'], '@');
    		
    		if( is_bool($field['sysDefault']) || !empty($field['sysDefault']) ){
    			if( $field['sysDefault'] === true){
    				$defaultVal = 'true';
    			}
    			else if( $field['sysDefault'] === false){
    				$defaultVal = 'false';
    			}
    			else{
    				$defaultVal = $field['sysDefault'];
    			}
    			$defaults[] = sprintf('ALTER TABLE %s ALTER COLUMN %s SET DEFAULT %s;', $this->_table['name'], $name, $defaultVal);
    		}
    		
    		if( $field['foreignKey'] ){
    			$fks[] = sprintf('ALTER TABLE %s ADD FOREIGN KEY (%s) REFERENCES %s (%s) ON UPDATE CASCADE ON DELETE SET NULL;', $this->_table['name'], $name, $field['foreignTable'], $field['foreignKey']);
    		}
    		
    		if( $field['isindex'] ){
    			$findexes[] = sprintf('CREATE INDEX INDEX_%s_%s ON %s USING btree (%s);', $this->_table['name'], $name, $this->_table['name'], $name );
    		}
    		
    		if( $field['isuniq'] ){
				$fUniqIndexes[] = sprintf( 'ALTER TABLE %s ADD UNIQUE (%s);', $this->_table['name'], $name );
    		}
    	}
    	
    	
    	$sql .= implode(", \n", $cols);
    	
    	$sql .= "\n)";
    	
    	if( $this->_table['inherits'] ){
    		$sql .= ' INHERITS (' . $this->_table['inherits'] . ')';
    	}
    	$sql .= ';';
    	
    	/*Generate foreign key declarations*/
    	if( $fks ){
	    	$sqlFkeys = implode("\n", $fks);
    	}
    	
    	/*Generate index declarations*/
    	if( $findexes ){
	    	$sqlIndexes = implode("\n", $fUniqIndexes);
    	}
    	if( $findexes ){
	    	$sqlIndexes = implode("\n", $findexes);
    	}
    	if($defaults){
	    	$sqlDefaults = implode("\n", $defaults);
    	}
    	
    	/*Triggers*/
		$trigger01 = sprintf('CREATE TRIGGER trig01_%s AFTER INSERT OR UPDATE 
		   ON %s FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();', $this->_table['name'], $this->_table['name']);
		$trigger02 = sprintf('CREATE TRIGGER trig02_%s AFTER DELETE 
		   ON %s FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();', $this->_table['name'], $this->_table['name']);
		
		/*Generate common constraints*/
		$sqlPkey = sprintf( 'ALTER TABLE %s ADD PRIMARY KEY (id);', $this->_table['name'] );
		
		if( $this->_sqls['views'] ){
			$views = implode("\n", $this->_sqls['views']);
		}
		
    	$sql = '/** SQL_SCRIPT>>' . "\n" . $sql . "\n" . '<<*/' . "\n\n";

    	$sql .= '/** SQL_INSERT>>' . "\n" . $sqlInsert . "\n" . '<<*/' . "\n\n";
    	
    	$sql .= '/** SQL_ALTER>>' . "\n" . $sqlDefaults . "\n" . $sqlPkey . "\n" . $sqlIndexes . "\n" . '<<*/' . "\n\n";
		
    	$sql .= '/** SQL_FKEY>>' . "\n" . $sqlFkeys . "\n" . '<<*/' . "\n\n";
    	
    	$sql .= '/** SQL_TRIGGER>>' . "\n" . $trigger01 . "\n" . $trigger02 . "\n" . '<<*/' . "\n\n";
    	
    	$sql .= '/** SQL_VIEW>>' . "\n" . $views . "\n" . '<<*/' . "\n\n";

    	return $sql;
    	
    } //End of method
    
    
    /**
     * 
     * @param \\ReflectionClass	$reflexionClass
     * @param string	$sysPropname
     * @param string	$appPropname
     * @param string	$toAppValue
     * @return string
     */
    private function _getBooleanLoaderSetter($reflexionClass, $sysPropname, $appPropname, $toAppValue = false){
        if( !strstr( $appPropname, 'is' ) ){
	        $setterMethod = 'is' . ucfirst( $appPropname );
        }
        else{
	        $setterMethod = $appPropname;
        }
		if( $reflexionClass->hasMethod($setterMethod) ){
        	$setter = "\$mapped->$setterMethod($toAppValue)";
		}
		else{
        	$setter = "\$mapped->$appPropname = $toAppValue";
		}
		return $setter;
    }
    
    
    /*************************GETTERS*****************************************************************/
    
    /**
     * @param \\ReflectionClass	$reflexionClass
     * @param string	$sysPropname
     * @param string	$appPropname
     * @return string
     */
    private function _getBooleanLoaderGetter($reflexionClass, $sysPropname, $appPropname){
        if( !strstr( $appPropname, 'is' ) ){
	        $getterMethod = 'is' . ucfirst( $appPropname );
        }
        else{
	        $getterMethod = $appPropname;
        }
		if( $reflexionClass->hasMethod($getterMethod) ){
        	$getter = "(integer) \$mapped->$getterMethod()";
		}
		else{
        	$getter = "(integer) \$mapped->$appPropname";
		}
		return $getter;
    }
    
    
    
    
    
    
    /*************************COLLECTION & LIST LOADER*****************************************************************/
    
    
    /** 
     * Creater collection through link table.
     * Example of output:
     * @code
	 *		public function getActivitiesList($mapped)
	 *		{
	 *			$List = new \Rbplmpg\Dao\DaoList( array('table'=>'view_wf_activity_links') );
	 *			$List->setConnexion( $this->getConnexion() );
	 *			$uid = $mapped->getUid();
	 *			$List->load("lparent='$uid'");
	 *			return $List;
	 *		}
     * @endcode
     */
    private function _getLinkCollectionLoaderMethod( $sysProp ){
    	
    	$sysPropname = $sysProp['sysName'];
    	$sysPropTyp = $sysProp['sysType'];
    	$appPropname = $sysProp['appName'];
    	$appPropTyp = $sysProp['appType'];
    	$propFkey = $sysProp['foreignKey'];
    	$propFtable = $sysProp['foreignTable'];
    	$linkTable = $sysProp['linkTable'];
    	
    	if(!$propFtable || !$propFkey){
    		trigger_error($sysPropname . ': This property is a Collection. You must set the foreignKey and foreignTable properties in the model defintion');
    	}
    	
    	/*Base method definition*/
        $method = new \Zend_Code\Generator_Php_Method();
        $publicPropName = trim($appPropname, '_');
        if( !$methodName ){
        	$methodName = 'get' . ucfirst( $publicPropName ) . 'List';
        }
        $method->setName($methodName);
        
        $parameter = new \Zend_Code\Generator_Php_Parameter();
        $parameter->setName('mapped');
        $method->setParameter($parameter);
        
        /*Tags @param and @return*/
        $docBlock = new \Zend_Code\Generator_Php_Docblock();
        $docBlock->setShortDescription('Getter for ' . $publicPropName . '. Return a list.');
        $docBlock->setTag( new \Zend_Code\Generator_Php_Docblock_Tag_Param(array('datatype'  => '\Rbplm\Dao\MappedInterface') ) );
        $docBlock->setTag( new \Zend_Code\Generator_Php_Docblock_Tag_Return(array('datatype'  => '\Rbplmpg\Dao\DaoList') ) );
        $method->setDocblock($docBlock);
        
        /*view to query. Must be generated too.*/
        $joinTable = $propFtable;
        $viewName = 'view_'. $joinTable . '_links';
        
        $this->_generateCollectionView($viewName, $joinTable, $linkTable);
        
		$body  = '$List = new \Rbplmpg\Dao\DaoList( array(\'table\'=>\'%s\') );'  . "\n"; //%s = $viewName
		$body .= '$List->setConnexion( $this->getConnexion() );'  . "\n";
		$body .= '$uid = $mapped->getUid();'  . "\n";
		$body .= '$List->load("lparent=\'$uid\'");'  . "\n";
		$body .= 'return $List;'  . "\n";
		$body  = sprintf($body, $viewName);
		
		$method->setBody($body);
		
		return $method->generate();
    }

    /**
     * Create collection on foreign table.
     * Example of output:
     * 
     * @code
	 *		public function getComments($mapped)
	 *		{
	 *			$List = new \Rbplmpg\Dao\DaoList( array('table'=>'comments') );
	 *			$List->setConnexion( $this->getConnexion() );
	 *			$uid = $mapped->getUid();
	 *			$List->load("anyobject_id='$uid'");
	 *			return $List;
	 *		}
	 *
	 *
     * @endcode
     */
    private function _getCollectionLoaderMethod( $sysProp )
    {
    	
    	$sysPropname = $sysProp['sysName'];
    	$sysPropTyp = $sysProp['sysType'];
    	$appPropname = $sysProp['appName'];
    	$appPropTyp = $sysProp['appType'];
    	
    	$foreignKey = $sysProp['foreignKey']; //i.e: uid | null
    	$foreignTable = $sysProp['foreignTable']; //i.e: wf_process | null
    	$linkTable = $sysProp['linkTable']; //i.e: anyobject_links | comments
    	$linkKey = $sysProp['linkKey']; //i.e: lparent | anyobject_id
    	
    	if( !$linkTable || !$linkKey ){
    		trigger_error( $sysPropname . ': This property is a Collection. You must set the linkKey and linkTable properties in the model defintion');
    	}
    	
    	$appGetter = 'get' . ucfirst($appPropname);
    	
    	/*Base method definition*/
        $method = new \Zend_Code\Generator_Php_Method();
        $publicPropName = trim($appPropname, '_');
        if( !$methodName ){
        	$methodName = 'get' . ucfirst( $publicPropName );
        }
        $method->setName($methodName);
        
        $parameter = new \Zend_Code\Generator_Php_Parameter();
        $parameter->setName('mapped');
        $method->setParameter($parameter);
        
        $parameter = new \Zend_Code\Generator_Php_Parameter();
        $parameter->setName('loadInCollection');
        $parameter->setDefaultValue(false);
        $method->setParameter($parameter);
        
        
        /*Tags @param and @return*/
        $docBlock = new \Zend_Code\Generator_Php_Docblock();
        $docBlock->setShortDescription('Getter for ' . $publicPropName . '. Return a list.');
        $docBlock->setTag( new \Zend_Code\Generator_Php_Docblock_Tag_Param(array('datatype'  => '\Rbplm\Dao\MappedInterface') ) );
        $docBlock->setTag( new \Zend_Code\Generator_Php_Docblock_Tag_Param(array('datatype'  => 'boolean') ) );
        $docBlock->setTag( new \Zend_Code\Generator_Php_Docblock_Tag_Return(array('datatype'  => '\Rbplmpg\Dao\DaoList') ) );
        $method->setDocblock($docBlock);
        
        if( $foreignKey && $foreignTable ){
	        /*view to query. Must be generated too.*/
	        $viewName = 'view_'. $foreignTable . '_links';
        	$this->_generateCollectionView( $viewName, $foreignTable, $foreignKey, $linkTable, $linkKey );
        }
        else{
        	$viewName = $linkTable;
        }
        
		$body  = '$List = new \Rbplmpg\Dao\DaoList( array(\'table\'=>\'%s\') );'  . "\n"; //%s = $viewName
		$body .= '$List->setConnexion( $this->getConnexion() );'  . "\n";
		$body .= '$uid = $mapped->getUid();'  . "\n";
		
		$body .= 'if($loadInCollection){'  . "\n";
		$body .= '	$List->loadInCollection($mapped->%s(), "lparent=\'$uid\'");'  . "\n"; //%s = $appGetter
		$body .= '}' . "\n";
		$body .= 'else{' . "\n";
		$body .= '	$List->load("' . $linkKey . '=\'$uid\'");'  . "\n";
		$body .= '}' . "\n";
		$body .= 'return $List;'  . "\n";
		$body  = sprintf($body, $viewName, $appGetter);
		
		$method->setBody($body);
		
		return $method->generate();
    }
    
    /**
     * Generate view for collection list
     */
    private function _generateCollectionView($viewName, $foreignTable, $foreignKey, $linkTable=null, $linkKey=null){
        if( !$linkTable ){
	        $linkTable = 'anyobject_links';
        }
        $sql  = 'CREATE OR REPLACE VIEW %s AS' . "\n"; //%s = $viewName
        $sql .= '	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*' . "\n";
        $sql .= '	FROM %s AS r' . "\n"; //%s = $foreignTable
        $sql .= '	JOIN %s AS l ON r.uid = l.lchild;'; //%s = $linkTable
		$sql  = sprintf($sql, $viewName, $foreignTable, $linkTable);
		
		$this->_sqls['views'][] = $sql;
    }
    
    
} //End of class


