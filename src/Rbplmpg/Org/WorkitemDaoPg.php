<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Org;

/**
 * $Id: RootDao.php 480 2011-06-21 18:19:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Org/RootDao.php $
 * $LastChangedDate: 2011-06-21 20:19:25 +0200 (Tue, 21 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 480 $
 */

/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (22, '\Rbplm\Org\Workitem', 'org_ou');
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Postgresql Dao class for \Rbplm\Org\Workitem.
 * 
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 */
class WorkitemDaoPg extends \Rbplm\Org\UnitDaoPg
{
	/**
	 *
	 * @var integer
	 */
	protected static $classId = 22;
}
