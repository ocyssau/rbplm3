<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Org;

use Rbplmpg\Dao as Dao;
use Rbplm\Sys\Date as DateTime;


/** SQL_SCRIPT>>
 CREATE TABLE org_ou(
 description varchar(255),
 created date,
 updated date,
 update_by uuid,
 create_by uuid NOT NULL
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (20, '\Rbplm\Org\Unit', 'org_ou');
INSERT INTO org_ou (id, uid, cid, name, label, description, created, create_by, owner)
                VALUES (2, '01234567-0123-0123-0123-0123456789ab', 20, 'RanchbePlm', 'RanchbePlm', 'Root node', '05-05-2014', '99999999-9999-9999-9999-00000000abcd', '99999999-9999-9999-9999-00000000abcd');
INSERT INTO org_ou (id, uid, cid, name, label, description, created, create_by, owner, parent)
                VALUES (3, '01234567-0123-0123-0123-0123456789ac', 20, 'Users', 'Users', 'Users OU', '05-05-2014', '99999999-9999-9999-9999-00000000abcd', '99999999-9999-9999-9999-00000000abcd', '01234567-0123-0123-0123-0123456789ab');
<<*/

/** SQL_ALTER>>
 ALTER TABLE org_ou ADD PRIMARY KEY (id);
 ALTER TABLE org_ou ADD UNIQUE (uid);
 ALTER TABLE org_ou ADD UNIQUE (path);
 ALTER TABLE org_ou ALTER COLUMN cid SET DEFAULT 20;
 CREATE INDEX INDEX_org_ou_owner ON org_ou USING btree (owner);
 CREATE INDEX INDEX_org_ou_uid ON org_ou USING btree (uid);
 CREATE INDEX INDEX_org_ou_name ON org_ou USING btree (name);
 CREATE INDEX INDEX_org_ou_label ON org_ou USING btree (label);
 CREATE INDEX INDEX_org_ou_path ON org_ou USING btree (path);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE org_ou ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_org_ou AFTER DELETE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Org\UnitTest
 *
 */
class UnitDaoPg extends Dao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'org_ou';

	/**
	 *
	 * @var integer
	 */
	protected static $classId = 20;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
	    'description'=>'description',
	    'created'=>'created',
	    'updated'=>'updated',
	    'owner'=>'ownerId',
	    'update_by'=>'updateById',
	    'create_by'=>'createById'
	    );

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbplm\Org\Unit	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->ownerId = $row['ownerId'];
			$mapped->createById = $row['createById'];
			$mapped->updateById = $row['updateById'];
			$mapped->description = $row['description'];
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
		}
		else{
			$mapped->ownerId = $row['owner'];
			$mapped->createById = $row['create_by'];
			$mapped->updateById = $row['update_by'];
			$mapped->description = $row['description'];
			$mapped->created = (new DateTime($row['created']));//->setTimeStamp($row['created']);
			$mapped->updated = (new DateTime($row['updated']));//->setTimeStamp($row['updated']);
		}
	} //End of function

} //End of class

