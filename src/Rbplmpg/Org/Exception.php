<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Org;

/**
 * @brief \Exception for Rbplm.
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class Exception extends \Rbplm\Sys\Exception
{
}
