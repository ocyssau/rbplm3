<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Org;

use Rbplmpg\Dao as Dao;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
CREATE TABLE org_project(
) INHERITS (org_ou);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (21, '\Rbplm\Org\Project', 'org_project');
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Postgresql Dao class for \Rbplm\Org\Project.
 * 
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 * 
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class ProjectDaoPg extends \Rbplm\Org\UnitDaoPg
{
	/**
	 * 
	 * @var string
	 */
	public static $table = 'org_ou';
	
	/**
	 *
	 * @var integer
	 */
	protected static $classId = 21;
}
