<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Org;


/**
 * $Id: RootDao.php 480 2011-06-21 18:19:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Org/RootDao.php $
 * $LastChangedDate: 2011-06-21 20:19:25 +0200 (Tue, 21 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 480 $
 */

/** SQL_SCRIPT>>
CREATE TABLE org_mockup(
) INHERITS (org_ou);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (23, '\Rbplm\Org\Mockup', 'org_mockup');
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
<<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
<<*/

/** SQL_DROP>>
<<*/


/**
 * @brief Postgresql Dao class for \Rbplm\Org\Project.
 * 
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 * 
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class MockupDaoPg extends \Rbplm\Org\UnitDaoPg
{
	/**
	 * 
	 * @var string
	 */
	public static $table = 'org_mockup';
	
	/**
	 *
	 * @var integer
	 */
	protected static $classId = 23;
}
