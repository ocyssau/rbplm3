<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Ged;

use Rbplmpg\Dao;
use Rbplm\Ged\Exception as Exception;
use Rbplm\Sys\Error as Error;



/** SQL_SCRIPT>>
CREATE TABLE ged_doctype(
	description text, 
	file_extension varchar(16), 
	file_type varchar(16), 
	icon varchar(255), 
	script_post_store varchar(255), 
	script_pre_store varchar(255), 
	script_post_update varchar(255), 
	script_pre_update varchar(255), 
	recognition_regexp text, 
	visu_file_extension varchar(16), 
	may_be_composite boolean
) INHERITS (anyobject);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (61, '\Rbplm\Ged\Doctype', 'ged_doctype');
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_doctype ADD PRIMARY KEY (id);
ALTER TABLE ged_doctype ADD UNIQUE (uid);
ALTER TABLE ged_doctype ADD UNIQUE (path);
ALTER TABLE ged_doctype ALTER COLUMN cid SET DEFAULT 61;
CREATE INDEX INDEX_ged_doctype_owner ON ged_doctype USING btree (owner);
CREATE INDEX INDEX_ged_doctype_uid ON ged_doctype USING btree (uid);
CREATE INDEX INDEX_ged_doctype_name ON ged_doctype USING btree (name);
CREATE INDEX INDEX_ged_doctype_label ON ged_doctype USING btree (label);
CREATE INDEX INDEX_ged_doctype_parent ON ged_doctype USING btree (parent);
CREATE INDEX INDEX_ged_doctype_cid ON ged_doctype USING btree (cid);
CREATE INDEX INDEX_ged_doctype_path ON ged_doctype USING btree (path);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_doctype ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_doctype AFTER INSERT OR UPDATE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_doctype AFTER DELETE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_ged_category_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_category AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Ged\Doctype
 * 
 * See the examples: Rbplm/Ged/DoctypeTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\DoctypeTest
 *
 */
class DoctypeDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'ged_doctype';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 110;
	
	public static $sysToApp = array(
	    'id'=>'pkey', 
	    'uid'=>'uid', 
	    'name'=>'name', 
	    'label'=>'label', 
	    'parent'=>'parentId', 
	    'cid'=>'classId', 
	    'path'=>'path', 
	    'description'=>'description', 
	    'owner'=>'ownerId', 
	    'file_extension'=>'fileExtension', 
	    'file_type'=>'fileType', 
	    'icon'=>'icon', 
	    'script_post_store'=>'scriptPostStore', 
	    'script_pre_store'=>'scriptPreStore', 
	    'script_post_update'=>'scriptPostUpdate', 
	    'script_pre_update'=>'scriptPreUpdate', 
	    'recognition_regexp'=>'recognitionRegexp', 
	    'visu_file_extension'=>'visuFileExtension', 
	    'may_be_composite'=>'mayBeComposite'
	    );
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Ged\Doctype	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		\Rbplmpg\Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->description = $row['description'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->fileExtension = $row['fileExtension'];
			$mapped->fileType = $row['fileType'];
			$mapped->icon = $row['icon'];
			$mapped->scriptPostStore = $row['scriptPostStore'];
			$mapped->scriptPreStore = $row['scriptPreStore'];
			$mapped->scriptPostUpdate = $row['scriptPostUpdate'];
			$mapped->scriptPreUpdate = $row['scriptPreUpdate'];
			$mapped->recognitionRegexp = $row['recognitionRegexp'];
			$mapped->visuFileExtension = $row['visuFileExtension'];
			$mapped->isMayBeComposite($row['mayBeComposite']);
		}
		else{
			$mapped->description = $row['description'];
			$mapped->ownerId = $row['owner'];
			$mapped->fileExtension = $row['file_extension'];
			$mapped->fileType = $row['file_type'];
			$mapped->icon = $row['icon'];
			$mapped->scriptPostStore = $row['script_post_store'];
			$mapped->scriptPreStore = $row['script_pre_store'];
			$mapped->scriptPostUpdate = $row['script_post_update'];
			$mapped->scriptPreUpdate = $row['script_pre_update'];
			$mapped->recognitionRegexp = $row['recognition_regexp'];
			$mapped->visuFileExtension = $row['visu_file_extension'];
			$mapped->isMayBeComposite($row['may_be_composite']);
		}
	} //End of function
	
    /**
     * Getter for categories. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @param boolean
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getCategories($mapped, $loadInCollection = false)
    {
        $List = new \Rbplmpg\Dao\DaoList( array('table'=>'view_ged_category_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        if($loadInCollection){
        	$List->loadInCollection($mapped->getCategories(), "lparent='$uid'");
        }
        else{
        	$List->load("lparent='$uid'");
        }
        return $List;
    }

    
	/**
	 * @param 	\Rbplm\Ged\Doctype	$mapped
	 * @param 	\Rbplm\Ged\Document 	$document
	 * @param 	string 				$file_extension
	 * @param 	string 				$file_type
	 */
	public function loadFromDocument( \Rbplm\Ged\Doctype $mapped, \Rbplm\Ged\Document $document, $fileExtension=null, $fileType=null ){

		$bind = array();
		$filters = array();

		if(!is_null($fileExtension)){
			$filters[] = 'file_extension = :fileExtension';
			$bind[':fileExtension'] = $fileExtension;
		}

		if(!is_null($fileType)){
			$filters[] = 'file_type = :fileType';
			$bind[':fileType'] = $fileType;
		}


		$filter = implode(' AND ', $filters);
		$sql = "SELECT * FROM $this->_table WHERE $filter";
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		if( $stmt->count() == 0){
			throw new Exception('NONE_VALID_DOCTYPE', Error::WARNING, $document->getNumber() );
		}

		//For each doctype test if current Document corresponding to regex
		while( $row = $stmt->fetch() ){
			if( !empty($row['recognition_regexp']) ){
				if(preg_match('/'.$row['recognition_regexp'].'/', $document->getNumber() ) ){
					$doctypeProps = $row; //assign doctype and exit loop
					break;
				}
			}else{
				$genericDoctype = $row; //if recognition_regexp field is empty, its a generic doctype
			}
		}

		//If no match doctypes, then assign the generic doctype, else return false
		if(!$doctypeProps && $genericDoctype){
			$doctypeProps = $genericDoctype;
		}

		if( !$doctypeProps ){
			throw new Exception('NONE_VALID_DOCTYPE', Error::WARNING, $document->getNumber() );
		}

		$this->loadFromArray($mapped, $doctypeProps);
	} //End of method
    
    
    
} //End of class

