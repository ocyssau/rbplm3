<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Ged\Docfile;

/** SQL_SCRIPT>>
CREATE TABLE ged_docfile_role(
	name varchar(255), 
	file_uid uuid NOT NULL, 
	role_id integer, 
	description text
);

<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (52, '\Rbplm\Ged\Docfile_Role', 'ged_docfile_role');
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_docfile_role ADD PRIMARY KEY (role_id);
CREATE INDEX INDEX_ged_docfile_role_name ON ged_docfile_role USING btree (name);
CREATE INDEX INDEX_ged_docfile_role_file_uid ON ged_docfile_role USING btree (file_uid);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_docfile_role ADD FOREIGN KEY (file_uid) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
<<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
<<*/

/**
 * @brief Dao class for \Rbplm\Ged\Docfile_Role
 * 
 * See the examples: Rbplm/Ged/Docfile/RoleTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\Docfile_RoleTest
 *
 */
class RoleDaoPg extends \Rbplmpg\Dao\Pg
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'ged_docfile_role';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 52;
	
	
	public static $sysToApp = array('name'=>'name', 'file_uid'=>'fileId', 'role_id'=>'roleId', 'description'=>'description');
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Ged\Docfile_Role	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->setName($row['name']);
			$mapped->setFileId($row['fileId']);
			$mapped->setRoleId($row['roleId']);
			$mapped->setDescription($row['description']);
		}
		else{
			$mapped->setName($row['name']);
			$mapped->setFileId($row['file_uid']);
			$mapped->setRoleId($row['role_id']);
			$mapped->setDescription($row['description']);
		}
	} //End of function
	
	
	/**
	 * @param \Rbplm\Ged\Docfile_Role   $mapped
	 * @return void
	 * @throws \Rbplm\Ged\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$sql = "INSERT INTO $table (name,file_uid,role_id,description) VALUES (:name,:fileId,:roleId,:description)";
		$bind = array(
					':name'=>$mapped->getName(),
					':fileId'=>$mapped->getFileId(),
					':roleId'=>$mapped->getRoleId(),
					':description'=>$mapped->getDescription()
		);
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
	}
	

} //End of class

