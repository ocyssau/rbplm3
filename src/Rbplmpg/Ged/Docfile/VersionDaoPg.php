<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Ged\Docfile;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
CREATE TABLE ged_docfile_version(
	number varchar NOT NULL, 
	description text, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	locked integer, 
	owner uuid NOT NULL, 
	update_by uuid, 
	create_by uuid NOT NULL, 
	lock_by uuid, 
	base uuid NOT NULL, 
	from_anyobject uuid, 
	data uuid, 
	access_code integer NOT NULL, 
	iteration integer NOT NULL, 
	version integer NOT NULL, 
	life_stage varchar
) INHERITS (anyobject);


CREATE TABLE ged_docfile_data_links(
	docfile uuid NOT NULL, 
	record uuid NOT NULL, 
	iteration integer NOT NULL DEFAULT 1
);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (51, '\Rbplm\Ged\Docfile_Version', 'ged_docfile_version');
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_docfile_version ADD PRIMARY KEY (id);
ALTER TABLE ged_docfile_version ADD UNIQUE (number);
ALTER TABLE ged_docfile_version ADD UNIQUE (uid);
ALTER TABLE ged_docfile_version ADD UNIQUE (path);

ALTER TABLE ged_docfile_version ALTER COLUMN iteration SET DEFAULT 1;
ALTER TABLE ged_docfile_version ALTER COLUMN version SET DEFAULT 1;
ALTER TABLE ged_docfile_version ALTER COLUMN life_stage SET DEFAULT 'init';
ALTER TABLE ged_docfile_version ALTER COLUMN cid SET DEFAULT 51;

CREATE INDEX INDEX_ged_docfile_version_number ON ged_docfile_version USING btree (number);
CREATE INDEX INDEX_ged_docfile_version_owner ON ged_docfile_version USING btree (owner);
CREATE INDEX INDEX_ged_docfile_version_update_by ON ged_docfile_version USING btree (update_by);
CREATE INDEX INDEX_ged_docfile_version_create_by ON ged_docfile_version USING btree (create_by);
CREATE INDEX INDEX_ged_docfile_version_lock_by ON ged_docfile_version USING btree (lock_by);
CREATE INDEX INDEX_ged_docfile_version_base ON ged_docfile_version USING btree (base);
CREATE INDEX INDEX_ged_docfile_version_from_anyobject ON ged_docfile_version USING btree (from_anyobject);
CREATE INDEX INDEX_ged_docfile_version_data ON ged_docfile_version USING btree (data);
CREATE INDEX INDEX_ged_docfile_version_access_code ON ged_docfile_version USING btree (access_code);
CREATE INDEX INDEX_ged_docfile_version_iteration ON ged_docfile_version USING btree (iteration);
CREATE INDEX INDEX_ged_docfile_version_version ON ged_docfile_version USING btree (version);
CREATE INDEX INDEX_ged_docfile_version_life_stage ON ged_docfile_version USING btree (life_stage);
CREATE INDEX INDEX_ged_docfile_version_uid ON ged_docfile_version USING btree (uid);
CREATE INDEX INDEX_ged_docfile_version_name ON ged_docfile_version USING btree (name);
CREATE INDEX INDEX_ged_docfile_version_label ON ged_docfile_version USING btree (label);
CREATE INDEX INDEX_ged_docfile_version_parent ON ged_docfile_version USING btree (parent);
CREATE INDEX INDEX_ged_docfile_version_cid ON ged_docfile_version USING btree (cid);
CREATE INDEX INDEX_ged_docfile_version_path ON ged_docfile_version USING btree (path);
--
-- For ged_docfile_data_links
--
ALTER TABLE ged_docfile_data_links ADD PRIMARY KEY (docfile, record, iteration);
-- CREATE INDEX INDEX_ged_docfile_data_links_docfile ON ged_docfile_data_links USING btree (docfile);
-- CREATE INDEX INDEX_ged_docfile_data_links_record ON ged_docfile_data_links USING btree (record);
-- CREATE INDEX INDEX_ged_docfile_data_links_iteration ON ged_docfile_data_links USING btree (iteration);

<<*/

/** SQL_FKEY>>
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (lock_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (base) REFERENCES ged_docfile (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (from_anyobject) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile_version ADD FOREIGN KEY (data) REFERENCES vault_record (uid) ON UPDATE CASCADE ON DELETE SET NULL;
--
-- For ged_docfile_data_links
--
ALTER TABLE ged_docfile_data_links ADD FOREIGN KEY (docfile) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ged_docfile_data_links ADD FOREIGN KEY (record) REFERENCES vault_record (uid) ON UPDATE CASCADE ON DELETE CASCADE;
<<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_docfile_version AFTER INSERT OR UPDATE 
		   ON ged_docfile_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_docfile_version AFTER DELETE 
		   ON ged_docfile_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_ged_docfile_role_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_docfile_role AS r
	JOIN anyobject_links AS l ON r.file_uid = l.lchild;
	
-- CREATE OR REPLACE VIEW view_vault_record_links AS
-- 	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
-- 	FROM vault_record AS r
-- 	JOIN anyobject_links AS l ON r.uid = l.lchild;

CREATE OR REPLACE VIEW view_vault_record_links AS
	SELECT l.docfile AS docfile, l.iteration AS iteration, r.*
	FROM vault_record AS r
	JOIN ged_docfile_data_links AS l ON r.uid = l.record;

<<*/

/**
 * @brief Dao class for \Rbplm\Ged\Docfile_Version
 * 
 * See the examples: Rbplm/Ged/Docfile/VersionTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\Docfile_VersionTest
 *
 */
class VersionDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'ged_docfile_version';
	
	/**
	 * Name of table containing link to vault data records.
	 * 
	 * @var string
	 */
	protected $_dataLinkTable = 'ged_docfile_data_links';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 51;
	
	
	public static $sysToApp = array('id'=>'pkey', 'uid'=>'uid', 'name'=>'name', 'parent'=>'parentId', 'cid'=>'classId', 'path'=>'path', 'number'=>'number', 'description'=>'description', 'created'=>'created', 'updated'=>'updated', 'closed'=>'closed', 'locked'=>'locked', 'owner'=>'ownerId', 'update_by'=>'updateById', 'create_by'=>'createById', 'lock_by'=>'checkoutById', 'base'=>'baseId', 'from_anyobject'=>'fromId', 'data'=>'dataId', 'access_code'=>'accessCode', 'iteration'=>'iteration', 'version'=>'version', 'life_stage'=>'lifeStage');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Ged\Docfile_Version	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return VersionDaoPg
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		
		$mapped->setNumber($row['number']);
		$mapped->description = $row['description'];
		$mapped->iteration = $row['iteration'];
		$mapped->version = $row['version'];
		
		if($fromApp){
			$mapped->ownerId = $row['ownerId'];
			$mapped->updateById = $row['updateById'];
			$mapped->createById = $row['createById'];
			$mapped->checkoutById = $row['checkoutById'];
			$mapped->baseId = $row['baseId'];
			$mapped->fromId = $row['fromId'];
			$mapped->dataId = $row['dataId'];
			$mapped->accessCode = $row['accessCode'];
			$mapped->iteration = $row['iteration'];
			$mapped->version = $row['version'];
			$mapped->setLifeStage($row['lifeStage']);
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
			$mapped->closed = $row['closed'];
			$mapped->locked = $row['locked'];
		}
		else{
			$mapped->ownerId = $row['owner'];
			$mapped->updateById = $row['update_by'];
			$mapped->createById = $row['create_by'];
			$mapped->checkoutById = $row['lock_by'];
			$mapped->baseId = $row['base'];
			$mapped->fromId = $row['from_anyobject'];
			$mapped->dataId = $row['data'];
			$mapped->accessCode = $row['access_code'];
			$mapped->setLifeStage($row['life_stage']);
			$mapped->created = new Rbplm\Sys\Date($row['created']);
			$mapped->updated = new Rbplm\Sys\Date($row['updated']);
			$mapped->closed = new Rbplm\Sys\Date($row['closed']);
			$mapped->locked = new Rbplm\Sys\Date($row['locked']);
		}
		return $this;
	} //End of function
	
	/**
	 */
	public function loadFromNumber($mapped, $number, $options = array() )
	{
		$filter = "number='$number'";
		return $this->load($mapped, $filter, $options );
	} //End of function
	
	/**
	 * @param \Rbplm\Ged\Docfile_Version			$mapped
	 * @return VersionDaoPg
	 * 
	 * @todo suppress and replace by instanciate in getter of mapped
	 */
	public function loadInternalLinks($mapped){
		$mapped->setBase( \Rbplm\Dao\Loader::load( $mapped->baseId, '\Rbplm\Ged\Docfile' ) );
		$mapped->setFrom( \Rbplm\Dao\Loader::load( $mapped->fromId, '\Rbplm\Ged\Document\Version' ) );
		$mapped->setCreateBy( \Rbplm\Dao\Loader::load( $mapped->createById, '\Rbplm\People\User' ) );
		$mapped->setlockBy( \Rbplm\Dao\Loader::load( $mapped->lockById, '\Rbplm\People\User' ) );
		$mapped->setOwner( \Rbplm\Dao\Loader::load( $mapped->ownerId, '\Rbplm\People\User' ) );
		$mapped->setUpdateBy( \Rbplm\Dao\Loader::load( $mapped->updateById, '\Rbplm\People\User' ) );
		return $this;
	}
		
	
    /**
     * Getter for datas. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @param boolean
     * @return \Rbplmpg\Dao\DaoList
     */
	/*
    public function getDatas($mapped, $loadInCollection = false)
    {
        $List = new Dao\DaoList( array('table'=>'view_vault_record_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        if($loadInCollection){
        	$List->loadInCollection($mapped->getLinks(), "lparent='$uid'");
        }
        else{
        	$List->load("lparent='$uid'");
        }
        return $List;
    }
    */

    /**
     * Load Vault record links in $mapped Docfile_Version.
     * 
     * To select iterations 1 to 4:
     * 	$Filter = 'iteration < 5'
     *
     * @param \Rbplm\Dao\MappedInterface
     * @param \Rbplmpg\Dao\Filter|string
     * @return VersionDaoPg
     */
    public function _load_vault_record( $mapped, $filter=null, $options=array() )
    {
    	/*
        $List = new Dao\DaoList( array('table'=>'view_vault_record_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getLinks(), "lparent='$uid'");
        */
    	
        $class = '\Rbplm\Vault\Record';
		$table = Dao\ClassDao::singleton()->getTable($class);
		$classId = Dao\ClassDao::singleton()->toId($class);
		$relatedUid = $mapped->getUid();
		$select = 'l.iteration AS iteration, r.*';
		
		if(is_a($filter, '\Rbplm\Dao\Filter\FilterInterface')){
			$filter = $filter->__toString();
		}
		
		/*
		SELECT l.iteration AS iteration, r.*
		FROM vault_record AS r
		JOIN ged_docfile_data_links AS l ON r.uid = l.record;
		*/
		
		$sql = "SELECT $select FROM $table AS r JOIN $this->_dataLinkTable AS l ON r.uid = l.record";
		if($filter){
			$sql .= " WHERE $filter AND docfile='$relatedUid'";
		}
		else{
			$sql .= " WHERE docfile='$relatedUid'";
		}
		$sql .= ' ORDER BY iteration ASC';
		$sql .= ';';
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute();
		
		while ($row = $stmt->fetch()) {
			$uid = $row['uid'];
			$LinkObj = \Rbplm\Dao\Registry::singleton()->get($uid);
			if(!$LinkObj){
				$LinkObj = new $class();
				\Rbplm\Dao\Factory::getDao($class)->loadFromArray($LinkObj, $row, false);
			}
			$mapped->getLinks()->add($LinkObj, $row['iteration']);
			\Rbplm\Dao\Registry::singleton()->add($LinkObj);
		}
		return $this;
    }
    
    /**
     * Load Roles in $mapped Docfile_Version
     * 
     * To select iterations 1 to 4:
     * 	$Filter = 'iteration < 5'
     *
     * @param \Rbplm\Dao\MappedInterface
     * @param \Rbplmpg\Dao\Filter|string
     * @return VersionDaoPg
     */
    public function _load_docfile_role( $mapped, $filter=null, $options=array() )
    {
        $List = new Dao\DaoList( array('table'=>'view_ged_docfile_role_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getLinks(), "lparent='$uid'");
		return $this;
    }
    
	/**
	 *
	 * @param \Rbplm\Ged\Docfile_Version	$mapped
	 * @param \Rbplm\Model\Collection	$linkCollection
	 * @return void
	 * @throws \Rbplm\Ged\Exception
	 *
	 */
	protected function _savelinks_Record($mapped, $linkCollection)
	{
		if( !$this->_slVRstmt ){
			$table = $this->_dataLinkTable;
			$sql = "DELETE FROM $table WHERE docfile=:docfileUid";
			$dlVRstmt = $this->getConnexion()->prepare($sql);
			$dlVRstmt->execute( array(':docfileUid'=>$mapped->getUid() ) );
			
			$sql = "INSERT INTO $table (docfile, record, iteration)
								 VALUES (:docfileUid, :recordUid,:iteration)";
			$this->_slVRstmt = $this->getConnexion()->prepare($sql);
		}
		
		$bind = array(
				':docfileUid'=>$mapped->getUid(),
				':recordUid'=>$linkCollection->current()->getUid(),
				':iteration'=>$linkCollection->getInfo(),
		);
		
		$this->_slVRstmt->execute($bind);
	}
	
	/**
	 * @param \Rbplm\Ged\Docfile_Version	$mapped
	 * @param \Rbplm\Model\Collection	$linkCollection
	 * @return void
	 * @throws \Rbplm\Ged\Exception
	 * @TODO: a finir et tester
	 */
	protected function _savelinks_Docfile_Role($mapped, $linkCollection)
	{
		if( !$this->_slGDRstmt ){
			$this->_slGDRstmt = $this->getConnexion()->prepare($sql);
		}
		
		$bind = array(
		);
		
		$this->_slGDRstmt->execute($bind);
	}
	
    
} //End of class

