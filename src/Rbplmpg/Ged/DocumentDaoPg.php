<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Ged;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
CREATE TABLE ged_document(
	number varchar NOT NULL, 
	description text, 
	last_version_id integer, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	update_by uuid, 
	create_by uuid NOT NULL
) INHERITS (anyobject);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (30, '\Rbplm\Ged\Document', 'ged_document'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_document ADD PRIMARY KEY (id);
ALTER TABLE ged_document ADD UNIQUE (uid);
ALTER TABLE ged_document ADD UNIQUE (number);
ALTER TABLE ged_document ADD UNIQUE (path);
ALTER TABLE ged_document ALTER COLUMN cid SET DEFAULT 30;
CREATE INDEX INDEX_ged_document_number ON ged_document USING btree (number);
CREATE INDEX INDEX_ged_document_owner ON ged_document USING btree (owner);
CREATE INDEX INDEX_ged_document_update_by ON ged_document USING btree (update_by);
CREATE INDEX INDEX_ged_document_create_by ON ged_document USING btree (create_by);
CREATE INDEX INDEX_ged_document_uid ON ged_document USING btree (uid);
CREATE INDEX INDEX_ged_document_name ON ged_document USING btree (name);
CREATE INDEX INDEX_ged_document_label ON ged_document USING btree (label);
CREATE INDEX INDEX_ged_document_parent ON ged_document USING btree (parent);
CREATE INDEX INDEX_ged_document_path ON ged_document USING btree (path);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_document ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_document AFTER INSERT OR UPDATE 
		   ON ged_document FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_ged_document AFTER DELETE 
		   ON ged_document FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_ged_document_version_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_document_version AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
 <<*/

/** SQL_DROP>>
 <<*/



/**
 * @brief Dao class for \Rbplm\Ged\Document
 * 
 * See the examples: Rbplm/Ged/DocumentTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\DocumentTest
 *
 */
class DocumentDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'ged_document';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 30;
	
	public static $sysToApp = array(
	    'number'=>'number', 
	    'description'=>'description', 
	    'last_version_id'=>'lastVersionId', 
	    'created'=>'created', 
	    'updated'=>'updated', 
	    'closed'=>'closed', 
	    'owner'=>'ownerId', 
	    'update_by'=>'updateById', 
	    'create_by'=>'createById'
	    );
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Ged\Document	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->setNumber($row['number']);
			$mapped->description = $row['description'];
			$mapped->lastVersionId = $row['lastVersionId'];
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
			$mapped->closed = $row['closed'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->updateById = $row['updateById'];
			$mapped->createById = $row['createById'];
		}
		else{
			$mapped->setNumber($row['number']);
			$mapped->description = $row['description'];
			$mapped->lastVersionId = $row['last_version_id'];
			$mapped->created = new Rbplm\Sys\Date($row['created']);
			$mapped->updated = new Rbplm\Sys\Date($row['updated']);
			$mapped->closed = new Rbplm\Sys\Date($row['closed']);
			$mapped->ownerId = $row['owner'];
			$mapped->updateById = $row['update_by'];
			$mapped->createById = $row['create_by'];
		}
	} //End of function
	
    /**
     * Getter for versions. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @param boolean
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getVersions($mapped, $loadInCollection = false)
    {
        $List = new \Rbplmpg\Dao\DaoList( 'view_ged_document_version_links' );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        if($loadInCollection){
        	$List->loadInCollection($mapped->getVersions(), "lparent='$uid'");
        }
        else{
        	$List->load("lparent='$uid'");
        }
        return $List;
    }

} //End of class

