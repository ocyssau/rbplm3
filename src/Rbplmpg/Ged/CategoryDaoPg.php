<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Ged;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
 CREATE TABLE ged_category(
 description varchar(255),
 icon varchar(255)
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (60, '\Rbplm\Ged\Category', 'ged_category');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE ged_category ADD PRIMARY KEY (id);
 ALTER TABLE ged_category ADD UNIQUE (uid);
 ALTER TABLE ged_category ADD UNIQUE (path);
 ALTER TABLE ged_category ALTER COLUMN cid SET DEFAULT 60;
 CREATE INDEX INDEX_ged_category_owner ON ged_category USING btree (owner);
 CREATE INDEX INDEX_ged_category_uid ON ged_category USING btree (uid);
 CREATE INDEX INDEX_ged_category_cid ON ged_category USING btree (cid);
 CREATE INDEX INDEX_ged_category_name ON ged_category USING btree (name);
 CREATE INDEX INDEX_ged_category_parent ON ged_category USING btree (parent);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE ged_category ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_ged_category AFTER INSERT OR UPDATE
 ON ged_category FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_ged_category AFTER DELETE
 ON ged_category FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Ged\Category
 *
 * See the examples: Rbplm/Ged/CategoryTest.php
 *
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\CategoryTest
 *
 */
class CategoryDaoPg extends Dao
{

    /**
     *
     * @var string
     */
    public static $table = 'ged_category';

    /**
     *
     * @var integer
     */
    protected static $classId = 20;


    public static $sysToApp = array(
        'id'=>'pkey',
        'uid'=>'uid',
        'cid'=>'classId',
        'name'=>'name',
        'label'=>'label',
        'parent'=>'parentId',
        'description'=>'description',
        'owner'=>'ownerId',
        'icon'=>'icon'
    );


    /**
     * Load the properties in the mapped object.
     *
     * @param \Rbplm\Ged\Category	$mapped
     * @param array $row			\PDO fetch result to load
     * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
     * @return void
     */
    public function loadFromArray( $mapped, array $row, $fromApp = false )
    {
        \Rbplmpg\Dao::loadFromArray($mapped, $row, $fromApp);
        if($fromApp){
            $mapped->description = $row['description'];
            $mapped->ownerId = $row['ownerId'];
            $mapped->setIcon($row['icon']);
        }
        else{
            $mapped->description = $row['description'];
            $mapped->ownerId = $row['owner'];
            $mapped->setIcon($row['icon']);
        }
    } //End of function


    /**
     * @param \Rbplm\Ged\Category   $mapped
     * @return void
     * @throws \Rbplm\Ged\Exception
     *
     */
    protected function _saveObject($mapped)
    {
        $bind = array(
            ':description'=>$mapped->description,
            ':ownerId'=>$mapped->ownerId,
            ':icon'=>$mapped->getIcon()
        );
        $this->_genericSave($mapped, $bind);
    }


} //End of class

