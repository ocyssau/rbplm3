<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Ged\Document;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
 CREATE TABLE ged_document_version(
 number varchar NOT NULL,
 description text,
 created integer NOT NULL,
 updated integer,
 closed integer,
 locked integer,
 owner uuid NOT NULL,
 update_by uuid,
 create_by uuid NOT NULL,
 lock_by uuid,
 base uuid NOT NULL,
 from_anyobject uuid,
 doctype uuid NOT NULL,
 access_code integer NOT NULL,
 iteration integer NOT NULL,
 version integer NOT NULL,
 life_stage varchar
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (31, '\Rbplm\Ged\Document\Version', 'ged_document_version');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE ged_document_version ADD PRIMARY KEY (id);
 ALTER TABLE ged_document_version ADD UNIQUE (uid);
 ALTER TABLE ged_document_version ADD UNIQUE (number);
 ALTER TABLE ged_document_version ADD UNIQUE (path);

 ALTER TABLE ged_document_version ALTER COLUMN iteration SET DEFAULT 1;
 ALTER TABLE ged_document_version ALTER COLUMN version SET DEFAULT 1;
 ALTER TABLE ged_document_version ALTER COLUMN life_stage SET DEFAULT 'init';
 ALTER TABLE ged_document_version ALTER COLUMN cid SET DEFAULT 30;

 CREATE INDEX INDEX_ged_document_version_number ON ged_document_version USING btree (number);
 CREATE INDEX INDEX_ged_document_version_owner ON ged_document_version USING btree (owner);
 CREATE INDEX INDEX_ged_document_version_update_by ON ged_document_version USING btree (update_by);
 CREATE INDEX INDEX_ged_document_version_create_by ON ged_document_version USING btree (create_by);
 CREATE INDEX INDEX_ged_document_version_lock_by ON ged_document_version USING btree (lock_by);
 CREATE INDEX INDEX_ged_document_version_base ON ged_document_version USING btree (base);
 CREATE INDEX INDEX_ged_document_version_from_anyobject ON ged_document_version USING btree (from_anyobject);
 CREATE INDEX INDEX_ged_document_version_doctype ON ged_document_version USING btree (doctype);
 CREATE INDEX INDEX_ged_document_version_access_code ON ged_document_version USING btree (access_code);
 CREATE INDEX INDEX_ged_document_version_iteration ON ged_document_version USING btree (iteration);
 CREATE INDEX INDEX_ged_document_version_version ON ged_document_version USING btree (version);
 CREATE INDEX INDEX_ged_document_version_life_stage ON ged_document_version USING btree (life_stage);
 CREATE INDEX INDEX_ged_document_version_uid ON ged_document_version USING btree (uid);
 CREATE INDEX INDEX_ged_document_version_name ON ged_document_version USING btree (name);
 CREATE INDEX INDEX_ged_document_version_label ON ged_document_version USING btree (label);
 CREATE INDEX INDEX_ged_document_version_parent ON ged_document_version USING btree (parent);
 CREATE INDEX INDEX_ged_document_version_cid ON ged_document_version USING btree (cid);
 CREATE INDEX INDEX_ged_document_version_path ON ged_document_version USING btree (path);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE ged_document_version ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (lock_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (base) REFERENCES ged_document (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (from_anyobject) REFERENCES ged_document_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 ALTER TABLE ged_document_version ADD FOREIGN KEY (doctype) REFERENCES ged_doctype (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_ged_document_version AFTER INSERT OR UPDATE
 ON ged_document_version FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_ged_document_version AFTER DELETE
 ON ged_document_version FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Ged\Document\Version
 *
 * See the examples: Rbplm/Ged/Document/VersionTest.php
 *
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\Document\VersionTest
 *
 */
class VersionDaoPg extends Dao
{

    /**
     *
     * @var string
     */
    public static $table = 'ged_document_version';

    /**
     *
     * @var integer
     */
    protected static $classId = 31;

    public static $sysToApp = array(
        'number'=>'number',
        'description'=>'description',
        'created'=>'created',
        'updated'=>'updated',
        'closed'=>'closed',
        'locked'=>'locked',
        'owner'=>'ownerId',
        'update_by'=>'updateById',
        'create_by'=>'createById',
        'lock_by'=>'lockById',
        'base'=>'baseId',
        'from_anyobject'=>'fromId',
        'doctype'=>'doctypeId',
        'access_code'=>'accessCode',
        'iteration'=>'iteration',
        'version'=>'version',
        'life_stage'=>'lifeStage'
        );


    /**
     * Load the properties in the mapped object.
     *
     * @param \Rbplm\Ged\Document\Version	$mapped
     * @param array $row			\PDO fetch result to load
     * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
     * @return void
     */
    public function loadFromArray( $mapped, array $row, $fromApp = false )
    {
        Dao::loadFromArray($mapped, $row, $fromApp);
        if($fromApp){
            $mapped->setNumber($row['number']);
            $mapped->description = $row['description'];
            $mapped->created = $row['created'];
            $mapped->updated = $row['updated'];
            $mapped->closed = $row['closed'];
            $mapped->locked = $row['locked'];
            $mapped->ownerId = $row['ownerId'];
            $mapped->updateById = $row['updateById'];
            $mapped->createById = $row['createById'];
            $mapped->lockById = $row['lockById'];
            $mapped->baseId = $row['baseId'];
            $mapped->fromId = $row['fromId'];
            $mapped->doctypeId = $row['doctype'];
            $mapped->accessCode = $row['accessCode'];
            $mapped->iteration = $row['iteration'];
            $mapped->version = $row['version'];
            $mapped->setLifeStage($row['lifeStage']);
        }
        else{
            $mapped->setNumber($row['number']);
            $mapped->description = $row['description'];
            $mapped->created = new Rbplm\Sys\Date($row['created']);
            $mapped->updated = new Rbplm\Sys\Date($row['updated']);
            $mapped->closed = new Rbplm\Sys\Date($row['closed']);
            $mapped->locked = new Rbplm\Sys\Date($row['locked']);
            $mapped->ownerId = $row['owner'];
            $mapped->updateById = $row['update_by'];
            $mapped->createById = $row['create_by'];
            $mapped->lockById = $row['lock_by'];
            $mapped->baseId = $row['base'];
            $mapped->fromId = $row['from_anyobject'];
            $mapped->doctypeId = $row['doctype'];
            $mapped->accessCode = $row['access_code'];
            $mapped->iteration = $row['iteration'];
            $mapped->version = $row['version'];
            $mapped->setLifeStage($row['life_stage']);
        }
    } //End of function

    /**
     * @param \Rbplm\Ged\Document\Version	$mapped
     * @return VersionDaoPg
     *
     * @todo a revoir
     */
    public function loadInternalLinks($mapped){
        $loader = \Rbplm\Dao\Factory::getLoader();
        $mapped->setBase( \Rbplm\Dao\Loader::load( $mapped->baseId, '\Rbplm\Ged\Document' ) );
        $mapped->setDoctype( \Rbplm\Dao\Loader::load( $mapped->doctypeId, '\Rbplm\Ged\Doctype' ) );
        $mapped->setFrom( \Rbplm\Dao\Loader::load( $mapped->fromId, '\Rbplm\Ged\Document\Version' ) );
        $mapped->setCreateBy( \Rbplm\Dao\Loader::load( $mapped->createById, '\Rbplm\People\User' ) );
        $mapped->setlockBy( \Rbplm\Dao\Loader::load( $mapped->lockById, '\Rbplm\People\User' ) );
        $mapped->setOwner( \Rbplm\Dao\Loader::load( $mapped->ownerId, '\Rbplm\People\User' ) );
        $mapped->setUpdateBy( \Rbplm\Dao\Loader::load( $mapped->updateById, '\Rbplm\People\User' ) );
        return $this;
    }

} //End of class

