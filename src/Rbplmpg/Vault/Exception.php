<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Vault;

/**
 * @brief \Exception for Rbplm.
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class Exception extends \Rbplm\Sys\Exception
{
}
