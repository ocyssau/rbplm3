<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Vault;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
-- SEQUENCE
-- anyobject_id_seq
-- 
CREATE SEQUENCE vault_reposit_seq
    START WITH 100
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


CREATE TABLE vault_reposit(
	id integer NOT NULL,
	uid uuid NOT NULL,
	name varchar(255),
	number varchar(255),
	description varchar(255),
	url varchar(255),
	type integer,
	mode integer,
	state integer,
	priority integer,
	maxsize integer,
	maxcount integer,
	create_by varchar(255),
	created date
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE vault_reposit ALTER COLUMN id SET DEFAULT nextval('vault_reposit_seq'::regclass);
ALTER TABLE vault_reposit ADD PRIMARY KEY (id);
ALTER TABLE vault_reposit ADD UNIQUE (uid);
ALTER TABLE vault_reposit ADD UNIQUE (url);
ALTER TABLE vault_reposit ADD UNIQUE (number);
CREATE INDEX INDEX_vault_reposit_name ON vault_reposit USING btree (name);
CREATE INDEX INDEX_vault_reposit_number ON vault_reposit USING btree (number);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Vault\Reposit
 * 
 * See the examples: Rbplm/Org/UnitTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Vault\RepositTest
 *
 */
class RepositDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'vault_reposit';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 605;
	
	/**
	 * 
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'pkey',
		'uid'=>'uid',
		'description'=>'description', 
		'name'=>'name',
		'number'=>'number',
		'url'=>'url',
		'type'=>'type',
		'mode'=>'mode',
		'state'=>'isActive',
		'priority'=>'priority',
		'maxsize'=>'maxsize',
		'maxcount'=>'maxcount',
		'create_by'=>'createBy',
		'created'=>'created'
	);
	
	/**
	 * @var string
	 */
	protected $_sequence_name = 'vault_reposit_seq';
	
	
	/**
	 * Constructor
	 *
	 * @param \PDO
	 *
	 */
	public function __construct( $conn=null )
	{
	    if( $conn ){
	        $this->connexion = $conn;
	    }
	    else{
	        $this->connexion = Connexion::get();
	    }
	    $this->metaModel = static::$sysToApp;
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Vault\Reposit	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->isActive($row['isActive']);
			$mapped->createBy = $row['createBy'];
		}
		else{
			$mapped->isActive($row['state']);
			$mapped->createBy = $row['create_by'];
		}
		$mapped->pkey = $row['id'];
		$mapped->setUid($row['uid']);
		$mapped->setDescription($row['description']);
		$mapped->setName($row['name']);
		$mapped->setNumber($row['number']);
		$mapped->setUrl($row['url']);
		$mapped->setType($row['type']);
		$mapped->setMode($row['mode']);
		$mapped->setPriority($row['priority']);
		$mapped->maxsize = $row['maxsize'];
		$mapped->maxcount = $row['maxcount'];
		$mapped->created = $row['created'];
		$mapped->classId = $this->_classId;
	} //End of function
	
	
	/**
	 * 
	 * @param \Rbplm\Vault\Reposit $mapped
	 * @param integer 		Type of reposit, one of constants \Rbplm\Vault\Reposit::TYPE_* or let blank to get type from $mapped object.
	 * @return void
	 */
	public function loadActive($mapped, $type=null)
	{
		if(is_null($type)){
			$type = $mapped->getType();
		}
		$filter = "state=1 AND type=" . $type . " ORDER BY priority ASC";
		return $this->load( $mapped, $filter );
	}
	
	/**
	 * 
	 * @param \Rbplm\Vault\Reposit $mapped
	 * @param string	$number
	 * @return void
	 */
	public function loadFromNumber($mapped, $number)
	{
		$filter = "number='$number'";
		return $this->load( $mapped, $filter );
	}
	
	
	/**
	 * @see library/Rbplm/Dao/\Rbplm\Dao\DaoInterface#suppress($mapped)
	 * 
	 * @param \Rbplm\Vault\Reposit
	 * @param boolean
	 * 
	 */
	public function suppress(\Rbplm\Dao\MappedInterface $mapped, $withChilds = false ) 
	{
		$this->connexion->beginTransaction();
		$sql = 'DELETE FROM ' . $this->_table . ' WHERE uid=:uid';
		$bind = array( ':uid'=>$mapped->getUid() );
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$this->connexion->commit();
	} //End of method
	

} //End of class
