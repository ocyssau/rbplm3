<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Vault;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
 CREATE SEQUENCE vault_id_seq
 START WITH 1
 INCREMENT BY 1
 NO MAXVALUE
 NO MINVALUE
 CACHE 1;

 CREATE TABLE vault_record (
 id integer NOT NULL PRIMARY KEY,
 uid uuid NOT NULL UNIQUE,
 cid integer DEFAULT 600,
 name varchar(256),
 created date NOT NULL,
 extension char(16),
 path varchar(256),
 rootname varchar(256),
 fullpath varchar(256),
 size integer,
 mtime integer,
 type varchar(32),
 md5 char(38)
 );
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (600, '\Rbplm\Vault\Record', 'vault_record');
 <<*/

/** SQL_ALTER>>
 CREATE INDEX INDEX_vault_record_uid ON vault_record (uid);
 CREATE INDEX INDEX_vault_record_name ON vault_record (name);
 ALTER TABLE vault_record ALTER COLUMN id SET DEFAULT nextval('vault_id_seq'::regclass);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS vault_record;
 DROP SEQUENCE IF EXISTS vault_id_seq;
 <<*/

/**
 * @brief Postgresql Dao class for \Rbplm\Vault\Record.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 */
class RecordDaoPg extends Dao
{

	protected static $classId = 600;
	public static $table = 'vault_record';
	protected $_sequence_name = 'vault_id_seq';


	public static $sysToApp = array(
		'id'=>'pkey',
		'uid'=>'uid',
		'cid'=>'classId',
		'name'=>'name',
		'created'=>'created',
		'extension'=>'extension',
		'path'=>'path',
		'rootname'=>'rootname',
		'fullpath'=>'fullpath',
		'size'=>'size',
		'mtime'=>'mtime',
		'type'=>'type',
		'md5'=>'md5'
	);


	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbplm\Ged\Doctype	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->pkey = $row['id'];
			$mapped->setUid($row['uid']);
			$mapped->setName($row['name']);
			$mapped->created 	= new DateTime($row['created']);
			$mapped->extension 	= $row['extension'];
			$mapped->path 		= $row['path'];
			$mapped->rootname 	= $row['rootname'];
			$mapped->fullpath 	= $row['fullpath'];
			$mapped->size 		= $row['size'];
			$mapped->mtime 		= $row['mtime'];
			$mapped->type 		= $row['type'];
			$mapped->md5 		= $row['md5'];
			$mapped->pkey = $row['pkey'];
		}
		else{
			$mapped->pkey = $row['id'];
			$mapped->setUid($row['uid']);
			$mapped->setName($row['name']);
			$mapped->created 	= new DateTime($row['created']);
			$mapped->extension 	= $row['extension'];
			$mapped->path 		= $row['path'];
			$mapped->rootname 	= $row['rootname'];
			$mapped->fullpath 	= $row['fullpath'];
			$mapped->size 		= $row['size'];
			$mapped->mtime 		= $row['mtime'];
			$mapped->type 		= $row['type'];
			$mapped->md5 		= $row['md5'];
		}
	} //End of function

    
	/**
	 * @see library/Rbplm/Dao/\Rbplm\Dao\DaoInterface#suppress($mapped)
	 *
	 * @param \Rbplm\Vault\Reposit
	 * @param boolean
	 *
	 */
	public function suppress( $mapped, $withChilds = false )
	{
		$this->_connexion->beginTransaction();
		$sql = 'DELETE FROM ' . $this->_table . ' WHERE uid=:uid';
		$bind = array( ':uid'=>$mapped->getUid() );
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		$this->_connexion->commit();
	} //End of method


} //End of class

