<?php
//%LICENCE_HEADER%

namespace Rbplmpg\People\User;


/**
 * $Id: PreferenceDaoPg.php 801 2012-04-18 21:08:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/People/User/PreferenceDaoPg.php $
 * $LastChangedDate: 2012-04-18 23:08:27 +0200 (mer., 18 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 801 $
 */

/** SQL_SCRIPT>>
CREATE TABLE people_user_preference(
	owner uuid, 
	cid integer, 
	preferences text, 
	enable boolean
);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (111, '\Rbplm\People\User\Preference', 'people_user_preference');
<<*/

/** SQL_ALTER>>
ALTER TABLE people_user_preference ADD PRIMARY KEY (owner);
ALTER TABLE people_user_preference ALTER COLUMN cid SET DEFAULT 111;
ALTER TABLE people_user_preference ALTER COLUMN enable SET DEFAULT true;
CREATE INDEX INDEX_people_user_preference_cid ON people_user_preference USING btree (cid);
<<*/

/** SQL_FKEY>>
ALTER TABLE people_user_preference ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE people_user_preference;
 <<*/


/** 
 * @brief Postgresql Dao class for \Rbplm\People\User\Preference.
 * 
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/People/UserTest.php
 *
 */
class PreferenceDaoPg extends \Rbplmpg\Dao{
	
	/**
	 * 
	 * @var string
	 */
	public static $table = 'people_user_preference';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 111;
	
	public static $sysToApp = array('cid'=>'classId', 'owner'=>'owner', 'preferences'=>'preferences', 'enable'=>'isEnable');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\People\User\Preference	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->classId = $row['classId'];
			$mapped->owner = $row['owner'];
			$mapped->setPreferences (\Rbplmpg\Dao::multivalToApp($row['preferences']));
			$mapped->isEnable($row['isEnable']);
		}
		else{
			$mapped->classId = $row['cid'];
			$mapped->owner = $row['owner'];
			$mapped->setPreferences (\Rbplmpg\Dao::multivalToApp($row['preferences']));
			$mapped->isEnable($row['enable']);
		}
	} //End of function
	
	
	/**
	 * @param \Rbplm\People\User\Preference   $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey ){
			$sql = "UPDATE $table SET
					cid = :classId,
					preferences = :preferences,
					enable = :isEnable
		            WHERE owner=:owner";
		}
		else{
			$sql = "INSERT INTO $table (owner,cid,preferences,enable) VALUES (:owner,:classId,:preferences,:isEnable)";
		}
		
		$bind = array(
					':classId'=>$mapped->classId,
					':owner'=>$mapped->getOwner(),
					':preferences'=>\Rbplmpg\Dao::multivalToPg( $mapped->getPreferences() ),
					':isEnable'=>(integer) $mapped->isEnable()
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			//$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
			$mapped->pkey = $mapped->getOwner();
		}
		/*
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
		*/
	}
	
	
	/**
	 * Load the preferences from the owner.
	 * 
	 * @param $mapped
	 * @param $ownerUid
	 * @param $expectedProperties
	 * @param $force
	 * @param $locks
	 * @return unknown_type
	 */
	public function loadFromOwner($mapped, $ownerUid, $expectedProperties = array(), $force = false, $locks = true)
	{
		$filter = "owner='$ownerUid'";
		return $this->load($mapped, $filter, $expectedProperties, $force, $locks);
	} //End of function
	
	
	/**
	 * @see library/Rbplm/Dao/\Rbplmpg\Dao#loadFromUid($mapped, $uid, $expectedProperties, $force, $locks)
	 * 
	 * Primary key of preference is the owner uid. This method overload the \Rbplmpg\Dao::loadFromUid method to adapt the
	 * filter from owner in place of uid property to use as primary key.
	 * It is a aloas of loadFromOwner method.
	 */
	public function loadFromUid($mapped, $ownerUid, $expectedProperties = array(), $force = false, $locks = true)
	{
		$filter = "owner='$ownerUid'";
		return $this->load($mapped, $filter, $expectedProperties, $force, $locks);
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbplm\People\User\Preference	$mapped
	 * @param array $row	\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 * 
	 */
	/*
	public function loadFromArray( $mapped, array $row, $fromApp = false)
	{
		foreach($row as $name=>$value){
			$mapped->$name = $value;
		}
		$mapped->loadedProperties = $row;
		$mapped->isLoaded(true);
	} //End of method
	*/
	
	
	/**
	 *
	 * @param \Rbplm\People\User\Preference	$mapped
	 * @throws \Rbplm\Sys\Exception
	 * @return void
	 *
	 */
	/*
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$id = $mapped->getOwner();
		//$res = $this->getConnexion()->query('SELECT count(owner) FROM '.$table.' WHERE owner=\''.$id.'\';', \PDO::FETCH_COLUMN, 0);
		$countStmt = $this->getConnexion()->prepare( 'SELECT count(owner) FROM '.$table.' WHERE owner=:owner' );
		$countStmt->execute( array(':owner'=>$id) );
		$count = $countStmt->fetchColumn(0);
		
		if( $count > 0 ){
			$fields = explode(',', $this->_stdInsertSelect);
			$valuesBind = explode(',', $this->_stdInsertValues);
			for( $i = 0; $i < count($fields); $i++ ){
				$aset[] = $fields[$i] . '=' . $valuesBind[$i];
			}
			$sset = implode(',', $aset);
			$sql = "UPDATE $table SET $sset WHERE owner=:owner";
		}
		else{
			$sql = "INSERT INTO $table ($this->_stdInsertSelect) VALUES ($this->_stdInsertValues)";
		}
		
		$bind = array( ':owner'=>$mapped->getOwner() );
		foreach($mapped->getPreferences() as $name=>$value){
			$bind[':'.$name] = $value;
		}
		
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
	} //End of method
	*/

} //End of class
