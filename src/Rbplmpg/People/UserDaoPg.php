<?php
//%LICENCE_HEADER%

namespace Rbplmpg\People;

use Rbplmpg\Dao as Dao;


/** SQL_SCRIPT>>
CREATE TABLE people_user(
	is_active boolean, 
	last_login integer, 
	login varchar(255) NOT NULL, 
	firstname varchar(255), 
	lastname varchar(255), 
	password varchar(255), 
	mail varchar(255), 
	wildspace varchar(255)
) INHERITS (anyobject);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (110, '\Rbplm\People\User', 'people_user');
INSERT INTO people_user (id, uid, cid, name, label, is_active, login)
                VALUES (1, '99999999-9999-9999-9999-00000000abcd', 110, 'admin', 'admin', true, 'admin');
INSERT INTO people_user (id, uid, cid, name, label, is_active, login, parent)
                VALUES (4, '99999999-9999-9999-9999-99999999ffff', 110, 'anonymous', 'anonymous', true, 'anonymous', '01234567-0123-0123-0123-0123456789ac');
<<*/

/** SQL_ALTER>>
ALTER TABLE people_user ADD PRIMARY KEY (id);
ALTER TABLE people_user ADD UNIQUE (login);
ALTER TABLE people_user ADD UNIQUE (uid);
ALTER TABLE people_user ADD UNIQUE (path);
ALTER TABLE people_user ALTER COLUMN cid SET DEFAULT 110;
CREATE INDEX INDEX_people_user_login ON people_user USING btree (login);
CREATE INDEX INDEX_people_user_uid ON people_user USING btree (uid);
CREATE INDEX INDEX_people_user_name ON people_user USING btree (name);
CREATE INDEX INDEX_people_user_label ON people_user USING btree (label);
CREATE INDEX INDEX_people_user_cid ON people_user USING btree (cid);
CREATE INDEX INDEX_people_user_path ON people_user USING btree (path);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_user AFTER DELETE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class for \Rbplm\People\User
 * 
 * See the examples: Rbplm/People/UserTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\People\UserTest
 *
 */
class UserDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'people_user';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 110;
	
	
	public static $sysToApp = array(
	    'is_active'=>'isActive', 
	    'last_login'=>'lastLogin', 
	    'login'=>'login', 
	    'firstname'=>'firstname', 
	    'lastname'=>'lastname', 
	    'password'=>'password', 
	    'mail'=>'mail', 
	    'wildspace'=>'wildspacePath'
	    );
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\People\User	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->isActive($row['isActive']);
			$mapped->lastLogin = $row['lastLogin'];
			$mapped->setLogin($row['login']);
			$mapped->firstname = $row['firstname'];
			$mapped->lastname = $row['lastname'];
			$mapped->setPassword($row['password']);
			$mapped->mail = $row['mail'];
			$mapped->wildspacePath = $row['wildspacePath'];
		}
		else{
			$mapped->isActive($row['is_active']);
			$mapped->lastLogin = $row['last_login'];
			$mapped->setLogin($row['login']);
			$mapped->firstname = $row['firstname'];
			$mapped->lastname = $row['lastname'];
			$mapped->setPassword($row['password']);
			$mapped->mail = $row['mail'];
			$mapped->wildspacePath = $row['wildspace'];
		}
	} //End of function
		
    /**
     * Getter for groups. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @return UserDaoPg
     */
    public function loadGroups($mapped)
    {
        $List = new Dao\DaoList( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getGroups(), "lparent='$uid' ORDER BY lindex ASC");
        return $this;
    }
    
    /**
     * Getter for groups. Return a list.
     *
     * @param string uuid
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getGroups($uid)
    {
        $List = new Dao\DaoList( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $List->load("lparent='$uid'");
        return $List;
    }
    
} //End of class

