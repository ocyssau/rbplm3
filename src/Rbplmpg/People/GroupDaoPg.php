<?php
//%LICENCE_HEADER%

namespace Rbplmpg\People;

use Rbplmpg\Dao as Dao;

/** SQL_SCRIPT>>
CREATE TABLE people_group(
	is_active boolean, 
	description varchar(255)
) INHERITS (anyobject);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (120, '\Rbplm\People\Group', 'people_group'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE people_group ADD PRIMARY KEY (id);
ALTER TABLE people_group ADD UNIQUE (uid);
ALTER TABLE people_group ADD UNIQUE (path);
ALTER TABLE people_group ALTER COLUMN cid SET DEFAULT 120;
CREATE INDEX INDEX_people_group_uid ON people_group USING btree (uid);
CREATE INDEX INDEX_people_group_name ON people_group USING btree (name);
CREATE INDEX INDEX_people_group_label ON people_group USING btree (label);
CREATE INDEX INDEX_people_group_path ON people_group USING btree (path);
<<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();

<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
CREATE OR REPLACE VIEW view_people_user_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_user AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
 <<*/

/**
 * @brief Dao class for \Rbplm\People\Group
 * 
 * See the examples: Rbplm/People/GroupTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\People\GroupTest
 *
 */
class GroupDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'people_group';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 120;
	
	public $isLeaf = true;
	
	public static $sysToApp = array(
	    'is_active'=>'isActive', 
	    'description'=>'description'
	    );

	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\People\Group	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
     * @return GroupDaoPg
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->isActive($row['isActive']);
		}
		else{
			$mapped->isActive($row['is_active']);
		}
		$mapped->setDescription($row['description']);
		return $this;
	} //End of function
	
	
    /**
     * Load groups.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @return GroupDaoPg
     */
    public function loadGroups($mapped)
    {
        $List = new Dao\DaoList( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getGroups(), "lparent='$uid'");
        return $this;
    }
    
    /**
     * Getter for groups. Return a list.
     *
     * @param string	uuid
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getGroups($uid)
    {
        $List = new Dao\DaoList( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $List->load("lparent='$uid'");
        return $List;
    }
    
    
    /**
     * Getter for groups. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface|string
     * @return \Rbplmpg\Dao\DaoList
     */
    public function loadGroupsRecursively($mapped)
    {
	    $uid = $mapped->getUid();
        $table = $this->_table;
		$sql = "WITH RECURSIVE graphe(name, related, lchild, data, depth, path, lindex) AS (
			SELECT g.name, g.related, g.lchild, g.data, 1, '/' || g.related, g.lindex
				FROM anyobject_links g WHERE g.related = '$uid'
			UNION ALL
				SELECT g.name, g.related, g.lchild, g.data, sg.depth + 1, path || '/' || g.lchild, g.lindex
				FROM anyobject_links g, graphe sg
				WHERE sg.lchild = g.related)
		SELECT ct.*, graphe.depth AS ldepth, graphe.path AS lpath, graphe.lindex, graphe.related AS lparent, graphe.data AS ldata FROM graphe
		JOIN $table AS ct ON ct.uid = graphe.lchild ORDER BY lpath ASC, lindex ASC;";
    	
        $List = new Dao\DaoList( array(), $this->getConnexion() );
        $List->loadFromSql($sql);
        
        foreach($List as $entry){
        	$group = $List->toObject(true);
        	if($entry['ldepth'] == 1){
        		$mapped->getGroups()->add($group);
        	}
        	$parent = \Rbplm\Dao\Registry::singleton()->get( $entry['lparent'] );
        	if(is_a($parent, '\Rbplm\People\Group')){
	        	$parent->getGroups()->add($group);
        	}
        }
        return $List;
    }
    
    /**
     * Load for users.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @return GroupDaoPg
     */
    public function loadUsers($mapped)
    {
        $List = new Dao\DaoList( array('table'=>'view_people_user_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getUsers(), "lparent='$uid'");
        return $this;
    }
    
    /**
     * Getter for users. Return a list.
     *
     * @param string	uuid
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getUsers($uid)
    {
        $List = new Dao\DaoList( array('table'=>'view_people_user_links'), $this->getConnexion() );
        $List->load("lparent='$uid'");
        return $List;
    }

} //End of class

