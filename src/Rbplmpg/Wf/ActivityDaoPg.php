<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Wf;


/** SQL_SCRIPT>>
CREATE TABLE wf_activity(
	description varchar(256), 
	normalized_name varchar(128), 
	process uuid, 
	is_interactive boolean DEFAULT false, 
	is_autoRouted boolean DEFAULT false, 
	is_automatic boolean DEFAULT false, 
	is_comment boolean DEFAULT false, 
	type varchar(32), 
	expiration_time integer
)INHERITS (anyobject);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (421, '\Rbplm\Wf\Activity_Activity', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (422, '\Rbplm\Wf\Activity_Start', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (423, '\Rbplm\Wf\Activity_End', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (424, '\Rbplm\Wf\Activity_Join', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (425, '\Rbplm\Wf\Activity_Split', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (426, '\Rbplm\Wf\Activity_Switch', 'wf_activity');
INSERT INTO classes (id, name, tablename) VALUES (427, '\Rbplm\Wf\Activity_Standalone', 'wf_activity');
<<*/

/** SQL_ALTER>>
ALTER TABLE wf_activity ADD PRIMARY KEY (id);
ALTER TABLE wf_activity ADD UNIQUE (uid);
ALTER TABLE wf_activity ADD UNIQUE (path);
ALTER TABLE wf_activity ALTER COLUMN cid SET DEFAULT 421;
CREATE INDEX INDEX_wf_activity_name ON wf_activity USING btree (name);
CREATE INDEX INDEX_wf_activity_label ON wf_activity USING btree (label);
<<*/

/** SQL_FKEY>>
ALTER TABLE wf_activity ADD FOREIGN KEY (cid) REFERENCES classes (id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_activity ADD FOREIGN KEY (process) REFERENCES wf_process (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
DROP TRIGGER IF EXISTS trig01_wf_activity ON wf_activity;
CREATE TRIGGER trig01_wf_activity AFTER INSERT OR UPDATE 
		   ON wf_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
DROP TRIGGER IF EXISTS trig02_wf_activity ON wf_activity;
CREATE TRIGGER trig02_wf_activity AFTER DELETE 
		   ON wf_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE wf_activity CASCADE;
 <<*/

//require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for \Rbplm\Wf\Activity
 * 
 * See the examples: Rbplm/Wf/ActivityTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Wf\ActivityTest
 *
 */
class ActivityDaoPg extends \Rbplmpg\Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'wf_activity';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 421;
	
	static $_sysToApp = array('description'=>'description', 'normalized_name'=>'normalizedName', 'process'=>'processId', 'is_interactive'=>'isInteractive', 'is_autoRouted'=>'isAutoRouted', 'is_automatic'=>'isAutomatic','is_comment'=>'isComment', 'type'=>'type', 'expiration_time'=>'expirationTime');
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Wf\Activity	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = true )
	{
		\Rbplmpg\Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->setDescription($row['description']);
			$mapped->setNormalizedName($row['normalizedName']);
			$mapped->processId = $row['processId'];
			$mapped->isInteractive($row['isInteractive']);
			$mapped->isAutoRouted($row['isAutoRouted']);
			$mapped->isAutomatic($row['isAutomatic']);
			$mapped->isComment($row['isComment']);
			$mapped->type = $row['type'];
			$mapped->expirationTime = $row['expirationTime'];
		}
		else{
			$mapped->setDescription($row['description']);
			$mapped->setNormalizedName($row['normalized_name']);
			$mapped->processId = $row['process'];
			$mapped->isInteractive($row['is_interactive']);
			$mapped->isAutoRouted($row['is_autoRouted']);
			$mapped->isAutomatic($row['is_automatic']);
			$mapped->isComment($row['is_comment']);
			$mapped->type = $row['type'];
			$mapped->expirationTime = $row['expiration_time'];
		}
	} //End of function
	
	
	/**
	 * @param \Rbplm\Wf\Activity   $mapped
	 * @return void
	 * @throws \Rbplm\Dao\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
					':description'=>$mapped->getDescription(),
					':normalizedName'=>$mapped->getNormalizedName(),
					':processId'=>$mapped->processId,
					':isInteractive'=>(integer) $mapped->isInteractive(),
					':isAutoRouted'=>(integer) $mapped->isAutoRouted(),
					':isAutomatic'=>(integer) $mapped->isAutomatic(),
					':isComment'=>(integer) $mapped->isComment(),
					':type'=>$mapped->type,
					':expirationTime'=>$mapped->expirationTime
		);
		$this->_genericSave($mapped, $bind);
	}
	

} //End of class

