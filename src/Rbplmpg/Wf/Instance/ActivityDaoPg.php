<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Wf\Instance;


/**
 * $Id: daoTemplate.tpl 495 2011-06-25 14:27:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-06-25 16:27:27 +0200 (sam., 25 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 495 $
 */


/** SQL_SCRIPT>>
CREATE TABLE wf_instance_activity(
	status varchar(64), 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (anyobject);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (431, '\Rbplm\Wf\Instance_Activity', 'wf_instance_activity');
<<*/

/** SQL_ALTER>>
ALTER TABLE wf_instance_activity ADD PRIMARY KEY (id);
ALTER TABLE wf_instance_activity ADD UNIQUE (uid);
ALTER TABLE wf_instance_activity ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN cid SET DEFAULT 431;
CREATE INDEX INDEX_wf_instance_activity_name ON wf_instance_activity USING btree (name);
CREATE INDEX INDEX_wf_instance_activity_label ON wf_instance_activity USING btree (label);
<<*/

/** SQL_FKEY>>
ALTER TABLE wf_instance_activity ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_wf_instance_activity AFTER INSERT OR UPDATE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_wf_instance_activity AFTER DELETE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.lparent AS lparent, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN anyobject_links AS l ON r.uid = l.lchild;
 <<*/

/** SQL_DROP>>
 <<*/

//require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for \Rbplm\Wf\Instance_Activity
 * 
 * See the examples: Rbplm/Wf/Instance/ActivityTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Wf\Instance_ActivityTest
 *
 */
class ActivityDaoPg extends \Rbplmpg\Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'wf_instance_activity';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 424;
	
	
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
		$this->_stdInsertSelect = 'uid,cid,name,label,status,owner,started,ended';
		$this->_stdInsertValues = ':uid,:cid,:name,:label,:status,:owner,:started,:ended';
		$this->_stdLoadSelect = 'id AS pkey, uid, cid, name, label, status, owner, started, ended';
	} //End of function
	
	

	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Wf\Instance_Activity	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = true )
	{
		\Rbplmpg\Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->status = $row['status'];
			$mapped->setOwner($row['owner']);
			$mapped->started = $row['started'];
			$mapped->ended = $row['ended'];
		}
		else{
			$mapped->status = $row['status'];
			$mapped->setOwner($row['owner']);
			$mapped->started = $row['started'];
			$mapped->ended = $row['ended'];
		}
	} //End of function


	/**
	 * @param \Rbplm\Wf\Instance_Activity   $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;

		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					uid = :uid,
					cid = :cid,
					name = :name, 
					label = :label,
					status = :status,
					owner = :owner,
					started = :started,
					ended = :ended
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table ($this->_stdInsertSelect) VALUES ($this->_stdInsertValues)";
		}
		
		$bind = array(
				':uid'=>$mapped->getUid(),
				':cid'=>$mapped->cid,
				':name'=>$mapped->getName(), 
				':label'=>$mapped->getLabel(),
				':status'=>$mapped->status,
				':owner'=>$mapped->getOwner(),
				':started'=>$mapped->started,
				':ended'=>$mapped->ended
		);
		
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
	}


    /**
     * Getter for previous. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getPrevious($mapped)
    {
        $List = new \Rbplmpg\Dao\DaoList( array('table'=>'view_wf_instance_activity_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("lparent='$uid'");
        return $List;
    }


    /**
     * Getter for comments. Return a list.
     *
     * @param \Rbplm\Dao\MappedInterface
     * @return \Rbplmpg\Dao\DaoList
     */
    public function getComments($mapped)
    {
        $List = new \Rbplmpg\Dao\DaoList( array('table'=>'wf_comment') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("anyobject_id='$uid'");
        return $List;
    }


} //End of class

