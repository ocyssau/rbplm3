<?php
//%LICENCE_HEADER%

namespace Rbplmpg;

use Rbplmpg\Dao as Dao;


/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (10, '\Rbplm\AnyObject', 'anyobject');
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\AnyObject
 * 
 * @see \Rbplmpg\Dao
 *
 */
class AnyObjectDaoPg extends Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'anyobject';
	
	/**
	 * As it is a abstract class, classId is not set.
	 * @var integer
	 */
	protected static $classId = 10;
	
	/**
	 * 
	 * @var array
	 */
	public static $sysToApp = array();
	
} //End of class
