<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys;


/**
 * $Id: daoTemplate.tpl 495 2011-06-25 14:27:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-06-25 16:27:27 +0200 (sam., 25 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 495 $
 */


/** SQL_SCRIPT>>
CREATE TABLE comments(
	uid uuid, 
	commented_id uuid, 
	title varchar(128), 
	body varchar(512)
);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (500, '\Rbplm\Sys\Comment', 'comments'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE comments ADD PRIMARY KEY (uid);
CREATE INDEX INDEX_comments_uid ON comments USING btree (uid);
CREATE INDEX INDEX_comments_commented_id ON comments USING btree (commented_id);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_comments AFTER INSERT OR UPDATE 
		   ON comments FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_comments AFTER DELETE 
		   ON comments FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

//require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for \Rbplm\Sys\Comment
 * 
 * See the examples: Rbplm/Sys/CommentTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Sys\CommentTest
 *
 */
class CommentDaoPg extends \Rbplmpg\Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'comments';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 500;
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
		$this->_stdInsertSelect = 'uid, commented_id, title, body';
		$this->_stdInsertValues = ':uid,:commentedId,:title,:body';
		$this->_stdLoadSelect = 'uid, commented_id AS "commentedId", title, body';
	} //End of function
	
	

	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Sys\Comment	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = true )
	{
		if($fromApp){
			$mapped->setUid(\Rbplm\Uuid::format($row['uid']));
			$mapped->setCommentedId($row['commentedId']);
			$mapped->setTitle($row['title']);
			$mapped->setBody($row['body']);
		}
		else{
			$mapped->setUid(\Rbplm\Uuid::format($row['uid']));
			$mapped->setCommentedId($row['commented_id']);
			$mapped->setTitle($row['title']);
			$mapped->setBody($row['body']);
		}
	} //End of function


	/**
	 * @param \Rbplm\Sys\Comment   $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;

		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					uid = :uid,
					commented_id = :commentedId,
					title = :title,
					body = :body
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table ($this->_stdInsertSelect) VALUES ($this->_stdInsertValues)";
		}
		
		$bind = array(
				    ':uid'=>$mapped->getUid(),
					':commentedId'=>$mapped->getCommentedId(),
					':title'=>$mapped->getTitle(),
					':body'=>$mapped->getBody()
		);
		
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
	}


} //End of class

