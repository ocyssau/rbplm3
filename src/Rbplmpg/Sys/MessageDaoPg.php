<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys;


/**
 * $Id: MessageDaoPg.php 806 2012-04-24 13:17:05Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Sys/MessageDaoPg.php $
 * $LastChangedDate: 2012-04-24 15:17:05 +0200 (mar., 24 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 806 $
 */

/** SQL_SCRIPT>>
CREATE TABLE messages_mailbox (
	id bigint NOT NULL,
	uid character(13) NOT NULL,
    owner uuid NOT NULL,
    subject text,
    body text,
    date date,
    fromuser uuid,
    touser text,
    is_read boolean,
	is_replied boolean,
  	is_flagged boolean,
  	priority integer,
  	type integer,
    PRIMARY KEY  (id)
);

CREATE SEQUENCE messages_mailbox_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Be careful to let command on many lines
CREATE TABLE messages_sent ( 
	LIKE messages_mailbox INCLUDING INDEXES 
);

CREATE SEQUENCE messages_sent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
    
-- Be careful to let command on many lines
CREATE TABLE messages_archive ( 
	LIKE messages_mailbox INCLUDING INDEXES 
);

CREATE SEQUENCE messages_archive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE messages_mailbox ALTER COLUMN id SET DEFAULT nextval('messages_mailbox_id_seq'::regclass);
ALTER TABLE messages_sent ALTER COLUMN id SET DEFAULT nextval('messages_sent_id_seq'::regclass);
ALTER TABLE messages_archive ALTER COLUMN id SET DEFAULT nextval('messages_archive_id_seq'::regclass);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE messages_mailbox;
DROP TABLE messages_sent;
DROP TABLE messages_archive;
DROP SEQUENCE messages_mailbox_id_seq;
DROP SEQUENCE messages_sent_id_seq;
DROP SEQUENCE messages_archive_id_seq;
 <<*/


/**
 * @brief Dao for message. This dao may be use as replacement of \Zend_Mail_Transport object to use a postgresql db 
 * as mail transfert system.
 * 
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Sys/MessageTest.php
 *
 */
class MessageDaoPg extends \Rbplmpg\Dao
{
	
	/**
	 * @var string
	 */
	public static $table = 'messages_mailbox';
	
	/**
	 * Mailbox table name
	 * @static string
	 */
	public static $mailbox_table = 'messages_mailbox';
	
	/**
	 * Sent messages table name
	 * @static string
	 */
	public static $sentbox_table = 'messages_sent';
	
	
	/**
	 * Sent messages table name
	 * @static string
	 */
	public static $archive_table = 'messages_archive';
	
	public static $sysToApp = array(
							'uid'=>'uid',
							'owner'=>'owner',
							'fromuser'=>'userFrom',
							'touser'=>'userTo',
							'subject'=>'subject',
							'body'=>'body',
							'date'=>'date',
							'is_read'=>'isRead',
							'is_replied'=>'isReplied',
							'is_flagged'=>'isFlagged',
							'priority'=>'priority',
							'type'=>'type'
	);
	
	/**
	 * Constructor
	 *
	 * @param array					$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 *
	 */
	public function __construct( array $config = array() )
	{
		parent::__construct( $config, $conn );
		
		switch($config['type']){
			case \Rbplm\Sys\Message::TYPE_SEND:
				$this->_table = self::$sentbox_table;
				$this->_classId = 801;
				break;
			case \Rbplm\Sys\Message::TYPE_MAILBOX:
				$this->_table = self::$mailbox_table;
				$this->_classId = 802;
				break;
			case \Rbplm\Sys\Message::TYPE_ARCHIVE:
				$this->_table = self::$archive_table;
				$this->_classId = 803;
				break;
		}
		
		$this->_stdInsertSelect = 'uid, message_id, owner, fromuser, touser, subject, body, date, is_read, is_replied, is_flagged, priority, type';
		$this->_stdInsertValues = ':uid, :messageId, :owner, :userFrom, :userTo, :subject, :body, :date, :isRead, :isReplied, :isFlagged, :priority, :type';
		//$this->_stdLoadSelect = $this->_stdInsertSelect;
	}
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Sys\Message	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->setUid($row['uid']);
			$mapped->setMessageId($row['id']);
			$mapped->setFrom($row['userFrom']);
			$mapped->addTo( \Rbplmpg\Dao::multivalToApp($row['userTo']) );
			$mapped->setSubject($row['subject']);
			$mapped->setBodyHtml($row['body']);
			$mapped->setDate($row['date']);
			$mapped->isRead = $row['isRead'];
			$mapped->isReplied = $row['isReplied'];
			$mapped->isFlagged = $row['isFlagged'];
			$mapped->priority = $row['priority'];
			$mapped->owner = $row['mailboxOwner'];
			$mapped->type = $row['type'];
		}
		else{
			$mapped->setUid($row['uid']);
			$mapped->setMessageId($row['id']);
			$mapped->setFrom($row['from']);
			$mapped->addTo( \Rbplmpg\Dao::multivalToApp($row['to']) );
			$mapped->setSubject($row['subject']);
			$mapped->setBodyHtml($row['body']);
			$mapped->setDate($row['date']);
			$mapped->isRead = $row['is_read'];
			$mapped->isReplied = $row['is_replied'];
			$mapped->isFlagged = $row['is_flagged'];
			$mapped->priority = $row['priority'];
			$mapped->owner = $row['owner'];
			$mapped->type = $row['type'];
		}
	} //End of function
		
    /**
     * Send mail. The table must be set to self::$mailbox_table.
     * 
	 * @param \Rbplm\Sys\Message $mapped
     * @throws \Rbplm\Sys\Exception
     */
    public function send($mapped)
    {
    	$this->_table = self::$sentbox_table;
    	$mapped->type = \Rbplm\Sys\Message::TYPE_SEND;
    	
    	//Write send message in mail box of each recipient
    	$this->_saveSent($mapped);
    	//Write message in sentbox of sender user
    	$this->_saveObject($mapped);
    }
    
	/**
	 *
	 * @param \Rbplm\Sys\Message $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 */
	protected function _saveObject($mapped)
	{
        if(null === $mapped->getMessageId() ) {
        	$mapped->setMessageId();
        }
		
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					is_read=:isRead, 
					is_replied=:isReplied,
					is_flagged=:isFlagged,
					priority=:priority,
					type=:type
					WHERE id=:id";
			
			$bind = array(
						 ':isRead'=>(int) $mapped->isRead,
						 ':isReplied'=>(int) $mapped->isReplied,
						 ':isFlagged'=>(int) $mapped->isFlagged,
						 ':priority'=>(int) $mapped->priority,
						 ':type'=>(int) $mapped->type,
						 ':id'=>$mapped->pkey
			);
		}
		else{
			$sql = "INSERT INTO $table ($this->_stdInsertSelect) VALUES ($this->_stdInsertValues)";
			
			$bind = array(
						 ':uid'=>$mapped->getUid(),
						 ':messageId'=>$mapped->getMessageId(),
					     ':owner'=>$mapped->owner,
						 ':userFrom'=>$mapped->getFrom(),
//						 ':userTo'=>'{' . implode( ',', $mapped->getRecipients() ) . '}',
						 ':userTo'=>\Rbplmpg\Dao::multivalToPg( $mapped->getRecipients() ),
						 ':subject'=>$mapped->getSubject(),
						 ':body'=>$mapped->getBodyHtml()->getContent(),
						 ':date'=>time(),
						 ':isRead'=>(int) $mapped->isRead,
						 ':isReplied'=>(int) $mapped->isReplied,
						 ':isFlagged'=>(int) $mapped->isFlagged,
						 ':priority'=>(int) $mapped->priority,
						 ':type'=>(int) $mapped->type
			);
		}
		$stmt = $this->getConnexion()->prepare($sql);

//var_dump($bind, $mapped->getRecipients());

		$stmt->execute($bind);
		if( $mapped->pkey == 0 ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
	} //End of method
	
	
	/**
	 *
	 * @param \Rbplm\Sys\Message $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 */
	protected function _saveSent( $mapped )
	{
        if(null === $mapped->getMessageId() ) {
        	$mapped->setMessageId();
        }
		
		$table = self::$mailbox_table;
		
		$sql = "INSERT INTO $table (
					 $this->_stdInsertSelect)
				VALUES($this->_stdInsertValues)";
		
		$bind = array(
					 ':messageId'=>$mapped->getMessageId(),
					 ':userFrom'=>$mapped->getFrom(),
					 ':userTo'=>'{' . implode( ',', $mapped->getRecipients() ) . '}',
					 ':subject'=>$mapped->getSubject(),
					 ':body'=>$mapped->getBodyHtml()->getContent(),
					 ':date'=>time(),
					 ':isRead'=>(int) $mapped->isRead,
					 ':isReplied'=>(int) $mapped->isReplied,
					 ':isFlagged'=>(int) $mapped->isFlagged,
					 ':priority'=>(int) $mapped->priority,
					 ':type'=>(int) \Rbplm\Sys\Message::TYPE_MAILBOX
		);
		
		$stmt = $this->getConnexion()->prepare($sql);
		foreach($mapped->getRecipients() as $recipient){
			$bind[':owner'] = $recipient;
			$bind[':uid'] = uniqid();
			$stmt->execute($bind);
		}
	} //End of method
	
	
	/**
	 * Move Messages to archive
	 * 
	 * @param string $filter		sql filter
	 */ 
	public function archive($filter)
	{
		$table = $this->_table;
		
		$sql = 'INSERT INTO ' . self::$archive_table . ' SELECT * FROM ' . $table . ' WHERE ' . $filter;
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute();
		
		$sql = 'DELETE FROM ' . $table . ' WHERE ' . $filter;
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute();
	}
	
	
	/**
	 * Move forward to the next Message and get it from the database
	 * 
	 * @param \Rbplm\Sys\Message $mapped
	 * @param integer	$uid	Uid of current position
	 * @param string	$filter	OPTIONAL Sql filter string
	 * 
	 */
	public function loadNextMessage($mapped, $uid, $filter = '')
	{
		if (!$uid){
			throw new \Rbplm\Sys\Exception('BAD_PARAMETER_OR_EMPTY', \Rbplm\Sys\Error::WARNING, '$uid');
		}
		
		$table = $this->_table;
		
		if($filter){
			$filter = ' AND ' . $filter;
		}
		
		$sql = "SELECT * FROM $table WHERE id > (SELECT id FROM $table WHERE uid=:uid LIMIT 1)" . $filter . ' ORDER BY id ASC LIMIT 1';
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute( array(':uid'=>$uid ) );
		$row = $stmt->fetch();
		
		if($row){
			$this->loadFromArray($mapped, $row);
		}
		else{
			throw new \Rbplm\Sys\Exception('CAN_NOT_BE_LOAD', \Rbplm\Sys\Error::WARNING, 'uid:' . $uid, $sql);
		}
	}
	
	/**
	 * Move backward to the next Message and get it from the database
	 * 
	 * @param \Rbplm\Sys\Message $mapped
	 * @param integer	$uid	Uid of current position
	 * @param string	$filter	OPTIONAL Sql filter string
	 */
	public function loadPrevMessage($mapped, $uid, $filter = '')
	{
		if (!$uid){
			throw new \Rbplm\Sys\Exception('BAD_PARAMETER_OR_EMPTY', \Rbplm\Sys\Error::WARNING, '$uid');
		}
		
		$table = $this->_table;
		
		if($filter){
			$filter = ' AND ' . $filter;
		}
		
		$sql = "SELECT * FROM $table WHERE id < (SELECT id FROM $table WHERE uid=:uid)" . $filter . ' ORDER BY id ASC';
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute( array(':uid'=>$uid ) );
		$row = $stmt->fetch();
		
		if($row){
			$this->loadFromArray($mapped, $row);
		}
		else{
			throw new \Rbplm\Sys\Exception('CAN_NOT_BE_LOAD', \Rbplm\Sys\Error::WARNING, 'uid:' . $uid, $sql);
		}
	}
	
} //End of class
