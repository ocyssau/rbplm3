<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta;


/**
 * $Id: daoTemplate.tpl 507 2011-07-04 06:30:11Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-07-04 08:30:11 +0200 (lun., 04 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 507 $
 */

/** SQL_SCRIPT>>
CREATE SEQUENCE meta_type_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
	
CREATE TABLE meta_type(
	id integer, 
	uid uuid NOT NULL, 
	appname varchar(128) NOT NULL, 
	sysname varchar(128) NOT NULL, 
	description varchar(255), 
	type varchar(32) NOT NULL,
	length integer, 
	regex varchar(255),
	is_require boolean, 
	is_hide boolean,
	return_name boolean, 
	is_multiple boolean, 
	display_both boolean, 
	is_tiltinglist boolean, 
	is_autocomplete boolean,
	select_list varchar(255), 
	selectdb_where varchar(255), 
	selectdb_table varchar(255), 
	selectdb_field_for_value varchar(255), 
	selectdb_field_for_display varchar(255)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE meta_type ALTER COLUMN id SET DEFAULT nextval('meta_type_id_seq'::regclass);
ALTER TABLE meta_type ADD PRIMARY KEY (id);
ALTER TABLE meta_type ADD UNIQUE (uid);

ALTER TABLE meta_type ALTER COLUMN is_require SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_hide SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN return_name SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_multiple SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN display_both SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_tiltinglist SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_autocomplete SET DEFAULT false;

CREATE INDEX INDEX_meta_type_appname ON meta_type USING btree (appname);
CREATE INDEX INDEX_meta_type_sysname ON meta_type USING btree (sysname);
CREATE INDEX INDEX_meta_type_type ON meta_type USING btree (type);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP SEQUENCE IF EXISTS meta_type_id_seq;
 <<*/

//require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for \Rbplm\Sys\Meta\Type
 * 
 * See the examples: Rbplm/Sys/Meta/TypeTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Sys\Meta\TypeTest
 *
 */
abstract class TypeDaoPg extends \Rbplmpg\Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'meta_type';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 600;
	
	
	/**
	 * SQL type for column.
	 * Must be a valid type for postgresql:
	 * @see http://docs.postgresqlfr.org/8.4/datatype.html
	 * 
	 * @var string
	 */
	protected $_type = 'text';
	
	
	/**
	 * 
	 * @var integer
	 */
	protected $_sequence_name = 'meta_type_id_seq';
	
	
	public function createInTable($mapped, $table)
	{
		/*Check if type is not existing in table*/
		try{
			$sql = "SELECT $mapped->sysName FROM $table LIMIT 1";
			$this->_connexion->exec($sql);
		}catch(\Exception $e){
			/*column is not existing*/
			/*Add column*/
			$sql = "ALTER TABLE $table ADD COLUMN $mapped->sysName $this->_type;";
			$this->_connexion->exec($sql);
		}
	}
	
	
} //End of class

