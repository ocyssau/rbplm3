<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Loader;


//require_once('Rbplm/Sys/Meta/Loader/Interface.php');

/**
 * $Id: Csv.php 520 2011-07-18 22:32:13Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Loader/Csv.php $
 * $LastChangedDate: 2011-07-19 00:32:13 +0200 (Tue, 19 Jul 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 520 $
 */
    
final class Ods implements \Rbplm\Sys\Meta\Loader\LoaderInterface
{
	/**
	 * 
	 * @var string
	 */
	protected $_filename;
	
	/**
	 * @var array
	 */
	protected $_config;
	
	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['filename'] = Full path to ods source file
	 *
	 */
	public function __construct( array $config=array(), $conn=null )
	{
		$this->_config = $config;
		$this->_filename = $config['filename'];
	} //End of function
	
	
	/**
	 * Load a model in \Rbplm\Sys\Meta\Model.
	 * 
	 * @param \Rbplm\Sys\Meta\Model		$Metamodel
	 * @return void
	 */
	public function load( \Rbplm\Sys\Meta\Model $Metamodel )
	{
		if( !file_exists($this->_filename) ){
			throw new \Rbplm\Sys\Exception('FS_METAMODEL_NOTFOUND_%0%', \Rbplm\Sys\Error::WARNING, array($this->_filename) );
		}
		
		$lines = $this->_parse_ods($this->_filename);
		
		foreach($lines as $line){
			/*Define appClass, appClass Id, inherited app class, sys class, inherited sys class*/
			if($line[0] == 'OBJ_KLAS'){
				$appClass = trim($line[1]);
				$appInherits = trim($line[2]);
				$classId = trim($line[3]);
				$sysClass = trim($line[4]);
				$sysInherits = trim($line[5]);
				if( $Metamodel->sysClassIsExisting($sysClass) ){
					throw new exception("$sysClass is yet in model, check your model definition to suppress double definition");
				}
				if( $Metamodel->appClassIsExisting($appClass) ){
					throw new exception("$appClass is yet in model, check your model definition to suppress double definition");
				}
				$Metamodel->addClassMapping($appClass, $sysClass, $classId);
				continue;
			}
			else if($line[0] == 'FIN'){
				/*Populate with inherited properties*/
				if($appInherits){
					$Metamodel->setAppInherits($appClass, $appInherits);
				}
				if($sysInherits){
					$Metamodel->setSysInherits($sysClass, $sysInherits);
				}
				/*Next class def*/
				unset($appClass, $sysClass, $appPropertyName, $sysPropertyName, $toSysFilter, $toAppFilter);
				continue;
			}
			/*Extract names of various parameters from the DTYP line declaration*/
			else if($line[0] == 'DTYP'){
				$dtypes = array();
				for ($i = 0; $i < count($line); $i++) {
					$dtypes[] = trim($line[$i]);
				}
				continue;
			}
			/*combime parameter with dtyp name and filter inputs*/
			else if($line[0] != 'DTYP'){
				if( !isset($appClass) || !isset($sysClass) || !$appClass || !$sysClass ){
					continue;
				}
				/*For each element on line, extract name of dtyp and affect the current value*/
				$keys = array();
				for ($i = 1; $i < count($dtypes); $i++) {
					if( !isset($line[$i]) || !isset($dtypes[$i]) ){
						continue;
					}
					$dtypName = $dtypes[$i];
					$dtypValue = trim($line[$i]);
					/* Convert boolean */
		        	/*replace true, false, y, n, yes, no by boolean*/
					if( in_array($dtypName, array('isagregation', 'iscomposition', 'iscollection', 'notnull', 'isindex', 'isuniq', 'ispkey', 'isautoincrement') ) ){
						if( $dtypValue == 'true' || $dtypValue == 'y' || $dtypValue == 'yes'){
							$dtypValue = true;
						}
						if( $dtypValue == 'false' || $dtypValue == 'n' || $dtypValue == 'no'){
							$dtypValue = false;
						}
						$dtypValue = (boolean) $dtypValue;
					}
					$keys[$dtypName] = $dtypValue;
				}
				if( !$keys['appName'] && !$keys['sysName'] ){ //empty line
					continue;
				}
				if( !$keys['appName'] ){
					var_dump($keys);
					throw new \Rbplm\Sys\Exception(sprintf('appName DTYP for %s is not define in model %s for class %s', $keys['sysName'], $this->_filename, $appClass), null, $keys);
				}
				if( !$keys['sysName'] ){
					var_dump($keys);
					throw new \Rbplm\Sys\Exception(sprintf('sysName DTYP for %s is not define in model %s for class %s', $keys['appName'], $this->_filename, $appClass), null, $keys);
				}
				$Metamodel->addAppProperty($appClass, $keys);
				$Metamodel->addSysProperty($sysClass, $keys);
			}
		}
	} //End of method
	
	
	/**
	 * Extremely naive implementation of a open office document parser.
	 * Return a array of rows as array of columns values.
	 * 
	 * @param string $filename
	 * @param integer $colLimit Limit the number of columns to parse.
	 * @return array
	 */
    private function _parse_ods($filename, $colLimit = 50)
    {
        $return = array();
        $simpleXml = simplexml_load_file('zip://' . $filename . '#content.xml');
		$domNode = dom_import_simplexml($simpleXml);
		$dom = new \DOMDocument('1.0', 'UTF-8');
		$domNode = $dom->importNode($domNode, true);
		$dom->appendChild($domNode);
		$xpobject = new \DOMXPath($dom);
		
		$sheets = $xpobject->evaluate('//office:document-content/office:body/office:spreadsheet/table:table');
		$sheet = $sheets->item(0);
		//var_dump( $sheet->attributes->getNamedItem('name')->value );die;
		
		if( $xRows = $xpobject->evaluate('table:table-row', $sheet) ){
			foreach ($xRows as $xRow) {
	        	$aRow = array();
	        	if( $xCells = $xpobject->evaluate('table:table-cell', $xRow) ){
	        		foreach($xCells as $xCell){
	        			$cell = $xpobject->evaluate('text:p', $xCell)->item(0);
	        			if( is_object($cell) ){
		        			$cellContent = $cell->nodeValue;
	        			}
	        			else{
	        				$cellContent = '';
	        			}
	        			//if text is found in cell
			            $aRow[] = trim( strip_tags( $cellContent ) );
	        			//retrieve repeat paramter to fill the output array with value for each cells
	        			if($xCell->hasAttributes()){
							$attributes = $xCell->attributes;
							if(!is_null($attributes)){
								$value = $attributes->getNamedItem('number-columns-repeated');
								if( is_object($value) ){
									if($repeated = (int)$value->value){
										if($repeated > $colLimit){
											$repeated = $colLimit;
										}
										//$merge = array_fill(count($aRow), $repeated-1, $cellContent);
										$merge = array_fill(0, $repeated-1, $cellContent);
										$aRow = array_merge( $aRow, $merge );
									}
								}
							}
						}
		            	
	        		}
	        	}
	            if($aRow){
		            $return[] = $aRow;
	            }
			}
		}
        return $return;
    }
}

