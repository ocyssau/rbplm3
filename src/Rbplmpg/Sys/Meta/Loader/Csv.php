<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Loader;


/**
 * $Id: Csv.php 672 2011-11-01 23:34:31Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Loader/Csv.php $
 * $LastChangedDate: 2011-11-02 00:34:31 +0100 (mer., 02 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 672 $
 */

//require_once('Rbplm/Sys/Meta/Loader/Interface.php');

/**
 * @brief Load the metamodel from csv file.
 * 
 * Assume this entry csv format string:
 * 
 * OBJ_KLAS,Rbplm_Model_Class_Name,Rbplm_Model_Class_Name_Inherits,cid,db_table_name,db_table_name_inherit_from.
 * DTYP,key,a2s,s2a,att,
 * ,RbplmPropertyName,filter_to_apply_on_store,filter_function_to_apply_on_load,in_table_field_name
 * FIN,,,,
 * 
 * In this format, line beggining by OBJ_KLAS define the class mapping:
 * 		column 1 : Class name in Rbplm application
 * 		column 2 : The classe inherits by Rbplm_Class
 * 		column 3 : The identifiant for the Rbplm_Class. Used in db to create relation.
 * 		column 4 : The table name to query for SQL db, the classe name for LDAP db, or other parameter needs by persistence objects system 
 * 		column 5 : The parent table for SQL engine with inherits functionnalities, or parent of class for LDAP db.
 * 		others columns are usables for futur usages.
 * 
 * 
 * Lines beggining by DTYP, define the header for parameters names.
 * The next line must define the value of each parameters define by DTYP.
 * There is not pre-define names for DTYP except for 
 * 	appName = Property name in Rbplm application
 * 	sysName = Property name in persistence system
 * 
 * The word "FIN" mark end of a class mapping definition bloc.
 * 
 * Example:
 * @code
 * OBJ_KLAS,\Rbplm\Org\Unit,\Rbplm\AnyObject,105,org_unit,anyobjects
 * DTYP,key,a2s,s2a,att,type,default,constraint,length,index,foreignKey,foreignTable
 * ,uid,,\Rbplm\Uuid::format,uid
 * ,name,,,name
 * ,description,,,description
 * ,owner,,,owner
 * ,parentId,,,parent
 * FIN,,,,
 * @endcode
 *
 */
final class Csv implements \Rbplm\Sys\Meta\Loader\LoaderInterface
{
	/**
	 * 
	 * @var string
	 */
	protected $_filename;
	
	/**
	 * 
	 * @var \SplFileObject
	 */
	protected $_file;
	
	/**
	 * @var array
	 */
	protected $_config;
	
	/**
	 * CSV delimiter
	 * @var string
	 */
	protected $_delimiter;
	
	/**
	 * CSV enclosure
	 * @var string
	 */
	protected $_enclosure;
	
	
	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['filename'] = Full path to csv source file
	 *
	 */
	public function __construct( array $config=array(), $conn=null )
	{
		$this->_config = $config;
		$this->_filename = $config['filename'];
		$this->_file = new \SplFileObject($this->_filename, 'rw', true);
		$this->_file->setFlags(\SplFileObject::SKIP_EMPTY);
		
		$this->_delimiter = ',';
		$this->_enclosure = '"';
		//$this->_file->setFlags(\SplFileObject::READ_CSV);
	} //End of function
	
	
	/**
	 * Load a model in \Rbplm\Sys\Meta\Model.
	 * 
	 * @param \Rbplm\Sys\Meta\Model		$Metamodel
	 * @return void
	 */
	public function load( \Rbplm\Sys\Meta\Model $Metamodel )
	{
		if( !file_exists($this->_filename) ){
			throw new \Rbplm\Sys\Exception('FS_METAMODEL_NOTFOUND', \Rbplm\Sys\Error::WARNING, $this->_filename);
		}
		
		while( !$this->_file->eof() ){
			$line = $this->_file->fgetcsv($this->_delimiter, $this->_enclosure, '\\');
			
			if($line[0] == 'OBJ_KLAS'){
				$appClass = trim($line[1]);
				$appInherits = trim($line[2]);
				$classId = trim($line[3]);
				$sysClass = trim($line[4]);
				$sysInherits = trim($line[5]);
				
				if( $Metamodel->sysClassIsExisting($sysClass) ){
					throw new exception("$sysClass is yet in model, check your model definition to suppress double definition");
				}
				if( $Metamodel->appClassIsExisting($appClass) ){
					throw new exception("$appClass is yet in model, check your model definition to suppress double definition");
				}
				
				$Metamodel->addClassMapping($appClass, $sysClass, $classId);
			}
			else if($line[0] == 'FIN'){
				unset($appClass, $sysClass, $appPropertyName, $sysPropertyName, $toSysFilter, $toAppFilter);
				continue;
			}
			/*Extract names of various parameters from the DTYP line declaration*/
			else if($line[0] == 'DTYP'){
				$dtypes = array();
				for ($i = 0; $i < count($line); $i++) {
					$dtypes[] = trim($line[$i]);
				}			
				continue;
			}
			/*combime parameter with dtyp name and filter inputs*/
			else if($line[0] != 'DTYP'){
				if( !$appClass || !$sysClass ){
					continue;
				}
				/*For each element on line, extract name of dtyp and affect the current value*/
				$keys = array();
				for ($i = 1; $i < count($dtypes); $i++) {
					$dtypName = $dtypes[$i];
					$dtypValue = trim($line[$i]);
					/* Convert boolean */
		        	/*replace true, false, y, n, yes, no by boolean*/
					if( in_array($dtypName, array('isagregation', 'iscomposition', 'iscollection', 'notnull', 'isindex', 'isuniq', 'ispkey', 'isautoincrement') ) ){
						if( $dtypValue == 'true' || $dtypValue == 'y' || $dtypValue == 'yes'){
							$dtypValue = true;
						}
						if( $dtypValue == 'false' || $dtypValue == 'n' || $dtypValue == 'no'){
							$dtypValue = false;
						}
						$dtypValue = (boolean) $dtypValue;
					}
					$keys[$dtypName] = $dtypValue;
				}
				
				if( !$keys['appName'] ){
					throw new \Rbplm\Sys\Exception(sprintf('appName DTYP for %s is not define in model %s for class %s', $keys['sysName'], $this->_filename, $appClass), null, $keys);
				}
				
				if( !$keys['sysName'] ){
					throw new \Rbplm\Sys\Exception(sprintf('sysName DTYP for %s is not define in model %s for class %s', $keys['appName'], $this->_filename, $appClass), null, $keys);
				}
				$Metamodel->addAppProperty($appClass, $keys);
				$Metamodel->addSysProperty($sysClass, $keys);
			}
			if($appInherits){
				$Metamodel->setAppInherits($appClass, $appInherits);
			}
			if($sysInherits){
				$Metamodel->setSysInherits($sysClass, $sysInherits);
			}
		}
		
	} //End of method
	
} //End of class


