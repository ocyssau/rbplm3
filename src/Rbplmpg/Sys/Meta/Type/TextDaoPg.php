<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Type;


/**
 * $Id: daoTemplate.tpl 507 2011-07-04 06:30:11Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-07-04 08:30:11 +0200 (lun., 04 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 507 $
 */


/** SQL_SCRIPT>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Sys\Meta\Type\Text
 * 
 * See the examples: Rbplm/Sys/Meta/Type/TextTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Sys\Meta\Type\TextTest
 *
 */
class TextDaoPg extends \Rbplm\Sys\Meta\TypeDaoPg
{

	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 601;
	
	
	public static $sysToApp = array('uid'=>'uid', 'id'=>'pkey', 'appname'=>'appName', 'sysname'=>'sysName', 'description'=>'description', 'type'=>'type', 'is_require'=>'isRequire', 'is_hide'=>'isHide', 'length'=>'length', 'regex'=>'regex');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Sys\Meta\Type\Text	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->setUid($row['uid']);
			$mapped->pkey = $row['pkey'];
			$mapped->appName = $row['appName'];
			$mapped->sysName = $row['sysName'];
			$mapped->description = $row['description'];
			$mapped->type = $row['type'];
			$mapped->isRequire($row['isRequire']);
			$mapped->isHide($row['isHide']);
			$mapped->length = $row['length'];
			$mapped->regex = $row['regex'];
		}
		else{
			$mapped->setUid($row['uid']);
			$mapped->pkey = $row['id'];
			$mapped->appName = $row['appname'];
			$mapped->sysName = $row['sysname'];
			$mapped->description = $row['description'];
			$mapped->type = $row['type'];
			$mapped->isRequire($row['is_require']);
			$mapped->isHide($row['is_hide']);
			$mapped->length = $row['length'];
			$mapped->regex = $row['regex'];
		}
	} //End of function
	
	
	/**
	 * @param \Rbplm\Sys\Meta\Type\Text   $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					uid = :uid,
					appname = :appName,
					sysname = :sysName,
					description = :description,
					type = :type,
					is_require = :isRequire,
					is_hide = :isHide,
					length = :length,
					regex = :regex
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table (uid,appname,sysname,description,type,is_require,is_hide,length,regex) VALUES (:uid,:appName,:sysName,:description,:type,:isRequire,:isHide,:length,:regex)";
		}
		
		$bind = array(
				':uid'=>$mapped->getUid(),
					':appName'=>$mapped->appName,
					':sysName'=>$mapped->sysName,
					':description'=>$mapped->description,
					':type'=>$mapped->type,
					':isRequire'=>(integer) $mapped->isRequire(),
					':isHide'=>(integer) $mapped->isHide(),
					':length'=>$mapped->length,
					':regex'=>$mapped->regex
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	

} //End of class

