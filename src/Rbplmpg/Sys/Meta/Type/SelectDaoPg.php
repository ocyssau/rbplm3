<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Type;


/**
 * $Id: daoTemplate.tpl 507 2011-07-04 06:30:11Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-07-04 08:30:11 +0200 (lun., 04 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 507 $
 */


/** SQL_SCRIPT>>
 <<*/

//require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for \Rbplm\Sys\Meta\Type\Select
 * 
 * See the examples: Rbplm/Sys/Meta/Type/SelectTest.php
 * 
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Sys\Meta\Type\SelectTest
 *
 */
class SelectDaoPg extends \Rbplmpg\Dao
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'meta_type';
	
	/**
	 * 
	 * @var integer
	 */
	protected static $classId = 610;
	
	
	public static $sysToApp = array('length'=>'length', 'return_name'=>'isReturnName', 'is_multiple'=>'isMultiple', 'select_list'=>'selectList', 'display_both'=>'isDisplayBoth', 'is_tiltinglist'=>'isTiltingList', 'is_autocomplete'=>'isAutocomplete');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in \Rbplm\Dao\Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param \PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbplm\Sys\Meta\Type\Select	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->length = $row['length'];
			$mapped->isReturnName($row['isReturnName']);
			$mapped->isMultiple($row['isMultiple']);
			$mapped->selectList = $row['selectList'];
			$mapped->isDisplayBoth($row['isDisplayBoth']);
			$mapped->isTiltingList($row['isTiltingList']);
			$mapped->isAutocomplete($row['isAutocomplete']);
		}
		else{
			$mapped->length = $row['length'];
			$mapped->isReturnName($row['return_name']);
			$mapped->isMultiple($row['is_multiple']);
			$mapped->selectList = $row['select_list'];
			$mapped->isDisplayBoth($row['display_both']);
			$mapped->isTiltingList($row['is_tiltinglist']);
			$mapped->isAutocomplete($row['is_autocomplete']);
		}
	} //End of function
	
	
	/**
	 * @param \Rbplm\Sys\Meta\Type\Select   $mapped
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					length = :length,
					return_name = :isReturnName,
					is_multiple = :isMultiple,
					select_list = :selectList,
					display_both = :isDisplayBoth,
					is_tiltinglist = :isTiltingList,
					is_autocomplete = :isAutocomplete
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table (length,return_name,is_multiple,select_list,display_both,is_tiltinglist,is_autocomplete) VALUES (:length,:isReturnName,:isMultiple,:selectList,:isDisplayBoth,:isTiltingList,:isAutocomplete)";
		}
		
		$bind = array(
					':length'=>$mapped->length,
					':isReturnName'=>(integer) $mapped->isReturnName(),
					':isMultiple'=>(integer) $mapped->isMultiple(),
					':selectList'=>$mapped->selectList,
					':isDisplayBoth'=>(integer) $mapped->isDisplayBoth(),
					':isTiltingList'=>(integer) $mapped->isTiltingList(),
					':isAutocomplete'=>(integer) $mapped->isAutocomplete()
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	

} //End of class

