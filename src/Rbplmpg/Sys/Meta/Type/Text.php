<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Type;


/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

//require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Text
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Sys/Meta/Type/TextTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Text extends \Rbplm\Sys\Meta\Type
{
	
	/**
     * @var string
     */
    public $type = \Rbplm\Sys\Meta\Type::TYPE_TEXT;
	

    /**
     * @var integer
     */
    public $length = null;

    /**
     * @var string
     */
    public $regex = null;

}



