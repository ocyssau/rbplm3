<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Type;


/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

//require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Select
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Sys/Meta/Type/SelectTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Select extends \Rbplm\Sys\Meta\Type
{
	
    /**
     * @var string
     */
    public $type = \Rbplm\Sys\Meta\Type::TYPE_SELECT;
	

    /**
     * @var integer
     */
    protected $length = null;

    /**
     * @var boolean
     */
    protected $_isReturnName = null;

    /**
     * @var boolean
     */
    protected $_isMultiple = null;

    /**
     * @var string
     */
    protected $selectList = null;

    /**
     * @var boolean
     */
    protected $_isDisplayBoth = null;

    /**
     * @var boolean
     */
    protected $_isTiltingList = null;

    /**
     * @var boolean
     */
    protected $_isAutocomplete = null;

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isReturnName($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isReturnName = $bool;
                }
                else{
                	return $this->_isReturnName;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isMultiple($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isMultiple = $bool;
                }
                else{
                	return $this->_isMultiple;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isDisplayBoth($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isDisplayBoth = $bool;
                }
                else{
                	return $this->_isDisplayBoth;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isTiltingList($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isTiltingList = $bool;
                }
                else{
                	return $this->_isTiltingList;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isAutocomplete($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isAutocomplete = $bool;
                }
                else{
                	return $this->_isAutocomplete;
                }
    }


}



