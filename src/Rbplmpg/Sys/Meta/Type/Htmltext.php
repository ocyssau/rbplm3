<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Type;


/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

//require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Htmltext
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Sys/Meta/Type/HtmltextTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Htmltext extends \Rbplm\Sys\Meta\Type
{

    /**
     * @var string
     */
    public $type = \Rbplm\Sys\Meta\Type::TYPE_HTMLTEXT;
	
	
    /**
     * @var integer
     */
    protected $length = null;

    /**
     * @var string
     */
    protected $regex = null;


}



