<?php
//%LICENCE_HEADER%

namespace Rbplmpg\Sys\Meta\Type;


/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

//require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Longtext
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Sys/Meta/Type/LongtextTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Longtext extends \Rbplm\Sys\Meta\Type
{

    /**
     * @var string
     */
    public $type = \Rbplm\Sys\Meta\Type::TYPE_LONGTEXT;
	
	
    /**
     * @var integer
     */
    protected $length = null;

    /**
     * @var string
     */
    protected $regex = null;


}



