<?php
// %LICENCE_HEADER%
namespace Rbplm;

final class Helper
{

	const RBPLM_VER = '0.8.0.5Beta2';

	/* Current version of RBPLM */
	const RBPLM_BUILD = '%RBBUILD%';

	/* Build number of RBPLM */
	const RBPLM_COPYRIGHT = '&#169;2007-2014 by Olivier CYSSAU';

	/* Copyright */
	protected static $_instance = null;

	/**
	 * 
	 * @var array
	 */
	protected static $_fileTypes = null;

	/*
	 *  authorized file extensions 
	 *  @var array 
	 */
	protected static $_fileExtensions = null;

	/*
	 *  authorized visu file extensions 
	 *  @var array
	 */
	protected static $_visuFileExtensions = null;

	/**
	 * 
	 * @var array
	 */
	protected static $_session = null;

	/**
	 * 
	 * @var string
	 */
	protected static $_sessionNs = null;

	/**
	 * 
	 * @var array
	 */
	protected static $_config = false;

	/**
	 *
	 * @var \Zend\Log\Writer\AbstractWriter
	 */
	protected static $_logger = null;

	/**
	 * 
	 * @var array
	 */
	protected static $_fMessenger = array();

	/**
	 * 
	 * @var string
	 */
	protected static $_browser = "firefox";

	/**
	 * 
	 * @var string
	 */
	public static $_publicPath;

	/**
	 * Get the full version identification.
	 *
	 * @return string
	 */
	public static function getFullVersionId()
	{
		return self::RBPLM_VER . '-' . self::RBPLM_BUILD;
	}

	/**
	 * Get the full version identification.
	 *
	 * @return string
	 */
	public static function getCopyright()
	{
		return self::RBPLM_COPYRIGHT;
	}

	/**
	 * Get the config instance
	 *
	 * @return array|string
	 */
	public static function getConfig($key = null)
	{
		if ( $key ) {
			return self::$_config[$key];
		}
		else {
			return self::$_config;
		}
	}

	/**
	 * Set the config
	 *
	 * @param array $config
	 * @return void
	 */
	public static function setConfig(array $config)
	{
		self::$_config = $config;
	}
}
