<?php
//%LICENCE_HEADER%

namespace RbplmTest;

use Rbplm\Rbplm;
use Rbplm\Any;
use Rbplm\AnyObject;
use Rbplm\AnyPermanent;
use Rbplm\Uuid;
use Rbplm\Link;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 *
 * @include Rbplm/Org/UnitTest.php
 *
 */
class AnyTest extends \Rbplm\Test\Test
{
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
        var_dump(Rbplm::getApiFullVersion());
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }

    /**
     *
     */
    function Test_AnyUuid()
    {
        Uuid::init('UniqId');
        var_dump(Uuid::$type);
        $uid = Uuid::newUid();

        $any = new Any();
        $any->newUid();
        $any->setUid($uid);
        var_dump($any->getUid());
        var_dump(Uuid::$type);
    }

    /**
     *
     */
    function Test_Instanciate()
    {
        $any = new Any();
        $any->newUid();
         
        //Null values
        $any->setName(null);
        $a = $any->getArrayCopy();
        assert($a['name'] === null);

        $any->setName(0);
        $a = $any->getArrayCopy();
        assert($a['name'] === 0);

        $any->setName('');
        $a = $any->getArrayCopy();
        assert($a['name'] === '');

        //var_dump( serialize($any->getUid()));
        //var_dump( $any->getArrayCopy());
        //var_dump( $any->getUid());

        $any = AnyObject::init('FORTEST');

        //var_dump( $any );
        //var_dump( $any->getArrayCopy());
    }

    /**
     *
     */
    function Test_AnyObjectHydrator()
    {
        $any = new AnyObject();

        $properties=array(
            'ownerId'=>'user',
            'createById'=>'user1',
            'created'=>'2015-05-20',
            'updateById'=>'user2',
            'updated'=>'2015-06-01',
            'closeById'=>'user3',
            'closed'=>'2020-01-01',
        );
        $any->hydrate($properties);
        //var_dump( $any->getArrayCopy());
    }

    /**
     *
     */
    function Test_AnyBewitched()
    {
        $any = new AnyObject(array('name'=>'FORTEST'));
        //var_dump( $any->name );
        assert($any->name == 'FORTEST');
    }

    /**
     *
     */
    function Test_AnyPermanent()
    {
        $any = new AnyPermanent();
        $any = AnyPermanent::init('FORTEST');
        assert($any->name == 'FORTEST');
        //var_dump( $any );
    }

    /**
     *
     */
    function Test_AnyItem()
    {
        $parent = AnyPermanent::init('PARENT');
        $any = AnyPermanent::init('FORTEST', $parent);
        assert($any->getParent() == $parent);
        assert($any->getParent(true) === null);

        $parent = AnyPermanent::init('PARENT');
        $any = AnyPermanent::init('FORTEST')->setParent($parent);
        assert($any->getParent() == $parent);
        //var_dump($parent->getChildren());
        assert( $parent->getChildren()[$any->getUid()] == $any);

        //var_dump($any->getParentUid());
    }

    /**
     *
     */
    function Test_Link()
    {
        //Method 1
        $parent = AnyPermanent::init('PARENT');
        $child = AnyPermanent::init('CHILD');
        $link = Link::init($parent, $child);
         
        assert($link->getParent(true)===null);
        assert($link->getParentUid()==$parent->getUid());
        assert($link->getChild(true)===null);
        assert($link->getChildUid()==$child->getUid());

        //Method 2
        $link = new Link(array(
            'parentUid'=>'parentuid',
            'parentId'=>1,
            'childUid'=>'childuid',
            'childId'=>2,
        ));
        $link->newUid();
        assert($link->getParent(true)==1);
        assert($link->getParentUid()=='parentuid');

        //Method 3
        $link = new Link();
        $link->newUid();
        $link->hydrate(array(
            'parentUid'=>'parentuid',
            'parentId'=>1,
            'childUid'=>'childuid',
            'childId'=>2,
        ));
        assert($link->getParent(true)==1);
        assert($link->getParentUid()=='parentuid');
        assert($link->getChild(true)==2);
        assert($link->getChildUid()=='childuid');

        //var_dump(Link::hash($parent,$child));
        //var_dump(Link::hash($parent->getUid(),$child->getUid()));
        
        assert( Link::hash($parent,$child) == Link::hash($parent->getUid(),$child->getUid()) );
        
        $link->data = array('data1','data2');
        $link->index = 10;
        
        //var_dump($link);
    }

}


