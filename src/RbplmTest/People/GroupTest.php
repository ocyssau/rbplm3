<?php
//%LICENCE_HEADER%

namespace RbplmTest\People;

use Rbplm\People\Group;
use Rbplm\Dao\Factory as DaoFactory;

/**
 * @brief Test class for \Rbplm\People\Group
 * 
 * @include Rbplm/People/GroupTest.php
 */
class GroupTest extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\People\Group
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	    $name=uniqid('gtester');
		$this->object = Group::init( $name );
		$this->object->setDescription('descriptionOfgtester');
		$this->object->isActive(true);
		
		assert( $this->object->getName() == $name );
		assert( $this->object->getDescription() == 'descriptionOfgtester' );
		assert( $this->object->isActive() === true );
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 */
	function Test_Dao(){
		$myGroup = $this->object;
		
		$Dao=DaoFactory::getDao($myGroup);
		$Dao->save( $myGroup );
		
		$loadedGroup = new Group();
		$Dao->loadFromUid( $loadedGroup, $myGroup->getUid() );
		assert( $loadedGroup->getUid() ==  $myGroup->getUid() );
		
		echo "Create some groups \n";
		$group0 = Group::init( uniqid('group0') )->setParent($myGroup->getParent());
		$group1 = Group::init( uniqid('group1') )->setParent($myGroup->getParent());
		$group2 = Group::init( uniqid('group2') )->setParent($myGroup->getParent());
		$group3 = Group::init( uniqid('group3') )->setParent($myGroup->getParent());
		
		/*
		 * Create a group tree.
		 * $myGroup
		 * 		|$group0
		 * 		|$group1
		 * 		|$group2
		 * 			|$group3
		 */
		$myGroup->getGroups()->add($group0);
		$myGroup->getGroups()->add($group1);
		$myGroup->getGroups()->add($group2);
		$group2->getGroups()->add($group3);
		
		$Dao->save($group0);
		$Dao->save($group1);
		$Dao->save($group2);
		$Dao->save($group3);
		
		echo 'Names of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->getName(), $group1->getName(), $group2->getName(), $group3->getName() );
		echo 'Uids of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->getUid(), $group1->getUid(), $group2->getUid(), $group3->getUid() );
		
		/*Walk along the groups tree relation with a iterator*/
		self::_displayGroupTree($myGroup);
		
		$Dao->save($myGroup);
		
		$uid = $myGroup->getUid();
		$group0Name = $group0->getName();
		
		unset($group0);
		unset($group1);
		unset($group2);
		unset($group3);
		
		$this->setUp();
		$myGroup = $this->object;
		
		$Dao->loadFromUid($myGroup, $uid);
		self::_displayGroupTree($myGroup);
		
		/*
		$GroupList = new \Rbplm\Dao\Pg\DaoList( array('table'=>'view_people_group_links'), \Rbplm\Dao\Connexion::get() );
		$GroupList->loadInCollection($myGroup->getGroups(), "lparent='$uid' ORDER BY lindex ASC");
		
		self::_displayGroupTree($myGroup);
		
		var_dump( $myGroup->getGroups()->getByIndex(0)->getName(), $group0Name, $myGroup->getUid() );
		assert( $myGroup->getGroups()->getByIndex(0)->getName() == $group0Name );
		
		//In other style with helpers methods of DAO
		$this->setUp();
		$Dao->loadFromUid($myGroup, $uid);
		$Dao->getGroups($myGroup, true);
		assert( $myGroup->getGroups()->getByIndex(0)->getName() == $group0Name );
		*/
		
		/*
		 * Load groups, and all sub-groups until infinite depth
		 */
		$List = $Dao->loadGroupsRecursively($myGroup);
		foreach( $List as $row ){
			var_dump($row);
		}
		self::_displayGroupTree($myGroup);
	}
	
	
	protected static function _displayGroupTree( $group ){
		/*Walk along the groups tree relation with a iterator*/
    	$it = new \RecursiveIteratorIterator( $group->getGroups(), \RecursiveIteratorIterator::SELF_FIRST );
    	$it->setMaxDepth(100);
    	echo $group->getName() . CRLF;
    	foreach($it as $node){
    		echo str_repeat('  ', $it->getDepth()+1) . $node->getName() . CRLF;
    	}
	}
}
