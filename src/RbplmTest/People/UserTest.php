<?php
//%LICENCE_HEADER%

namespace RbplmTest\People;


/**
 * @brief Test class for \Rbplm\People\User
 * 
 * @include Rbplm/People/UserTest.php
 * 
 */
class UserTest extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\People\User
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$ParentOu = new \Rbplm\Org\Unit( array('name'=>uniqid('PeopleTests') ), \Rbplm\Org\Root::singleton() );
		$this->object = new \Rbplm\People\User( array('name'=>uniqid('tester')), $ParentOu );
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * @return unknown_type
	 */
	function Test_SetterGetter(){
		$ParentOu = new \Rbplm\Org\Unit( array('name'=>'PeopleTests'), \Rbplm\Org\Root::singleton() );
		$object = new \Rbplm\People\User( array('name'=>'tester'), $ParentOu );
		
		$object->setLogin('tester');
		assert( $object->getLogin() == 'tester' );
		
		//Login is same that name
		assert( $object->getName() == $object->getLogin() );
		
		$parent = $this->object->getParent();
	}
	
	
	/**
	 * 
	 */
	function Test_Preference(){
		$this->setUp();
		$this->object->login = uniqid('USERPREFSTEST');
		
		$prefs = $this->object->getPreference();
		
		foreach($prefs->getPreferences() as $key=>$value ){
			assert( true );
		}
		
		/*
		 * initialy, all is set to default
		 */
		assert( $prefs->lang == 'default' );
		
		$prefs->css_sheet = 'perso/mycss.css';
		$prefs->lang = 'fr';
		assert( $prefs->lang == 'fr' );
		
		/*
		 * If not enable user prefs, return always default
		 */
		$prefs->isEnable(false);
		assert( $prefs->lang == 'default' );
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	function Test_GroupAssociation(){
		//Get the group collection.
		$Groups = $this->object->getGroups();
		assert( $Groups->count() == 0 );
		
		$group0 = new \Rbplm\People\Group( 'group0', $this->object->getParent() );
		$group1 = new \Rbplm\People\Group( 'group1', $this->object->getParent() );
		$group2 = new \Rbplm\People\Group( 'group2', $this->object->getParent() );
		
		$Groups->add($group0, 'group');
		$Groups->add($group1, 'group');
		$Groups->add($group2, 'group');
	}
	
	
	/**
	 */
	function Test_Dao(){
		$this->setUp();
		$this->object->setLogin( uniqid('USERTEST') );
		
		/*Init the loader*/
		\Rbplm\Dao\Pg\Loader::setConnexion( \Rbplm\Dao\Connexion::get() );
		
		/*First save the parent OU*/
		try{
			$Parent = $this->object->getParent();
			$OuDao = new \Rbplm\Org\UnitDaoPg( array(), \Rbplm\Dao\Connexion::get() );
			$OuDao->save($Parent);
		}
		catch(\Exception $e){
			echo 'Parent is probably yet saved: ' . $e->getMessage() .CRLF;
			$OuDao->loadFromPath( $Parent, $Parent->getPath() );
			$this->object->setParent( $Parent );
		}
		
		/*Save object*/
		$UserDao = new \Rbplm\People\UserDaoPg( array(), \Rbplm\Dao\Connexion::get() );
		$UserDao->save( $this->object );
		
		/*Load in a new object*/
		$object = new \Rbplm\People\User();
		$UserDao->loadFromUid( $object, $this->object->getUid() );
		\Rbplm\Dao\Pg\Loader::loadParent( $object );
		assert( $object->getName() ==  $this->object->getName() );
		assert( \Rbplm\Uuid::compare($object->getParent()->getUid(), $this->object->getParent()->getUid()) );
		
		/*Create some groups*/
		$group0 = new \Rbplm\People\Group( array('name'=>uniqid('group0')), $this->object->getParent() );
		$group1 = new \Rbplm\People\Group( array('name'=>uniqid('group1')), $this->object->getParent() );
		$group2 = new \Rbplm\People\Group( array('name'=>uniqid('group2')), $this->object->getParent() );
		
		$GroupDao = new \Rbplm\People\GroupDaoPg( array() );
		$GroupDao->setConnexion( \Rbplm\Dao\Connexion::get() );
		$GroupDao->save($group0);
		$GroupDao->save($group1);
		$GroupDao->save($group2);
		
		/*Assign groups to user and save*/
		$Groups = $this->object->getGroups();
		$Groups->add($group0);
		$Groups->add($group1);
		$Groups->add($group2);
		$UserDao->save( $this->object );
		
		$uid = $this->object->getUid();
		
		/*Reload groups links in a list*/
		$List = new \Rbplm\Dao\Pg\DaoList( array('table'=>'view_people_group_links') );
		$List->setConnexion( \Rbplm\Dao\Connexion::get() );
		$List->load("lparent='$uid' ORDER BY lindex ASC");
		
		/*Convert list to collection*/
		$collection = new \Rbplm\Model\Collection();
		$List->loadInCollection($collection);
		
		/*Test if list is conform*/
		$i = 0;
		foreach($List as $entry){
//			var_dump( $entry['name'] , $Groups->getByIndex($i)->getName() );
			assert( $entry['name'] == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		/*Test if collection is conform*/
		$i = 0;
		foreach($collection as $current){
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			assert( get_class($current) == '\Rbplm\People\Group' );
			$i++;
		}
		
		
		/*In other style with helpers methods of DAO*/
		$this->setUp();
		$UserDao->loadFromUid($this->object, $uid);
		$UserDao->loadGroups($this->object, true);
		assert( $this->object->getGroups()->getByIndex(0)->getName() == $group0->getName() );
		
		/*
		 * Test user preferences DAO
		 */
		$PrefDao = new \Rbplm\People\User\PreferenceDaoPg();
		$PrefDao->setConnexion( \Rbplm\Dao\Connexion::get() );
		$prefs = $this->object->getPreference();
		
		$prefs->css_sheet = 'perso/mycss.css';
		$prefs->lang = 'fr';
		$prefs->isEnable(true);
		$PrefDao->save($prefs);
		
		$prefs2 = new \Rbplm\People\User\Preference( $prefs->getOwner() );
		$PrefDao->loadFromOwner($prefs2, $prefs2->getOwner() );

//var_dump( $prefs2->lang , $prefs->lang );
//var_dump( $prefs2->getPreferences() );

		assert($prefs2->lang == $prefs->lang);
		assert($prefs2->css_sheet == $prefs->css_sheet);
		
		/*
		 * Test update preference
		 */
		$prefs->lang = 'en';
		$PrefDao->save($prefs);
		
		$prefs2 = new \Rbplm\People\User\Preference( $prefs->getOwner() );
		$PrefDao->loadFromOwner($prefs2, $prefs2->getOwner() );
		
//var_dump( $prefs2->lang , $prefs->lang );
		assert($prefs2->lang == $prefs->lang);
		
		return;
		
		/*
		 * *************************************************************************************
		 * And with \Rbplm\Model\CollectionListBridge:
		 * *************************************************************************************
		 */
		//Load list in collection
		$object = new \Rbplm\People\User();
		$Collection = $object->getLinks();
		$CollectionDao = new \Rbplm\Model\CollectionListBridge($Collection, $List);
		
		
		//test if dao collection is conform
		$i=0;
		foreach($CollectionDao as $key=>$current){
			var_dump( $key, $current->getName() );
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		
		//test if collection is conform
		$i=0;
		foreach($Collection as $key=>$current){
			var_dump( $key, $current->getName() );
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		assert($Collection->count() == 3);
		
	}
	
	function _Test_DaoLoadMethode1()
	{
	    $User = CurrentUser::get();
	
	    /**************************************************************************************************************/
	    /* How to load ACLs. */
	    /*First, clean Acl and reset Org_Root and currentUser groups, just for test, not used in real life*/
	    //$this->setUp();
	     
	    /* How to load ACLs. METHODE 1:*/
	     
	    echo "Reload previous Groups, Org_Ou and User, and retrieve ACL from Db. \n";
	    $Links = DaoFactory::getDao($User)->getLinksRecursively( $User, 'cid=120 OR cid=110' );
	    foreach($Links as $row){
	        var_dump($row);
	    }
	     
	    /*
	     * Load groups of the user, second parameter of getGroups is set to true to load in internal groups collection of \Rbplm\People\User
	    * else return a simple list Rbplm_Dao_List object.
	    */
	    echo "Load groups of the user \n";
	    echo 'Tree of groups after getCurrentUser:' . CRLF;
	    self::_displayCollectionTree( $User->getGroups() );
	     
	    DaoFactory::get($User)->getGroups( $User, true );
	    echo 'Tree of groups after getGroups:' . CRLF;
	    self::_displayCollectionTree( $User->getGroups() );
	    
	    /*
	     * To load all sub-groups for each groups of the user, use getGroupsRecursively
	    */
	    foreach($User->getGroups() as $group){
	        DaoFactory::get($group)->loadGroupsRecursively( $group );
	    }
	    echo 'Tree of groups after loadGroupsRecursively:' . CRLF;
	    self::_displayCollectionTree( $User->getGroups() );
	}
	
}


