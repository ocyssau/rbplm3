<?php
//%LICENCE_HEADER%

namespace RbplmTest\Model;

use Rbplm\AnyObject;
use Rbplm\Org;
use Rbplm\Dao\Factory as DaoFactory;

/**
 * @brief Test class for \Rbplm\AnyObject.
 * 
 * @include Rbplm/Model/ComponentTest.php
 * 
 */
class ComponentTest extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\AnyObject
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$name = 'Tested';
		$this->object = AnyObject::init($name)->setParent(Org\Root::singleton());
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * 
	 */
	function Test_DaoLinks()
	{
		$Parent = \Rbplm\Org\Root::singleton();

		$Component = AnyObject::init(uniqId('testLinksComponent'))->setParent($Parent);
		
		$lchildComp1 = AnyObject::init(uniqId('testlchildComponent1_'))->setParent($Parent);

		$lchildComp2 = AnyObject::init(uniqId('testlchildComponent2_'))->setParent($Parent);

		$Component->addLink($lchildComp1, 'linkType1')
				  ->addLink($lchildComp2, 'linkType1');
		
		$Dao = DaoFactory::getDao($Component);
		
		$Dao->save($lchildComp1);
		$Dao->save($lchildComp2);
		$Dao->save($Component);
		
		$uid1 = $Component->getUid();
		$L1Uid = $lchildComp1->getUid();
		$L2Uid = $lchildComp2->getUid();
		
		//Reload links
		$Component = new AnyObject();
		$lchildComp1 = null;
		$lchildComp2 = null;
		
		$Dao->loadFromUid($Component, $uid1);
		
		return;
		
		$Dao->loadLinks($Component, '\Rbplm\AnyObject');
		
		$L1 = $Component->getLinks()->getByIndex(0);
		$L2 = $Component->getLinks()->getByIndex(1);
		
		assert( $L1->getUid() == $L1Uid );
		assert( $L2->getUid() == $L2Uid );
		
		foreach($Component->getLinks() as $l){
			var_dump( $l->getName() );
		}
	}
	
	
	/**
	 */
	function Test_Uuid()
	{
		echo 'Uuid format of \Rbplm\Uuid::newUid: ' . CRLF;
		var_dump( \Rbplm\Uuid::newUid() );
		
		echo 'Uuid format of getUid return: ' . CRLF;
		$component = AnyObject::init();
		var_dump( $component->getUid() );
		
		echo 'Uuid format of getUid return after set uuid: ' . CRLF;
		$component->setUid('{ree5456454-4464rtyrt454-456465}');
		var_dump( $component->getUid() );
	}
	
	/**
	 * 
	 */
	function Test_ExtendProperties()
	{
		$Component = AnyObject::init('component001');
		$Component->notdefined = 'value001';
		assert($Component->notdefined === 'value001');
	}
	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_SerializeUnserialize(){
		$name = 'serialized';
		$Component = AnyObject::init($name)->setParent( Org\Root::singleton() );
		
		$serialized = serialize($Component);
		//var_dump($serialized);
		$Component2 = unserialize($serialized);
		//var_dump( $Component2 );
		assert( $Component->getName() == $Component2->getName() );
		assert( $Component->getUid() == $Component2->getUid() );
		
		$xmlSerialized = $Component->xmlSerialize();
		var_dump( $xmlSerialized );
	}

	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_GetParent(){
		$ret = $this->object->getParent();
		assert( $ret instanceof Org\Root );
	}

	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_SetParent(){
	}

	
	/**
	 * From class: \Rbplm\AnyObject
	 */
	function Test_Label()
	{
		$this->object->setLabel('invalid label:accentué, virgule, spaces, tiret-haut');
		var_dump( $this->object->getLabel() );
		assert( $this->object->label === 'invalid_labelaccentu_virgule_spaces_tiret_haut' );
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_CloneAndNewUid()
	{
	    $clone = clone($this->object);
	    var_dump($clone->getUid(), $this->object->getUid());
	    assert($clone->getUid() <> $this->object->getUid() );
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_GetName()
	{
	    $ret = $this->object->getName();
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_SetName()
	{
	    $ret = $this->object->setName($name);
	}
	
	/**
	 * From class: \Rbplm\Any
	 */
	function Test_GetUid()
	{
	    $ret = $this->object->getUid();
	}

	/**
	 * 
	 */
	function Test_Structure10()
	{
		$this->setUp();
		$o2 = $this->object;
		
		echo "Create a depth = 10 structure \n";
		$i=0;
		while($i < 10)
		{
			$o1 = AnyObject::init('Parent '.$i)->setParent($o2);
			//Populate with 100 nodes
			$j=0;
			while($j < 100){
				$oj = AnyObject::init('Parent '.$i.'_'.$j)->setParent($o1);
				$j++;
			}
			$o2 = $o1;
			$i++;
		}
		
		/*
		For deep structure, php may block execution on nested function getPath(true), and return message like:
		Fatal error: Maximum function nesting level of '100' reached, aborting!
		
		To squize this error, increase the depth nesting level parameter and be carefull to circular references
		*/
	}
	
	/**
	 *
	 */
	function Test_Structure1000()
	{
	    $this->setUp();
	
	    echo "Create a struture with 1000 childs nodes \n";
	    $i=0;
	    while($i < 1000){
	        $o3 = AnyObject::init('Node_' . $i)->setParent( $this->object ); //create double relation;
	        $o3 = null;
	        $i++;
	    }
	}
	
	
	public function Test_StructureQueue()
	{
	    /* Test with a simple queue of strings
	     *  no founded limit until 1000000 loops
	    */
	    $queue = new \SplQueue();
	    while($i < 1000)
	    {
	        $queue->enqueue('String_' . $i);
	        $i++;
	    }
	    
	    /* Test with a simple queue of stdObject
	     *  limit between 1E5 and 1E6 loops
	    */
	    $queue = new \SplQueue();
	    while($i < 1000)
	    {
	        $o = new stdObject();
	        $o->name = 'String_' . $i;
	        $queue->enqueue( $o );
	        $o = null;
	        $i++;
	    }
	}
	
}


