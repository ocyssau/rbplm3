<?php
//%LICENCE_HEADER%

namespace RbplmTest\Ged\Document;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Proxy\Generic;
use Rbplm\Dao\Connexion;
use Rbplm\Dao\Pg\DaoList;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\AccessCode;
use Rbplm\Ged\Historize;

use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsata;

use Rbplm\Sys\Date as DateTime;

use Rbplm\Uuid;

/**
 * @brief Test class for \Rbplm\Ged\Document\Version.
 * @include Rbplm/Ged/Document/VersionTest.php
 * 
 */
class VersionTest extends \Rbplm\Test\Test
{
    /**
     * @var    \Rbplm\Ged\Document
     * @access protected
     */
    protected $object;
	
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
        $ou = \Rbplm\Org\Unit::init(uniqid('myOu'));
        $ou->setParent(\Rbplm\Org\Root::singleton());
        
        $Base = Document::init('BaseDocument');
        $Base->setNumber( uniqid());
        $Base->description = 'My test base document';
        $Base->setParent($ou);
        
    	$this->object = DocumentVersion::init('document1'.uniqid());
    	$this->object->setNumber(uniqid());
    	$this->object->setBase($Base);
    	
    	if($this->_withDao){
    	    DaoFactory::getDao($ou)->save($ou);
    	    DaoFactory::getDao($Base)->save($Base);
    	    DaoFactory::getDao($this->object)->save($this->object);
    	}
    }
	
	function Test_SetParent(){
		$container = \Rbplm\Org\Unit::init('Container1');
		$ret = $this->object->setParent($container);
		assert($ret === $this->object);
	}
	
	function Test_GetParent(){
		$container = \Rbplm\Org\Unit::init();
		$this->object->setParent($container);
		
		$ret = $this->object->getParent();
		assert( $ret === $container);
	}
	
	function UNACTIVE_Test_AddDocfile(){
		$role = DocumentVersion::DF_ROLE_ANNEX;
		
		$docfiles = $this->object->getDocfiles();
		$ret = $docfiles->add(new DocfileVersion(array('name'=>'Annex001')), $role);
		$ret = $docfiles->add(new DocfileVersion(array('name'=>'Annex002')), $role);
		$ret = $docfiles->add(new DocfileVersion(array('name'=>'Annex003')), $role);
		
		assert($this->object->getDocfiles()->count() == 3);
		
		$ret = $this->object->associateDocfile( new DocfileVersion(array('name'=>'Annex004')), $role );
		
		assert( is_a( $ret, '\Rbplm\Ged\Docfile\Version') );
		assert($this->object->getDocfiles()->count() == 4);
	}
	
	function UNACTIVE_Test_GetDocfile(){
		$docfile = DocfileVersion::init('Annex001');
		$this->object->getDocfiles()->add($docfile, DocumentVersion::DF_ROLE_ANNEX);
		
		$docfile2 = DocfileVersion::init('Main001');
		$this->object->getDocfiles()->add($docfile2, DocumentVersion::DF_ROLE_MAIN);
		
		$docfile3 = DocfileVersion::init('Main002');
		$this->object->getDocfiles()->add($docfile3, DocumentVersion::DF_ROLE_MAIN);
		
		$ret = $this->object->getDocfiles();
		assert( is_a($ret, '\Rbplm\Model\Collection') );
		
		$ret->rewind();
		while( $ret->valid() ){
			if( $ret->getInfo() == DocumentVersion::DF_ROLE_MAIN ){
				$mainDf = $ret->current();
				break;
			}
			$ret->next();
		}
		assert($mainDf === $docfile2);
	}
	
	function _Test_DaoSaveLoad(){
		$ou = \Rbplm\Org\Unit::init(uniqid('myOu'));
		$ou->setParent(\Rbplm\Org\Root::singleton());
		//DaoFactory::getDao($ou)->save($ou);
		
		$Document1 = DocumentVersion::init();
		$Document1->setParent($ou);
		$Document1->description = 'My test version document';
		$Document1->setName('My_Document_Version001');
		$Document1->setNumber( uniqid('My_Document_Version001') );
		$Document1->version = 1;
		$Document1->iteration = 1;
		$Document1->state = 'asDefined';
		$Document1->doctypeId = Uuid::format('00000000-1111-2222-2222-000000000065');
		
		$Base = Document::init();
		DaoFactory::getDao($Base)->save($Base);

		$Document1->setBase( $Base );
		DaoFactory::getDao($Document1)->save($Document1);
		
        $Document2 = DocumentVersion::init();
        DaoFactory::getDao($Document2)->loadFromUid( $Document2, $Document1->getUid(), array(), true );
        
        var_dump( $Document1->description, $Document2->description, $Document1->getUid(), $Document2->getUid(), $Document1->createById, $Document2->createById);
        assert( $Document1->description == $Document2->description );
        assert( $Document1->name == $Document2->name );
        assert( $Document1->number == $Document2->number );
        assert( $Document1->version == $Document2->version );
        assert( $Document1->iteration == $Document2->iteration );
        assert( Uuid::compare($Document1->getUid(), $Document2->getUid()) );
        assert( Uuid::compare($Document1->updateById, $Document2->updateById ) );
        assert( Uuid::compare($Document1->createById, $Document2->createById ) );
        assert( Uuid::compare($Document1->ownerId, $Document2->ownerId ) );
        assert( $Document1->updated == $Document2->updated );
        assert( $Document1->created == $Document2->created );
	}
	
	function Test_DaoCheckInCheckOut()
	{
	    $Document1 = DocumentVersion::init('document1'.uniqid());
	    $Document1->setNumber(uniqid());
	    $Document1->setName('document'.$Document1->getNumber());
	    $Document1->setBase(Document::init('BaseDocument')->setNumber( uniqid()));
	    
	    echo '--SAVE DOCUMENT v1--'.CRLF;
	    DaoFactory::getDao($Document1)->save($Document1);
	    var_dump($Document1->getUid());
	    var_dump($Document1->getName());
	    
	    \Rbplm\Sys\Filesystem::isSecure(false);
	    $dataPath=realpath('../../data');
	    var_dump($dataPath);
	    
	    //Vault file
	    echo '--PREPARE FILES ON DISK--'.CRLF;
	    $file1 = $dataPath.'/testfile1.txt';
	    $file2 = $dataPath.'/testfile2.txt';
	    touch($file1);
	    touch($file2);
	    file_put_contents($file1, 'version001' . "\n");
	    file_put_contents($file2, 'version001' . "\n");
	    
	    $reposit=Reposit::init($dataPath.'/repositTest/'.uniqId());
	    $vault = new \Rbplm\Vault\Vault($reposit, DaoFactory::getDao('\Rbplm\Vault\Record'));
	    
	    echo '--CREATE DOCFILES--'.CRLF;
	    $Docfile1 = DocfileVersion::init('docfile1.'.uniqid());
	    $Docfile1->setBase(Docfile::init(uniqid('baseDocfile1')));
	    $Docfile1->setNumber(uniqid());
	    $Docfile1->setName($Document1->getName().'-df1');
	    $Docfile1->setParent($Document1);
	    
	    $Docfile2 = DocfileVersion::init('docfile2.'.uniqid());
	    $Docfile2->setBase(Docfile::init(uniqid('baseDocfile2')));
	    $Docfile2->setNumber(uniqid());
	    $Docfile2->setName($Document1->getName().'-df2');
	    $Docfile2->setParent($Document1);
	    
	    echo '--SAVE RECORDS IN VAULT--'.CRLF;
	    $record1=Record::init();
	    $vault->record( $record1, $file1, 'file', $Docfile1->getName().'.txt' );
	    $record2=Record::init();
	    $vault->record( $record2, $file2, 'file', $Docfile2->getName().'.txt' );
	    
	    var_dump($reposit->getUrl());
	    var_dump($vault->getReposit()->getUrl());
	    var_dump($vault->getLastRecord()->fullpath);
	    
	    echo '--SAVE DOCFILE v1--'.CRLF;
	    $Docfile1->setData( $record1 );
	    $Docfile2->setData( $record2 );
	    DaoFactory::getDao($Docfile1)->save($Docfile1);
	    DaoFactory::getDao($Docfile2)->save($Docfile2);
	    
	    $Docfiles=array($Docfile1,$Docfile2);
	    
	    echo '--CHECKOUT DOCUMENT--'.CRLF;
	    $aCode = $Document1->checkAccess();
	    if( ($aCode > 1) || ($aCode < 0 || $aCode === 1) ){
	    	throw new Exception('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('access_code'=>$aCode));
	    }
	    else{
	    	$Document1->lock(AccessCode::CHECKOUT);
	    }
	    assert( $Document1->checkAccess() == AccessCode::CHECKOUT );
	    assert( $Document1->getLockBy()->getUid() == CurrentUser::get()->getUid() );
	    assert( $Document1->lockById == CurrentUser::get()->getUid() );
	    
	    echo '--CHECKOUT DOCFILES--'.CRLF;
	    //checkOut associated files
	    foreach( $Docfiles as $docfile ){
	    	if( $docfile->checkAccess() !== 0 ){
	    		continue;
	    	}
	    	else{
	    		$docfile->lock(AccessCode::CHECKOUT);
	    	}
	    }
	    
	    echo '--CHECK IF DOCFILES ARE CHECKOUT BY CURRENT USER--'.CRLF;
	    foreach( $Docfiles as $docfile ){
	    	if( $docfile->checkAccess() == 1 AND $docfile->getLockBy()->uid==CurrentUser::get()->uid){
	    		echo '--UPDATE RECORDS--'.CRLF;
	    		file_put_contents($file1, 'version002' . "\n");
	    		Historize::depriveRecordToIteration($reposit, $docfile->getData(), $docfile->iteration);
			    $vault->record( $docfile->getData(), $file1, 'file', $docfile->getName() . '.file1.txt' );
			    echo '--CHECKIN DOCFILE--'.CRLF;
			    $docfile->lock(0);
			    $docfile->setUpdateBy(CurrentUser::get());
			    $docfile->updated=new DateTime();
			    $docfile->iteration=$docfile->iteration+1;
			    DaoFactory::getDao($docfile)->save($docfile);
	    	}
	    	else{
	    		throw new Exception(Error::LIMIT_ACCESS_VIOLATION,Error::LIMIT_ACCESS_VIOLATION,array());
	    	}
	    }
	}
	
	function Test_CheckOut(){
	}
}

