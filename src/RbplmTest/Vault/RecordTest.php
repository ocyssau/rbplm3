<?php
//%LICENCE_HEADER%

namespace RbplmTest\Vault;

use Rbplm\Vault\Record;
use \Rbplm\Dao\Factory as DaoFactory;

/**
 * @brief Test class for Record.
 * 
 * @include Rbplm/Vault/RecordTest.php
 * 
 */
class RecordTest extends \Rbplm\Test\Test
{
	/**
	 * @var    Record
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	/**
	 * From class: \Rbplm\Ged\Docfile_Version
	 */
	function Test_Dao()
	{
		/*Create new records require for tests*/
		$record1 = new Record();
		$record2 = new Record();
		$record3 = new Record();
		
		$fsData = new \Rbplm\Sys\Fsdata('./resources/doctypes.csv');
		
		/*Save the records*/
		$Dao = DaoFactory::getDao(Record::$classId);
		$Dao->save($record1->init()->setFsdata($fsData));
		$Dao->save($record2->init()->setFsdata($fsData));
		$Dao->save($record3->init()->setFsdata($fsData));
	}

}
