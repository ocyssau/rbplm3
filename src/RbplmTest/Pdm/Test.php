<?php
//%LICENCE_HEADER%

namespace RbplmTest\Pdm;

use Rbplm\Pdm\Effectivity;
use Rbplm\Pdm\Product;
use Rbplm\Pdm\Configuration;
use Rbplm\Measure;
use Rbplm\Pdm\Usage;

/**
 * Test class for Pdm module
 */
class Test extends \Rbplm\Test\Test
{
	/**
	 * @access protected
	 */
	protected $object;


	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}


	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}


	public function Test_PdmTest()
	{
		$parentOu = new \Rbplm\Org\Unit( array('name'=>'myParentOu'), \Rbplm\Org\Root::singleton() );

		$baseProduct001 = new Product( array('name'=>'BaseProduct001', 'description'=>'base product'), $parentOu );
		$productVersion001 = $baseProduct001->newVersion( array('name'=>'ProductVersion001', 'description'=>'init'), $parentOu );
		assert( $productVersion001 instanceof Product\Version );
		assert( $productVersion001->name == 'ProductVersion001');
		assert( $productVersion001->description == 'init');

		assert( is_array($baseProduct001->getVersions()) );
		$versions = $baseProduct001->getVersions();
		assert( end($versions) == $productVersion001 );
		assert( $baseProduct001->lastVersionId == 1 );

		$physicalProperties = $productVersion001->getPhysicalProperties();
		$physicalProperties->setVolume( 20, Measure\Volume::CUBIC_METER );
		$physicalProperties->setWeight( 30, Measure\Weight::KILOGRAM );
		$physicalProperties->setGravityCenter( 10, 10, 10, Measure\Length::MILLIMETER );
		$physicalProperties->setWetSurface( 30, Measure\Area::SQUARE_MILLIMETER );
		assert( $physicalProperties instanceof Product\PhysicalProperties );

		$productVersion001Instance = new Product\Instance( array('name'=>'ProductVersion001Instance.1'), $parentOu, $productVersion001 );
		$productVersion001Instance->getUsage()->setQuantity(4);
		$position2 = $productVersion001Instance->getPosition();
		$position2[9] = -120;
		$position2[10] = 150;
		$position2[11] = 200;
		$productVersion001Instance->nomenclature = 11;

		//assert( $productVersion001Instance->getPath() == '/RanchbePlm/myParentOu/ProductVersion001Instance1' );
		//assert( $productVersion001Instance->getPath(false, '_name') == '/RanchbePlm/myParentOu/ProductVersion001Instance.1' );
		assert( $productVersion001Instance->nomenclature == 11 );
		assert( $productVersion001Instance->getPosition()->offsetGet(9) === -120 );
		assert( $productVersion001Instance->getPosition()->offsetGet(10) === 150 );
		assert( $productVersion001Instance->getPosition()->offsetGet(11) === 200 );
		assert( $productVersion001Instance->getUsage()->getQuantity() == 4 );
		assert( $productVersion001Instance->getUsage()->getType() == Usage::TYPE_QUANTIFIED );
		assert( $productVersion001Instance->getUsage()->getUnit() == Usage::UNIT_SCALAR );

		$conceptContext = new Product\Concept\Context(array('name'=>'conceptContext', 'application'=>'mechanical design'), $parentOu);
		$concept = new Product\Concept(array('name'=>'Concept', 'description'=>'A new Concept'), $parentOu);
    	$concept->setmarketContext($conceptContext);
		assert($concept->getName() == 'Concept');
		assert($concept->description == 'A new Concept');
		assert($concept->getMarketContext()->application == 'mechanical design');

		$effectivity = new Effectivity\Lot();
		$effectivity->setId('lot001');
		$effectivity->setSize(1000);
		assert( $effectivity->getId() == 'lot001' );
		assert( $effectivity->getSize() == 1000 );
		assert( $effectivity->check(500, 'lot001') === true );
		assert( $effectivity->getType() == Effectivity::EFFECTIVITY_TYPE_LOT );

		$effectivity = new Effectivity\Dated();
		$effectivity->setStart( new \Rbplm\Sys\Date('2010-01-01') );
		$effectivity->setEnd( new \Rbplm\Sys\Date('2011-01-01') );
		assert( $effectivity->check( new \Rbplm\Sys\Date('2010-06-01') ) === true );
		assert( $effectivity->getType() == Effectivity::EFFECTIVITY_TYPE_DATED );

		$effectivity = new Effectivity\Dated();
		$effectivity->setStart( new \Rbplm\Sys\Date('01-01-01') );

		/*test just before 2038 bug...*/
		assert( $effectivity->check( new \Rbplm\Sys\Date('2038-01-18') ) === true );

		$effectivity = new Effectivity\Dated();
		$effectivity->setEnd( new \Rbplm\Sys\Date('2011-01-01') );
		assert( $effectivity->check( new \Rbplm\Sys\Date('01-01-01') ) === true );

		$effectivity = new Effectivity\Snumbered();
		$effectivity->setStart('PROD01_0001');
		$effectivity->setEnd('PROD01_4999');
		assert( $effectivity->check( 'PROD01_0001' ) === true );
		assert( $effectivity->check( 'PROD01_4999' ) === true );
		assert( $effectivity->check( 'PROD01_4000' ) === true );
		assert( $effectivity->check( 'PROD01_5000' ) === false );
		assert( $effectivity->check( 'PROD02_4000' ) === false );
		assert( $effectivity->getType() == Effectivity::EFFECTIVITY_TYPE_NUMBERED );

		$configuration = new Configuration\Effectivity(array('name'=>'myTestedConfiguration'), $parentOu, $effectivity);
		assert( $configuration->getEffectivity() === $effectivity );
		assert( $configuration->getParent() === $parentOu );

		$configuration = new Configuration\Effectivity( array('name'=>'myTestedConfiguration') );
		$configuration->setEffectivity($effectivity);
		assert( $configuration->getEffectivity() === $effectivity );
	}


	/**
	 * Use case create a Product and a Product\Version and a Product\Instance of this version in a OrganizationalUnit
	 * and put it in a Root Product\Version.
	 */
	public function Test_PdmTutoriel()
	{
		$parentOu = new \Rbplm\Org\Unit( array('name'=>'myParentOu'), \Rbplm\Org\Root::singleton() );
		$kKCarProductContext = new Product\Context( array('application'=>'mechanical design', 'name'=>'kk car context'), $parentOu );

		/*
		 * Create a basic product definition of a wheel
		 */
		$wheel = new Product( array(), $kKCarProductContext );
		$wheel->setName( 'wheel' );
		$wheel->description = '15pouce wheel alu for kekes';

		/*
		 * Create a version for wheel
		 */
		$wheel001 = $wheel->newVersion( 'wheel001', $kKCarProductContext );
		$wheel001->description = 'Initial version';

		/*
		 * Affect mechanicals properties to wheel
		 */
		$physicalProperties = $wheel001->getPhysicalProperties();
		$physicalProperties->setVolume( 20, Measure\Volume::CUBIC_METER );
		$physicalProperties->setWeight( 30, Measure\Weight::KILOGRAM );
		$physicalProperties->setGravityCenter( 10, 10, 10, Measure\Length::MILLIMETER );
		$physicalProperties->setWetSurface( 30, Measure\Area::SQUARE_MILLIMETER );

		/*
		 * Affect a material to wheel
		 */
		$material = new Product\Material(array('name'=>'AU4G'), $parentOu);
		$material->setDensity(2450, Measure\Density::GRAM_PER_CUBIC_METER);
		$wheel001->setMaterial($material);

		/*
		 * create a car sheet metal body
		 */
		$sheetMetalBody = new Product( array('name'=>'sheetMetalBody', 'description'=>'The car body'), $kKCarProductContext );
		$sheetMetalBody001 = $sheetMetalBody->newVersion( 'sheetMetalBody001', $kKCarProductContext );
		$sheetMetalBody001->description = 'Initial version';
		$sheetMetalBody001->setMaterial($material);
		$physicalProperties = $sheetMetalBody001->getPhysicalProperties();
		$physicalProperties->setVolume( 3, Measure\Volume::CUBIC_METER );
		$physicalProperties->setWeight( 400, Measure\Weight::KILOGRAM );
		$physicalProperties->setGravityCenter( 100, 0, 300, Measure\Length::MILLIMETER );
		$physicalProperties->setWetSurface( 30000, Measure\Area::SQUARE_MILLIMETER );

		/*
		 * Create a steering wheel
		 */
		$steeringW = new Product( array('name'=>'steering_wheel', 'description'=>'The car steering wheel'), $kKCarProductContext );
		$steeringW001 = $steeringW->newVersion( 'steeringw001', $kKCarProductContext );
		$steeringW001->description = 'The car steering wheel version 1';

		/*
		 * Create steering column
		 */
		$steeringColumn = new Product( array('name'=>'steering_column', 'description'=>'The car steering column'), $kKCarProductContext );
		$steeringColumn001 = $steeringColumn->newVersion( array('name'=>'steeringc001', 'description'=>'The car steering column version 1'), $kKCarProductContext );

		/*
		 * Create chassis
		 */
		$chassis = new Product( array('name'=>'chassis', 'description'=>'The car chassis'), $kKCarProductContext );
		$chassis001 = $chassis->newVersion( array('name'=>'chassis001', 'description'=>'The car chassis version 1'), $kKCarProductContext );

		/*
		 * create seats
		 */
		$seat = new Product( array('name'=>'seats', 'description'=>'The car seats'), $kKCarProductContext );
		$seat001 = $seat->newVersion( array('name'=>'seats001', 'description'=>'The car seats version 1'), $kKCarProductContext );

		/*
		 * create engine option 1
		 */
		$engine950cc = new Product( array('name'=>'engine950cc', 'description'=>'The car engine of 950cc'), $kKCarProductContext );
		$engine950cc001 = $seat->newVersion( array('name'=>'engine950cc001', 'description'=>'The car engine of 950cc version 1'), $kKCarProductContext );

		/*
		 * create engine option 2
		 */
		$engine1200cc = new Product( array('name'=>'engine1200cc', 'description'=>'The car engine of 1200cc'), $kKCarProductContext );
		$engine1200cc001 = $seat->newVersion( array('name'=>'engine1200cc001', 'description'=>'The car engine 1200cc version 1'), $kKCarProductContext );

		/*
		 * create engine option 3
		 */
		$engine1600cc = new Product( array('name'=>'engine1600ccV6', 'description'=>'The car engine of 1600cc V6'), $kKCarProductContext );
		$engine1600cc001 = $seat->newVersion( array('name'=>'engine1600ccV6', 'description'=>'The car engine 1600cc V6 version 1'), $kKCarProductContext );

		/*
		 * Create a root product for car
		 */
		$car = new Product( array('name'=>'car'), $kKCarProductContext );
		$car001 = $car->newVersion( 'car001', $kKCarProductContext );
		$car001->description = 'A car with 4 wheels and a body for keke ';

		/*Create a dashboard*/
		$dashboard = new Product( array('name'=>'dashboard', 'description'=>'The car dashboard'), $kKCarProductContext );
		$dashboard001 = $dashboard->newVersion( array('name'=>'dashboard001', 'description'=>'The car engine dashboard 1'), $kKCarProductContext );

		/*
		 * get versions collection of a product
		 */
		$versions = $wheel->getVersions();


		/*
		 * Now create this structure:
		 *
		 * Car001/
		 * 		Chassis001/
		 * 				Wheels001
		 * 				SteeringColumns001
		 * 				Engine001
		 * 		Body001/
		 * 				SheetMetalBody001
		 * 				Seats
		 * 				Dashboard
		 *
		 */


		/*
		 * Create instance for car.
		 * $parentOu is a Ou as context, its better to create a real context.
		 */
		 $productDefinitionContext = new Product\Definition\Context(array('name'=>'part definition', 'lifeCycleStage'=>'design') );

		$car001Instance = new Product\Instance( array('name'=>$car001->getName() . '.1'), $productDefinitionContext );

		/*Create chassis instance*/
		$chassis001Instance = new Product\Instance( array('name'=>$chassis001->getName() . '.1'), $car001Instance, $chassis001 );

		/*
		 * Create instance for wheels
		 */
		$wheel001Instance = new Product\Instance( array('name'=>$wheel001->getName() . '.1'), $chassis001Instance );
		$wheel001Instance->setProduct($wheel001);
		$wheel001Instance->getUsage()->setQuantity(4);
		$position2 = $wheel001Instance->getPosition();
		$position2[9] = -120;
		$position2[10] = 150;
		$position2[11] = 200;
		$wheel001Instance->nomenclature = 11;
		$wheel001Instance->description = 'Roues arrieres';

		/*Create a empty instance to group steering assembly*/
		$steeringAssyInstance = new Product\Instance( array('name'=>'steeringAssy.1'), $chassis001Instance );

		/*Steering column instance*/
		$steering001ColumnInstance = new Product\Instance( array('name'=>$steeringColumn001->getName() . '.1'), $steeringAssyInstance, $steeringColumn001 );

		/*Create instance for steering*/
		$steering001WInstance = new Product\Instance( array('name'=>$steeringW001->getName() . '.1', 'nomenclature'=>12), $steeringAssyInstance, $steeringW001 );

		/*Engine 950cc*/
		$engine950cc001Instance = new Product\Instance( array('name'=>$engine950cc001->getName() . '.1'), $chassis001Instance, $engine950cc001 );

		/*Engine 1600cc V6*/
		$engine1600cc001Instance = new Product\Instance( array('name'=>$engine1600cc001->getName() . '.1'), $chassis001Instance, $engine1600cc001 );

		/* Create body instance
		 * This is a little special because it has not assiociated Product\Version.
		 */
		$body001Instance = new Product\Instance( array('name'=>'body001.1'), $car001Instance );

		/*Sheet metal body*/
		$sheetMetalBody001Instance = new Product\Instance( array('name'=>$sheetMetalBody001->getName() . '.1'), $body001Instance, $body001 );

		/*Seats*/
		$seat001Instance = new Product\Instance( array('name'=>$seat001->getName() . '.1'), $body001Instance, $seat001 );

		/*Dashboard*/
		$dashboard001Instance = new Product\Instance( array('name'=>$dashboard001->getName() . '.1'), $body001Instance, $dashboard001 );


		/*Just for fun, display the tree*/
		/*
		$childrenIterator = new \RecursiveIteratorIterator( $productDefinitionContext->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($childrenIterator as $node){
    		echo $node->getPath() . "\n";
    	}
		echo '--------------------------------------------------------------------' . "\n";
		*/

		/*
		 * We may too explore the concept.
		 * We can see that there are 3 engines for this car.
		 * We must apply a configuration to select the engine.
		 */
		/*
		$childrenIterator = new \RecursiveIteratorIterator( $kKCarProductContext->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($childrenIterator as $node){
    		if(is_a($node, 'Product\Version')){
	    		echo $node->getPath() .' (' . get_class($node) . ") \n";
    		}
    	}
		echo '--------------------------------------------------------------------' . "\n";
		*/

		/*
		 * CONFIGURATION
		 *
		 */

		//First create a new Product\Concept\Context
		$conceptContext = new Product\Concept\Context(array('name'=>'EnglandMarket', 'application'=>'mechanical design'), $parentOu);
		//Product\Concept define class of similar products that an organization provides to its customers.
		$concept = new Product\Concept(array('name'=>'NewKekeCar', 'description'=>'A new car for the kekes'), $parentOu);
    	$concept->setmarketContext($conceptContext);

		/*
		 * Create Configurations for $concept
		 */
		$ci001 = new Configuration\Item(array('number'=>'carKK001Conf001', 'name'=>'carConf001', 'description'=>'A cheap car', 'purpose'=>'demo'), $concept);
		$ci002 = new Configuration\Item(array('id'=>'carKK001Conf002', 'name'=>'carConf002', 'description'=>'A very keke car', 'purpose'=>'demo'), $concept);

		/* Create design for CHASSIS assembly
		 */
		$ciDesign001 = new Configuration\Design( array('name'=>'Design001'), $ci001, $chassis001Instance);
		$ciDesign002 = new Configuration\Design( array('name'=>'Design002'), $ci002, $chassis001Instance );

		//$ciDesign003 = new Configuration\Design( array('name'=>'Design001'), $ci001, $body001Instance);
		//$ciDesign004 = new Configuration\Design( array('name'=>'Design001'), $ci002, $body001Instance);

		/*
		 * In this example, engine is under a CI and have many option select by configuration.
		 * All other components of the chassis, are put in Configuration design component.
		 *
		 * The components under Body001 are not submit by configuration and are directly attach to concept.
		 *
		 */

		$engine950ccEffectivity = new Effectivity\Lot( array('lotId'=>'l001', 'lotSize'=>1000) );
		$engine950ccEffectivityConf = new Configuration\Effectivity( array('name'=>'Effectivity01'), $ciDesign001, $engine950ccEffectivity, $engine950cc001Instance);

		$engine1600ccEffectivity = new Effectivity\Lot( array('lotId'=>'l002', 'lotSize'=>1000) );
		$engine1600ccEffectivityConf = new Configuration\Effectivity( array('name'=>'Effectivity01'), $ciDesign002, $engine1600ccEffectivity, $engine1600cc001Instance);


		/*
		 * Create effectivity for all other components of car
		 */
		foreach( array($wheel001Instance, $steeringAssyInstance) as $component ){
			$effectivity = new Effectivity\Lot( array('lotId'=>'l001', 'lotSize'=>1000) );
			$effectivityConf = new Configuration\Effectivity( array('name'=>$component->getName() . '.effectivity' ), $ciDesign001, $effectivity, $component);

			$effectivity2 = new Effectivity\Lot( array('lotId'=>'l002', 'lotSize'=>1000) );
			$effectivityConf2 = new Configuration\Effectivity( array('name'=>$component->getName() . '.effectivity' ), $ciDesign002, $effectivity2, $component);
		}


		/*attach body to concept without configuration design*/
		//$concept->getChild()->add($body001Instance);


		/*
		 * Create effectivity for all other components of car
		 */
		//foreach( array($chassis001Instance, $wheel001Instance, $steeringAssyInstance, $body001Instance, $sheetMetalBody001Instance, $seat001Instance, $dashboard001Instance) as $component ){}


		/*
		 * Explore the concept.
		 */
		/*
		$childrenIterator = new \RecursiveIteratorIterator( $concept->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($childrenIterator as $node){
    		if( is_a($node, 'Configuration\Effectivity') ){
    			if( $node->getEffectivity()->check(null, 'l001') ){
	    			$current = $node->getUsage();
    			}
    		}
    		else if( is_a($node, 'Product\Instance') ){
    			$current = $node;
    		}
    		if( $current ){
	    		echo $current->getPath() .' (' . get_class($current) . ") \n";
    		}
    		$current = null;
    	}
		echo '--------------------------------------------------------------------' . "\n";
		*/
	}


	/**
     * Example of traduction of STEP shema into Rbplm;
	 */
	public function Test_Steptutorial()
	{

		$parentOu = new \Rbplm\Org\Unit( array('name'=>'myParentOu'), \Rbplm\Org\Root::singleton() );

		/*
		 * Product context is just a OU for products
		 * #8=APPLICATION_CONTEXT('');
		 * #9=PRODUCT_CONTEXT('', #8, '');
		 */
		 $pContext9 = new Product\Context( array('application'=>'', 'name'=>'application context'), $parentOu );
		 /* Base product
		  * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
		  */
		 $product10 = new Product( array('number'=>'PC-0023', 'name'=>'PC system'), $pContext9 );
		/*Product Version. Put in OU $pContext9, same that the base product, but may be in other place
		 * #11=PRODUCT_DEFINITION_FORMATION('D', 'description of PC-0023,D', #10);
		 */
		 $productVersion11 = $product10->newVersion('D', $pContext9);
		 $productVersion11->description = 'description of PC-0023,D';
		 /* @todo Ignore category for the moment but must be implement in futur versions.
		 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10, #18, #20, #21, #22, #23));
		 */

		 /*  Create other base products and versions.
		 * #18=PRODUCT('MB-0013', 'Mainboard', $, (#9));
		 * #19=PRODUCT_DEFINITION_FORMATION('F', 'description of MB-0013,F', #18);
		 * #20=PRODUCT('PSU-0009', 'Power supply unit', 'Power supply unit 220V', (#9));
		 * #21=PRODUCT('PSU-0011', 'Power supply unit', 'Power supply unit 110V', (#9));
		 * #22=PRODUCT('PR-0133', 'CPU', 'Pentium II 233', (#9));
		 * #23=PRODUCT('PR-0146', 'CPU', 'Pentium II 266', (#9));
		 * #24=PRODUCT_DEFINITION_FORMATION('A', 'description of PSU-0009,A', #20);
		 * #25=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0009,B', #20);
		 * #26=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0011,B', #21);
		 * #27=PRODUCT_DEFINITION_FORMATION('A', 'description of PR-0133,A', #22);
		 * #28=PRODUCT_DEFINITION_FORMATION('C', 'description of PR-0146,C', #23);
		 */
		 $product18 = new Product( array('number'=>'MB-0013', 'name'=>'Mainboard'), $pContext9 );
		 $productVersion19 = $product18->newVersion('F', $pContext9);
		 $productVersion19->description = 'description of MB-0013,F';

		 $product20 = new Product( array('number'=>'PSU-0009','name'=>'Power supply unit', 'description'=>'Power supply unit 220V'), $pContext9 );
		 $productVersion24 = $product20->newVersion('A', $pContext9);
		 $productVersion25 = $product20->newVersion('B', $pContext9);

		 $product21 = new Product( array('number'=>'PSU-0011','name'=>'Power supply unit', 'description'=>'Power supply unit 110V'), $pContext9 );
		 $productVersion26 = $product21->newVersion('B', $pContext9);

		 $product22 = new Product( array('number'=>'PR-0133','name'=>'CPU', 'description'=>'Pentium II 233'), $pContext9 );
		 $productVersion27 = $product22->newVersion('B', $pContext9);

		 $product23 = new Product( array('number'=>'PR-0146','name'=>'CPU', 'description'=>'Pentium II 266'), $pContext9 );
		 $productVersion28 = $product23->newVersion('A', $pContext9);


		 /* Define the context for apply a view
		 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
		 */
		 $pContextDefinition12 = new Product\Definition\Context(array('name'=>'part definition', 'lifeCycleStage'=>'design') );

		 /* Create instance for product versions
		 * #29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
		 * #30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
		 * #31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
		 * #33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
		 * #34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
		 * #35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
		 * #36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
		 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
		 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
		 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
		 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
		 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
		 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
		 */

		//#29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
		$productInstance29 = new Product\Instance( array('name'=>'pc_v1', 'description'=>'design view on PC-0023,D') );
		$productInstance29->setProduct($productVersion11);
		$productInstance29->setContext($pContextDefinition12);

		//#30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
		$productInstance30 = new Product\Instance( array('name'=>'mb_v1', 'description'=>'design view on MB-0013,F') );
		$productInstance30->setProduct($productVersion19);
		$productInstance30->setContext($pContextDefinition12);

		//#31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
		$productInstance31 = new Product\Instance( array('name'=>'psu1A_v1', 'description'=>'design view on PSU-0009,A') );
		$productInstance31->setProduct($productVersion24);
		$productInstance31->setContext($pContextDefinition12);

		//#33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
		$productInstance33 = new Product\Instance( array('name'=>'psu1B_v1', 'description'=>'design view on PSU-0009,B') );
		$productInstance33->setProduct($productVersion25);
		$productInstance33->setContext($pContextDefinition12);

		//#34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
		$productInstance34 = new Product\Instance( array('name'=>'psua_v1', 'description'=>'design view on PSU-0011,B') );
		$productInstance34->setProduct($productVersion26);
		$productInstance34->setContext($pContextDefinition12);

		//#35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
		$productInstance35 = new Product\Instance( array('name'=>'pr1_v1', 'description'=>'design view on PR-0133,A') );
		$productInstance35->setProduct($productVersion27);
		$productInstance35->setContext($pContextDefinition12);

		//#36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
		$productInstance36 = new Product\Instance( array('name'=>'pr2_v1', 'description'=>'design view on PR-0146,C') );
		$productInstance36->setProduct($productVersion28);
		$productInstance36->setContext($pContextDefinition12);

		/*
		 * Set the relations
		 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
		 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
		 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
		 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
		 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
		 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
		 */
		$productInstance29->setParent($pContext9);
		$productInstance30->setParent($productInstance29);
		$productInstance31->setParent($productInstance29);
		$productInstance33->setParent($productInstance29);
		$productInstance34->setParent($productInstance29);
		$productInstance35->setParent($productInstance30);
		$productInstance36->setParent($productInstance30);

    	echo 'Display all parent until root of a instance--------------------------------------------------------' . "\n";
    	/*
    	$Iterator = new \ParentIterator($productInstance34);
    	foreach($Iterator as $node){
    		var_dump( $node->getPath(), $Iterator->key(), get_class($node) );
    	}

    	echo 'Iterate all tree from $parentOu--------------------------------------------------------' . "\n";
		$childrenIterator = new \RecursiveIteratorIterator( $parentOu->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
    	foreach($childrenIterator as $node){
    		echo 'NodePath:' . $node->getPath() . "\n";
    		echo 'Level:' . $childrenIterator->getDepth() . "\n";
    		echo 'Class:' . get_class($node) . "\n";
    		echo '----------------------------------' . "\n";
    		//var_dump( $node->getPath(), $childrenIterator->getDepth(), get_class($node) );
    	}

    	echo 'Iterate all tree from $parentOu but display only instances--------------------------------------------------------' . "\n";
    	foreach($childrenIterator as $node){
    		if( is_a( $node , 'Product\Instance') ){
    			var_dump( $node->getPath(), $childrenIterator->key() );
    		}
    	}

    	echo 'Iterate all tree from $parentOu but display only product version--------------------------------------------------------' . "\n";
    	foreach($childrenIterator as $node){
    		if( is_a( $node , 'Product\Version') ){
    			var_dump( $node->getPath(), $childrenIterator->key() );
    		}
    	}


    	echo 'Iterate all tree from $parentOu but display only product base--------------------------------------------------------' . "\n";
    	foreach($childrenIterator as $node){
    		if( is_a( $node , 'Product') ){
    			var_dump( $node->getPath(), $childrenIterator->key() );
    		}
    	}

    	echo 'End of iterations display--------------------------------------------------------' . "\n";
    	*/

		/**************************************************************
		 * Configuration
		 ***************************************************************/


		/*
		 * The products that are sold by an organization to its customers are often defined as variations or configurations of a common product model, referred to as product concept.
		 * This product concept is defined in a market context and can be seen as a logical container for all of its variations.
		 * In the PDM Schema, product concept identification is the representation of these product concepts with their related market context.
		 *
		 * A Product\Concept represents a conceptual idea of a class of similar products that are offered to a market.
		 * No design or manufacturing related product data can be attached to the Product\Concept directly.
		 * The members of that product class are the different configurations that are available for the Product\Concept.
		 * Within the PDM Schema, these configurations are defined explicitly as configuration\items (see Configuration items),
		 *  each of them being potentially related to the parts that implement the particular configuration.
		 *  A Product\Concept will usually be referenced by at least one associated configuration\item specifying a particular product configuration.
		 *
		 *
		 * It is recommended to instantiate this entity exactly once in the data set.
		 * There is no standard mapping for the name and market\segment\type attributes of a Product\Concept\context.
		 * Neither AP203 nor AP214 define requirements for a Product\Concept\context.
		 * It is therefore recommended to use the default values for the entities and attributes not supported
		 * 	by the preprocessor as described in the 'General information' section of this usage guide.
		 *
		 *
		 * #100=Product\Concept_CONTEXT('pcc_name1', #1, '');
		 * #1=APPLICATION_CONTEXT('');
		 * #2=Product\Concept('PC-M01', 'PC model name1', 'PC system', #100);
			//A concept_context define context of the Product\Concept. It is just a OU.
			//Concept define the base product to build as base of many version and configuration. Here a PC.
		 */
		$conceptContext100 = new Product\Concept\Context(array('name'=>'pcc_name1', 'application'=>''), $parentOu);
		$concept2 = new Product\Concept(array('number'=>'PC-M01', 'name'=>'PC model name1', 'description'=>'PC system'), $parentOu);
    	$concept2->setmarketContext($conceptContext100);

		/* Create CIs for $concept2
		 * #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
		 * #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
		 * @todo The Product\Concept #2 is not lchild to CI. See if need and how.
		 */
		$properties = array('number'=>'PC-Conf1', 'name'=>'Base Config Europe', 'description'=>'PC system standard configuration for Europe', 'purpose'=>'demo');
		$ci5 = new Configuration\Item($properties, $concept2);

		$properties = array('number'=>'PC-Conf2', 'name'=>'Base Config US', 'description'=>'PC system standard configuration for US', 'purpose'=>'demo');
		$ci6 = new Configuration\Item($properties, $concept2);

		var_dump( $ci5->getPath() );
		var_dump( $ci6->getPath() );

		 /* Associate CI to a Product version
		 * #15=CONFIGURATION_DESIGN(#5, #11);
		 * #16=CONFIGURATION_DESIGN(#6, #11);
		 */
		$ciDesign15 = new Configuration\Design(array('name'=>'CD15'));
		$ciDesign15->setParent($ci5);
		$ciDesign15->setDesign($productVersion11);

		$ciDesign16 = new Configuration\Design(array('name'=>'CD16'));
		$ciDesign16->setParent($ci6);
		$ciDesign16->setDesign($productVersion11);

		//$ci5->setParent($productVersion11);
		//$ci6->setParent($productVersion11);

		/*
		 * Configuration effectivity;
		 * Configuration effectivity allows control of the constituent parts that should be used to build the physical instances of a product configuration.
		 * The composition of the product configurations for planned units of manufacture may be controlled for a given time period, lot, or serial number range.
		 * This is managed using dated\effectivity, lot\effectivity, or serial\numbered\effectivity.
		 */

		/*Apply configuration on power supply 220V, V1
		 *
		 * The entity configuration\effectivity contains information about the planned usage of components in a product configuration.
		 * It defines the valid use of a particular product\definition occurrence at a certain position in the assembly structure for a specified product configuration.
		 *
		 * #43=(CONFIGURATION_EFFECTIVITY(#15)
		 *      DATED_EFFECTIVITY(#970, #910)
		 *     EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#38)
		 *     );
		 * #910=DATE_AND_TIME(#920, #930);
		 * #920=CALENDAR_DATE(2000, 1, 7);
		 * #930=LOCAL_TIME(0, 0, 0., #940);
		 * #940=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 * #970=DATE_AND_TIME(#980, #990);
		 * #980=CALENDAR_DATE(1999, 31, 3);
		 * #990=LOCAL_TIME(0, 0, 0., #1000);
		 * #1000=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 */
		$effectivity43 = new Effectivity\Dated( array('start'=>'1999-03-31', 'end'=>'2000-06-30' ) );
		$configurationEffectivity43 = new Configuration\Effectivity( array('name'=>'Effectivity43') );
		$configurationEffectivity43->setEffectivity($effectivity43);
		$configurationEffectivity43->setUsage($productInstance31);
		$configurationEffectivity43->setParent($ciDesign15);
		//$configurationEffectivity43->setDesign($ciDesign15);


		/*Apply configuration on power supply 220V, V2*/
		/* #45=(CONFIGURATION_EFFECTIVITY(#15)
		 *      DATED_EFFECTIVITY($, #1010)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#39)
		 *     );
		 * #1010=DATE_AND_TIME(#1020, #1030);
		 * #1020=CALENDAR_DATE(1999, 1, 4);
		 * #1030=LOCAL_TIME(0, 0, 0., #1031);
		 * #1031=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 */
		$effectivity45 = new Effectivity\Dated( array('start'=>'2000-07-01' ) );
		$configurationEffectivity45 = new Configuration\Effectivity( array('name'=>'Effectivity45') );
		$configurationEffectivity45->setEffectivity($effectivity45);
		$configurationEffectivity45->setUsage($productInstance33);
		$configurationEffectivity45->setParent($ciDesign15);


		/*Apply configuration on power supply 110V, V1*/
		/* #46=(CONFIGURATION_EFFECTIVITY(#16)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#40)
		 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000567', $)
		 *    );
		 */
		$effectivity46 = new Effectivity\Snumbered( array('start'=>'PS253-000567') );
		$configurationEffectivity46 = new Configuration\Effectivity( array('name'=>'Effectivity46') );
		$configurationEffectivity46->setEffectivity($effectivity46);
		$configurationEffectivity46->setUsage($productInstance34);
		$configurationEffectivity46->setParent($ciDesign16);




		 /*Apply configuration on CPU #41 from Dated on Europe config*/
		 /* #47=(CONFIGURATION_EFFECTIVITY(#15)
		 *      DATED_EFFECTIVITY($, #1160)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#41)
		 *    );
		 * #1160=DATE_AND_TIME(#1170, #1180);
		 * #1170=CALENDAR_DATE(2000, 1, 10);
		 * #1180=LOCAL_TIME(0, 0, 0., #1190);
		 * #1190=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 */
		$effectivity47 = new Effectivity\Dated( array('end'=>'2000-10-01' ) );
		$configurationEffectivity47 = new Configuration\Effectivity( array('name'=>'Effectivity47') );
		$configurationEffectivity47->setEffectivity($effectivity47);
		$configurationEffectivity47->setUsage($productInstance35);
		$configurationEffectivity47->setParent($ciDesign15);


		 /*Apply configuration on CPU #41 from SNUM on US config*/
		/* #48=(CONFIGURATION_EFFECTIVITY(#16)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#41)
		 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000345', 'PS253-000976')
		 *     );
		 */
		$effectivity48 = new Effectivity\Snumbered( array('start'=>'PS253-000345', 'end'=>'PS253-000976') );
		$configurationEffectivity48 = new Configuration\Effectivity( array('name'=>'Effectivity48') );
		$configurationEffectivity48->setEffectivity($effectivity48);
		$configurationEffectivity48->setUsage($productInstance35);
		$configurationEffectivity48->setParent($ciDesign16);


		 /*Apply configuration on CPU #42 from SNUM on US config*/
		/* #49=(CONFIGURATION_EFFECTIVITY(#16)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#42)
		 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000977', $)
		 *     );
		 */
		$effectivity49 = new Effectivity\Snumbered( array('start'=>'PS253-000977') );
		$configurationEffectivity49 = new Configuration\Effectivity( array('name'=>'Effectivity49') );
		$configurationEffectivity49->setEffectivity($effectivity49);
		$configurationEffectivity49->setUsage($productInstance36);
		$configurationEffectivity49->setParent($ciDesign16);


		var_dump( $productVersion11->getPath() );

		/*
		 * May have many type of effectivity on same component?
		 *
		 * YES, but if intersect other type of effectivity, component is select twice.
		 * But it is easy to create procedure to calculate effectivity on one or other type.
		 */
		$effectivityB49 = new Effectivity\Dated( array('end'=>'2000-10-01' ) );
		$configurationEffectivityB49 = new Configuration\Effectivity( array('name'=>'EffectivityB49') );
		$configurationEffectivityB49->setEffectivity($effectivityB49);
		$configurationEffectivityB49->setUsage($productInstance36);
		$configurationEffectivityB49->setParent($ciDesign16);


		/*
		 * Display tree of configuration of product concept $concept2
		 */

		echo '----------------------------------------------------------------------------------------------' . "\n";
		echo 'Display configs on Product Concept $concept2' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";

		/*
		$childrenIterator = new \RecursiveIteratorIterator( $concept2->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
    	foreach($childrenIterator as $node){
    		if( is_a($node, 'Configuration\Effectivity') ){
    			var_dump( $node->getUsage()->getPath() );

    			//...here, compare validity with a query to choose to select or not...

    		}
    		var_dump( $node->getPath(), $childrenIterator->key() );
    		echo '================================================' . "\n";
    	}
    	*/


    	/*
    	 * How to extract tree of instance product from configuration.
    	 *
    	 * This output is not a complet PC, the STEP require that all element must have a effectivity, it is not case here.
    	 */
		echo '----------------------------------------------------------------------------------------------' . "\n";
    	echo 'How to extract tree of instance product for EU configuration' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";

    	$now = new \Rbplm\Sys\Date('1999-04-01');
    	$snum = 'PS253-000977';

    	/*
		$childrenIterator = new \RecursiveIteratorIterator( $ci5->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
    	foreach($childrenIterator as $node){
    		if( is_a($node, 'Configuration\Effectivity') ){
    			if( $node->getEffectivity()->getType() == Effectivity::EFFECTIVITY_TYPE_DATED ){
    				if( $node->getEffectivity()->check($now) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    			else if( $node->getEffectivity()->getType() == Effectivity::EFFECTIVITY_TYPE_NUMBERED ){
    				if( $node->getEffectivity()->check($snum) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    		}
    	}
    	*/


		echo '----------------------------------------------------------------------------------------------' . "\n";
    	echo 'How to extract tree of instance product for US configuration' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";

		/*
		 * Extract only Snumbered effectivities:
		 */
		/*
		$childrenIterator = new \RecursiveIteratorIterator( $ci6->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
    	foreach($childrenIterator as $node){
    		if( is_a($node, 'Configuration\Effectivity') ){
    			if( $node->getEffectivity()->getType() == Effectivity::EFFECTIVITY_TYPE_DATED ){
    				if( $node->getEffectivity()->check($now) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    			else if( $node->getEffectivity()->getType() == Effectivity::EFFECTIVITY_TYPE_NUMBERED ){
    				if( $node->getEffectivity()->check($snum) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    		}
    	}
    	*/

		echo '----------------------------------------------------------------------------------------------' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";

	}



}


