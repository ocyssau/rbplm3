<?php



class Serializer
{

	/**
	 * @return 	string
	 */
	public static function xmlSerialize(Serializable $object)
	{
		$properties = $object->__serialize();

		$SXml = new \SimpleXMLElement('<properties/>');
		foreach( $properties as $name=>$val ){
			if($name == '_plugins'){
				continue;
			}
			if( !is_object($val) && !is_null($val) ){
				$name = trim($name, '_');
				if( is_array($val) ){
					$e = $SXml->addChild($name, '');
					foreach($val as $keyj=>$valj){
						$e->addChild($keyj, $valj);
					}
				}
				else{
					$SXml->addChild($name, $val);
				}
			}
		}
		return $SXml->asXml();
	}

	/**
	 * @param string
	 * @return void
	 */
	public static function unserialize( $serialized )
	{
		$properties = unserialize( $serialized );
		$serialized->hydrate($properties);
	}
}
