<?php
namespace Rbplm;

/**
 */
trait Item
{

	/**
	 * @var array
	 */
	protected $children = [];

	/**
	 * @var string
	 */
	protected $nodelabel = '';

	/**
	 * @var string
	 */
	protected $nodeid = '';

	/**
	 * @var \Rbplm\Item
	 */
	protected $parent;

	/**
	 * @var string
	 */
	public $parentUid;

	/**
	 * @var string
	 */
	public $parentId;

	/**
	 * @var string
	 */
	protected $dn;

	/**
	 *
	 * @param array $properties
	 * @return \Rbplm\Item
	 */
	public function itemHydrate(array $properties)
	{
		(array_key_exists('parentId', $properties)) ? $this->parentId = $properties['parentId'] : null;
		(array_key_exists('parentUid', $properties)) ? $this->parentUid = $properties['parentUid'] : null;
		(isset($properties['parent'])) ? $this->setParent($properties['parent']) : null;
		(isset($properties['children'])) ? $this->children = $properties['children'] : null;
		(isset($properties['nodelabel'])) ? $this->nodelabel = $properties['nodelabel'] : null;
		(isset($properties['nodeid'])) ? $this->nodeid = $properties['nodeid'] : null;
		(isset($properties['path'])) ? $this->path = $properties['path'] : null;
		(isset($properties['dn'])) ? $this->dn = $properties['dn'] : null;
		return $this;
	}

	/**
	 * Set the label
	 *
	 * @param	string	$string
	 * @param	boolean	$toFormat	If true, suppress all specials char, spaces and replace - by _
	 * @return ItemInterface
	 */
	public function setNodelabel($string, $toFormat = true)
	{
		if ( $toFormat ) {
			$string = str_replace(' ', '_', $string);
			$string = str_replace('-', '_', $string);
			$string = preg_replace('/[^0-9A-Za-z\_]/', '', $string);
			$this->nodelabel = $string;
		}
		else {
			$this->nodelabel = $string;
		}
		return $this;
	}

	/**
	 * Get the label
	 *
	 * @return string
	 */
	public function getNodelabel()
	{
		return $this->nodelabel;
	}

	/**
	 * Get the node identifiant
	 *
	 * @return string
	 */
	public function getNodeid()
	{
		return $this->nodeid;
	}

	/**
	 * @param ItemInterface $parent
	 * @param boolean $bidirectionnal
	 * @return Item
	 */
	public function setParent(ItemInterface $parent, $bidirectionnal = true)
	{
		$this->parent = $parent;
		$this->parentUid = $parent->getUid();
		$this->parentId = $parent->getId();
		if ( $bidirectionnal ) {
			$parent->addChild($this, false);
		}
		return $this;
	}

	/**
	 *
	 * @return Item|int
	 */
	public function getParent($asId = false)
	{
		if ( $asId ) {
			return $this->parentId;
		}
		else {
			return $this->parent;
		}
	}

	/**
	 * @return string
	 */
	public function getParentUid()
	{
		return $this->parentUid;
	}

	/**
	 * if $bidirectionnal, create relation from $child->$parent
	 *
	 * @param ItemInterface $child
	 * @param boolean $bidirectionnal
	 * @return Item
	 */
	public function addChild(ItemInterface $child, $bidirectionnal = true)
	{
		$this->children[$child->getUid()] = $child;
		if ( $bidirectionnal ) {
			$child->setParent($this, false);
		}
		return $this;
	}

	/**
	 * Return array of Any
	 *
	 * @return array
	 */
	public function getChildren()
	{
		return $this->children;
	}

	/**
	 * @param string
	 * @return Item
	 */
	public function setDn($dn)
	{
		$this->dn = $dn;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDn()
	{
		return $this->dn;
	}
}
