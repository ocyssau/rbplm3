<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged;

/**
 * @brief Enum of access code.
 *
 * The value of the access code permit or deny action:<ul>
 * <li>0: Modification of Document is permit.</li>
 * <li>1: Modification of Document is in progress. The checkOut is deny. You can only do checkIn.</li>
 * <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
 * <li>6: Canceled.</li>
 * <li>7: RESERVED.</li>
 * <li>8: RESERVED.</li>
 * <li>9: RESERVED.</li>
 * <li>10: The Document is lock.You can only upgrade his indice.</li>
 * <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
 * <li>15: Indice is upgraded. You can only archive it.</li>
 * <li>20: The Document is archived.</li>
 * </ul>
 *
 */
class AccessCode
{

	/** All modifications enables*/
	const FREE = 0;

	/** Checkouted by a user */
	const CHECKOUT = 1;

	/** A workflow is started */
	const INWORKFLOW = 5;

	/** Level 1 */
	const LOCKEDLEVEL1 = 6;
	const CANCELED = 6;

	/** Level 2 */
	const LOCKEDLEVEL2 = 7;

	/** Level 3 */
	const LOCKEDLEVEL3 = 8;

	/** Level 4 */
	const LOCKEDLEVEL4 = 9;

	/** Checkout is locked, Metadatas edition is locked */
	const VALIDATE = 10;

	/** Checkout is locked, Metadatas edition is locked, versioning is locked */
	const LOCKED = 11;

	/** Document is suppressed */
	const SUPPRESS = 12;

	/** Document is a iteration */
	const ITERATION = 13;

	/** Document is a version */
	const VERSION = 14;

	/** Document is a version */
	const HISTORY = 15;

	/** Document is archived */
	const ARCHIVE = 20;

	protected static $_lockCode = array(
		0 => 'Unlocked',
		1 => 'Checkout',
		5 => 'InWorkflow',
		6 => 'Canceled',
		7 => 'LockedLevel2',
		8 => 'LockedLevel3',
		9 => 'LockedLevel4',
		10 => 'Validate',
		11 => 'Locked',
		12 => 'MarkedToSuppress',
		13 => 'Iteration',
		14 => 'Version',
		15 => 'MarkedForHistory',
		20 => 'Archived'
	);

	/**
	 * @param integer $code
	 * @return string
	 */
	public static function getName($code)
	{
		return self::$_lockCode[$code];
	}
}
