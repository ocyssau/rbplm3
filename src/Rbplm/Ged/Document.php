<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged;

use Rbplm\Ged\Document\Version as DocumentVersion;

use Rbplm\AnyPermanent;
use Rbplm\Org\Unit;
use Rbplm\People\CurrentUser;
use Rbplm\Signal;
use Rbplm\Sys\Date as DateTime;

/**
 * @brief Document is a base definition of ged anyobject.
 *
 * A Document is anyobject of ranchbe wich is manage
 *   by a container type "document_manager". It just a database record with
 *   relations to
 *       - 0,n Rb_Docfile
 *   A Document may have many docfiles.
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Ged/DocumentTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Document extends AnyPermanent
{
	static $classId = '569e924be82e7';

	const SIGNAL_POST_SETNUMBER = 'setnumber.post';
	const SIGNAL_POST_NEWVERSION = 'newversion.post';

	/**
	 * @var string
	 */
	protected $number = null;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * Collection of Document_Version
	 *
	 * @var \Rbplm\Link\Collection
	 */
	protected $_versions = null;

	/**
	 * @var integer
	 */
	protected $lastVersionId = 0;

	/**
	 * Init properties.
	 *
	 * @return Document
	 */
	public static function init($name=null, $parent=null)
	{
		$obj=parent::init($name,$parent);
		$obj->created = new DateTime();
		$obj->updated = new DateTime();
		$obj->setCreateBy(CurrentUser::get());
		$obj->setUpdateBy(CurrentUser::get());
		$obj->setOwner(CurrentUser::get());
		$obj->_number = $obj->getUid();
		return $obj;
	}

	/**
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param string $number
	 * @return \Rbplm\Ged\Document
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		Signal::trigger(self::SIGNAL_POST_SETNUMBER, $this);
		return $this;
	}

	/**
	 * Create a new version for current document.
	 *
	 * @param	string
	 * @param	\Rbplm\Org\Unit
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function newVersion( $name, Unit $ou = null)
	{
		$version = DocumentVersion::init();
		$version->setName( $name );
		if($ou){
			$version->setParent( $ou );
		}
		$version->version = $this->lastVersionId++;
		$version->setBase($this);
		$this->getLinks()->add($version);

		Signal::trigger(self::SIGNAL_POST_NEWVERSION, $this);
		return $version;
	}

}
