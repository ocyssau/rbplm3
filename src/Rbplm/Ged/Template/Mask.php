<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged\Template;

use Rbplm\Any;
use Rbplm\Item;
use Rbplm\Mapped;

/**
 */
class Mask extends Any implements \Rbplm\Dao\MappedInterface
{
	use Item, Mapped;

	/** @var string */
	static $classId = 'docseedermask';

	/**
	 *
	 * @var array
	 */
	protected $map = array();

	/**
	 *
	 * @var array
	 */
	protected $hooks = array();

	/**
	 */
	public function __construct()
	{}

	/**
	 *
	 * @param string $name
	 * @return Mask
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->setName($name);
		$obj->setUid($name);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name= $properties['name'] : null;
		(isset($properties['map'])) ? $this->map = $properties['map'] : null;
		(isset($properties['hooks'])) ? $this->hooks = $properties['hooks'] : null;
		return $this;
	}

	/**
	 */
	public function getMap()
	{
		return $this->map;
	}

	/**
	 *
	 * @param array $map
	 * @return Mask
	 */
	public function setMap($map)
	{
		$this->map = $map;
		return $this;
	}

	/**
	 */
	public function getHooks()
	{
		return $this->hooks;
	}

	/**
	 *
	 * @param array $hooks
	 * @return Mask
	 */
	public function setHooks($hooks)
	{
		$this->hooks = $hooks;
		return $this;
	}

	/**
	 *
	 * @param Hook $hook
	 * @return Mask
	 */
	public function addHook($hook)
	{
		$this->hooks[$hook->getName()] = $hook;
		return $this;
	}
} /* End of class */
