<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged\Template;


/**
 */
class Hook implements \JsonSerializable
{

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $replacementChain;

	/**
	 * @var string
	 */
	protected $templateHook;

	/**
	 */
	public function __construct($name)
	{
		$this->name = $name;
	}

	/**
	 * @param string $name
	 * @return Hook
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $string
	 * @return Hook
	 */
	public function setReplacementChain($string)
	{
		$this->replacementChain= $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getReplacementChain()
	{
		return $this->replacementChain;
	}

	/**
	 * @param string $string
	 * @return Hook
	 */
	public function setTemplateHook($string)
	{
		$this->templateHook= $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTemplateHook()
	{
		return $this->templateHook;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['name'])) ? $this->name= $properties['name'] : null;
		(isset($properties['templateHook'])) ? $this->templateHook= $properties['templateHook'] : null;
		(isset($properties['replacementChain'])) ? $this->replacementChain= $properties['replacementChain'] : null;
		return $this;
	}

	/**
	 * function called when encoded with json_encode
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
} /* End of class */
