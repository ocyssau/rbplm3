<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged;

use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;

/**
 * @brief Helper class for create history copy of ged components.
 *
 *
 */
class Historize
{

	/**
	 * Move Record into defined iteration reposit.
	 *
	 * @param Reposit $reposit
	 * @param Record $record
	 * @param integer $iterationId
	 * @return Record
	 */
	public static function depriveRecordToIteration(Reposit $reposit, Record $record, $iterationId)
	{
		$toPath = $reposit->getIterationPath() .'/'. $record->rootname.'.'.$iterationId.$record->extension;
		$fsdata=$record->getFsdata()->copy( $toPath, 0755, true);
		$record->getFsdata()->suppress();
		$record->setFsdata($fsdata);
		return $record;
	}

	/**
	 *
	 * @param Reposit $reposit
	 * @param Record $record
	 * @param integer $versionId
	 * @return Record
	 */
	public static function depriveRecordToVersion(Reposit $reposit, Record $record, $versionId)
	{
		$toPath = $reposit->getVersionPath() .'/'. $record->rootname.'.'.$versionId.$record->extension;
		$fsdata=$record->getFsdata()->copy( $toPath, 0755, true);
		$record->getFsdata()->suppress();
		$record->setFsdata($fsdata);
		return $record;
	}

	/**
	 * @param Reposit $reposit
	 * @param Record $record
	 * @param integer $versionId
	 * @return Record
	 */
	public static function restoreRecordToHeadline(Reposit $reposit, Record $record)
	{
		$toPath = $reposit->getPath() .'/'. $record->rootname.'.'.$record->extension;
		$fsdata = $record->getFsdata()->copy( $toPath, 0755, true);
		$record->getFsdata()->suppress();
		$record->setFsdata($fsdata);
		return $record;
	}

	/**
	 * Create a new docfile as a history copy of $headDocfile.
	 * Return new docfile.
	 *
	 * @param DocfileVersion $headDocfile
	 */
	public static function depriveDocfileToIteration(DocfileVersion $headDocfile)
	{
		$docfile = clone($headDocfile);
		$docfile->newUid();
		$docfile->lock(AccessCode::HISTORY);
		$docfile->setLifeStage('HISTORY');
		return $docfile;
	}

	/**
	 * @param Document $headDocument
	 */
	public static function depriveDocumentToIteration(Document $headDocument)
	{
		$document=clone($headDocument);
		$document->newUid();
		$document->lock(AccessCode::HISTORY);
		$document->setLifeStage('HISTORY');
		return $document;
	}

	/**
	 * Create a new docfile as a history copy of $headDocfile.
	 * Return new docfile.
	 *
	 * @param DocfileVersion $headDocfile
	 */
	public static function depriveDocfileToVersion(DocfileVersion $headDocfile)
	{
		$docfile=clone($headDocfile);
		$docfile->newUid();
		$docfile->lock(AccessCode::HISTORY);
		$docfile->setLifeStage('HISTORY');
		return $docfile;
	}

	public static function depriveDocumentToVersion(Document $headDocument)
	{
		$document=clone($headDocument);
		$document->newUid();
		$document->lock(AccessCode::HISTORY);
		$document->setLifeStage('HISTORY');
		return $document;
	}


} //End of class

