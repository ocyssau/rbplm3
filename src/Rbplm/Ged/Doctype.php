<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged;

use Rbplm\AnyPermanent;

/**
 * @brief Doctype is type for document.
 *
 * Doctype are based on extension of main docfile of document and on document naming rules.
 * It may be too explicitly affected.
 * To doctype is associate scripts path and name to execute on events.
 *
 * @see Rbplm/Ged/DoctypeTest.php
 * @license GPL-V3: Rbplm/licence.txt
 */
class Doctype extends AnyPermanent
{

	static $classId = '569e921e55b03';

	/**
	 *
	 * @var string
	 */
	protected $number = null;

	/**
	 *
	 * @var string
	 */
	protected $description = null;

	/**
	 *
	 * @var array
	 */
	protected $fileExtensions = array();

	/**
	 *
	 * @var string
	 */
	protected $fileType = null;

	/**
	 *
	 * @var integer
	 */
	protected $priority = 0;

	/**
	 *
	 * @var string
	 */
	protected $icon = null;

	/** @var string */
	public $preStoreClass = null;

	/** @var string */
	public $preStoreMethod = null;

	/** @var string */
	public $postStoreClass = null;

	/** @var string */
	public $postStoreMethod = null;

	/** @var string */
	public $preUpdateClass = null;

	/** @var string */
	public $preUpdateMethod = null;

	/** @var string */
	public $postUpdateClass = null;

	/** @var string */
	public $postUpdateMethod = null;

	/**
	 *
	 * @var string
	 */
	protected $recognitionRegexp = null;

	/**
	 *
	 * @var string
	 */
	protected $visuFileExtension = null;

	/**
	 *
	 * @var boolean
	 */
	protected $mayBeComposite = false;

	/** @var \Rbplm\Ged\Document\Version */
	protected $template;

	/** @var integer */
	public $templateId;

	/** @var string */
	public $templateUid;

	/** @var \Rbplm\Any \Docseeder\Model\Template\Mask */
	protected $mask;

	/** @var integer */
	public $maskId;

	/** @var string */
	public $maskUid;

	/** @var \Rbplm\Any \Docseeder\Model\Number\Rule */
	protected $numberGenerator;

	/** @var string */
	protected $numberGeneratorClass;

	/**
	 *
	 * @var string
	 */
	public static $defaultDoctypeUid = 'doctype46g9A65';

	/**
	 *
	 * @var string
	 */
	public static $defaultDoctypeId = 1;

	/** @var boolean */
	public $docseeder;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Doctype
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['fileExtensions'])) ? $this->fileExtensions = $properties['fileExtensions'] : null;
		(isset($properties['fileType'])) ? $this->fileType = $properties['fileType'] : null;
		(isset($properties['priority'])) ? $this->priority = $properties['priority'] : null;
		(isset($properties['icon'])) ? $this->icon = $properties['icon'] : null;
		(isset($properties['recognitionRegexp'])) ? $this->recognitionRegexp = $properties['recognitionRegexp'] : null;
		(isset($properties['visuFileExtension'])) ? $this->visuFileExtension = $properties['visuFileExtension'] : null;
		(isset($properties['mayBeComposite'])) ? $this->mayBeComposite = $properties['mayBeComposite'] : null;
		(isset($properties['domain'])) ? $this->domain = $properties['domain'] : null;
		(isset($properties['templateId'])) ? $this->templateId = $properties['templateId'] : null;
		(isset($properties['templateUid'])) ? $this->templateUid = $properties['templateUid'] : null;
		(isset($properties['maskId'])) ? $this->maskId = $properties['maskId'] : null;
		(isset($properties['maskUid'])) ? $this->maskUid = $properties['maskUid'] : null;
		(isset($properties['docseeder'])) ? $this->docseeder = $properties['docseeder'] : null;

		/**/
		(isset($properties['preStoreClass'])) ? $this->preStoreClass = $properties['preStoreClass'] : null;
		(isset($properties['preStoreMethod'])) ? $this->preStoreMethod = $properties['preStoreMethod'] : null;
		(isset($properties['postStoreClass'])) ? $this->postStoreClass = $properties['postStoreClass'] : null;
		(isset($properties['postStoreMethod'])) ? $this->postStoreMethod = $properties['postStoreMethod'] : null;

		/**/
		(isset($properties['preUpdateClass'])) ? $this->preUpdateClass = $properties['preUpdateClass'] : null;
		(isset($properties['preUpdateMethod'])) ? $this->preUpdateMethod = $properties['preUpdateMethod'] : null;
		(isset($properties['postUpdateClass'])) ? $this->postUpdateClass = $properties['postUpdateClass'] : null;
		(isset($properties['postUpdateMethod'])) ? $this->postUpdateMethod = $properties['postUpdateMethod'] : null;

		/**/
		(isset($properties['numberGeneratorClass'])) ? $this->numberGeneratorClass = $properties['numberGeneratorClass'] : null;
		(isset($properties['numberGenerator'])) ? $this->numberGenerator = $properties['numberGenerator'] : null;

		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 *
	 * @param string $number
	 * @return \Rbplm\Ged\Doctype
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 *
	 * @param integer $int
	 * @return \Rbplm\Ged\Doctype
	 */
	public function setPriority($int)
	{
		$this->priority = $int;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getFileExtensions()
	{
		return $this->fileExtensions;
	}

	/**
	 *
	 * @return Doctype
	 */
	public function setFileExtensions($array)
	{
		$this->fileExtensions = $array;
		return $this;
	}

	/**
	 *
	 * @return Doctype
	 */
	public function addFileExtensions($string)
	{
		$this->fileExtensions[] = $string;
		return $this;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function mayBeComposite($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->mayBeComposite = $bool;
		}
		else {
			return $this->mayBeComposite;
		}
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return Doctype
	 */
	public function setTemplate($document)
	{
		$this->templateId = $document->getId();
		$this->templateUid = $document->getUid();
		$this->template = $document;
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function getTemplate($asId = false)
	{
		if ( $asId ) {
			return $this->templateId;
		}
		return $this->template;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Template\Mask $mask
	 * @return Doctype
	 */
	public function setMask($mask)
	{
		$this->maskId = $mask->getId();
		$this->maskUid = $mask->getUid();
		$this->mask = $mask;
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Template\Mask
	 */
	public function getMask($asId = false)
	{
		if ( $asId ) {
			return $this->maskId;
		}
		return $this->mask;
	}

	/**
	 * @param string $class
	 * @return Doctype
	 */
	public function setNumberGeneratorClass($class)
	{
		$this->numberGeneratorClass = $class;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumberGeneratorClass()
	{
		return $this->numberGeneratorClass;
	}
} /* End of class */
