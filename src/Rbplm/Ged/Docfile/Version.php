<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged\Docfile;

use Rbplm\AnyPermanent;
use Rbplm\Vault\Record;
use Rbplm\People\CurrentUser;
use Rbplm\People\UserInterface as User;
use Rbplm\Signal;
use Rbplm\Ged\AccessCode;
use Rbplm\Sys\Date as DateTime;

/**
 * @brief Version of a docfile.
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Ged/Docfile/VersionTest.php
 * @version $Rev: 820 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Version extends AnyPermanent
{

	static $classId = '569e92b86d248';

	const SIGNAL_PRE_CHECKOUT = 'checkout.pre';

	const SIGNAL_POST_CHECKOUT = 'checkout.post';

	const SIGNAL_PRE_CHECKIN = 'checkin.pre';

	const SIGNAL_POST_CHECKIN = 'checkin.post';

	const SIGNAL_PRE_DELETE = 'delete.pre';

	const SIGNAL_POST_DELETE = 'delete.post';

	const SIGNAL_PRE_LOCK = 'lock.pre';

	const SIGNAL_PRE_UNLOCK = 'unlock.pre';

	const SIGNAL_PRE_REPLACEFILE = 'replacefile.pre';

	const SIGNAL_POST_REPLACEFILE = 'replacefile.post';

	/**
	 *
	 * @var string
	 */
	protected $number = null;

	/**
	 *
	 * @var string
	 */
	public $description = null;

	/**
	 *
	 * @var User
	 */
	protected $lockBy = null;

	/** @var integer */
	public $lockById = null;

	/** @var string */
	public $lockByUid = null;

	/**
	 *
	 * @var DateTime
	 */
	protected $locked = null;

	/**
	 * ofDocfile docfile reference for this version
	 *
	 * @var \Rbplm\Ged\Docfile
	 */
	protected $ofDocfile = null;

	/** @var integer */
	public $ofDocfileId = null;

	/** @var string */
	public $ofDocfileUid = null;

	/**
	 *
	 * @var Version
	 */
	protected $from = null;

	/** @var integer */
	public $fromId = null;

	/** @var string */
	public $fromUid = null;

	/**
	 * Current data iteration
	 *
	 * @var \Rbplm\Vault\Record
	 */
	protected $data = null;

	/** @var integer */
	public $dataId = null;

	/** @var string */
	public $dataUid = null;

	/**
	 * Reference to vaulted data record iteration
	 * Its a array where key 0 is data and key 1 is iteration id
	 *
	 * @var array
	 */
	protected $previousData = null;

	/**
	 *
	 * @var array
	 */
	protected $roles = null;

	/**
	 *
	 * @var integer
	 */
	protected $mainrole = 4;

	/**
	 *
	 * @var integer
	 */
	protected $accessCode = 0;

	/**
	 *
	 * @var integer
	 */
	public $iteration = 1;

	/**
	 *
	 * @var integer
	 */
	public $version = 1;

	/**
	 *
	 * @var string
	 */
	protected $lifeStage = 'init';

	/**
	 *
	 * @return Version
	 */
	public static function init($name = null, $parent = null)
	{
		$obj = parent::init($name, $parent);
		$obj->created = new DateTime();
		$obj->updated = new DateTime();
		$obj->setCreateBy(CurrentUser::get());
		$obj->setUpdateBy(CurrentUser::get());
		$obj->setOwner(CurrentUser::get());
		$obj->number = $obj->getUid();
		$obj->setMainRole(Role::MAIN);
		return $obj;
	}

	/**
	 */
	public function __clone()
	{
		$this->newUid();
		$this->setCreated(new DateTime());
		$this->setUpdated(new DateTime());
		$this->setCreateBy(CurrentUser::get());
		$this->setUpdateBy(CurrentUser::get());
		$this->setOwner(CurrentUser::get());
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);

		(isset($properties['locked'])) ? $this->locked = new DateTime($properties['locked']) : null;
		(isset($properties['lockById'])) ? $this->lockById = $properties['lockById'] : null;
		(isset($properties['lockByUid'])) ? $this->lockByUid= $properties['lockByUid'] : null;
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['accessCode'])) ? $this->accessCode = $properties['accessCode'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['designation'])) ? $this->description = $properties['designation'] : null;
		(isset($properties['iteration'])) ? $this->iteration = $properties['iteration'] : null;
		(isset($properties['version'])) ? $this->version = $properties['version'] : null;
		(isset($properties['lifestage'])) ? $this->lifestage = $properties['lifestage'] : null;
		(isset($properties['fromId'])) ? $this->fromId = $properties['fromId'] : null;
		(isset($properties['fromUid'])) ? $this->fromUid = $properties['fromUid'] : null;
		(isset($properties['ofDocfileId'])) ? $this->ofDocfileId = $properties['ofDocfileId'] : null;
		(isset($properties['ofDocfileUid'])) ? $this->ofDocfileUid = $properties['ofDocfileUid'] : null;
		(isset($properties['dataId'])) ? $this->dataId = $properties['dataId'] : null;
		(isset($properties['dataUid'])) ? $this->dataUid = $properties['dataUid'] : null;
		(isset($properties['roles'])) ? $this->roles = $properties['roles'] : null;
		(isset($properties['mainrole'])) ? $this->mainrole = $properties['mainrole'] : null;

		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 *
	 * @param string $Number
	 * @return Version
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 * @param boolean $asUid
	 * @return User
	 */
	public function getLockBy($asUid = false)
	{
		if ( $asUid === true ) {
			return $this->lockByUid;
		}
		else {
			return $this->lockBy;
		}
	}

	/**
	 *
	 * @param User $LockBy
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setLockBy(User $lockBy)
	{
		$this->lockBy = $lockBy;
		$this->lockById = $lockBy->getId();
		$this->lockByUid = $lockBy->getUid();
		return $this;
	}

	/**
	 *
	 * @return integer|\Rbplm\Ged\Docfile\Version
	 */
	public function getOfDocfile($asId = false)
	{
		if ( $asId === true ) {
			return $this->ofDocfileId;
		}
		else {
			return $this->ofDocfile;
		}
	}

	/**
	 *
	 * @param \Rbplm\Ged\Docfile $ofDocfile
	 * @return Version
	 */
	public function setOfDocfile($ofDocfile)
	{
		$this->ofDocfile = $ofDocfile;
		$this->ofDocfileId = $ofDocfile->getId();
		$this->ofDocfileUid = $ofDocfile->getUid();
		return $this;
	}

	/**
	 *
	 * @return Version
	 */
	public function getFrom($asId = false)
	{
		if ( $asId === true ) {
			return $this->fromId;
		}
		else {
			return $this->from;
		}
	}

	/**
	 *
	 * @param Version $From
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setFrom(Version $from)
	{
		$this->from = $from;
		$this->fromId = $from->getId();
		$this->fromUid = $from->getUid();
		return $this;
	}

	/**
	 *
	 * @return Record
	 */
	public function getData($asId = false)
	{
		if ( $asId === true ) {
			return $this->dataId;
		}
		else {
			if ( !$this->data ) {
				$this->data = new Record();
			}
			return $this->data;
		}
	}

	/**
	 *
	 * @param Record $Data
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setData(Record $data)
	{
		$this->data = $data;
		$this->dataId = $data->getId();
		$this->dataUid = $data->getUid();
		return $this;
	}

	/**
	 *
	 * @param
	 *        	integer
	 * @return Version
	 */
	public function setMainrole($roleId)
	{
		$this->mainrole = $roleId;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getMainrole()
	{
		return $this->mainrole;
	}

	/**
	 *
	 * @return array
	 */
	public function getRoles()
	{
		return $this->roles;
	}

	/**
	 *
	 * @param
	 *        	integer
	 * @return Version
	 */
	public function addRole($roleId)
	{
		$this->roles[$roleId] = $roleId;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLifeStage()
	{
		return $this->lifeStage;
	}

	/**
	 *
	 * @param string $LifeStage
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setLifeStage($lifeStage)
	{
		$this->lifeStage = $lifeStage;
		return $this;
	}

	/**
	 * This method can be used to check if a Docfile is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 */
	public function checkAccess()
	{
		if ( $this->accessCode === false ) {
			return 100;
		}
		else {
			return (int)$this->accessCode;
		}
	}

	/**
	 * Get the collection of data for the docfile.
	 * Collection is a queue where the top is the last iteration of the data.
	 *
	 * @return array
	 */
	public function getPreviousData()
	{
		return $this->previousData[0];
	}

	/**
	 * Lock a file.
	 * This method is call by the checkout method. When a file is checkOut he is locked to prevent a second checkout.
	 *
	 * @param integer $code
	 *        	code to set for access code.
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function lock($code = AccessCode::CHECKOUT, User $by = null)
	{
		if ( $code == $this->accessCode ) {
			return $this;
		}

		/* Notiy observers on event */
		Signal::trigger(self::SIGNAL_PRE_LOCK, $this);

		$this->accessCode = (int)$code;
		$this->locked = new DateTime();

		if ( $by ) {
			$this->setLockBy($by);
		}

		return $this;
	}

	/**
	 * This method can be used to unlock a Document.
	 *
	 * @return \Rbplm\Ged\Docfile\Version
	 *
	 */
	public function unlock()
	{
		/* Notiy observers on event */
		Signal::trigger(self::SIGNAL_PRE_UNLOCK, $this);

		$this->accessCode = AccessCode::FREE;
		$this->lockBy = null;
		$this->lockById = null;
		$this->lockByUid = null;
		$this->locked = null;
		return $this;
	}
}
