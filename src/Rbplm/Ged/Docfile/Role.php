<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged\Docfile;

/**
 * Enum of ROLES
 * @brief Define Role of a docfile for the document. Role can be MAIN, VISU, PICTURE, or ANNEX.
 *
 *  A MAIN docfile is minimum document requierement.
 *  A MAIN docfile may be use to visualisation or a other docfile may be defined to the Role
 *  A PICTURE may be used for VISU
 *  All type may be ANNEX
 *
 */
class Role
{
	/** Main file */
	const MAIN = 1;

	/** A part of the document */
	const FRAGMENT = 4;

	/** Annex to document */
	const ANNEX = 8;

	/** To use to visualize document */
	const VISU = 16;

	/** A picture to show content of document */
	const PICTURE = 32;

	/** Converted in other format */
	const CONVERT = 64;

	/**
	 * Return the name of the role from his id
	 *
	 * @param int $roleId
	 * @return string
	 * @throws \Exception
	 */
	public static function getName($roleId)
	{
		switch($roleId){
			case self::MAIN:
				return 'MAIN';
				break;
			case self::FRAGMENT:
				return 'FRAGMENT';
				break;
			case self::ANNEX:
				return 'ANNEX';
				break;
			case self::VISU:
				return 'VISU';
				break;
			case self::PICTURE:
				return 'PICTURE';
				break;
			case self::CONVERT:
				return 'CONVERT';
				break;
			default:
				throw new \Exception('Unknow role id');
		}
	}

	/**
	 * Return a arraywhere key is role id and value is role name
	 * @return array
	 */
	public static function toArray()
	{
		return array(
			self::MAIN=>'MAIN',
			self::FRAGMENT=>'FRAGMENT',
			self::ANNEX=>'ANNEX',
			self::VISU=>'VISU',
			self::PICTURE=>'PICTURE',
			self::CONVERT=>'CONVERT',
			self::MAIN=>'MAIN',
			self::MAIN=>'MAIN',
			self::MAIN=>'MAIN',
		);
	}

} //End of class
