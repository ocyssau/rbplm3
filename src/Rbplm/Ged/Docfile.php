<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged;

use Rbplm\AnyPermanent;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\People\CurrentUser;
use Rbplm\Signal;
use Rbplm\Link\Collection as LinkCollection;
use Rbplm\Sys\Date as DateTime;

/**
 * @brief Base definition for link to vault recorded datas.
 *
 * This is a class generated with \Rbplm\Model\Generator.
 *
 * @see Rbplm/Ged/DocfileTest.php
 * @version $Rev: 766 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Docfile extends AnyPermanent
{
	static $classId = '569e91f77eb45';

	const SIGNAL_POST_SETNUMBER = 'setnumber.post';
	const SIGNAL_POST_NEWVERSION = 'newversion.post';

	/**
	 * @var string
	 */
	protected $_number = null;

	/**
	 * @var string
	 */
	protected $description = null;

	/**
	 * Collection of Docfile_Version
	 *
	 * @var LinkCollection
	 */
	protected $_versions = null;

	/**
	 * @var integer
	 */
	protected $lastVersionId = null;

	/**
	 * Init properties.
	 * Use to init properties on new object. Must be call after new object call:
	 * @code
	 * $object = new Docfile_Version();
	 * $object->init();
	 * @endcode
	 *
	 * @return \Rbplm\Ged\Docfile
	 */
	public static function init($name=null, $parent=null)
	{
		$obj=parent::init($name, $parent);
		$obj->created = new DateTime();
		$obj->updated = new DateTime();
		$obj->setCreateBy(CurrentUser::get());
		$obj->setUpdateBy(CurrentUser::get());
		$obj->setOwner(CurrentUser::get());
		$obj->_number = $obj->getUid();
		return $obj;
	}

	/**
	 * @return string
	 */
	public function getNumber()
	{
		return $this->_number;
	}

	/**
	 * @param string $Number
	 * @return \Rbplm\Ged\Docfile
	 */
	public function setNumber($Number)
	{
		$this->_number = $Number;
		Signal::trigger(self::SIGNAL_POST_SETNUMBER, $this);
		return $this;
	}

	/**
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function getVersions()
	{
		if( !$this->_versions ){
			$this->_versions = new LinkCollection( array('name'=>'versions'), $this );
			$this->getLinks()->add( $this->_versions );
		}
		return $this->_versions;
	}

	/**
	 * Create a new version for current Docfile.
	 *
	 * @param	string
	 * @param	\Rbplm\Org\Unit
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function newVersion( $name, \Rbplm\Org\Unit $ou = null){
		$version = new DocfileVersion();
		$version->setName( $name );
		if($ou){
			$version->setParent( $ou );
		}
		$version->version = $this->lastVersionId++;
		$version->setBase( $this );
		$this->getVersions()->add( $version );

		Signal::trigger(self::SIGNAL_POST_NEWVERSION, $this);

		return $version;
	}
}
