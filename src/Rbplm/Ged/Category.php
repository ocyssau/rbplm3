<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged;

use Rbplm\Org\Unit;

/**
 * @brief Category associate to a Document.
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Ged/CategoryTest.php
 * @version $Rev: 760 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Category extends Unit
{

    static $classId = '569e918a134ca';
    
    /**
     * @var string
     */
    protected $_icon = null;

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->_icon;
    }

    /**
     * @param string $Icon
     * @return void
     */
    public function setIcon($Icon)
    {
        $this->_icon = $Icon;
        return $this;
    }
}
