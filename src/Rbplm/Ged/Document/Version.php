<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged\Document;

use Rbplm\AnyPermanent;
use Rbplm\People\CurrentUser;
use Rbplm\People\UserInterface as User;
use Rbplm\Ged\Category;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Docfile;
use Rbplm\Signal;
use Rbplm\Ged\AccessCode;
use Rbplm\Sys\Date as DateTime;

/**
 *
 * @brief Version of a document.
 *
 * @see VersionTest
 * @see \Rbplm\Ged\Document $Rev: 820 $
 *
 *      Emited signals :
 *      - self::SIGNAL_POST_SETSTATE
 *      - self::SIGNAL_POST_SETNUMBER
 *      - self::SIGNAL_PRE_CHECKIN
 *      - self::SIGNAL_POST_CHECKIN
 *      - self::SIGNAL_PRE_UPDATE
 *      - self::SIGNAL_POST_UPDATE
 *      - self::SIGNAL_PRE_CHECKOUT
 *      - self::SIGNAL_POST_CHECKOUT
 *      - self::SIGNAL_PRE_LOCK
 *      - self::SIGNAL_PRE_UNLOCK
 *      - self::SIGNAL_PRE_DELETE
 *      - self::SIGNAL_POST_DELETE
 *      - self::SIGNAL_PRE_ADDFILE
 *      - self::SIGNAL_POST_ADDFILE
 *      @verbatim
 *      @endverbatim
 *
 */
class Version extends AnyPermanent
{

	static $classId = '569e92709feb6';

	const SIGNAL_POST_SETNUMBER = 'setnumber.post';

	const SIGNAL_POST_SETSTATE = 'setstate.post';

	const SIGNAL_PRE_CREATE = 'create.pre';

	const SIGNAL_POST_CREATE = 'create.post';

	const SIGNAL_PRE_EDIT = 'edit.pre';

	const SIGNAL_POST_EDIT = 'edit.post';

	const SIGNAL_PRE_SAVE = 'save.pre';

	const SIGNAL_POST_SAVE = 'save.post';

	const SIGNAL_PRE_CHECKOUT = 'checkout.pre';

	const SIGNAL_POST_CHECKOUT = 'checkout.post';

	const SIGNAL_PRE_CHECKIN = 'checkin.pre';

	const SIGNAL_POST_CHECKIN = 'checkin.post';

	const SIGNAL_PRE_UPDATE = 'update.pre';

	const SIGNAL_POST_UPDATE = 'update.post';

	const SIGNAL_PRE_CANCELCHECKOUT = 'cancelcheckout.pre';

	const SIGNAL_POST_CANCELCHECKOUT = 'cancelcheckin.post';

	const SIGNAL_PRE_DELETE = 'delete.pre';

	const SIGNAL_POST_DELETE = 'delete.post';

	const SIGNAL_PRE_COPY = 'copy.pre';

	const SIGNAL_POST_COPY = 'copy.post';

	const SIGNAL_PRE_MOVE = 'move.pre';

	const SIGNAL_POST_MOVE = 'move.post';

	const SIGNAL_PRE_LOCK = 'lock.pre';

	const SIGNAL_POST_LOCK = 'lock.post';

	const SIGNAL_PRE_UNLOCK = 'unlock.pre';

	const SIGNAL_POST_UNLOCK = 'unlock.post';

	const SIGNAL_PRE_NEWVERSION = 'newversion.pre';

	const SIGNAL_POST_NEWVERSION = 'newversion.post';

	const SIGNAL_PRE_ADDFILE = 'addfile.pre';

	const SIGNAL_POST_ADDFILE = 'addfile.post';

	const DF_ROLE_MAIN = 'df_role_main';

	const DF_ROLE_MAINVISU = 'df_role_mainvisu';

	const DF_ROLE_MAINPICTURE = 'df_role_mainpicture';

	const DF_ROLE_MAINANNEX = 'df_role_mainannex';

	const DF_ROLE_VISU = 'df_role_visu';

	const DF_ROLE_PICTURE = 'df_role_picture';

	const DF_ROLE_ANNEX = 'df_role_annex';

	/**
	 *
	 * @var string
	 */
	protected $number = null;

	/**
	 *
	 * @var integer
	 */
	protected $accessCode = 0;

	/**
	 *
	 * @var string
	 */
	public $description = null;

	/**
	 *
	 * @var string
	 */
	protected $label = null;

	/**
	 * Identifiant for the iteration.
	 *
	 * @var integer
	 */
	public $iteration = 1;

	/**
	 * Identifiant for the version.
	 *
	 * @var integer
	 */
	public $version = 1;

	/**
	 *
	 * @var string
	 */
	public $lifeStage = 'init';

	/**
	 *
	 * @var \Rbplm\Ged\Branch
	 */
	protected $branch = null;

	/**
	 *
	 * @var integer
	 */
	public $branchId = null;

	/**
	 *
	 * @var string
	 */
	public $branchUid = null;

	/**
	 *
	 * @var DateTime
	 */
	protected $locked = null;

	/**
	 *
	 * @var User
	 */
	protected $lockBy = null;

	/** @var integer */
	public $lockById = null;

	/** @var string */
	public $lockByUid = null;

	/**
	 *
	 * @var Version
	 */
	protected $from = null;

	/** @var integer */
	public $fromId = null;

	/** @var string */
	public $fromUid = null;

	/**
	 *
	 * @var \Rbplm\Ged\Document
	 */
	protected $ofDocument;

	/** @var integer */
	public $ofDocumentId;

	/** @var string */
	public $ofDocumentUid;

	/**
	 *
	 * @var \Rbplm\Ged\Doctype
	 */
	protected $doctype = null;

	/** @var integer */
	public $doctypeId = null;

	/** @var string */
	public $doctypeUid = null;

	/**
	 *
	 * @var \Rbplm\Ged\Category
	 */
	protected $category = null;

	/** @var integer */
	public $categoryId = null;

	/** @var string */
	public $categoryUid = null;

	/**
	 * Collection of docfile attach to this Document
	 *
	 * @var array
	 */
	protected $docfiles = [];

	/** @var boolean */
	public $asTemplate = false;

	/**
	 *
	 * @var string
	 */
	protected $tags = '';

	/**
	 * Init properties.
	 *
	 * @param string $name
	 * @param \Rbplm\Org\Unit $parent
	 * @return \Rbplm\Ged\Document\Version
	 */
	public static function init($name = NULL, $parent = NULL)
	{
		$class = get_called_class();
		$obj = new $class(null, $parent);
		$obj->newUid();
		$obj->setCreated(new DateTime());
		$obj->setUpdated(new DateTime());
		$obj->setCreateBy(CurrentUser::get());
		$obj->setUpdateBy(CurrentUser::get());
		$obj->setOwner(CurrentUser::get());
		$obj->setNumber($obj->getUid());
		$obj->setName($name);
		$obj->doctypeUid = Doctype::$defaultDoctypeUid;
		$obj->doctypeId = Doctype::$defaultDoctypeId;
		$obj->branchId = 1;
		$obj->branchUid = 'master';
		return $obj;
	}

	/**
	 */
	public function __clone()
	{
		$this->newUid();
		$this->setCreated(new DateTime());
		$this->setUpdated(new DateTime());
		$this->setCreateBy(CurrentUser::get());
		$this->setUpdateBy(CurrentUser::get());
		$this->setOwner(CurrentUser::get());
		$this->docfiles = [];
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);

		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['accessCode'])) ? $this->accessCode = $properties['accessCode'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['designation'])) ? $this->description = $properties['designation'] : null;
		(isset($properties['label'])) ? $this->label = $properties['label'] : null;
		(isset($properties['iteration'])) ? $this->iteration = $properties['iteration'] : null;
		(isset($properties['version'])) ? $this->version = $properties['version'] : null;
		(isset($properties['lifeStage'])) ? $this->lifeStage = $properties['lifeStage'] : null;
		(isset($properties['fromId'])) ? $this->fromId = $properties['fromId'] : null;
		(isset($properties['fromUid'])) ? $this->fromUid = $properties['fromUid'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		(isset($properties['asTemplate'])) ? $this->asTemplate = (boolean)$properties['asTemplate'] : null;
		(isset($properties['tags'])) ? $this->tags = $properties['tags'] : null;
		(isset($properties['doctypeId'])) ? $this->doctypeId = $properties['doctypeId'] : null;
		(isset($properties['doctypeUid'])) ? $this->doctypeUid = $properties['doctypeUid'] : null;

		/* enable null values : */
		(array_key_exists('categoryId', $properties)) ? $this->categoryId = $properties['categoryId'] : null;
		(array_key_exists('categoryUid', $properties)) ? $this->categoryUid = $properties['categoryUid'] : null;
		(array_key_exists('branchId', $properties)) ? $this->branchId = $properties['branchId'] : null;
		(array_key_exists('branchUid', $properties)) ? $this->branchUid = $properties['branchUid'] : null;
		(array_key_exists('ofDocumentId', $properties)) ? $this->ofDocumentId = $properties['ofDocumentId'] : null;
		(array_key_exists('ofDocumentUid', $properties)) ? $this->ofDocumentUid = $properties['ofDocumentUid'] : null;
		(array_key_exists('lockById', $properties)) ? $this->lockById = $properties['lockById'] : null;
		(array_key_exists('lockByUid', $properties)) ? $this->lockByUid = $properties['lockByUid'] : null;

		if ( isset($properties['locked']) ) {
			$date = $properties['locked'];
			if ( $date instanceof \DateTime ) {
				$this->locked = $date;
			}
			elseif ( is_string($date) ) {
				$this->locked = new DateTime($properties['locked']);
			}
		}

		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getDocfiles()
	{
		return $this->docfiles;
	}

	/**
	 *
	 * @return Docfile\Version
	 */
	public function getMainDocfile()
	{
		reset($this->docfiles);
		return current($this->docfiles);
	}

	/**
	 * Add docfile to docfiles array.
	 * Key is the docfile uid to prevent twice reference to same docfile
	 *
	 * @param
	 *        	Docfile\Version
	 */
	public function addDocfile(Docfile\Version $docfile)
	{
		$this->docfiles[$docfile->getUid()] = $docfile;
		$docfile->setParent($this, false);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 *
	 * @param string $number
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		Signal::trigger(self::SIGNAL_POST_SETNUMBER, $this);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 *
	 * @param string $string
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setLabel($string)
	{
		$this->label = $string;
		return $this;
	}

	/**
	 *
	 * @return Category
	 */
	public function getCategory($asId = false)
	{
		if ( $asId == true ) {
			return $this->categoryId;
		}
		return $this->category;
	}

	/**
	 *
	 * @param Category $category
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setCategory(Category $category)
	{
		$this->category = $category;
		$this->categoryId = $category->getId();
		$this->categoryUid = $category->getUid();
		return $this;
	}

	/**
	 * Lock a Document with code.
	 *
	 * The value of the access code permit or deny action:
	 *
	 * @see AccessCode
	 *
	 * @param integer $code
	 *        	new access code. value of constants in AccessCode::*
	 * @param User $by
	 * @return \Rbplm\Ged\Document\Version
	 *
	 */
	function lock($code = AccessCode::CHECKOUT, User $by = null)
	{
		if ( $code == $this->accessCode ) {
			return $this;
		}

		/* Notiy observers on event */
		Signal::trigger(self::SIGNAL_PRE_LOCK, $this);

		$this->accessCode = (int)$code;
		$this->locked = new DateTime();

		if ( $by ) {
			$this->setlockBy($by);
		}

		return $this;
	}

	/**
	 * This method can be used to unlock a Document.
	 *
	 * @return \Rbplm\Ged\Document\Version
	 *
	 */
	public function unlock()
	{
		/* Notiy observers on event */
		Signal::trigger(self::SIGNAL_PRE_UNLOCK, $this);

		$this->accessCode = AccessCode::FREE;
		$this->lockBy = null;
		$this->lockById = null;
		$this->lockByUid = null;
		$this->locked = null;
		return $this;
	}

	/**
	 *
	 * @param boolean $asUid
	 * @return User | string
	 */
	public function getLockBy($asUid = false)
	{
		if ( $asUid == true ) {
			return $this->lockByUid;
		}
		return $this->lockBy;
	}

	/**
	 *
	 * @return DateTime|String
	 */
	public function getLocked($format = null)
	{
		if ( !$this->locked instanceof DateTime ) {
			$this->locked = new DateTime();
		}

		if ( $format ) {
			return $this->locked->format($format);
		}
		else {
			return $this->locked;
		}
	}

	/**
	 *
	 * @param User $lockBy
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setlockBy(User $lockBy)
	{
		$this->lockBy = $lockBy;
		$this->lockById = $this->lockBy->getId();
		$this->lockByUid = $this->lockBy->getUid();
		return $this;
	}

	/**
	 *
	 * @return Version
	 */
	public function getFrom($asId = false)
	{
		if ( $asId == true ) {
			return $this->fromId;
		}
		return $this->from;
	}

	/**
	 *
	 * @param Version $From
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setFrom(Version $from)
	{
		$this->from = $from;
		$this->fromId = $this->from->getId();
		$this->fromUid = $this->from->getUid();
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Doctype
	 */
	public function getDoctype($asId = false)
	{
		if ( $asId == true ) {
			return $this->doctypeId;
		}
		return $this->doctype;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Doctype $doctype
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setDoctype(Doctype $doctype)
	{
		$this->doctype = $doctype;
		$this->doctypeId = $doctype->getId();
		$this->doctypeUid = $doctype->getUid();
		return $this;
	}

	/**
	 * Getter for ofDocument document
	 *
	 * @return \Rbplm\Ged\Document
	 */
	public function getOfDocument($asId = false)
	{
		if ( $asId == true ) {
			return $this->ofDocumentId;
		}
		return $this->ofDocument;
	}

	/**
	 * Setter for ofDocument document
	 *
	 * @param \Rbplm\Ged\Document $document
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function setOfDocument(\Rbplm\Ged\Document $document)
	{
		$this->ofDocument = $document;
		$this->ofDocumentId = $document->getId();
		$this->ofDocumentUid = $document->getUid();
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLifeStage()
	{
		return $this->lifeStage;
	}

	/**
	 *
	 * @param string $lifeStage
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setLifeStage($lifeStage)
	{
		$this->lifeStage = $lifeStage;
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Branch $branch
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setBranch($branch)
	{
		$this->branch = $branch;
		$this->branchId = $branch->getId();
		$this->branchUid = $branch->getUid();
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Branch $branch
	 */
	public function getBranch($asId = false)
	{
		if ( $asId == true ) {
			return $this->branchId;
		}
		return $this->branch;
	}

	/**
	 *
	 * @return string
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 *
	 * @param string $string
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public function setTags($string)
	{
		$this->tags = $string;
		return $this;
	}

	/**
	 * This method can be used to check if a Document is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 *
	 * @return integer
	 */
	public function checkAccess()
	{
		if ( $this->accessCode === false ) {
			return 100;
		}
		else {
			return (int)$this->accessCode;
		}
	}
} /* End of class */
