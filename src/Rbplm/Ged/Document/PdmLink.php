<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged\Document;

/**
 * @brief link between document
 *
 */
class PdmLink extends \Rbplm\Link
{
    public static $classId = 'pdmlink4r3v49';
}
