<?php
//%LICENCE_HEADER%

namespace Rbplm\Ged\Document;

/**
 * @brief link between document
 *
 */
class Link extends \Rbplm\Link
{
    public static $classId = '56a3f337b1a82';


    /**
     *
     * @var boolean
     */
    public $isComponent = false;


    /**
     * Link type
     *
     * @var string
     */
    public $linType;

    /**
     * Hydrator.
     * Load the properties in the mapped object.
     *
     * @param array $properties
     *        	\PDO fetch result to load
     * @return Link
     */
    public function hydrate(array $properties)
    {
    	parent::hydrate($properties);
    	(isset($properties['isComponent'])) ? $this->isComponent = $properties['isComponent'] : null;
    	(isset($properties['linType'])) ? $this->linType = $properties['linType'] : null;
    	return $this;
    }
}
