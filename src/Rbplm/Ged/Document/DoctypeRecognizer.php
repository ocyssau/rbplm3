<?php
// %LICENCE_HEADER%
namespace Rbplm\Ged\Document;

use Rbplm\Ged\Doctype;
use Rbplm\Ged\Exception;

/**
 * @brief To recognize the type of Document
 *
 * @see DoctypeDao::loadFromDocument
 * @see Emited signals :
 *
 *      @verbatim
 *      @endverbatim
 *
 */
class DoctypeRecognizer
{

	public static $genericRegEx;

	/**
	 *
	 * @param String $extension
	 * @param String $type
	 * @return \Rbplm\Ged\Doctype
	 * @throws \Rbplm\Ged\Exception SEE DoctypeDao::loadFromDocument
	 */
	function setDocType($document, $extension, $type)
	{
		// If there is at least one doctype link to container,
		// then doctype choice is restricted to this list.
		// First, test if there is at least one doctype link to container
		if ( $this->getContainer()->hasDoctype() ) {
			$doctypeList = $this->getContainer()->getDoctypes(extension, $type, true);
		}
		else { // If no doctypes links to container, get all doctypes
			$dtparams = array();
			if ( !is_null($extension) ) $dtparams['exact_find']['file_extension'] = $extension;
			if ( !is_null($type) ) $dtparams['exact_find']['file_type'] = $type;
			$doctypeList = Doctype::get()->getAll($dtparams);
		}
		// If no valid doctypes, then this Document is not authorized
		if ( !$doctypeList ) {
			throw new Exception('This doctype is not permit');
		}

		// For each doctype test if current Document corresponding to regex
		foreach( $doctypeList as $type ) {
			if ( !empty($type['recognition_regexp']) ) {
				if ( preg_match('/' . $type['recognition_regexp'] . '/', $this->getProperty('document_number')) ) {
					$doctype = $type; // assign doctype and exit loop
					break;
				}
			}
			else {
				$genericDoctype = $type; // if recognition_regexp field is empty, its a generic doctype
			}
		}

		// If no match doctypes, then assign the generic doctype, else return false
		if ( !$doctype && $genericDoctype ) {
			/* But the number must be respect the defined generic regex */
			$genericRegEx = self::$genericRegEx;
			if ( !empty($genericRegEx) ) {
				if ( preg_match('/' . $genericRegEx['recognition_regexp'] . '/', $this->getProperty('document_number')) ) {
					$doctype = $genericDoctype;
				}
			}
			else {
				$doctype = $genericDoctype;
			}
		}

		if ( !$doctype ) {
			throw new Exception('This doctype is not permit');
		}

		$this->setProperty('doctype_id', $doctype['doctype_id']);
		return $this->doctype = Doctype::get($doctype['doctype_id']);
	} // End of method
}
