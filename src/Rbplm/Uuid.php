<?php
// %LICENCE_HEADER%
namespace Rbplm;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 *
 * Note you need php-uuid installed.
 * @code
 * //On ubuntu, do simply
 * sudo apt-get install php5-uuid
 * @endcode
 *
 * @see http://fr.wikipedia.org/wiki/Universal_Unique_Identifier
 * @author Marius Karthaus
 * @author Eric COSNARD
 * @author Olivier CYSSAU
 *
 */
class Uuid
{

	/**
	 *
	 * @var \Rbplm\Uuid\GeneratorInterface
	 */
	public static $generator;

	/**
	 *
	 * @var string
	 */
	public static $type;

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function init($type = 'EmulateUuid')
	{
		$class = '\Rbplm\Uuid\\' . $type;
		self::$generator = new $class();
		self::$type = $type;
		return self::$generator;
	}

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid()
	{
		return self::$generator->newUid();
	}

	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * @param
	 *        	string uuid $uuid
	 * @return string uuid
	 */
	public static function format($uuid)
	{
		return self::$generator->format($uuid);
	}

	/**
	 * Compare 2 uuid and return true if same value.
	 *
	 * @param
	 *        	string uuid $uuid1
	 * @param
	 *        	string uuid $uuid2
	 * @return boolean
	 */
	public static function compare($uuid1, $uuid2)
	{
		if ( strtolower(trim($uuid1, '{}')) == strtolower(trim($uuid2, '{}')) ) {
			return true;
		}
		else {
			return false;
		}
	}
}
