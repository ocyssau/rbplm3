<?php
//%LICENCE_HEADER%
namespace Rbplm\Dao;

use Rbplm\Dao\Filter\Op;

interface FilterInterface
{

	/**
	 *
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Op::OP_*
	 * @return FilterInterface
	 */
	public function andfind($what, $where, $op = Op::CONTAINS);

	/**
	 *
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Op::OP_*
	 * @return FilterInterface
	 */
	public function orfind($what, $where, $op = Op::CONTAINS);

	/**
	 * @param integer	$page
	 * @param integer	$limit
	 * @return FilterInterface
	 */
	public function page($page, $limit);

	/**
	 * @param string	$by
	 * @param string	$dir = ASC|DESC
	 * @return FilterInterface
	 */
	public function sort($by, $dir = 'ASC');

	/**
	 * @return string
	 */
	public function __toString();

	/**
	 * @param array	$select
	 * @return FilterInterface
	 */
	public function select($select);

	/**
	 * @return string
	 */
	public function selectToString();

	/**
	 * @return string
	 */
	public function orderToString();

	/**
	 * @return integer
	 */
	public function getLimit();

	/**
	 * @return integer
	 */
	public function getOffset();

	/**
	 * Return the direction for sorting : 
	 * 		ASC for ascendant direction
	 * 		DESC for descendant direction
	 * 
	 * @return string
	 */
	public function getOrder();

	/**
	 * Return columns names to used for sorting.
	 * If $asString is true, return each columns separated by ","
	 * Else return all columns in a array
	 *
	 * @return string|array
	 */
	public function getSort($asString = true);
}

