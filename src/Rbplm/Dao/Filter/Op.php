<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao\Filter;

/**
 *
 * Enum for operator
 *
 */
class Op
{
	const CONTAINS = 'contains'; 		// ~
	const NOTCONTAINS = 'notcontains'; 	// !~
	const BEGIN = 'begin'; 				// .*
	const NOTBEGIN = 'notbegin';		// !.*
	const END = 'end';					// *
	const NOTEND = 'notend';			// !*
	const LIKE = 'like';				// like
	const MATCH = 'match';				// MATCH()
	const EQUAL = 'equal';				// =
	const NOTEQUAL = 'notequal';		// !=
	const IN = 'in';					//
	const NOTIN = 'notin';				//
	const SUP = 'sup';					// >
	const EQUALSUP = 'equalsup';		// =>
	const INF = 'inf';					// <
	const EQUALINF = 'equalinf';		// =<
	const NOTBETWEEN = 'notbetween'; 	// NOT BETWEEN
	const REGEX = 'regex'; 				// MATCH REGEX
	const ISNULL = 'null';				//
	const NOTNULL = 'notnull';			//
	const FINDINSET = 'findinset';			//FIND_IN_SET>0 condition
	const WITHPASSTHROUGH = 'withpassthrough'; //string search contains pass through chars
	
	const OP_CONTAINS = 'contains'; 		// ~
	const OP_NOTCONTAINS = 'notcontains'; 	// !~
	const OP_BEGIN = 'begin'; 				// .*
	const OP_NOTBEGIN = 'notbegin';		// !.*
	const OP_END = 'end';					// *
	const OP_NOTEND = 'notend';			// !*
	const OP_LIKE = 'like';				// like
	const OP_EQUAL = 'equal';				// =
	const OP_NOTEQUAL = 'notequal';		// !=
	const OP_IN = 'in';					//
	const OP_NOTIN = 'notin';				//
	const OP_SUP = 'sup';					// >
	const OP_EQUALSUP = 'equalsup';		// =>
	const OP_INF = 'inf';					// <
	const OP_EQUALINF = 'equalinf';		// =<
	const OP_NOTBETWEEN = 'notbetween'; 	// NOT BETWEEN

} /* End of class */
