<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;


/**
 * @brief Generic storage class helps to manage global data.
 *
 */
class Registry extends \ArrayObject
{

    /**
     * Singleton pattern.
     * @var Registry
     */
    private static $_singleton = null;

    /**
     * If false, add methode is unactive
     */
    private $_isActive = true;

    /**
     * Constructs a parent \ArrayObject with default
     * ARRAY_AS_PROPS to allow acces as an object
     *
     * @param array $array data array
     * @param integer $flags \ArrayObject flags
     */
    public function __construct($array = array(), $flags = parent::ARRAY_AS_PROPS)
    {
        parent::__construct($array, $flags);
    }

    /**
     * Generate Uid from classId and object Id.
     *
     * @param string $class
     * @param string $id
     */
    public static function hash($class, $id)
    {
    	return $id.$class;
    }

    /**
     * Singleton pattern.
     *
     * @return Registry
     */
    public static function singleton()
    {
    	if( !self::$_singleton ){
    		self::$_singleton = new self();
    	}
    	return self::$_singleton;
    }


    /**
     * Singleton pattern.
     *
     * @return Registry
     */
    public function isActive($bool = null)
    {
    	if( is_bool($bool) ){
    		$this->_isActive = $bool;
    	}
    	return $this->_isActive;
    }

    /**
     * @return Registry
     */
    public function getFromUid($uid)
    {
    	if( $uid && $this->offsetExists($uid) ){
    		return $this->offsetGet($uid);
    	}
    	return null;
    }

    /**
     * @return Registry
     */
    public function getFromId($id, $classId)
    {
    	$hash = $id.$classId;
    	if( $id && $this->offsetExists($hash) ){
    		return $this->offsetGet($hash);
    	}
    	return null;
    }

    /**
     *
     * @param Any
     */
    public function add( $anyobject )
    {
    	if($this->_isActive){
    		$this->offsetSet($anyobject->getUid(), $anyobject);
    		$id = $anyobject->getId();
    		if($id){
    			$classId = $anyobject::$classId;
    			$hash = $id.$classId;
    			$this->offsetSet($hash, $anyobject);
    		}
    	}
    	return $this;
    }

    /**
     *
     * @param Any
     */
    public function exist( $uid )
    {
    	return $this->offsetExists($uid);
    }


    /**
     *
     * @param Any
     */
    public function reset( $uid = null )
    {
    	if($uid){
    		$this->offsetUnset($uid);
    	}
    	else{
    		self::$_singleton = new self();
    	}
    }

}
