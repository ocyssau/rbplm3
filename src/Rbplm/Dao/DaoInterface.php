<?php
// %LICENCE_HEADER%
namespace Rbplm\Dao;

/**
 *
 * @brief Interface for Dao to single object.
 */
interface DaoInterface
{

	/**
	 * Return the connexion to db
	 *
	 * @throws Exception
	 * @return \PDO
	 */
	public function getConnexion();

	/**
	 * Set the connexion to db
	 *
	 * @param $conn \PDO
	 * @throws Exception
	 * @return DaoInterface
	 */
	public function setConnexion($conn);

	/**
	 * Basic options may be :
	 * 'lock' =>boolean If true, lock the db records in accordance to the use db system.
	 * Default is false.
	 * 'force' =>boolean If true, force to query db to reload links. Default is false.
	 * 'expectedproperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
	 * 'withtrans' boolean If true, open and close a transaction before/after execution of save*, delete* methodes
	 *
	 * @param
	 *        	string option name
	 * @param
	 *        	string option value
	 * @return DaoInterface
	 */
	public function setOption($key, $value);

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string|\Rbplm\Dao\FilterInterface $filter
	 *        	[OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Filter
	 * @param array $bind
	 * @throws \Rbplm\Sys\Exception
	 * @return MappedInterface
	 */
	public function load(MappedInterface $mapped, $filter = null, $bind = null);

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string $uid
	 * @throws Exception
	 * @return DaoInterface
	 *
	 */
	public function loadFromUid(MappedInterface $mapped, $uid);

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string $id
	 * @throws Exception
	 * @return DaoInterface
	 */
	public function loadFromId(MappedInterface $mapped, $id);

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string $name
	 * @throws Exception
	 * @return DaoInterface
	 */
	public function loadFromName(MappedInterface $mapped, $name);

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string $sql
	 * @param array $bind
	 * @throws Exception
	 */
	public function loadFromSql(MappedInterface $mapped, $sql, $bind = null);

	/**
	 * To render permanent.
	 * Update or create the records.
	 *
	 * @param MappedInterface $mapped
	 * @throws Exception
	 * @return MappedInterface
	 */
	public function save(MappedInterface $mapped);

	/**
	 * Update a entity from array
	 * $data = array('property name in app mode'=>'property value')
	 *
	 * @param integer $id
	 *        	Id of entity to update
	 * @param array $data
	 * @return DaoInterface
	 */
	public function updateFromArray($id, $data);

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param MappedInterface $mapped
	 * @param array $row
	 *        	\PDO fetch result to load
	 * @param boolean $fromApp
	 *        	If true, assume that keys $row are name of properties as set in model, else are set as in persitence system.
	 * @return MappedInterface
	 */
	public function hydrate($mapped, array $row, $fromApp = false);

	/**
	 * Suppress current record in database
	 *
	 * @param MappedInterface $mapped
	 * @param boolean $withChilds
	 *        	If true, suppress all childs and childs of childs
	 * @throws Exception
	 * @return DaoInterface
	 */
	public function delete($mapped, $withChilds = false);
}
