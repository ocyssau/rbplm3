<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;

/**
 * @brief Factory for Dao.
 *
 * Factory to instanciate DAOs from a specified model classe id.
 *
 * A map may be use to convert model class to dao class.
 *
 */
interface FactoryInterface
{

    /**
     * Singleton getter
     * @return FactoryInterface
     */
    public static function get();

    /**
     * @param string $key
     * @param string $value
     */
    public function setOption( $key, $value );

    /**
     * Map for convert model class to dao class
     *
     * @param array $map
     */
    public function setMap( array $map );

    /**
     * Set the connexion to use
     *
     * @param Connexion
     * @return FactoryInterface
     */
    public function setConnexion($conn);

    /**
     * Return the using connexion
     * @return Connexion
     */
    public function getConnexion();

    /**
     * Return a dao object for the mapped object.
     *
	 * @param \Rbplm\Any|integer $anyOrId
     * @return DaoInterface
     */
    public function getDao( $anyOrId );

    /**
     * Return a ListInterface object for the mapped object.
     *
     * @param string $classId 	Buizness object class id
     * @return ListInterface
     */
    public function getList( $classId );

    /**
     * Return a FilterInterface object for the mapped object.
     *
     * @param string $classId 	Buizness object class id
     * @return FilterInterface
     */
    public function getFilter( $classId );

    /**
     * Return a instance of class specified by id.
     *
     * @param string $classId 	Buizness object class id
     * @return \Rbplm\AnyPermanent
     */
    public function getModel( $classId );

    /**
     * Return a initialized instance of class specified by id.
     *
     * @return \Rbplm\AnyPermanent
     */
    public function getNewModel( $classId );

    /**
     * Return the dao class name from config.
     *
     * @param string $classId 	Buizness object class id
     * @return string
     */
    public function getDaoClass( $classId );

    /**
     * Return the Model Class Name.
     *
     * @param string $classId 	Buizness object class id
     * @return string
     */
    public function getModelClass( $classId );

    /**
     * @param string $classId
     */
    public function getTable( $classId );

    /**
     * If $classId is null, return all contents of map, else return line keyed by $classId
     *
     * @param string $classId 	Buizness object class id
     */
    public function getMap( $classId = null );


    /**
     * Erase all objects recorded in registry, or just the $object.
     *
     * @param DaoInterface
     * @return void
     */
    public function reset( $object = null );
}
