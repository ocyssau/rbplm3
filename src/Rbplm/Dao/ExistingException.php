<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;

/**
 * @brief \Exception for Rbplm.
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class ExistingException extends \Rbplm\Sys\Exception
{
}
