<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;


/**
 * @brief Instanciate a anyobject and load properties in from the given Uuid.
 *
 * It is a builder class to instanciate the Rbplm objects from a Uuid. instanciate the corresponding class
 * and load properties from the storage.
 *
 */
interface LoaderInterface
{

	/**
	 * Set a option
	 * Option key may be :
	 * 		select				Array of property to load in $mapped. As a SELECT in SQL
	 * 		useregistry			If true, get object from registry, default is true
	 * 		uidkeyname
	 * 		idkeyname
	 * 		parentidkeyname
	 * 		childidkeyname
	 * 		force				If true, force to query db to reload links.
	 * 		lock				If true, lock the db records in accordance to the use db system.
	 *
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function setOptions( $key, $value );


	/**
	 * Instanciate and load a anyobject from his id.
	 *
	 * @param string				$uid	Uuid of the object to load.
	 * @return MappedInterface
	 * @throws Exception
	 */
	public function loadFromId($id, $classId);

	/**
	 * Instanciate and load a anyobject from his Uuid.
	 *
	 * @param string				$uid	Uuid of the object to load.
	 * @return MappedInterface
	 * @throws Exception
	 */
	public function loadFromUid($uid, $classId);

} //End of class
