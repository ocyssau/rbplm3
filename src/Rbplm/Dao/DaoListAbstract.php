<?php
// %LICENCE_HEADER%
namespace Rbplm\Dao;

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * A list of records in db.
 * This class implements a iterator.
 * Query postgresql database to create a list of scalar values in a iterator class.
 * Example and tests: Rbplm/Dao/Pg/ListTest.php
 *
 * @todo Check and improve update list procedures.
 *
 */
abstract class DaoListAbstract implements ListInterface
{

	/**
	 * valid keys are :
	 * force,lock,select,toApp,mode
	 *
	 * @var array
	 */
	protected $options = array(
		'table' => null,
		'alias' => null,
		'force' => false,
		'lock' => false,
		'select' => '*',
		'mode' => \PDO::FETCH_ASSOC,
		'dao' => null
	);

	/**
	 * Name of table to query for write
	 *
	 * @var string
	 */
	protected $table = '';

	/**
	 *
	 * @var ConnexionInterface
	 */
	protected $connexion;

	/**
	 * @var MetamodelTranslatorInterface
	 */
	protected $translator;

	/**
	 * If true, the datas are loaded from db, else its a new record not yet save
	 *
	 * @var boolean
	 */
	protected $isLoaded = false;

	/**
	 *
	 * @var \PDOStatement
	 */
	public $stmt;

	/**
	 *
	 * @var integer
	 */
	protected $_count = 0;

	/**
	 * Position of the iterator
	 *
	 * @var string
	 */
	protected $_key = 0;

	/**
	 * Current iterator item
	 */
	protected $_current;

	/**
	 * Flag to mark the key of $current
	 *
	 * @var integer
	 */
	protected $loadedKey;

	/**
	 * Flag to mark the key of $current
	 *
	 * @var integer
	 */
	protected $level;

	/**
	 * The used filter to get datas
	 *
	 * @var string
	 */
	protected $filter;
	
	/**
	 * Map of properties name as define in db to name as define in model.
	 *
	 * @var array
	 */
	public static $sysToApp = [];
	
	/**
	 * Define filter to use to convert property value from db to model and from model to db.
	 * Simply, this map define name of property in db semantic and associate a filter
	 * The filter must be the prefix of methodes as named prefixToApp and prefixToSys.
	 *
	 * Example :
	 * array('sysname'=>'json');
	 * and the current class define methods jsonToApp and jsonToSys
	 *
	 * @var array
	 */
	public static $sysToAppFilter = [];
	
	/**
	 * Merge of self::$sysToApp and DaoSier::$sysToApp
	 * List of varname as use in BDD to varname as use in application
	 *
	 * @var array
	 */
	public $metaModel;
	
	/**
	 *
	 * @var array
	 */
	public $metaModelFilters;
	
	/**
	 * Constructor
	 *
	 * @param string $table
	 * @param \PDO
	 */
	public function __construct($table, \PDO $conn = null)
	{
		$this->options['table'] = $table;

		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}
		
		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;
	}

	/**
	 * Reinit properties
	 *
	 * @return void
	 */
	protected function _reset()
	{
		$this->stmt = null;
		$this->isLoaded == false;
		$this->propsToSuppress = array();
		$this->linksToSuppress = array();

		$this->_current = null;
		$this->_key = 0;
		$this->_count = null;
		$this->level = null;
		$this->loadedKey = null;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::getConnexion()
	 * @return \PDO
	 */
	public function getConnexion()
	{
		if ( !$this->connexion ) {
			throw new Exception('CONNEXION_IS_NOT_SET');
		}
		return $this->connexion;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::setConnexion()
	 * @param \PDO $conn
	 * @return DaoListAbstract
	 */
	public function setConnexion(\PDO $conn)
	{
		$this->connexion = $conn;
		return $this;
	}

	/**
	 * @param MetamodelTranslatorInterface $translator
	 * @return DaoListAbstract
	 */
	public function setTranslator(MetamodelTranslatorInterface $translator)
	{
		$this->translator = $translator;
		return $this;
	}

	/**
	 * @return MetamodelTranslatorInterface
	 */
	public function getTranslator()
	{
		if ( !isset($this->translator) ) {
			$this->translator = new \Rbplm\Dao\MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::getStmt() Getter to \PDOStatement composed object.
	 *
	 * @return \PDOStatement
	 */
	public function getStmt()
	{
		return $this->stmt;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::setStmt() Setter to \PDOStatement composed object.
	 * @param \PDOStatement $stmt
	 * @return DaoListAbstract
	 */
	public function setStmt(\PDOStatement $stmt)
	{
		$this->_reset();
		$this->stmt = $stmt;
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.ListInterface::getConfig()
	 *
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.ListInterface::setOption()
	 *
	 * @return DaoListAbstract
	 */
	public function setOption($key, $value)
	{
		$key = strtolower($key);
		$this->options[$key] = $value;
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.ListInterface::setOption()
	 *
	 * @return DaoListAbstract
	 */
	public function select($array)
	{
		$this->options['select'] = implode(', ', $array);
		return $this;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param
	 *        	string | DaoFilter $filter
	 * @param
	 *        	array
	 * @return DaoListAbstract
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}

		if ( $filter instanceof FilterInterface ) {
			$filter->setTable($this->options['table']);
			$join = $filter->withToString();
			$table = $filter->getAliasedTable();
			$select = $filter->selectToString();
			$filterAsStr = $filter->__toString();
			$sql = "SELECT $select FROM $table $join WHERE $filterAsStr";
		}
		elseif ( $filter && is_string($filter) ) {
			$options = $this->options;
			($options['select'] != '') ? $select = $options['select'] : $select = '*';
			($options['alias'] != '') ? $table = $options['table'] . ' AS ' . $options['alias'] : $table = $options['table'];
			$filterAsStr = $filter;
			$sql = "SELECT $select FROM $table WHERE $filterAsStr";
		}
		else {
			$options = $this->options;
			($options['select'] != '') ? $select = $options['select'] : $select = '*';
			($options['alias'] != '') ? $table = $options['table'] . ' AS ' . $options['alias'] : $table = $options['table'];
			$filterAsStr = null;
			$sql = "SELECT $select FROM $table";
		}
		$this->lastSql = $sql;

		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);

		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::loadFromSql() Populate list from sql request.
	 *      The request must return all property of object with system name.
	 *
	 * @param
	 *        	string Sql request
	 * @param array $options
	 *        	Is array of options.
	 *        	'lock' =>boolean If true, lock records, default is false
	 *        	'force' =>boolean If true, force to reload, default is false
	 * @return DaoListAbstract
	 */
	public function loadFromSql($sql, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}
		$this->lastSql = $sql;
		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = '';
		return $this;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::save() Save content of array $list in db.
	 *
	 *      $list is array where key are name of properties and values, values to save.
	 *      Each entry of array must respect same number and name of properties.
	 *
	 *      $pkey is array where values are name of key involved in primary key definition.
	 *      The values of this key must be presents in $list entry.
	 *
	 *      Example:
	 *      <code>
	 *      $list = array();
	 *      $list[] = array('uid'=>$uuid1, 'name'=>$name1, 'description'=>$desc1);
	 *      $list[] = array('uid'=>$uuid2, 'name'=>$name2, 'description'=>$desc2);
	 *      $oList->save($list, false, array('uid'));
	 *      </code>
	 *
	 * @param array $list
	 * @return DaoListAbstract
	 */
	public function save($list)
	{
		$options = $this->options;
		$pkeys = $options['pkey'];
		$table = $options['table'];

		if ( !$pkeys ) {
			throw new Exception('pkey option must be set');
		}

		$select = array_keys($list[0]); // array('pname')
		$bind = array();
		foreach( $select as $p ) {
			$bind[] = ':' . $p; // array(':pname')
		}

		/* INSERT */
		$sKeys = implode(',', $select);
		$sValues = implode(',', $bind);
		$insertSql = "INSERT INTO $table ($sKeys) VALUES ($sValues)";

		/* UPDATE */
		$select2 = $select;
		$bind2 = $bind;
		if ( $pkeys ) {
			$aWhere = array();
			foreach( array_intersect_key(array_combine($select, $bind), array_flip($pkeys)) as $keyname => $keybind ) { // array('pkey'=>':pkey')
				$aWhere[] = $keyname . '=' . $keybind; // string 'pkey'=':pkey'
				unset($select2[array_search($keyname, $select)]); // suppress pkey from $select
				unset($bind2[array_search($keybind, $bind)]); // suppress pkey from $bind
			}
			$sWhere = implode(' AND ', $aWhere);
		}
		else {
			$sWhere = "uid=':uid'";
			unset($select2[array_search('uid', $select)]); // suppress uid from $select
			unset($bind2[array_search(':uid', $bind)]); // suppress :uid from $bind
		}

		$sKeys = implode(',', $select2);
		$sValues = implode(',', $bind2);
		$updateSql = "UPDATE $table SET ($sKeys) = ($sValues) WHERE " . $sWhere;

		/* EXECUTE */
		$insertStmt = $this->connexion->prepare($insertSql);
		$updateStmt = $this->connexion->prepare($updateSql);
		if ( $options['priority'] == 'update' ) {
			foreach( $list as $entry ) {
				/* Convert boolean to integer! \PDO dont do that! Grr.. */
				foreach( $entry as $k => $att ) {
					if ( is_bool($att) ) {
						$entry[$k] = (integer)$att;
					}
				}
				$b = array_combine($bind, array_values($entry));
				try {
					$updateStmt->execute($b);
				}
				catch( \PDOException $e ) {
					$insertStmt->execute($b);
				}
			}
		}
		else {
			foreach( $list as $entry ) {
				$b = array_combine($bind, array_values($entry));
				try {
					$insertStmt->execute($b);
				}
				catch( \PDOException $e ) {
					$updateStmt->execute($b);
				}
			}
		}
		return $this;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::delete()
	 *
	 * @param string $filter
	 *        	sql filter.
	 * @return DaoListAbstract
	 */
	public function delete($filter)
	{
		if ( !$filter ) {
			throw new Exception('NONE_FILTER_SET');
		}

		$table = $this->options['table'];
		$sql = "DELETE FROM $table WHERE $filter";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		return $this;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::countAll() Count result from a sql request.
	 *
	 *      Is different of method count wich necessit to get the real query and receive the real datas.
	 *      Here, select only count().
	 *
	 * @param string | \Rbplm\Dao\FilterAbstract $filter
	 * @return integer
	 */
	public function countAll($filter, $bind = null)
	{
		$options = $this->options;
		$table = $options['table'];

		if ( $filter instanceof FilterInterface ) {
			if ( $table ) {
				$filter->setTable($table);
			}
			$join = $filter->withToString();
			$table = $filter->getAliasedTable();
			$filter = $filter->whereToString();
			$sql = "SELECT COUNT(*) FROM $table $join WHERE $filter";
		}
		elseif ( $filter && is_string($filter) ) {
			$sql = "SELECT COUNT(*) FROM $table WHERE $filter";
		}
		else {
			$sql = "SELECT COUNT(*) FROM $table";
		}
		$this->lastSql = $sql;
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::areExisting() Test if values in array $list of the property $propNameToTest are existing.
	 *      Return a array of values for each properties defined by $returnSelect.
	 *
	 * @param string $propNameToTest
	 *        	Name of property to test
	 * @param array|string $list
	 *        	List of value relatives to $propNameToTest to test
	 * @param array $returnSelect
	 *        	List of properties to pu in return for each tested element
	 * @return array (Value of property $propNameToTest => array( 'selected property'=>'selected property value' ))
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		if ( is_array($returnSelect) ) {
			$select = implode(',', $returnSelect);
		}
		else {
			$select = $propNameToTest;
		}

		$table = $this->options['table'];

		$sql = "SELECT $select FROM $table WHERE $propNameToTest=:testedValue";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$out = [];
		foreach( $list as $testedValue ) {
			$bind = array(
				':testedValue' => $testedValue
			);
			$stmt->execute($bind);
			$out[$testedValue] = $stmt->fetch();
		}
		return $out;
	}

	/**
	 *
	 * @see Rbplm\Dao.ListInterface::toArray() Translate current stmt into array.
	 * @return array
	 */
	public function toArray()
	{
		$mode = $this->options['mode'];
		return $this->stmt->fetchAll($mode);
	}

	/**
	 *
	 * @see \Iterator::valid()
	 * @return boolean
	 */
	public function valid()
	{
		if ( $this->_key < $this->count() ) {
			return true;
		}
		else {
			$this->stmt->closeCursor();
			return false;
		}
	}

	/**
	 *
	 * @see \Iterator::current() Gets the current iterator item
	 *
	 * @return array
	 */
	public function current()
	{
		$mode = $this->options['mode'];
		if ( $this->loadedKey !== $this->_key ) {
			$this->_current = $this->stmt->fetch($mode);

			if ( $this->translator ) {
				$this->_current = $this->translator->toApp($this->_current);
			}

			$this->loadedKey = $this->_key;
		}
		return $this->_current;
	}

	/**
	 * Value of the iterator key for the current pointer
	 * (translated by the metamodel)
	 *
	 * @see \Iterator::key()
	 * @return string
	 */
	public function key()
	{
		return $this->_key;
	}

	/**
	 *
	 * @see \Iterator::next()
	 * @return void
	 */
	public function next()
	{
		$this->_key++;
	}

	/**
	 *
	 * @see \Iterator::rewind()
	 * @return void
	 */
	public function rewind()
	{
		if ( $this->_key > 0 ) {
			$this->stmt->execute();
			$this->_key = 0;
		}
	}

	/**
	 *
	 * @see \Countable::count()
	 * @return integer
	 */
	public function count()
	{
		if ( !$this->_count ) {
			$this->_count = $this->stmt->rowCount();
		}
		return $this->_count;
	}

	/**
	 * Alias for delete
	 */
	public function suppress($filter)
	{
		return $this->delete($filter);
	}
	
} /* End of class */
