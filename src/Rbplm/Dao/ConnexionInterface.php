<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;

/**
 * @brief Factory for connexions resources.
 * Require a configuration that must be set before use:
 * @code
 * Connexion::setConfig($conf)
 * @endcode
 *
 * $conf is a array with keys:
 * 	array($name=>array(
 * 		'adapter'=>mysql|mssql|postgresql|ldap|openldap|activedirectory
 * 		'params'=>specific parameters for each adapter
 * 	))
 * 	$name is a arbitrary name for connexion.
 *
 *
 *  a named connexion is simply get with:
 *  @code
 *  $conn = Connexion::get($name);
 *  @endcode
 *
 *  to list require params, see methods
 *  Connexion::_connectPdo for mysql, mssql, postgresql adapters
 *  and
 *  Connexion::_connectLdap for ldap, openldap and activedirectory adapters
 *
 *
 */
interface ConnexionInterface
{

    /**
     * Get the config
     * 
     * @return array
     */
    public static function getConfig();

    /**
     * Set the config
     * 
     * array( 'connexion name'=> array(
     *				'adapter'=>'', //activedirectory, openldap, ldap, mysql, pgsql
     *				'params'=>array(
     *					'host'=>
     *					'dbname'=>
     *					'port'=>
     *					'username'=>
     *					'password'=>
     * 
     * @param array $config
     * @return void
     */
    public static function setConfig(array $config);

    /**
     * Set the default connexion name.
     *
     * Default connexion name is 'default'
     *
     * @param string
     * @return void
     */
    public static function setDefault($name);

    /**
     * Get the default connexion name.
     *
     * @return string
     */
    public static function getDefault();
    
    /**
     * Get db abstract access object.
     *
     * This method work as a singleton, factory and accessor.
     * Only if connexion is not existing, she will be created and stored in a internal registry,
     * else the previous created connexion is returned.
     *
     * Connexion params are extracted  from config set in rbplm. The config must define at least :
     * 	array($name=>array(
     * 		'adapter'=>mysql|mssql|postgresql|ldap|openldap|activedirectory
     * 		'params'=>specific parameters for each adapter
     * 	))
     *
     * @param string	name of connexion to use
     * @return \PDO
     */
    public static function get($name = null);

    /**
     *
     * Add a connexion to registry.
     * @param string $name
     * @param object $conn		A object used by DAO to access to datas.
     * May be any type, because its just used by appropriate DAO.
     */
    public static function add($name, $conn);
    
} //End of class