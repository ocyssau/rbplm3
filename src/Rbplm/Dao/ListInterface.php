<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;


/**
 * @brief Interface for list dao.
 *
 * List dao contains a list od records simply as scalar values and not in objects.
 *
 */
interface ListInterface extends \Iterator, \Countable
{

	/**
	 * Return the connexion to db as \PDO, \Zend_Ldap, Adodb or anyelse
	 *
	 * @return \PDO
	 */
	public function getConnexion();


	/**
	 * Set the connexion to db
	 *
	 * @param \PDO $conn
	 * @return ListInterface
	 */
	public function setConnexion(\PDO $conn );

	/**
	 * Getter to \PDOStatement composed object.
	 *
	 * @return \PDOStatement
	 */
	public function getStmt();

	/**
	 * Setter to \PDOStatement composed object.
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	public function setStmt(\PDOStatement $stmt);

	/**
	 * Getter for internal config.
	 * @return array
	 */
	public function getOptions();

	/**
	 * Setter for internal config.
	 *
	 * @param string
	 * @param string
	 * @return ListInterface
	 */
	public function setOption($key, $value);

	/**
	 * @param string | DaoFilter $filter
	 * @param array
	 * @return ListInterface
	 */
	public function load($filter = null, $bind=null);


	/**
	 * Populate list from sql request.
	 * The request must return all property of object with system name.
	 *
	 * @param string			Sql request
	 * @return DaoListAbstract
	 */
	public function loadFromSql($sql, $bind=null);


	/**
	 * To render permanent
	 *
	 * @param array 	$array
	 *
	 * @return ListInterface
	 */
	public function save( $array );

	/**
	 * Suppress records in database.
	 *
	 * @return ListInterface
	 */
	public function delete( $filter );


	/**
	 * Count all records return by a request.
	 * Not load data in list, just return the count.
	 *
	 * Is different of method count wich necessit to get the real query and receive the real datas.
	 *
	 * @param string	$filter
	 * @return integer
	 *
	 */
	public function countAll( $filter );

	/**
	 * Test if values in array $list of the property $propNameToTest are existing.
	 * Return a array of values for each properties defined by $returnSelect.
	 *
	 * @param string		$propNameToTest	Name of property to test
	 * @param array|string	$list			List of value relatives to $propNameToTest to test
	 * @param array			$returnSelect	List of properties to pu in return for each tested element
	 * @return array		(Value of property $propNameToTest => array( 'selected property'=>'selected property value' ))
	 */
	public function areExisting( $propNameToTest, $list, $returnSelect=null );

	/**
	 * Translate current stmt into array.
	 * @return array
	 */
	public function toArray();
}
