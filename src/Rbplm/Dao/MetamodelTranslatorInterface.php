<?php
// %LICENCE_HEADER%
namespace Rbplm\Dao;

/**
 *
 * @brief Interface for Translator
 *
 */
interface MetamodelTranslatorInterface
{

	/**
	 *
	 * Translate property name and value to model semantic
	 *
	 * @param array|string $in
	 *        	Db properties name or array (key as db column name => value)
	 * @return array Model properties
	 */
	public function toApp($in);

	/**
	 * Translate current row data from model to db semantic
	 *
	 * @param array|string $in
	 *        	Application property name or array (key as model property name => value)
	 * @return array System attributs
	 */
	public function toSys($in);

	/**
	 * Get select with AS applicationName
	 *
	 * @return array
	 */
	public function getSelectAsApp();
}
