<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;

/**
 * @brief Factory for Dao.
 *
 * Factory to instanciate DAOs from a specified model classe id.
 *
 * A map may be use to convert model class to dao class.
 *
 *
 */
abstract class FactoryAbstract implements FactoryInterface
{

	/**
	 * Singleton instance
	 *
	 * @var FactoryInterface
	 */
	protected static $instance;

	/**
	 * @var ConnexionInterface
	 */
	protected $connexion;

	/**
	 * Registry of instanciated DAO.
	 *
	 * @var array	Array of DaoInterface where key is buizness object class name.
	 */
	protected $registry;

	/**
	 * @var Loader
	 */
	protected $loader;

	/**
	 * Array of map buizness object to Dao class to instanciate and connexion to use.
	 * map key is the class id
	 * value is array where
	 * - key 0 is dao class name
	 * - key 1 is table name
	 * - key 2 is model class
	 *
	 * @var array
	 */
	protected $map = array();

	/**
	 * Array of map buizness object to Dao class to instanciate and connexion to use.
	 * @var array
	 */
	protected $options = array('listclass'=>'', 'filterclass'=>'');

	/**
	 * @param string $name
	 */
	public function __construct()
	{
		$this->registry = array();
		self::$instance = $this;
	}

	/**
	 * Singleton
	 *
	 * @return FactoryAbstract
	 */
	public static function get()
	{
		if(!isset(static::$instance)){
			new static();
		}
		return static::$instance;
	}

	/**
	 * @param $conn ConnexionInterface
	 * @return FactoryAbstract
	 */
	public function setConnexion($conn)
	{
		$this->connexion = $conn;
		return $this;
	}

	/**
	 * @return \PDO
	 */
	public function getConnexion()
	{
		return $this->connexion;
	}

	/**
	 * @param string $key
	 * @param string $value
	 * @return FactoryAbstract
	 */
	public function setOption( $key, $value )
	{
		$key = strtolower($key);
		$this->options[$key] = $value;
		return $this;
	}

	/**
	 * @param array $map
	 * @return FactoryAbstract
	 */
	public function setMap( array $map )
	{
		$this->map = $map;
		return $this;
	}

	/**
	 * @param string | \Rbplm\Any $anyOrId
	 * @return DaoInterface
	 */
	public function getDao($anyOrId)
	{
		if(is_object($anyOrId)){
			$classId = $anyOrId->cid;
		}
		else{
			$classId = $anyOrId;
		}

		if(!isset($this->registry[$classId])){
			$daoClass = $this->map[$classId][0];
			$dao  = new $daoClass($this->connexion);

			/*keep a reference to the factory*/
			$dao->factory = $this;
			$this->registry[$classId] = $dao;
		}

		return $this->registry[$classId];
	}

	/**
	 * Return a instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return \Rbplm\AnyPermanent
	 */
	public function getModel( $classId )
	{
		$class = $this->map[$classId][2];
		return new $class();
	}

	/**
	 * Return a initialized instance of class specified by id.
	 *
	 * @return \Rbplm\AnyPermanent
	 */
	public function getNewModel( $classId )
	{
		$class = $this->map[$classId][2];
		return $class::init();
	}

	/**
	 * @return ListInterface
	 */
	public function getList( $classId )
	{
		$listClass = $this->options['listclass'];
		$table = $this->map[$classId][1];
		$daoClass = $this->map[$classId][0];
		$list = new $listClass($table, $this->connexion);
		$list->setOption('dao', $daoClass);

		/*keep a reference to the factory*/
		$list->factory = $this;
		return $list;
	}

	/**
	 * @return FilterInterface
	 */
	public function getFilter( $classId )
	{
		$daoClass = $this->map[$classId][0];
		$filterClass = $this->options['filterclass'];
		$filter = new $filterClass($daoClass);
		return $filter;
	}

	/**
	 * @return string
	 */
	public function getDaoClass( $classId )
	{
		return $this->map[$classId][0];
	}

	/**
	 * @return string
	 */
	public function getModelClass( $classId )
	{
		return $this->map[$classId][2];
	}

	/**
	 * @return string
	 */
	public function getTable( $classId )
	{
		return $this->map[$classId][1];
	}

	/**
	 *
	 * @param integer $cid
	 */
	public function getMap($cid=null)
	{
		if($cid){
			return $this->map[$cid];
		}
		else{
			return $this->map;
		}
	}

	/**
	 * @return Loader
	 */
	public function getLoader()
	{
		if(!isset($this->loader)){
			$this->loader = new Loader($this);
		}
		return $this->loader;
	}

	/**
	 * @param Loader $loader
	 * @return FactoryAbstract
	 */
	public function setLoader($loader)
	{
		$this->loader = $loader;
		return $this;
	}

	/**
	 * Erase all objects recorded in registry, or just the $object.
	 *
	 * @param DaoInterface
	 * @return void
	 */
	public function reset( $object = null )
	{
		if( $object ){
			foreach( $this->registry as $key=>$in ){
				if( $in == $object){
					unset($this->registry[$key]);
					break;
				}
			}
		}
		else{
			$this->registry = array();
		}
	}
}
