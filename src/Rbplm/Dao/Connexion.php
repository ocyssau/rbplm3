<?php
// %LICENCE_HEADER%
namespace Rbplm\Dao;

#use Rbplm\Dao\ConnexionException;

/**
 * @brief Factory for connexions resources.
 * Require a configuration that must be set before use:
 * @code
 * Connexion::setConfig($conf)
 * @endcode
 *
 * $conf is a array with keys:
 * array($name=>array(
 * 'adapter'=>mysql|mssql|postgresql|ldap|openldap|activedirectory
 * 'params'=>specific parameters for each adapter
 * ))
 * $name is a arbitrary name for connexion.
 *
 *
 * a named connexion is simply get with:
 * @code
 * $conn = Connexion::get($name);
 * @endcode
 *
 * to list require params, see methods
 * Connexion::_connectPdo for mysql, mssql, postgresql adapters
 * and
 * Connexion::_connectLdap for ldap, openldap and activedirectory adapters
 */
class Connexion implements ConnexionInterface
{

	/**
	 *
	 * @var array
	 */
	protected static $_config = [];

	/**
	 * Associative array where key is name of classe that require the connexion
	 *
	 * @var array Collection of DaoInterface
	 */
	protected static $_dbs = [];

	/**
	 * Name of default connexion to use.
	 *
	 * @var string
	 */
	protected static $_defaultConnName = 'default';

	/**
	 * Get the config
	 *
	 * @return array
	 */
	public static function getConfig()
	{
		return self::$_config;
	}

	/**
	 * Set the config
	 *
	 * $config['Connexion Name'][adapter] may be activedirectory |openldap | ldap | mysql | pgsql
	 *
	 * @param array $config ['Connexion Name' => [adapter=>string, params=>[host=>string, dbname=>string, port=>string, username=>string, password=>string]]]
	 * @return void
	 */
	public static function setConfig(array $config)
	{
		self::$_config = $config;
	}

	/**
	 * Add a named connexion configuration
	 *
	 * $config[adapter] may be activedirectory |openldap | ldap | mysql | pgsql
	 *
	 * @param string $name Name for the new configuration = name for the new connexion
	 * @param array $config [adapter=>string, params=>[host=>string, dbname=>string, port=>string, username=>string, password=>string]]
	 * @return void
	 */
	public static function addConfig($name, array $config)
	{
		self::$_config[$name] = $config;
	}

	/**
	 * Set the default connexion name.
	 *
	 * Default connexion name is 'default'
	 *
	 * @param
	 *        	string
	 * @return void
	 */
	public static function setDefault($name)
	{
		self::$_defaultConnName = $name;
	}

	/**
	 * Get the default connexion name.
	 *
	 * @return string
	 */
	public static function getDefault()
	{
		return self::$_defaultConnName;
	}

	/**
	 * Create Pdo object
	 *
	 * @param array $options
	 * @param boolean $db_consult_only
	 * @return \PDO
	 */
	protected static function _connectPdo(array $params, $adapter, $readonly = false)
	{
		/* Database connexion parameters */
		$driver = $adapter;
		$host = $params['host'];
		$dbname = $params['dbname'];
		$port = $params['port'];

		(isset($params['autocommit'])) ? $autoCommit = $params['autocommit'] : $autoCommit = 0;

		/*  read only access user */
		if ( $readonly ) {
			$user = $params['readonly']['username'];
			$pass = $params['readonly']['password'];
		}
		/* Full access user */
		else {
			$user = $params['username'];
			$pass = $params['password'];
		}

		try {
			$dsn = "$driver:host=$host;dbname=$dbname";
			if ( $port ) {
				$dsn .= ";port=$port";
			}
			if ( $driver == 'mysql' ) {
				$dbh = new \PDO($dsn, $user, $pass, array(
					\PDO::ATTR_PERSISTENT => false,
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="TRADITIONAL"'
				));
				/* Autocommit off */
				$dbh->setAttribute(\PDO::ATTR_AUTOCOMMIT, $autoCommit);
			}
			else {
				$dsn .= ';user=' . $user . ';password=' . $pass;
				$dbh = new \PDO($dsn);
			}
			$dbh->exec("SET CHARACTER SET utf8");
			$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			/* Conversion des valeurs NULL et chaines vides: */
			$dbh->setAttribute(\PDO::ATTR_ORACLE_NULLS, \PDO::NULL_EMPTY_STRING);
			/* Casse pour le nom de colonnes */
			$dbh->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_NATURAL);
		}
		catch( \PDOException $e ) {
			$msg = "DB CONNEXION ERROR, Check if server is up :" . $e->getMessage() . ' -- Dsn: ' . "$driver:host=$host;dbname=$dbname";
			throw new ConnexionException($msg);
		}

		return $dbh;
	}

	/**
	 * Create Ldap connexion
	 *
	 * @param array $options
	 * @param boolean $db_consult_only
	 * @return
	 *
	 * @todo : implement this method
	 */
	protected static function _connectLdap(array $options = null, $db_consult_only = false)
	{}

	/**
	 * Get db abstract access object.
	 *
	 * This method work as a singleton, factory and accessor.
	 * Only if connexion is not existing, she will be created and stored in a internal registry,
	 * else the previous created connexion is returned.
	 *
	 * Connexion params are extracted from config set in rbplm. The config must define at least :
	 * array($name=>array(
	 * 'adapter'=>mysql|mssql|postgresql|ldap|openldap|activedirectory
	 * 'params'=>specific parameters for each adapter
	 * ))
	 *
	 * @param
	 *        	string name of connexion to use
	 * @return \PDO
	 */
	public static function get($name = null)
	{
		if ( !$name ) {
			$name = self::$_defaultConnName;
		}
		if ( !isset(self::$_dbs[$name]) ) {
			$adapter = self::$_config[$name]['adapter'];
			$params = self::$_config[$name]['params'];
			switch ($adapter) {
				case 'activedirectory':
				case 'ldap':
				case 'openldap':
					self::$_dbs[$name] = self::_connectLdap($params);
					break;
				case 'mysql':
				case 'mssql':
				case 'pgsql':
					self::$_dbs[$name] = self::_connectPdo($params, $adapter);
					break;
				default:
					throw new ConnexionException('none defined adapter', E_ERROR);
					break;
			}
		}
		return self::$_dbs[$name];
	}

	/**
	 *
	 * Add a connexion to registry.
	 *
	 * @param string $name
	 * @param object $conn
	 *        	A object used by DAO to access to datas.
	 *        	May be any type, because its just used by appropriate DAO.
	 */
	public static function add($name, $conn)
	{
		self::$_dbs[$name] = $conn;
	}

	/**
	 *
	 * Connexion name is existing ?
	 *
	 * @param string $name
	 */
	public static function has($name)
	{
		return isset(self::$_dbs[$name]);
	}

	/**
	 *
	 * Remove connexion from the registry
	 *
	 * @param string $name
	 */
	public static function close($name)
	{
		if(isset(self::$_dbs[$name])){
			unset(self::$_dbs[$name]);
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param string $name
	 *        	Connexion name
	 * @param boolean $bool
	 */
	public function setAutocommit($bool, $name = null)
	{
		(!$name) ? $name = self::$_defaultConnName : null;
		self::$_dbs[$name]->setAttribute(\PDO::ATTR_AUTOCOMMIT, (bool)$bool);
	}
}
