<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;

/**
 * @brief Factory for Dao.
 *
 * Factory to instanciate DAOs from a specified model classe name.
 *
 * Examples:
 * Get the dao of buizness class \Rbplm\Org\Unit
 * @code
 * $Dao = Factory::getDao( '\Rbplm\Org\Unit' );
 * //To get only class name
 * $DaoClassName = Factory::getDaoClass( '\Rbplm\Org\Unit' );
 * @endcode
 *
 * Instanciate the appropriate Filter object:
 * @code
 * $Filter = Factory::getFilter( '\Rbplm\Org\Unit' );
 * //Equivalent to
 * $Filter = Factory::getDao( '\Rbplm\Org\Unit' )->newFilter();
 * @endcode
 *
 * Instanciate the appropriate List object
 * @code
 * $List = Factory::get()->getList( '\Rbplm\Org\Unit' );
 * //Equivalent to
 * $List = Factory::getDao( '\Rbplm\Org\Unit' )->newList();
 * @endcode
 *
 */
class Factory extends FactoryAbstract
{
}
