<?php
// %LICENCE_HEADER%
namespace Rbplm\Dao;

use Rbplm\Dao\Filter\Op;
use Rbplm\Sys\Error;

/**
 * Create params to search in database.
 */
abstract class FilterAbstract implements FilterInterface
{

	/**
	 * Specify type of input.
	 * May be true if input is in application semantic, or false if input is in database semantic.
	 *
	 * @var boolean
	 */
	private $options = array(
		'asapp' => false,
		'paginationIsActive' => true,
		'translator' => null
	);

	/**
	 * Limit the number of records in the result
	 *
	 * @var integer
	 */
	protected $limit = null;

	/**
	 * Pagination page to display
	 *
	 * @var integer
	 */
	protected $offset = null;

	/**
	 * field use for sort sql option
	 *
	 * @var array
	 */
	protected $sortBy = [];

	/**
	 * field use for sort sql option
	 *
	 * @var string
	 */
	protected $sortOrder = 'ASC';

	/**
	 * Input special where close.
	 * Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
	 *
	 * @var array
	 */
	protected $where = [];

	/**
	 * To set junctions
	 *
	 * @var array
	 */
	protected $with = [];

	/**
	 * To set request to union
	 *
	 * @var array
	 */
	protected $union = [];

	/**
	 * any query to add to end
	 *
	 * @var string
	 */
	protected $extra;

	/**
	 * Array of attributs to select
	 *
	 * @var array
	 */
	protected $select = [];

	/**
	 * Alias for used table
	 *
	 * @var string
	 */
	protected $alias;

	/**
	 *
	 * @var string
	 */
	protected $table;

	/**
	 *
	 * @var string
	 */
	protected $groupBy = null;

	/**
	 *
	 * @var boolean
	 */
	protected $distinct = false;

	/**
	 *
	 * @param string $key
	 * @param string $value
	 * @return FilterAbstract
	 */
	public function setOption($key, $value)
	{
		$this->options[$key] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $what
	 * @return string
	 */
	protected static function quoteWhat($what, $passthroughPre = '', $passthroughPost = '')
	{
		$pdo = Connexion::get();
		if ( substr($what, 0, 4) == '@SQL' ) {
			$what = substr($what, 4);
		}
		else if ( substr($what, 0, 1) != ':' ) {
			$what = $passthroughPre . $what . $passthroughPost;
			$what = $pdo->quote($what);
		}
		return $what;
	}

	/**
	 * Find string $what in column $where on condition defined by $op
	 * $what may be a sub query.
	 * In this case the string $what must begin with '@SQL' characters
	 * $what may be a binded PDO attribute as :attribute
	 * $what will be quoted
	 *
	 * @param string $what
	 *        	a word to find
	 * @param string $where
	 *        	property name
	 * @param string $op
	 *        	One of constant Rbplm\Dao\Filter\Op::*
	 * @return FilterAbstract
	 */
	protected function _find($what, $where, $op = Op::CONTAINS, $boolOp = 'AND')
	{
		if ( !$where ) {
			return;
		}

		$passthroughPre = '';
		$passthroughPost = '';

		switch ($op) {
			case Op::EQUAL:
				$op = '=';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::LIKE:
				$op = 'LIKE';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::BEGIN:
				$op = 'LIKE';
				$passthroughPost = '%';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::END:
				$op = 'LIKE';
				$passthroughPre = '%';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::CONTAINS:
				$op = 'LIKE';
				$passthroughPre = '%';
				$passthroughPost = '%';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::NOTCONTAINS:
				$op = '<>';
				$passthroughPre = '%';
				$passthroughPost = '%';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::NOTEND:
				$op = '<>';
				$passthroughPre = '%';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::NOTBEGIN:
				$op = '<>';
				$passthroughPost = '%';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::ISNULL:
				$sql = $where . ' IS NULL';
				$this->where[] = array(
					$sql,
					$boolOp
				);
				return $this;
				break;
			case Op::NOTNULL:
				$sql = $where . ' IS NOT NULL';
				$this->where[] = array(
					$sql,
					$boolOp
				);
				return $this;
				break;
			case Op::SUP:
				$op = '>';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::NOTEQUAL:
				$op = '<>';
				break;
			case Op::EQUALSUP:
				$op = '>=';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::INF:
				$op = '<';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::EQUALINF:
				$op = '<=';
				$what = self::quoteWhat($what, $passthroughPre, $passthroughPost);
				break;
			case Op::NOTBETWEEN:
				$op = 'NOT BETWEEN';
				if ( is_array($what) ) {
					$what1 = self::quoteWhat($what[0]);
					$what2 = self::quoteWhat($what[1]);
				}
				else {
					throw new \UnexpectedValueException('$what must be a array with values of min and max');
				}
				$what = $what1 . ' AND ' . $what2;
				break;
			case Op::FINDINSET:
				$what = self::quoteWhat($what, '', '');
				$sql = "FIND_IN_SET($what, $where) > 0";
				$this->where[] = array(
					$sql,
					$boolOp
				);
				return $this;
				break;
			case Op::MATCH:
				// $what = '\'"'.$what.'"\'';
				$what = self::quoteWhat($what, '', '');
				$sql = "MATCH($where) AGAINST ($what IN BOOLEAN MODE)";
				$this->where[] = array(
					$sql,
					$boolOp
				);
				return $this;
				break;
			default:
				throw (new Exception('UNKNOW_OPERATOR', Error::WARNING));
		}

		$sql = $where . ' ' . $op . ' ' . $what;
		$this->where[] = array(
			$sql,
			$boolOp
		);
		return $this;
	}

	/**
	 * $options=array(
	 * 'on'=>string [OPTIONAL],
	 * 'righton'=>string [OPTIONAL, IGNORE IF on IS SET]
	 * 'lefton'=>string [OPTIONAL, IGNORE IF on IS SET]
	 * 'alias'=>string [OPTIONAL]
	 * 'select'=>array [OPTIONAL]
	 * 'table'=>string
	 * direction=>outer|inner
	 * )
	 *
	 * @return FilterAbstract
	 */
	public function with(array $options)
	{
		if ( isset($options['on']) ) {
			$options['righton'] = $options['on'];
			$options['lefton'] = $options['on'];
		}

		(isset($options['alias'])) ? $alias = $options['alias'] : $alias = null;

		if ( isset($options['select']) ) {
			foreach( $options['select'] as $select ) {
				$this->select[] = $alias . '.' . $select;
			}
		}

		$this->with[] = array(
			'table' => $options['table'],
			'lefton' => $options['lefton'],
			'righton' => $options['righton'],
			'alias' => $alias,
			'direction' => (isset($options['direction'])) ? $options['direction'] : 'outer'
		);
		return $this;
	}

	/**
	 * Add a alias name for the main table defined in options
	 *
	 * @param string $alias
	 * @return FilterAbstract
	 */
	public function setAlias($alias)
	{
		$this->alias = trim($alias, '.');
		return $this;
	}

	/**
	 *
	 * @throws Exception
	 */
	public function getAlias()
	{
		return $this->alias;
	}

	/**
	 * Set the used table name of sql db
	 *
	 * @param string $table
	 * @return FilterAbstract
	 */
	public function setTable($table)
	{
		$this->table = $table;
		return $this;
	}

	/**
	 * Define a sql query to union with the main query
	 *
	 * @param array $options
	 *        	= array('alias', 'table', 'select')
	 *
	 * @return FilterAbstract
	 */
	public function union(array $options)
	{
		(isset($options['alias'])) ? $alias = $options['alias'] : $alias = null;
		(isset($options['table'])) ? $table = $options['table'] : $table = null;

		$selects = array();

		if ( isset($options['select']) ) {
			foreach( $options['select'] as $select ) {
				$selects[] = $alias . '.' . $select;
			}
		}
		else {
			$selects = array(
				$alias . '.*'
			);
		}

		$this->union[] = array(
			'table' => $table,
			'alias' => $alias,
			'select' => $selects
		);

		return $this;
	}

	/**
	 *
	 * @param FilterInterface $subFilter
	 * @return FilterAbstract
	 */
	public function suband($subFilter)
	{
		$this->where[] = array(
			'(' . $subFilter->__toString() . ')',
			'AND'
		);
		return $this;
	}

	/**
	 *
	 * @param FilterInterface $subFilter
	 * @return FilterAbstract
	 */
	public function subor($subFilter)
	{
		$this->where[] = array(
			'(' . $subFilter->__toString() . ')',
			'AND'
		);
		return $this;
	}

	/**
	 *
	 * @param string $what
	 *        	a word to find
	 * @param string $where
	 *        	property name
	 * @param string $op
	 *        	One of constant \Rbplm\Dao\Filter\Op::*
	 * @return $subFilter
	 */
	public function andfind($what, $where, $op = Op::EQUAL)
	{
		return $this->_find($what, $where, $op, 'AND');
	}

	/**
	 *
	 * @param string $what
	 *        	a word to find
	 * @param string $where
	 *        	property name
	 * @param string $op
	 *        	One of constant \Rbplm\Dao\Filter\Op::*
	 * @return FilterAbstract
	 */
	public function orfind($what, $where, $op = Op::EQUAL)
	{
		return $this->_find($what, $where, $op, 'OR');
	}

	/**
	 *
	 * @param boolean|null $bool
	 * @return FilterAbstract|boolean
	 */
	public function isDistinct($bool = null)
	{
		if ( !is_null($bool) ) {
			$this->distinct = $bool;
			return $this;
		}
		else {
			return $this->distinct;
		}
	}

	/**
	 *
	 * @param integer $int
	 * @return FilterAbstract
	 */
	public function page($page, $pageLimit)
	{
		$this->offset = ($page - 1) * $pageLimit;
		$this->limit = $pageLimit;

		if ( $this->offset < 0 ) {
			$this->offset = 0;
		}

		return $this;
	}

	/**
	 * Define sort order options
	 * If $by is a array, define sort for each key define in.
	 * Note that only on direction is available.
	 *
	 * @param string|array $by
	 * @param string $dir
	 * @return FilterAbstract
	 */
	public function sort($by, $dir = 'ASC')
	{
		if ( is_string($by) ) {
			$this->setSort([
				$by
			]);
		}
		else {
			$this->setSort($by);
		}

		$this->sortOrder = $dir;

		return $this;
	}

	/**
	 * Define sort order options
	 *
	 * @param string|array $by
	 * @return FilterAbstract
	 */
	public function setSort(array $by)
	{
		if ( $this->options['asapp'] == true ) {
			$translatorclass = $this->options['translator'];
			if ( method_exists($translatorclass, 'toSys') ) {
				$by = call_user_func([
					$translatorclass,
					'toSys'
				], array_flip($by));
				$by = array_flip($by);
			}
			else {
				$this->sortBy = $by;
			}
		}
		else {
			$this->sortBy = $by;
		}

		return $this;
	}

	/**
	 * Get children of node
	 *
	 * @param $path string
	 *        	Path as define in application
	 * @param $option array
	 *        	Options
	 *        	Options may be :
	 *        	level: integer The level after current node to query
	 *        	depth: Match "depth" levels from level "level"
	 *        	pathAttributeName: string Name of system attribute for path, default is 'path'.
	 *        	boolOp: string Boolean operator, may be 'AND' or 'OR', default is AND
	 *
	 * @example Examples:
	 *          @code
	 *          level = 2
	 *          depth = null
	 *          path = 'Root'
	 *          return nodes:
	 *          Root.Node0-1.Node1-1
	 *          Root.Node0-1.Node1-2
	 *          Root.Node0-1.Node1-3
	 *
	 *          level = 2
	 *          depth = 1
	 *          path = 'Root.Node1'
	 *          return nodes:
	 *          Root.Node0-1.Node1-1
	 *          Root.Node0-1.Node1-1.Node1-1-1
	 *          Root.Node0-1.Node1-1.Node1-1-2
	 *          Root.Node0-1.Node1-2
	 *          Root.Node0-1.Node1-2.Node1-2-1
	 *          Root.Node0-1.Node1-2.Node1-2-2
	 *          Root.Node0-1.Node1-3
	 *          Root.Node0-1.Node1-3.Node1-3-1
	 *          Root.Node0-1.Node1-3.Node1-3-2
	 *
	 *          level = null
	 *          depth = 1
	 *          path = 'Root.Node1'
	 *          return nodes:
	 *          Root.Node0-1
	 *          Root.Node0-1.Node1-1
	 *          Root.Node0-1.Node1-1.Node1-1-1
	 *          Root.Node0-1.Node1-1.Node1-1-2
	 *          Root.Node0-1.Node1-2
	 *          Root.Node0-1.Node1-2.Node1-2-1
	 *          Root.Node0-1.Node1-2.Node1-2-2
	 *          Root.Node0-1.Node1-3
	 *          Root.Node0-1.Node1-3.Node1-3-1
	 *          Root.Node0-1.Node1-3.Node1-3-2
	 *          @endcode
	 *
	 * @see FilterInterface::children()
	 *
	 * @return FilterAbstract
	 */
	public function children($path, $options = array())
	{
		/*
		 * ltree request @see http://www.sai.msu.su/~megera/postgres/gist/ltree/
		 *
		 * The following quantifiers are recognized for '*' (like in Perl):
		 * {n} Match exactly the level n
		 * {n,} Match at the level n and next
		 * {n,m} Match m levels from level n
		 * {,m} Match at maximum m levels (eq. to {0,m})
		 *
		 * It is possible to use several modifiers at the end of a label:
		 * @ Do case-insensitive label matching
		 * Do prefix matching for a label
		 * % Don't account word separator '_' in label matching, that is 'Russian%' would match 'Russian_nations', but not 'Russian'
		 *
		 * lquery could contains logical '!' (NOT) at the beginning of the label and '|' (OR) to specify possible alternatives for label matching.
		 *
		 * Example of lquery:
		 * Top.*{0,2}.sport*@.!football|tennis.Russ*|Spain
		 * a) b) c) d) e)
		 * A label path should
		 * a) begins from a node with label 'Top'
		 * b) and following zero or 2 labels until
		 * c) a node with label beginning from case-insensitive prefix 'sport'
		 * d) following node with label not matched 'football' or 'tennis' and
		 * e) ends on node with label beginning from 'Russ' or strictly matched 'Spain'. *
		 */
		$o = array_merge(array(
			'level' => null,
			'depth' => 0,
			'pathAttributeName' => 'path',
			'boolOp' => 'and'
		), $options);
		$path = trim($path, '/');
		$level = $o['level'];
		$depth = $o['depth'];
		$maxLevel = $level + $depth;
		$pathAttName = $o['pathAttributeName'];
		$boolOp = $o['boolOp'];

		if ( $level && $depth ) {
			$scope = '{' . $level . ',' . $maxLevel . '}';
		}
		else if ( $depth ) {
			$scope = '{,' . $depth . '}';
		}
		else if ( $level ) {
			$scope = '{' . $level . '}';
		}
		$sql = "$pathAttName::ltree ~ '$path.*$scope'";
		$this->where[] = array(
			$sql,
			$boolOp
		);
		return $this;
	}

	/**
	 * Get the ancestors of a node.
	 *
	 * @param $path string
	 *        	Path as define in application
	 * @param $option array
	 *        	Options
	 *
	 *        	Options may be :
	 *        	minLevel: integer Min level of ancestors to query. If level is negatif, calcul level from current node, else calcul from root node. Default is 0.
	 *        	maxLevel: Max level to query. May be negatif. Default is level of current node.
	 *        	pathAttributeName: string Name of system attribute for path, default is 'path'.
	 *        	boolOp: string Boolean operator, may be 'AND' or 'OR', default is AND
	 *
	 *        	If maxLevel = 0, max is unactivate.
	 *
	 *        	Examples:
	 *        	minLevel = 1
	 *        	maxLevel = 1
	 *        	path = 'Root.Node1.Node2.Node3'
	 *        	return node Root.Node1
	 *
	 *        	minLevel = -1
	 *        	maxLevel = -1
	 *        	path = 'Root.Node1.Node2.Node3'
	 *        	return node Root.Node1.Node2
	 *
	 *        	maxLevel = -1
	 *        	minLevel = 0
	 *        	path = 'Root.Node1.Node2.Node3'
	 *        	return nodes :
	 *        	Root.Node1.Node2
	 *        	Root.Node1
	 *        	Root
	 *
	 *        	maxLevel = 0
	 *        	minLevel = 0
	 *        	path = 'Root.Node1.Node2.Node3'
	 *        	return nodes :
	 *        	Root.Node1.Node2.Node3
	 *        	Root.Node1.Node2
	 *        	Root.Node1
	 *        	Root
	 *
	 *        	maxLevel = -1
	 *        	minLevel = 0
	 *        	path = 'Root.Node1.Node2.Node3'
	 *        	return nodes :
	 *        	Root.Node1.Node2
	 *        	Root.Node1
	 *        	Root
	 *
	 * @return FilterAbstract
	 */
	public function ancestors($path, $options = array())
	{
		$o = array_merge(array(
			'maxLevel' => 0,
			'minLevel' => 0,
			'pathAttributeName' => 'path',
			'boolOp' => 'and'
		), $options);
		$path = trim($path, '/');
		$maxLevel = $o['maxLevel'];
		$minLevel = $o['minLevel'];
		$pathAttName = $o['pathAttributeName'];
		$boolOp = $o['boolOp'];

		if ( $minLevel === 0 && $maxLevel > 0 ) {
			$sql = "$pathAttName::ltree @> subpath('$path', 0, $maxLevel)";
		}
		else if ( $minLevel === 0 && $maxLevel < 0 ) {
			$sql = "$pathAttName::ltree @> subpath('$path', 0, nlevel('$path')+$maxLevel)";
		}
		else if ( $minLevel === $maxLevel && $minLevel > 0 ) {
			$sql = "$pathAttName::ltree ~ subpath('$path', 0, $minLevel)::text::lquery";
		}
		else if ( $minLevel === $maxLevel && $minLevel < 0 ) {
			$sql = "$pathAttName::ltree ~ subpath('$path', 0, nlevel('$path')-$minLevel)::text::lquery";
		}
		else {
			$sql = "$pathAttName::ltree @> '$path'";
			if ( $minLevel > 0 ) {
				$sql .= " AND nlevel('$path') >= $minLevel";
			}
			else if ( $minLevel < 0 ) {
				$sql .= " AND nlevel('$path') >= nlevel('$path')+$minLevel";
			}
		}
		$this->where[] = array(
			$sql,
			$boolOp
		);
		return $this;
	}

	/**
	 *
	 * @param array $select
	 * @return FilterAbstract
	 */
	public function select($select)
	{
		$this->select = $select;
		return $this;
	}

	/**
	 *
	 * @return array|string
	 */
	public function selectToString()
	{
		if ( $this->select ) {
			$select = implode(', ', $this->select);
		}
		else {
			$select = '*';
		}

		if ( $this->distinct ) {
			$select = 'DISTINCT ' . $select;
		}

		return $select;
	}

	/**
	 *
	 * @return array
	 */
	public function getSelect()
	{
		return $this->select;
	}

	/**
	 *
	 * @return array|string
	 */
	public function withToString()
	{
		$string = null;

		if ( is_array($this->with) ) {
			$t = array();
			foreach( $this->with as $with ) {
				$rightAlias = $with['alias'];

				/* Right member */
				if ( $rightAlias ) {
					$rightTable = $with['table'] . ' AS  ' . $rightAlias;
					$rightOn = $rightAlias . '.' . $with['righton'];
				}
				else {
					$rightTable = $with['table'];
					$rightOn = $with['righton'];
				}

				/* Left member */
				if ( $this->alias ) {
					$leftOn = $this->alias . '.' . $with['lefton'];
				}
				elseif ( $this->table ) {
					$leftOn = $this->table . '.' . $with['lefton'];
				}
				else {
					$leftOn = $with['lefton'];
				}

				/* Direction */
				if ( $with['direction'] == 'inner' ) {
					$str = "INNER JOIN $rightTable";
				}
				else {
					$str = "LEFT OUTER JOIN $rightTable";
				}

				if ( $leftOn and $rightOn ) {
					$str = $str . " ON $leftOn = $rightOn";
				}

				$t[] = $str;
			}
			$string = implode(' ', $t);
		}
		return $string;
	}

	/**
	 */
	public function whereToString()
	{
		$filter = '';
		if ( $this->where ) {
			$i = 0;
			$awhere = [];
			foreach( $this->where as $where ) {
				if ( $i > 0 ) {
					$awhere[] = $where[1] . ' ' . $where[0];
				}
				else {
					$awhere[] = $where[0];
				}
				$i++;
			}
			$swhere = implode(' ', $awhere);
			$filter .= $swhere;
		}
		else {
			$filter = ' 1=1';
		}
		return $filter;
	}

	/**
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\FilterInterface::orderToString()
	 */
	public function orderToString()
	{
		$filter = '';

		$sort = $this->getSort(true);
		if ( $sort ) {
			$sorder = ' ORDER BY ' . $sort . ' ' . $this->getOrder();
			$filter .= $sorder;
		}
		if ( $this->limit ) {
			$filter .= ' LIMIT ' . $this->getLimit();
		}
		if ( $this->offset ) {
			$filter .= ' OFFSET ' . $this->getOffset();
		}

		return $filter;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\FilterInterface::getLimit()
	 */
	public function getLimit()
	{
		return $this->limit;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\FilterInterface::getOffset()
	 */
	public function getOffset()
	{
		return $this->offset;
	}

	/**
	 * Return the direction for sorting : 
	 * 		ASC for ascendant direction
	 * 		DESC for descendant direction
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\FilterInterface::getOrder()
	 */
	public function getOrder()
	{
		return $this->sortOrder;
	}

	/**
	 * Return columns names to used for sorting.
	 * If $asString is true, return each columns separated by ","
	 * Else return all columns in a array
	 * 
	 * {@inheritdoc}
	 * @see \Rbplm\Dao\FilterInterface::getSort($asString = true)
	 */
	public function getSort($asString = true)
	{
		if ( $asString ) {
			return implode(', ', $this->sortBy);
		}
		else {
			return $this->sortBy;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function __toString()
	{
		$filter = $this->whereToString();

		if ( $this->extra ) {
			$filter .= ' ' . $this->extra;
		}

		if ( $this->groupBy ) {
			$filter .= ' ' . $this->groupByToString();
		}

		if ( $this->options['paginationIsActive'] == true ) {
			$filter .= $this->orderToString();
		}

		return $filter;
	}

	/**
	 *
	 * @throws Exception
	 */
	public function getAliasedTable()
	{
		if ( $this->table ) {
			if ( $this->alias ) {
				$table = $this->table . ' AS ' . $this->alias;
			}
			else {
				$table = $this->table;
			}
		}
		else {
			throw new Exception('$this->table is not defined');
		}
		return $table;
	}

	/**
	 *
	 * @return FilterAbstract
	 */
	public function groupBy($name)
	{
		$this->groupBy = $name;
		return $this;
	}

	/**
	 *
	 * @return FilterAbstract
	 */
	public function groupByToString()
	{
		$groupBy = '';
		if ( $this->groupBy ) {
			$groupBy = 'GROUP BY ' . $this->groupBy;
		}
		return $groupBy;
	}

	/**
	 *
	 * @param
	 *        	string [OPTIONAL] $table
	 * @param
	 *        	string [OPTIONAL] $alias
	 * @return string
	 */
	public function toSql($table = null, $alias = null)
	{
		$join = $this->withToString($table, $alias);
		$select = $this->selectToString();
		$filter = $this->__toString();

		!(is_null($alias)) ? $this->alias = $alias : null;
		!(is_null($table)) ? $this->table = $table : null;
		$table = $this->getAliasedTable();
		$sql = "SELECT $select FROM $table $join WHERE $filter";

		return $sql;
	}
} /* End of class */
