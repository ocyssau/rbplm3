<?php
// %LICENCE_HEADER%
namespace Rbplm\Dao;

use Rbplm\Sys\Exception as Exception;
use Rbplm\Sys\Error as Error;
use Rbplm\Signal as Signal;

/**
 * @brief Abstract Dao for plain schemas as funded in mysql database
 *
 */
abstract class Mysql implements DaoInterface
{

	/**
	 * Date format use by db
	 *
	 * @var string
	 */
	const DATE_FORMAT = 'Y-m-d H:i:s';

	const SIGNAL_PRE_SAVE = 'save.pre';

	const SIGNAL_POST_SAVE = 'save.post';

	const SIGNAL_PRE_LOAD = 'load.pre';

	const SIGNAL_POST_LOAD = 'load.post';

	const SIGNAL_PRE_DELETE = 'delete.pre';

	const SIGNAL_POST_DELETE = 'delete.post';

	/**
	 * valid keys are :
	 * force,lock,select,asapp,withtrans
	 *
	 * @var array
	 */
	public $options = array(
		'force' => false,
		'lock' => false,
		'select' => false,
		'asapp' => false,
		'withtrans' => true
	);

	/**
	 * @var string
	 */
	public static $pkeyName = 'id';

	/**
	 * View table to query for read
	 *
	 * @var string
	 */
	public static $vtable;

	/**
	 *
	 * @var string
	 */
	protected $_vtable;

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 *
	 * @var string
	 */
	public static $table = '';

	/** @var string */
	protected $_table;

	/**
	 * Table name where write links definition.
	 *
	 * @var string
	 */
	public static $ltable = '';

	/** @var string */
	protected $_ltable;

	/**
	 * Table name where read links definition.
	 *
	 * @var string
	 */
	public static $lvtable = '';

	/** @var string */
	protected $_lvtable;

	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @var \Rbplm\Dao\MetamodelTranslator
	 */
	protected $translator;

	/**
	 * Map of properties name as define in db to name as define in model.
	 *
	 * @var array
	 */
	public static $sysToApp = [];

	/**
	 * Define filter to use to convert property value from db to model and from model to db.
	 * Simply, this map define name of property in db semantic and associate a filter
	 * The filter must be the prefix of methodes as named prefixToApp and prefixToSys.
	 *
	 * Example :
	 * array('sysname'=>'json');
	 * and the current class define methods jsonToApp and jsonToSys
	 *
	 * @var array
	 */
	public static $sysToAppFilter = [];

	/**
	 * Merge of self::$sysToApp and DaoSier::$sysToApp
	 * List of varname as use in BDD to varname as use in application
	 *
	 * @var array
	 */
	public $metaModel;

	/**
	 *
	 * @var array
	 */
	public $metaModelFilters;

	/**
	 *
	 * @var array
	 */
	public $selectAsApp = [];

	/**
	 * Reference to the factory used to initiate this dao
	 *
	 * @var FactoryInterface
	 */
	public $factory;

	/**
	 * Name of sequence to use.
	 * If is null or empty string, assume than pkey is a autonum.
	 * If sequenceName is === false, none sequence is required
	 *
	 * @var string
	 */
	public static $sequenceName = '';

	/* */
	public static $sequenceKey = 'id';

	/** @var string */
	protected $_sequenceName = '';

	/** @var string */
	protected $_sequenceKey = 'id';

	/**
	 *
	 * @var \PDOStatement
	 */
	protected $insertStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	protected $updateStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	protected $updateFromArrayStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	protected $seqStmt;

	/**
	 *
	 * @var \PDOStatement
	 */
	protected $deleteStmt;

	/**
	 *
	 * @var string
	 */
	protected $deleteFilter;

	/**
	 * To recreate connexion after unserialize
	 */
	public function __wakeup()
	{
		$this->connexion = Connexion::get();
		return $this;
	}

	/**
	 * Clean connexion before serialize.
	 *
	 * @return array
	 */
	public function __sleep()
	{
		$this->connexion = null;
		return get_object_vars($this);
	}

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param array $bind
	 *        	Bind definition to add to generic bind construct from $sysToApp
	 * @param array $select
	 *        	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws Exception
	 *
	 */
	abstract protected function _update($mapped, $select = null);

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param array $bind
	 *        	Bind definition to add to generic bind construct from $sysToApp
	 * @param array $select
	 *        	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws Exception
	 *
	 */
	abstract protected function _insert($mapped, $select = null);

	/**
	 *
	 * @return \Rbplm\Dao\MetamodelTranslator
	 */
	public function getTranslator()
	{
		if ( !isset($this->translator) ) {
			$this->translator = new \Rbplm\Dao\MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 *
	 * @see Rbplm\Dao.DaoInterface::getConnexion() Return the connexion to db as \PDO, \Zend_Ldap, Adodb or anyelse
	 *
	 * @throws Exception
	 * @return \PDO
	 */
	public function getConnexion()
	{
		if ( !$this->connexion ) {
			throw new Exception('CONNEXION_IS_NOT_SET');
		}
		return $this->connexion;
	}

	/**
	 *
	 * @see Rbplm\Dao.DaoInterface::getConnexion() Return the connexion to db as \PDO, \Zend_Ldap, Adodb or anyelse
	 *
	 * @throws Exception
	 * @return \PDO
	 */
	public function getTable()
	{
		return $this->_table;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::setConnexion() Set the connexion to db
	 *
	 * @param $conn \PDO
	 * @throws Exception
	 * @return DaoInterface
	 */
	public function setConnexion($conn)
	{
		if ( $conn instanceof \PDO ) {
			$this->connexion = $conn;
		}
		else {
			throw new \Exception('Accept only \PDO instance');
		}
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::setOption() Basic options may be :
	 *      'lock' =>boolean If true, lock the db records in accordance to the use db system. Default is false.
	 *      'force' =>boolean If true, force to query db to reload links. Default is false.
	 *      'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
	 *      'withTrans' boolean If true, open and close a transaction before/after execution of save*, delete* methodes
	 *
	 * @param
	 *        	string option name
	 * @param
	 *        	string option value
	 * @return DaoInterface
	 */
	public function setOption($key, $value)
	{
		$key = strtolower($key);
		$this->options[$key] = $value;
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::load()
	 *
	 * @param MappedInterface $mapped
	 * @param string|\Rbplm\Dao\FilterAbstract $filter
	 *        	[OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Filter
	 * @param array $bind
	 * @throws \Rbplm\Sys\Exception
	 * @return MappedInterface
	 */
	public function load(MappedInterface $mapped, $filter = null, $bind = null)
	{
		if ( $mapped->isLoaded() == true && $this->options['force'] == false ) {
			// Rbplm::log('Object ' . $mapped->getUid() . ' is yet loaded');
			return;
		}

		Signal::trigger(self::SIGNAL_PRE_LOAD, $this);

		$table = $this->_table;

		$select = 'obj.*';
		$sql = "SELECT $select FROM $table AS obj";

		if ( $filter instanceof FilterInterface ) {
			$filter = $filter->__toString();
		}

		if ( is_string($filter) ) {
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();

		if ( $row ) {
			$this->hydrate($mapped, $row);
		}
		else {
			throw new NotExistingException(sprintf('CAN_NOT_BE_LOAD_FROM %s', $sql), Error::WARNING);
		}

		$mapped->dao = $this;
		$mapped->isLoaded(true);
		Signal::trigger(self::SIGNAL_POST_LOAD, $this);

		return $mapped;
	}

	/**
	 *
	 * @see DaoInterface::loadFromUid()
	 */
	public function loadFromUid(MappedInterface $mapped, $uid)
	{
		$filter = 'obj.' . $this->getTranslator()->toSys('uid') . '=:uid';
		$bind = array(
			':uid' => $uid
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @see DaoInterface::loadFromId()
	 */
	public function loadFromId(MappedInterface $mapped, $id)
	{
		$filter = 'obj.' . $this->getTranslator()->toSys('id') . '=:id';
		$bind = array(
			':id' => $id
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @see DaoInterface::loadFromName()
	 */
	public function loadFromName(MappedInterface $mapped, $name)
	{
		$filter = 'obj.' . $this->getTranslator()->toSys('name') . '=:name';
		$bind = array(
			':name' => $name
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @see DaoInterface::loadFromSql()
	 *
	 * @param MappedInterface $mapped
	 * @param string $sql
	 * @throws Exception
	 */
	public function loadFromSql(MappedInterface $mapped, $sql, $bind = null)
	{
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();

		if ( $row ) {
			$this->hydrate($mapped, $row);
		}
		else {
			throw new NotExistingException(sprintf('CAN_NOT_BE_LOAD_FROM %s', $sql), Error::WARNING);
		}

		$mapped->isLoaded(true);
		$mapped->dao = $this;
		return $mapped;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::save() To render permanent. Update or create the records.
	 *
	 * @param MappedInterface $mapped
	 * @throws Exception
	 * @return MappedInterface
	 */
	public function save(MappedInterface $mapped)
	{
		$withTrans = $this->options['withtrans'];
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;

		if ( $withTrans ) $this->connexion->beginTransaction();
		try {
			if ( $mapped->{self::$pkeyName} > 0 || $mapped->isLoaded() == true ) {
				$this->_update($mapped);
			}
			else {
				$this->_insert($mapped);
			}
			if ( $withTrans ) $this->connexion->commit();
			$mapped->isLoaded(true);
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		$mapped->isSaved(true);
		$mapped->dao = $this;
		return $mapped;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::updateFromArray() Update a entity from array
	 *      $data = array('property name in app mode'=>'property value')
	 *
	 * @param integer $id
	 *        	Id of entity to update
	 * @param array $data
	 * @return DaoInterface
	 *
	 */
	public function updateFromArray($id, $data)
	{
		$sysToApp = $this->metaModel;
		$set = array();
		$bind = array();
		$table = $this->_table;

		/* update of primary key is forbidden in all cases */
		if ( isset($data[self::$pkeyName]) ) {
			unset($data[self::$pkeyName]);
		}

		/* */
		foreach( $sysToApp as $asSys => $asApp ) {
			if ( isset($data[$asApp]) ) {
				$set[] = $asSys . '=:' . $asApp;
				$bind[':' . $asApp] = $data[$asApp];
			}
			else if ( $asApp == self::$pkeyName ) {
				$idField = $asSys;
			}
		}

		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		if ( $withTrans ) $this->connexion->beginTransaction();

		try {
			$bind[':id'] = $id;
			$sql = 'UPDATE ' . $table . ' SET ' . implode(',', $set) . ' WHERE ' . $idField . '=:id;';
			$this->updateFromArrayStmt = $this->connexion->prepare($sql);
			$this->updateFromArrayStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::hydrate() Hydrator.
	 *      Load the properties in the mapped object.
	 *
	 * @param \Rbplm\AnyObject $mapped
	 * @param array $row
	 *        	\PDO fetch result to load
	 * @param boolean $fromApp
	 *        	If true, assume that keys $row are name of properties as set in model, else are set as in persitence system.
	 * @return \Rbplm\AnyObject
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		$properties = array();
		if ( $fromApp ) {
			foreach( $this->metaModel as $asSys => $asApp ) {
				$properties[$asApp] = $row[$asApp];
			}
		}
		else {
			$sysToAppFilter = $this->metaModelFilters;
			foreach( $this->metaModel as $asSys => $asApp ) {
				if ( isset($sysToAppFilter[$asSys]) ) {
					$filterMethod = $sysToAppFilter[$asSys] . 'ToApp';
					$properties[$asApp] = static::$filterMethod($row[$asSys]);
				}
				else {
					$properties[$asApp] = $row[$asSys];
				}
			}
		}

		$mapped->hydrate($properties);
		return $mapped;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::delete() Delete current record in database fromhis uid
	 *
	 * @param string $uid Uuid
	 * @param boolean $withChilds
	 *        	If true, suppress all childs and childs of childs
	 * @param boolean $withTrans
	 *        	If true, open a new transaction
	 * @throws Exception
	 * @return DaoInterface
	 */
	abstract public function delete($uid, $withChilds = null, $withTrans = null);

	/**
	 * Alias for delete
	 */
	public function suppress($mapped, $withChilds = null)
	{
		return $this->delete($mapped, $withChilds);
	}

	/**
	 * @deprecated
	 * replace by $this->getTranslator()->toApp()
	 *
	 * @see Rbplm\Dao.DaoInterface::toApp() Translate property name and value to model semantic
	 *
	 * @param array|string $in
	 *        	Db properties name or array (key as db column name => value)
	 * @return array Model properties
	 */
	public static function toApp($in)
	{
		$sysToApp = array_merge(static::$sysToApp, self::$sysToApp);
		$sysToAppFilter = static::$sysToAppFilter;

		if ( is_array($in) ) {
			$out = array();
			foreach( $in as $asSys => $value ) {
				if ( isset($sysToApp[$asSys]) ) {
					$appName = $sysToApp[$asSys];
					if ( isset($sysToAppFilter[$asSys]) ) {
						$filterMethod = $sysToAppFilter[$asSys] . 'ToApp';
						$value = static::$filterMethod($value);
					}
					$out[$appName] = $value;
				}
				else {
					$out[$asSys] = $value;
				}
			}
		}
		else {
			if ( isset($sysToApp[$in]) ) {
				$out = $sysToApp[$in];
			}
			else {
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 * @deprecated
	 * replace by $this->getTranslator()->toSys()
	 *
	 * @see Rbplm\Dao.DaoInterface::toSys() Translate current row data from model to db semantic
	 *
	 * @param array|string $in
	 *        	Application property name or array (key as model property name => value)
	 * @return array System attributs
	 */
	public static function toSys($in)
	{
		$out = array();
		$appToSys = array_flip(array_merge(static::$sysToApp, self::$sysToApp));
		$sysToAppFilter = static::$sysToAppFilter;

		if ( is_array($in) ) {
			foreach( $in as $appName => $value ) {
				if ( isset($appToSys[$appName]) ) {
					$sysName = $appToSys[$appName];
					if ( isset($sysToAppFilter[$sysName]) ) {
						$filterMethod = $sysToAppFilter[$sysName] . 'ToSys';
						$value = static::$filterMethod($value);
					}
					$out[$sysName] = $value;
				}
				else {
					$out[$appName] = $value;
				}
			}
		}
		else {
			if ( isset($appToSys[$in]) ) {
				$out = $appToSys[$in];
			}
			else {
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 * @deprecated
	 * replace by $this->getTranslator()->getSelectAsApp()
	 *
	 * Get select with AS applicationName
	 *
	 * @param string $alias
	 *        	To set the table alias name
	 * @return array
	 */
	public function getSelectAsApp($alias = null)
	{
		if ( !$this->selectAsApp ) {
			$select = array();
			if ( $alias ) $alias = '`' . $alias . '`.';
			foreach( $this->metaModel as $asSys => $asApp ) {
				$select[$asApp] = $alias . '`' . $asSys . '`' . ' AS `' . $asApp . '`';
			}
			$this->selectAsApp = $select;
		}
		return $this->selectAsApp;
	}
} /* End of class */
