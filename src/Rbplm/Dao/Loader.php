<?php

namespace Rbplm\Dao;

/**
 * Loader is a helper to instanciate and load object from db.
 * As he use a registry, if a object is loaded twice, the same reference instance is returned.
 * Implement singelton, so instance is call with static get method.
 *
 * 1. Instanciate object
 * 2. Get his Dao
 * 3. Load object from[option]
 *
 *
 * @example :
 * $loader = Loader::get();
 * $document1 = $loader->loadFromId($id, \Rbplm\Ged\Document\Version::$classId);
 *
 * Some options may be setted :
 * force :
 * lock :
 * useregistry :
 * uidkeyname :
 * idkeyname :
 * parentkeyname :
 * childkeyname :
 * dao :
 *
 */
class Loader implements LoaderInterface
{
	/**
	 * Singleton instance
	 * @var Loader
	 */
	protected static $instance;

	/**
	 * @var array
	 */
	protected $options = array(
		'force'=>false,
		'locks'=>false,
		'useregistry'=>true,
		'uidkeyname'=>'uid',
		'idkeyname'=>'id',
		'parentkeyname'=>'parentId',
		'childkeyname'=>'childId',
		'dao'=>null,
	);

	/**
	 * @var Factory
	 */
	protected $factory;

	/**
	 * @var Registry
	 */
	protected $registry;

	/**
	 *
	 */
	public function __construct($factory)
	{
		$this->registry = new Registry();
		$this->factory = $factory;
		static::$instance = $this;
	}

	/**
	 * Singleton getter
	 * @return Loader
	 */
	public static function get()
	{
		if(!isset(static::$instance)){
			throw new \Exception('Loader must be construct before use get()');
		}
		return static::$instance;
	}

	/**
	 * @param Factory
	 * @return Loader
	 */
	public function setFactory($factory)
	{
		$this->factory = $factory;
		return $this;
	}

	/**
	 * @return Factory
	 */
	public function getFactory()
	{
		return $this->factory;
	}

	/**
	 * Set a		$object = $this->factory->getModel($classId);
 option
	 * Option key may be :
	 * 		force
	 * 		lock
	 * 		useregistry
	 * 		uidkeyname
	 * 		idkeyname
	 * 		parentidkeyname
	 * 		childidkeyname
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return Loader
	 */
	public function setOptions( $key, $value )
	{
		$key = strtolower($key);
		$this->options[$key] = $value;
		return $this;
	}

	/**
	 * @param string $id	Id of the object to load.
	 * @param string $classId	Id of the object class to load.
	 * @return \Rbplm\Any
	 * @throws Exception
	 */
	public function loadFromId($id, $classId)
	{
		$useRegistry = $this->options['useregistry'];
		if($useRegistry){
			$object = $this->registry->getFromId($id, $classId);
			if($object){
				return $object;
			}
		}

		$object = $this->factory->getModel($classId);
		$dao = $this->factory->getDao($classId);
		$dao->loadFromId($object, $id);

		if($useRegistry){
			$this->registry->add($object);
		}
		return $object;
	}

	/**
	 * @param string $uid	Uuid of the object to load.
	 * @param string $classId	Id of the object class to load.
	 *
	 * @return \Rbplm\Any
	 * @throws Exception
	 */
	public function loadFromUid($uid, $classId)
	{
		$useRegistry = $this->options['useregistry'];
		if($useRegistry){
			$object = $this->registry->getFromUid($uid);
			if($object){
				return $object;
			}
		}

		$object = $this->factory->getModel($classId);
		$dao = $this->factory->getDao($classId);
		$dao->loadFromUid($object, $uid);

		if($useRegistry){
			$this->registry->add($object);
		}
		return $object;
	}
}
