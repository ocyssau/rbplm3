<?php
//%LICENCE_HEADER%

namespace Rbplm\Dao;


/**
 * @brief A mapped object may be associate to a Dao.
 * 
 * A mapped object is object associate to a dao and is permanently saved.
 * So to render permanent a object, just implements this interface and create the Dao implementing \Rbplm\Dao\DaoInterface.
 * 
 */
interface MappedInterface
{

	/**
	 * Setter/Getter
	 * Return true if the object is loaded from database
	 * 
	 * @param boolean	$bool	Set load state
	 * @return boolean
	 */
	public function isLoaded( $bool = null );
	
	/**
	 * Setter/Getter
	 * Return true if the object is saved in database
	 * 
	 * @param boolean	$bool	Set saved state
	 * @return boolean
	 */
	public function isSaved( $bool = null );
	
	/**
	 * Used to identify record in db
	 * @param integer $int
	 * @return MappedInterface
	 */
	public function setId($int);
	
	/**
	 * @return string
	 */
	public function getId();
	
	/**
	 * Set the uid
	 *
	 * @param string
	 * @return MappedInterface
	 */
	public function setUid($uid);
	
	/**
	 * Get the uid
	 *
	 * @return String
	 */
	public function getUid();
	
	/**
	 * Hydrator.
	 * Load the properties in the object.
	 *
	 * @param array $properties key=property name in model sementic, $value=$property value
	 * @return MappedInterface
	 */
	public function hydrate( array $properties );
	
} /* End of class */
