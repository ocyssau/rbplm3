<?php
namespace Rbplm;

use Rbplm\People\UserInterface;
use Rbplm\People\User;

/**
 */
trait Owned
{

	/**
	 *
	 * @var User
	 */
	protected $owner;

	/**
	 *
	 * @var integer
	 */
	public $ownerId;

	/**
	 *
	 * @var string
	 */
	public $ownerUid;

	/**
	 *
	 * @param string $name
	 * @return Owned
	 */
	public static function ownerinit($obj)
	{
		$properties = [
			'ownerId' => User::SUPER_USER_ID,
			'ownerUid' => User::SUPER_USER_UUID
		];
		$obj->ownedHydrate($properties);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Owned
	 */
	protected function ownedHydrate(array $properties)
	{
		(array_key_exists('ownerId', $properties)) ? $this->ownerId = $properties['ownerId'] : null;
		(array_key_exists('ownerUid', $properties)) ? $this->ownerUid = $properties['ownerUid'] : null;
		(isset($properties['owner']) && $properties['owner'] instanceof UserInterface) ? $this->setOwner($properties['owner']) : null;
		return $this;
	}

	/**
	 *
	 * @param UserInterface $user
	 * @return Owned
	 */
	public function setOwner(UserInterface $user)
	{
		$this->owner = $user;
		$this->ownerId = $user->getId();
		$this->ownerUid = $user->getLogin();
		return $this;
	}

	/**
	 *
	 * @return UserInterface|string
	 */
	public function getOwner($asUid = false)
	{
		if ( $asUid ) {
			return $this->ownerUid;
		}
		else {
			return $this->owner;
		}
	}
}
