<?php
//%LICENCE_HEADER%
namespace Rbplm\Acl;

/**
 * @brief Initialize Acl resource or role on anyobject and People of Rbplm.
 * 
 * See the examples: Rbplm/Acl/AclTest.php
 * 
 * @see \Rbplm\Acl\AclTest
 *
 */
class Initializer
{

	/**
	 * Init Acl resource of a \Rbplm\AnyObject.
	 * 
	 * @param \Rbplm\AnyObject $anyobject
	 * @return void
	 */
	public static function initResource(\Rbplm\AnyObject $anyobject)
	{
		if ( $anyobject->getParent() ) {
			self::initResource($anyobject->getParent());
			if ( !Acl::singleton()->hasResource($anyobject->getUid()) ) {
				Acl::singleton()->addResource($anyobject->getUid(), $anyobject->getParent()
					->getUid());
			}
		}
		else {
			if ( !Acl::singleton()->hasResource($anyobject->getUid()) ) {
				Acl::singleton()->addResource($anyobject->getUid());
			}
		}
	}

	/**
	 * Add role of current object and parent lchild roles objects to ACL.
	 * 
	 * Create Role for current object and for each of his parents groups.
	 * As it is a recusrsive method, create too Role for each parent groups of groups.
	 * Add each Role to Acl and set correctly parent in accordance to groups links relations.
	 * 
	 * @param \Rbplm\Any $Role
	 * @return void
	 */
	public static function initRole(\Rbplm\Any $Role)
	{
		$Acl = Acl::singleton();
		$groups = $Role->getGroups();
		$parents = array();
		foreach( $groups as $group ) {
			self::initRole($group);
			$parents[] = $group->getUid();
		}
		if ( !$Acl->hasRole($Role->getUid()) ) {
			$Acl->addRole($Role->getUid(), $parents);
		}
	}
}
