<?php
//%LICENCE_HEADER%
namespace Rbplm\Acl;

use Zend\Permissions\Acl\Acl as ZendPermAcl;
use Rbplm\Dao\MappedInterface;

/**
 * @brief Class represent a acl.
 * 
 * Acl extends the \Zend_Acl. See the zend documentation:
 * http://framework.zend.com/manual/en/zend.acl.introduction.html
 * 
 * Acl is define by:
 * 	- resource: a object under access control
 * 	- role: a object asking access to resource
 *  - privilege: what is to do: see, create, suppress...
 *  - rule: ALLOW or DENY
 *  
 *  Acl is a tree where each resource inherit from other, role inherit from other.
 *  
 *  Acl implement singleton pattern and must be instanciate with singleton method:
 *  @code
 *  	$Acl = Acl::singleton();
 *  @endcode
 *  
 *  To re-init the ACL instance, you may use newSingleton:
 *  @code
 *  	$Acl1 = Acl::singleton();
 *  	$Acl2 = Acl::newSingleton();
 *  	assert( $Acl1 != $Acl2 );
 *  @endcode
 *  
 *	See the examples: Rbplm/Acl/AclTest.php
 *
 *	@see AclTest
 *
 */
class Acl extends ZendPermAcl implements MappedInterface
{

	/**
	 * 
	 * @var \Rbplm\Acl\Acl
	 */
	protected static $_instance;

	/**
	 * @var boolean
	 */
	protected $_isLoaded;

	/** 
	 * Singleton pattern
	 * 
	 * @return \Rbplm\Acl\Acl
	 */
	public static function singleton()
	{
		if ( !self::$_instance ) {
			self::$_instance = new Acl();
		}
		return self::$_instance;
	}

	/** 
	 * Create a new singleton instance. The previous singleton is lost.
	 * 
	 * @return \Rbplm\Acl\Acl
	 */
	public static function newSingleton()
	{
		self::$_instance = new Acl();
		return self::$_instance;
	}

	/**
	 *
	 * @param array $properties
	 * @return Rule
	 */
	public function hydrate(array $properties)
	{
		return $this;
	}

	/**
	 * Set the uid
	 *
	 * @param string
	 * @return Acl
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
		return $this;
	}

	/**
	 * Get the uid
	 *
	 * @return string
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * Set the id
	 *
	 * @param integer
	 * @return Acl
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * Get the id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Implements \Rbplm\Dao\MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/\Rbplm\Dao\MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded($bool = null)
	{
		if ( $bool === null ) {
			return $this->loaded;
		}
		else {
			return $this->loaded = (boolean)$bool;
		}
	}

	/**
	 * Implements \Rbplm\Dao\MappedInterface.
	 *
	 * @see library/Rbplm/Dao/\Rbplm\Dao\MappedInterface#isLoaded($bool)
	 *
	 */
	public function isSaved($bool = null)
	{
		if ( $bool === null ) {
			return $this->saved;
		}
		else {
			return $this->saved = (boolean)$bool;
		}
	}

	/**
	 * Get rules
	 * Expose rules, require by DAOs
	 *
	 * @return Array
	 */
	public function getAllRules()
	{
		return $this->rules['byResourceId'];
	}
}
