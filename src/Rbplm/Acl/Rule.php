<?php
//%LICENCE_HEADER%
namespace Rbplm\Acl;

use \Rbplm\Dao\MappedInterface;

/**
 * @brief A rule apply to a resource/role pair.
 */
class Rule implements MappedInterface
{
	use \Rbplm\Mapped;

	/** @var string */
	protected $resourceId;

	/** @var string */
	protected $roleId;

	/** @var string */
	protected $privilege;

	/** @var string */
	protected $rule;

	/**
	 * 
	 * @param array $properties
	 */
	public function __construct(array $properties = null)
	{
		if ( is_array($properties) ) {
			$this->hydrate($properties);
		}
	}

	/**
	 * 
	 * @param array $properties
	 * @return Rule
	 */
	public function hydrate(array $properties)
	{
		isset($properties['resourceId']) ? $this->resourceId = $properties['resourceId'] : null;
		isset($properties['roleId']) ? $this->roleId = $properties['roleId'] : null;
		isset($properties['rule']) ? $this->rule = $properties['rule'] : null;
		isset($properties['privilege']) ? $this->privilege = $properties['privilege'] : null;
		return $this;
	}

	/**
	 * Set the uid
	 *
	 * @param string
	 * @return Rule
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
		return $this;
	}

	/**
	 * Get the uid
	 *
	 * @return string
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * Setter
	 * @param string	$string
	 * @return void
	 */
	public function setRule($string)
	{
		$this->rule = $string;
		return $this;
	}

	/**
	 * Getter
	 * @return string
	 */
	public function getRule()
	{
		return $this->rule;
	}

	/**
	 * Setter
	 * @param string	$string
	 * @return Rule
	 */
	public function setPrivilege($string)
	{
		$this->privilege = $string;
		return $this;
	}

	/**
	 * Getter
	 * @return string
	 */
	public function getPrivilege()
	{
		return $this->privilege;
	}

	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return Rule
	 */
	public function setResourceId($uid)
	{
		$this->resourceId = $uid;
		return $this;
	}

	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getResourceId()
	{
		return $this->resourceId;
	}

	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return Rule
	 */
	public function setRoleId($uid)
	{
		$this->roleId = $uid;
		return $this;
	}

	/**
	 * Getter.
	 * Return uuid.
	 * 
	 * @return string
	 */
	public function getRoleId()
	{
		return $this->roleId;
	}
}
