<?php
namespace Rbplm;

use Rbplm\Sys\Date as DateTime;

/**
 */
trait ArrayObject
{

	/**
	 * Alias for __serialize; implement arrayObject interface
	 * @return 	array
	 */
	public function getArrayCopy()
	{
		$return = [];
		foreach($this as $name=>$value){
			if($name == '_plugins'||$name == 'children'||$name == 'links'||$name == 'dao'){
				continue;
			}
			elseif(is_scalar($value)){
				$return[$name] = $value;
			}
			elseif(is_array($value)){
				$return[$name] = array();
				foreach($value as $subkey=>$subvalue){
					if($subvalue instanceof Any){
						$return[$name][$subkey]=$subvalue->getUid();
					}
					elseif($subvalue instanceof DateTime){
						$return[$name][$subkey]=$value->format('d-m-Y');
					}
					elseif(is_scalar($subvalue)){
						$return[$name][$subkey]=$subvalue;
					}
				}
			}
			elseif($value instanceof DateTime){
				$return[$name] = $value->format('d-m-Y');
			}
			elseif($value instanceof Any){
				$return[$name] = $value;
			}
			elseif($value === null){
				$return[$name] = null;
			}
			else{
				$return[$name] = $value;
			}
		}
		return $return;
	}

}