<?php
namespace Rbplm;

/**
 */
trait Mapped
{

	/**
	 *
	 * @var integer
	 */
	public $id = null;

	/**
	 *
	 * @var \Rbplm\Dao\DaoInterface;
	 */
	public $dao = null;

	/**
	 * Implements MappedInterface.
	 * True if object is loaded from db.
	 *
	 * @var boolean
	 */
	protected $loaded = false;

	/**
	 * Implements MappedInterface.
	 *
	 * @var boolean
	 */
	protected $saved = false;

	/**
	 *
	 * @param string $name
	 * @return AnyObject
	 */
	public static function mappedinit($obj, $name = null)
	{
		$obj->newUid();

		if ( !$name ) {
			$name = $obj->getUid();
		}
		$obj->setName($name);

		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Any
	 */
	protected function mappedHydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = (int)$properties['id'] : null;
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return Any
	 */
	public function newUid()
	{
		$this->uid = Uuid::newUid();
		$this->id = null;
		$this->saved = false;
		$this->loaded = false;
		return $this;
	}

	/**
	 * Used to identify record in db
	 *
	 * @param integer $int
	 * @return Any
	 */
	public function setId($int)
	{
		$this->id = (int)$int;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Implements MappedInterface.
	 *
	 * @see \Rbplm\Dao\MappedInterface#isLoaded($bool)
	 *
	 */
	public function isLoaded($bool = null)
	{
		if ( $bool === null ) {
			return $this->loaded;
		}
		else {
			return $this->loaded = (boolean)$bool;
		}
	}

	/**
	 * Implements MappedInterface.
	 *
	 * @see \Rbplm\Dao\MappedInterface#isLoaded($bool)
	 *
	 */
	public function isSaved($bool = null)
	{
		if ( $bool === null ) {
			return $this->saved;
		}
		else {
			return $this->saved = (boolean)$bool;
		}
	}

	/**
	 * Implement arrayObject interface
	 *
	 * @param array $properties
	 */
	public function populate($properties)
	{
		return $this->hydrate($properties);
	}
}
