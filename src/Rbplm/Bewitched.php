<?php
namespace Rbplm;

use Rbplm\Sys\Exception;

/**
 */
trait Bewitched
{

	/**
	 * Magic method
	 * Getter for properties
	 *
	 * @param string $name
	 */
	public function __get($name)
	{
		if ( $name[0] == '_' ) {
			throw new Exception(sprintf('PROTECTED_PROPERTY_ACCESS_%s', $name));
		}

		$methodName = 'get' . ucfirst($name);
		if ( method_exists($this, $methodName) ) {
			return $this->$methodName();
		}
		elseif(isset($this->$name)) {
			return $this->$name;
		}
		else{
			throw new \InvalidArgumentException("Undefined property $name");
		}
	}

	/**
	 * Magic method
	 * Setter for properties
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 *
	 */
	public function __set($name, $value)
	{
		$methodName = 'set' . ucfirst($name);
		if ( method_exists($this, $methodName) ) {
			$this->$methodName($value);
		}
		else {
			$this->$name = $value;
		}
		return;
	}

	/**
	 * Unset.
	 *
	 * @return void
	 */
	public function __unset($propertyName)
	{
		$this->$propertyName = null;
	}
}
