<?php
namespace Rbplm;

/**
 */
interface ItemInterface
{

	/**
	 * Set the label
	 *
	 * @param string $string
	 * @param boolean $toFormat	If true, suppress all specials char, spaces and replace - by _
	 * @return ItemInterface
	 */
	public function setNodelabel($string, $toFormat = true);

	/**
	 * Get the label
	 *
	 * @return string
	 */
	public function getNodelabel();

	/**
	 * Get the node identifiant
	 *
	 * @return string
	 */
	public function getNodeid();

	/**
	 * @param ItemInterface $parent
	 * @param boolean $bidirectionnal
	 * @return ItemInterface
	 */
	public function setParent(ItemInterface $parent, $bidirectionnal = true);

	/**
	 *
	 * @return ItemInterface|int
	 */
	public function getParent($asId = false);

	/**
	 * @return string
	 */
	public function getParentUid();

	/**
	 * if $bidirectionnal, create relation from $child->$parent
	 *
	 * @param ItemInterface $child
	 * @param boolean $bidirectionnal
	 * @return ItemInterface
	 */
	public function addChild(ItemInterface $child, $bidirectionnal = true);

	/**
	 * Return array of Any
	 *
	 * @return array
	 */
	public function getChildren();

	/**
	 * @param string
	 * @return ItemInterface
	 */
	public function setDn($dn);

	/**
	 * @return string
	 */
	public function getDn();
}
