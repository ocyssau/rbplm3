<?php
// %LICENCE_HEADER%
namespace Rbplm;

use Rbplm\Dao\MappedInterface;

/**
 * @brief anyobject is just one element in composite object chain.
 *
 * Implement design pattern COMPOSITE OBJECT.
 *
 * Example and tests: Rbplm/Model/anyobjectTest.php
 *
 *
 * Emited signals :
 * - self::SIGNAL_POST_SETPARENT
 *
 */
class AnyPermanent extends AnyObject implements MappedInterface, ItemInterface
{
	use Mapped, Item;

	/**
	 * Must be define in children classes
	 *
	 * @var integer
	 */
	static $classId = 'aaaa0000ccccf';

	/**
	 *
	 * @param array|string $properties
	 *        	properties or name
	 * @param \Rbplm\Model\CompositComponentInterface $parent
	 * @return void
	 */
	public function __construct($properties = null, $parent = null)
	{
		parent::__construct($properties);
		if ( $parent ) {
			$this->setParent($parent);
		}
	}

	/**
	 *
	 * @param string $name
	 * @param AnyObject $parent
	 * @return AnyPermanent
	 */
	public static function init($name = null, $parent = null)
	{
		$obj = parent::init($name);
		if ( $parent ) {
			$obj->setParent($parent);
		}
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		$this->itemHydrate($properties);
		$this->mappedHydrate($properties);
		return $this;
	}
} /* End of class */

