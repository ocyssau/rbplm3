<?php
// %LICENCE_HEADER%
namespace Rbplm;

use Rbplm\Dao\MappedInterface;

/**
 * @brief
 */
class Link extends Any implements MappedInterface
{
	use Mapped;

	static $classId = 'aaaa0000ddddf';

	const RULE_NONE = 0;

	const RULE_PARENT = 2;

	const RULE_ING = 4;

	/** @var string */
	protected $rdn;

	/** @var \Rbplm\Any */
	protected $parent;

	/** @var string */
	public $parentUid;

	/** @var integer */
	public $parentId;

	/** @var string */
	public $parentCid;

	/** @var string */
	public $parentSpacename;

	/** @var int */
	public $parentSpaceId;

	/** @var \Rbplm\Any */
	protected $child;

	/** @var string */
	public $childUid;

	/** @var integer */
	public $childId;

	/** @var string */
	public $childCid;

	/** @var string */
	public $childSpacename;

	/** @var int */
	public $childSpaceId;

	/** @var integer */
	public $index = 0;

	/** @var mixed */
	public $data = null;

	/** @var boolean */
	protected $suppressed = false;

	/**
	 *
	 * @var integer
	 */
	public $accessCode = 0;

	/**
	 * md5 of parent uuid and child uuid concatenation.
	 * Identify unicitiy of link.
	 *
	 * @var string
	 */
	public $hash;

	/**
	 *
	 * @param string $name
	 * @return \Rbplm\AnyObject
	 */
	public static function init($parent = null, $child = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		if ( $parent ) $obj->setParent($parent);
		if ( $child ) $obj->setChild($child);
		return $obj;
	}

	/**
	 *
	 * @param string|integer|AnyObject $parent
	 * @param string|integer|AnyObject $child
	 * @return string Md5
	 */
	public static function hash($parent, $child)
	{
		(is_object($parent)) ? $puid = $parent->getUid() : $puid = $parent;
		(is_object($child)) ? $cuid = $child->getUid() : $cuid = $child;

		$hash = md5($puid . $cuid);
		return $hash;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Link
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid = $properties['parentUid'] : null;
		(isset($properties['parentCid'])) ? $this->parentCid = $properties['parentCid'] : null;
		(isset($properties['parentSpacename'])) ? $this->parentSpacename = $properties['parentSpacename'] : null;
		(isset($properties['parentSpaceId'])) ? $this->parentSpaceId = $properties['parentSpaceId'] : null;
		(isset($properties['childId'])) ? $this->childId = $properties['childId'] : null;
		(isset($properties['childUid'])) ? $this->childUid = $properties['childUid'] : null;
		(isset($properties['childCid'])) ? $this->childCid = $properties['childCid'] : null;
		(isset($properties['childSpacename'])) ? $this->childSpacename = $properties['childSpacename'] : null;
		(isset($properties['childSpaceId'])) ? $this->childSpaceId = $properties['childSpaceId'] : null;
		(isset($properties['lindex'])) ? $this->index = (int)$properties['lindex'] : null;
		(isset($properties['index'])) ? $this->index = (int)$properties['index'] : null;
		(isset($properties['suppressed'])) ? $this->suppressed = (boolean)$properties['suppressed'] : null;
		(isset($properties['isSuppressed'])) ? $this->suppressed = (boolean)$properties['isSuppressed'] : null;
		(isset($properties['rdn'])) ? $this->rdn = $properties['rdn'] : null;

		(isset($properties['data'])) ? $this->data = $properties['data'] : null;

		return $this;
	}

	/**
	 *
	 * @param string $rdnString
	 */
	public function setRdn($rdnString)
	{
		$this->rdn = $rdnString;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getRdn()
	{
		return $this->rdn;
	}

	/**
	 *
	 * @param AnyPermanent $any
	 * @return Link
	 */
	public function setParent(Any $any)
	{
		$this->parent = $any;
		$this->parentUid = $any->getUid();
		$this->parentId = $any->getId();
		$this->parentCid = $any::$classId;
		if(isset($any->spacename)){
			$this->parentSpacename = $any->spacename;
		}
		if(isset($any->spaceId)){
			$this->parentSpaceId = $any->spaceId;
		}
		return $this;
	}

	/**
	 *
	 * @param boolean $asId
	 *        	If true, return the id only
	 * @return AnyPermanent|string
	 */
	public function getParent($asId = false)
	{
		if ( $asId ) {
			return $this->parentId;
		}
		else {
			return $this->parent;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getParentUid()
	{
		return $this->parentUid;
	}

	/**
	 *
	 * @param AnyPermanent $any
	 * @return Link
	 */
	public function setChild(Any $any)
	{
		$this->child = $any;
		$this->childUid = $any->getUid();
		$this->childId = $any->getId();
		$this->childCid = $any::$classId;
		if(isset($any->spacename)){
			$this->childSpacename = $any->spacename;
		}
		if(isset($any->spaceId)){
			$this->childSpaceId = $any->spaceId;
		}
		return $this;
	}

	/**
	 *
	 * @return AnyPermanent|string
	 */
	public function getChild($asId = false)
	{
		if ( $asId ) {
			return $this->childId;
		}
		else {
			return $this->child;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getChildUid()
	{
		return $this->childUid;
	}

	/**
	 */
	public function isSuppressed($bool = null)
	{
		if ( $bool === null ) {
			return $this->suppressed;
		}
		else {
			return $this->suppressed = (boolean)$bool;
		}
	}
} /* End of class */
