<?php
// %LICENCE_HEADER%
namespace Rbplm\Org;

use Rbplm\Any;

/**
 * @brief Rbplm is the root node of the COMPOSITE OBJECTS hierarchy.
 *
 * @verbatim
 * Root
 * |OrgamizationUnit
 * |Document
 * |Document_Version
 * |Docfile_Version
 * |Data
 * @endverbatim
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 */
class Root extends Unit
{

	static $classId = '569e93dc9439c';

	/**
	 *
	 * Singleton instance
	 *
	 * @var Root
	 */
	static $_instance;

	/**
	 * Uid for the root component
	 *
	 * @var String uuid
	 */
	const UUID = '01234567-0123-0123-0123-0123456789ab';

	/**
	 * Use singleton
	 *
	 * @return void
	 * @access private
	 */
	function __construct()
	{
		$this->name = 'RanchbePlm';
		$this->label = 'RanchbePlm';
		$this->uid = self::UUID;
		$this->parent = null;
		$this->cid = static::$classId;
	}

	/**
	 * Singleton method
	 *
	 * @return Root
	 */
	static public function singleton()
	{
		if ( !self::$_instance ) {
			self::$_instance = new Root();
		}
		return self::$_instance;
	}

	/**
	 *
	 * @see \Rbplm\AnyObject#setParent($object)
	 */
	public function setParent(Any $parent, $bidirectionnal = true)
	{
		throw new Exception('SET_PARENT_IS_NOT_ENABLE');
	}

	/**
	 *
	 * @see \Rbplm\AnyObject#getParent()
	 */
	function getParent($asId = false)
	{}
}

