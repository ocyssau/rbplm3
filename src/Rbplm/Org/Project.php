<?php
// %LICENCE_HEADER%
namespace Rbplm\Org;

use DateTime as DateTimeInterface;
use Rbplm\Sys\Date as DateTime;
use Rbplm\Ged\AccessCode;

/**
 * @brief Project is a Ou that contains workitems.
 *
 * @verbatim
 * \Rbplm\Org\Root
 * |Project
 * |Workitem
 * |Document
 * |Document_Version
 * |Docfile_Version
 * |Data
 * @endverbatim
 */
class Project extends Unit
{
	use \Rbplm\LifeControl;

	/**
	 * 
	 * @var string
	 */
	static $classId = '569e93c6ee156';

	/**
	 *
	 * @var \Rbplm\Any
	 */
	protected $process;

	/**
	 *
	 * @var string
	 */
	public $processUid;

	/**
	 *
	 * @var string
	 */
	public $processId;

	/**
	 *
	 * @var string
	 */
	public $repositUid;

	/**
	 *
	 * @var string
	 */
	public $repositId;

	/**
	 *
	 * @var string
	 */
	public $versionId;

	/**
	 *
	 * @var integer
	 */
	protected $accessCode = 0;

	/**
	 *
	 * @var string
	 */
	public $lifeStage = 'init';

	/**
	 *
	 * @var string
	 */
	protected $number;

	/**
	 *
	 * @var \DateTime
	 */
	protected $forseenCloseDate;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		$this->lifeControlHydrate($properties);
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['processId'])) ? $this->processId = $properties['processId'] : null;
		(isset($properties['processUid'])) ? $this->processUid = $properties['processUid'] : null;
		(isset($properties['repositId'])) ? $this->repositId = $properties['repositId'] : null;
		(isset($properties['repositUid'])) ? $this->repositUid = $properties['repositUid'] : null;
		(isset($properties['versionId'])) ? $this->versionId = $properties['versionId'] : null;
		(isset($properties['accessCode'])) ? $this->accessCode = $properties['accessCode'] : null;
		if ( isset($properties['forseenCloseDate']) ) {
			$date = $properties['forseenCloseDate'];
			if ( $date instanceof \DateTime ) {
				$this->setForseenCloseDate($date);
			}
			else {
				$this->setForseenCloseDate(new DateTime($date));
			}
		}
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Any $process
	 */
	public function setProcess($process)
	{
		$this->process = $process;
		$this->processId = $process->getId();
		$this->processUid = $process->getUid();
	}

	/**
	 *
	 * @return \Rbplm\Any
	 */
	public function getProcess($asId = false)
	{
		if ( $asId == true ) {
			return $this->processId;
		}
		return $this->process;
	}

	/**
	 * @param \Rbplm\Vault\Reposit $reposit
	 * @return \Rbplm\Vault\Reposit
	 */
	public function setReposit(\Rbplm\Vault\Reposit $reposit)
	{
		$this->reposit = $reposit;
		$this->repositId = $reposit->getId();
		$this->repositUid = $reposit->getUid();
		return $this;
	}

	/**
	 * @return \Rbplm\Vault\Reposit
	 */
	public function getReposit($asId = false)
	{
		if ( $asId == true ) {
			return $this->repositId;
		}
		return $this->reposit;
	}

	/**
	 *
	 * @param DateTime $date
	 * @return \Rbplm\Any
	 */
	public function setForseenCloseDate(DateTimeInterface $date = null)
	{
		$this->forseenCloseDate = $date;
		return $this;
	}

	/**
	 *
	 * @return DateTime
	 */
	public function getForseenCloseDate($format = null)
	{
		if ( !$this->forseenCloseDate instanceof DateTime ) {
			$this->forseenCloseDate = new DateTime();
		}

		if ( $format ) {
			return $this->forseenCloseDate->format($format);
		}
		else {
			return $this->forseenCloseDate;
		}
	}

	/**
	 *
	 * @param string $number
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * Close the project
	 *
	 * The value of the access code permit or deny action:<ul>
	 * <li>0: Modification of Document is permit.</li>
	 * <li>1: Modification of Document is in progress. The checkOut is deny. You can only do checkIn.</li>
	 * <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
	 * <li>10: The Document is lock.You can only upgrade his indice.</li>
	 * <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
	 * <li>15: Indice is upgraded. You can only archive it.</li>
	 * <li>20: The Document is archived.</li>
	 * </ul>
	 *
	 * @param integer $code
	 *        	new access code.
	 * @param \Rbplm\People\User $by
	 * @return \Rbplm\Org\Project
	 *
	 */
	public function close($code, \Rbplm\People\User $by)
	{
		if ( $code == $this->accessCode ) {
			return $this;
		}
		$this->accessCode = (int)$code;
		$this->setCloseBy($by);
		$this->setClosed(new DateTime());
		return $this;
	}

	/**
	 * This method can be used to re-open project
	 *
	 * @return \Rbplm\Ged\Document\Version
	 *
	 */
	public function open()
	{
		$this->accessCode = AccessCode::FREE;
		parent::open();
		return $this;
	}

	/**
	 * This method can be used to check if a Document is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 *
	 * @return integer
	 */
	public function checkAccess()
	{
		if ( $this->accessCode === false ) {
			return 100;
		}
		else {
			return (int)$this->accessCode;
		}
	}
}
