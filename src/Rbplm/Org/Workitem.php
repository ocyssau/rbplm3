<?php
// %LICENCE_HEADER%
namespace Rbplm\Org;

/**
 *
 * @brief Workitem is a Ou that contains documents.
 *
 * @verbatim
 * \Rbplm\Org\Root
 *	 |Project
 *			|Workitem
 *	 			|Document
 *	 			|Document_Version
 * 					|Docfile_Version
 *						|Data
 * @endverbatim
 *
 *
 * @Entity
 */
class Workitem extends Project
{

	static $classId = '569e94192201a';

	/**
	 *
	 * @var Workitem
	 */
	protected $_alias;

	/**
	 *
	 * @var integer
	 */
	public $aliasId;

	/**
	 *
	 * @var string
	 */
	public $spacename;

	/**
	 *
	 * @param Workitem $wi
	 */
	public function setAlias(Workitem $wi)
	{
		$this->_alias = $wi;
		$this->aliasId = $wi->getUid();
	}

	/**
	 *
	 * @return Workitem
	 */
	public function getAlias()
	{
		return $this->_alias;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['aliasId'])) ? $this->aliasId = $properties['aliasId'] : null;
		(isset($properties['alias'])) ? $this->_alias = $properties['alias'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		return $this;
	}
}
