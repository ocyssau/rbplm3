<?php
//%LICENCE_HEADER%

namespace Rbplm\Org\Container;


/**
 * @brief Mockup is a Ou that contains files.
 *
 * @verbatim
 * \Rbplm\Org\Root
 *	 |Project
 *			|Mockup
 *	 			|Document
 *	 			|Document_Version
 * 					|Docfile_Version
 *						|Data
 * @endverbatim
 * 
 * 
 */
class Favorite extends \Rbplm\AnyPermanent
{
	public static $classId = '569b832a77794';
}
