<?php
//%LICENCE_HEADER%

namespace Rbplm\Org;

/**
 * @brief Mockup is a Ou that contains files.
 *
 * @verbatim
 * \Rbplm\Org\Root
 *	 |Project
 *			|Mockup
 *	 			|Document
 *	 			|Document_Version
 * 					|Docfile_Version
 *						|Data
 * @endverbatim
 * 
 */
class Bookshop extends Project
{
    static $classId = '569e93ae9000f';
}
