<?php
// %LICENCE_HEADER%
namespace Rbplm\Org;

use Rbplm\AnyPermanent;

/**
 * @brief Organizational unit.
 *
 * Xtract from wikipedia:
 *
 * OU provides a way of classifying objects located in directories, or names in a digital certificate hierarchy,
 * typically used either to differentiate between objects with the same name
 * (John Doe in OU "marketing" versus John Doe in OU "customer service"),
 * or to parcel out authority to create and manage objects (for example: to give rights for user-creation to local
 * technicians instead of having to manage all accounts from a single central group).
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class Unit extends AnyPermanent
{
	static $classId = '569e93ef2cbbc';

	/**
	 *
	 * @var string
	 */
	public $description;

	/**
	 *
	 * @var string
	 */
	public $lifeStage;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Unit
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['lifeStage'])) ? $this->lifeStage = $properties['lifeStage'] : null;
		return $this;
	}
} /* End of class */
