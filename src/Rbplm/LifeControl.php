<?php
namespace Rbplm;

use Rbplm\People\UserInterface;
use Rbplm\People\User;
use Rbplm\Sys\Date as DateTime;
use DateTime as DateTimeInterface;

/**
 */
trait LifeControl
{

	/**
	 *
	 * @var User
	 */
	protected $updateBy = null;

	/** @var integer */
	public $updateById = null;

	/** @var string */
	public $updateByUid = null;

	/**
	 *
	 * @var User
	 */
	protected $createBy = null;

	/** @var integer */
	public $createById = null;

	/** @var string */
	public $createByUid = null;

	/**
	 *
	 * @var User
	 */
	protected $closeBy = null;

	/** @var integer */
	public $closeById = null;

	/**
	 *
	 * @var DateTime
	 */
	protected $updated;

	/**
	 *
	 * @var DateTime
	 */
	protected $created;

	/**
	 *
	 * @var DateTime
	 */
	protected $closed = null;

	/**
	 *
	 * @param LifeControl $obj;
	 * @return Any
	 */
	public static function lifeinit($obj)
	{
		$properties = array(
			'updated' => new DateTime(),
			'created' => new DateTime(),
			'createById' => User::SUPER_USER_ID,
			'updateById' => User::SUPER_USER_ID,
			'createByUid' => User::SUPER_USER_UUID,
			'updateByUid' => User::SUPER_USER_UUID
		);
		$obj->lifeControlHydrate($properties);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Any
	 */
	protected function lifeControlHydrate(array $properties)
	{
		(isset($properties['createById'])) ? $this->createById = $properties['createById'] : null;
		(isset($properties['createByUid'])) ? $this->createByUid = $properties['createByUid'] : null;

		(isset($properties['updateById'])) ? $this->updateById = $properties['updateById'] : null;
		(isset($properties['updateByUid'])) ? $this->updateByUid = $properties['updateByUid'] : null;

		(isset($properties['closeById'])) ? $this->closeById = $properties['closeById'] : null;
		(isset($properties['closeByUid'])) ? $this->closeByUid = $properties['closeByUid'] : null;

		(isset($properties['createBy']) && $properties['createBy'] instanceof UserInterface) ? $this->setCreateBy($properties['createBy']) : null;
		(isset($properties['updateBy']) && $properties['updateBy'] instanceof UserInterface) ? $this->setUpdateBy($properties['updateBy']) : null;
		(isset($properties['closeBy']) && $properties['closeBy'] instanceof UserInterface) ? $this->setCloseBy($properties['closeBy']) : null;

		if ( isset($properties['created']) ) {
			$date = $properties['created'];
			if ( $date instanceof \DateTime ) {
				$this->setCreated($date);
			}
			elseif ( is_string($date) ) {
				$this->setCreated(new DateTime($date));
			}
		}

		if ( isset($properties['updated']) ) {
			$date = $properties['updated'];
			if ( $date instanceof \DateTime ) {
				$this->setUpdated($date);
			}
			elseif ( is_string($date) ) {
				$this->setUpdated(new DateTime($date));
			}
		}

		if ( isset($properties['closed']) ) {
			$date = $properties['closed'];
			if ( $date instanceof \DateTime ) {
				$this->setClosed($date);
			}
			elseif ( is_string($date) ) {
				$this->setClosed(new DateTime($date));
			}
		}

		return $this;
	}

	/**
	 *
	 * @return Any
	 */
	public function open()
	{
		$this->closed = null;
		$this->closeById = null;
		$this->closeByUid = null;
		$this->closeBy = null;
		return $this;
	}

	/**
	 *
	 * @param UserInterface $user
	 * @return LifeControl
	 */
	public function setCreateBy(UserInterface $user)
	{
		$this->createBy = $user;
		$this->createById = $user->getId();
		$this->createByUid = $user->getUid();
		return $this;
	}

	/**
	 *
	 * @param boolean $asUid
	 * @return UserInterface | string
	 */
	public function getCreateBy($asUid = false)
	{
		if ( $asUid ) {
			return $this->createByUid;
		}
		else {
			return $this->createBy;
		}
	}

	/**
	 *
	 * @param UserInterface $user
	 * @return LifeControl
	 */
	public function setUpdateBy(UserInterface $user)
	{
		$this->updateBy = $user;
		$this->updateById = $user->getId();
		$this->updateByUid = $user->getUid();
		return $this;
	}

	/**
	 *
	 * @param boolean $asUid
	 * @return UserInterface | string
	 */
	public function getUpdateBy($asUid = false)
	{
		if ( $asUid ) {
			return $this->updateByUid;
		}
		else {
			return $this->updateBy;
		}
	}

	/**
	 *
	 * @param UserInterface $user
	 * @return LifeControl
	 */
	public function setCloseBy(UserInterface $user)
	{
		$this->closeBy = $user;
		$this->closeById = $user->getId();
		$this->closeByUid = $user->getUid();
		return $this;
	}

	/**
	 *
	 * @param boolean $asUid
	 * @return UserInterface | string
	 */
	public function getCloseBy($asUid = false)
	{
		if ( $asUid ) {
			return $this->closeByUid;
		}
		else {
			return $this->closeBy;
		}
	}

	/**
	 *
	 * @param DateTime $date
	 * @return LifeControl
	 */
	public function setCreated(DateTimeInterface $date)
	{
		$this->created = $date;
		return $this;
	}

	/**
	 *
	 * @param string $format
	 *        	[optional]
	 * @return DateTime
	 */
	public function getCreated($format = null)
	{
		if ( !$this->created instanceof DateTime ) {
			$this->created = new DateTime();
		}

		if ( $format ) {
			return $this->created->format($format);
		}
		else {
			return $this->created;
		}
	}

	/**
	 *
	 * @param DateTime $date
	 * @return LifeControl
	 */
	public function setClosed(DateTimeInterface $date = null)
	{
		$this->closed = $date;
		return $this;
	}

	/**
	 *
	 * @param string $format
	 *        	[optional]
	 * @return DateTime | null
	 */
	public function getClosed($format = null)
	{
		if ( is_null($this->closed) ) {
			return null;
		}

		if ( !$this->closed instanceof DateTime ) {
			$this->closed = new DateTime();
		}

		if ( $format ) {
			return $this->closed->format($format);
		}
		else {
			return $this->closed;
		}
	}

	/**
	 *
	 * @param DateTime $date
	 * @return LifeControl
	 */
	public function setUpdated(DateTimeInterface $date)
	{
		$this->updated = $date;
		return $this;
	}

	/**
	 *
	 * @param string $format
	 *        	[optional]
	 * @return DateTime
	 */
	public function getUpdated($format = null)
	{
		if ( !$this->updated instanceof DateTime ) {
			$this->updated = new DateTime();
		}

		if ( $format ) {
			return $this->updated->format($format);
		}
		else {
			return $this->updated;
		}
	}
}