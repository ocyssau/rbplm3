<?php
namespace Rbplm;

/**
 */
trait Plugable
{

	/**
	 *
	 * @var array
	 */
	protected $_plugins = array();

	/**
	 *
	 * @param string $name
	 * @param array $args
	 */
	public function __call($name, $args)
	{
		/* is the helper already loaded? */
		$plugin = $this->_getPlugin($name);

		/* call the helper method */
		return call_user_func_array(array(
			$plugin,
			$name
		), $args);
	}

	/**
	 * Retrieve a plugin object
	 *
	 * @param string $type
	 * @param string $name
	 * @return object
	 */
	private function _getPlugin($name)
	{
		$name = ucfirst($name);

		if ( !isset($this->_plugins[$name]) ) {
			$class = '\Rbplm\Plugins\Model\\' . $name;
			$this->_plugins[$name] = new $class();
			if ( method_exists($this->_plugins[$name], 'setComponent') ) {
				$this->_plugins[$name]->setComponent($this);
			}
		}
		return $this->_plugins[$name];
	}
}
