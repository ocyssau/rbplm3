<?php
//%LICENCE_HEADER%

namespace Rbplm\Vault;

/**
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class ExistingDirectoryException extends \Rbplm\Sys\Exception
{
}
