<?php
// %LICENCE_HEADER%
namespace Rbplm\Vault;

use Rbplm\LifeControl;
use Rbplm\Bewitched;
use Rbplm\Mapped;
use Rbplm\Any;
use Rbplm\Uuid;
use Rbplm\Sys\Directory;
use Rbplm\Signal;
use Rbplm\Rbplm;
use Rbplm\Dao\MappedInterface;

/**
 * A Reposit is a url to a directory or other emplacement.
 * Each Reposit is recorded in database.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 */
class Reposit extends Any implements MappedInterface
{
	use LifeControl, Bewitched, Mapped;

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e955d1d1c4';

	/**
	 * Type reposit
	 */
	const TYPE_REPOSIT = 1;

	/**
	 * Type read
	 */
	const TYPE_READ = 2;

	/**
	 * For cache, create symbolics links to datas
	 */
	const MODE_SYMLINK = 1;

	/**
	 * For cache, create copy of data in cache
	 */
	const MODE_COPY = 2;

	/**
	 *
	 * @var string
	 */
	protected $number = '';

	/**
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 *
	 * @var string
	 */
	protected $path = '';

	/**
	 *
	 * @var integer
	 */
	protected $type = 1;

	/**
	 *
	 * @var integer
	 */
	protected $mode = 1;

	/**
	 *
	 * @var integer
	 */
	protected $priority = 2;

	/**
	 *
	 * @var integer
	 */
	protected $active = 1;

	/**
	 * Max size of reposit in Go
	 */
	protected $maxsize = 1024;

	/**
	 * Max number of elements in reposit
	 */
	protected $maxcount = 60000;

	/**
	 * Max number of elements in reposit
	 */
	protected $spacename = 'default';
	
	/**
	 *
	 * @var string
	 */
	static $versionDirname = '__versions';

	/**
	 *
	 * @var string
	 */
	static $iterationDirname = '__iterations';
	
	/**
	 *
	 * @var string
	 */
	static $trashDirname = '__trash';

	/**
	 *
	 * @param string $path
	 *        	Path to reposit
	 * @return \Rbplm\Vault\Reposit
	 */
	public static function init($path=null)
	{
		$class = get_called_class();
		/* @var \Rbplm\Vault\Reposit $obj */
		$obj = new $class();

		if($path){
			//self::initPath($path);
			$obj->setPath($path);
		}
		$obj->setUid(Uuid::newUid());
		return $obj;
	}

	/**
	 * Init the directory where store files
	 */
	public static function initPath($path)
	{
		if ( empty($path) ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'path'));
		}
		
		/* Check if Reposit dir is existing */
		if ( !is_dir($path) ) {
			Rbplm::log('Try to create Reposit directory ' . $path);
			Directory::create($path, 0755);
		}
		
		$versionPath = $path . '/' . self::$versionDirname;
		if ( !is_dir($versionPath) ) {
			Rbplm::log('Try to create Reposit version directory ' . $versionPath);
			Directory::create($versionPath, 0755);
		}
		
		$iterationPath = $path . '/' . self::$iterationDirname;
		if ( !is_dir($iterationPath) ) {
			Rbplm::log('Try to create Reposit iteration directory ' . $iterationPath);
			Directory::create($iterationPath, 0755);
		}
		
		$trashPath = $path . '/' . self::$trashDirname;
		if ( !is_dir($trashPath) ) {
			Rbplm::log('Try to create Reposit trash directory ' . $trashPath);
			Directory::create($trashPath, 0755);
		}
		
		return true;
	}

	/**
	 *
	 * @param integer $int
	 * @return Reposit
	 */
	public function setMode($int)
	{
		$this->mode = (int)$int;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 *
	 * @param integer $int
	 * @return Reposit
	 */
	public function setType($int)
	{
		$this->type = (int)$int;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Priority is integer 0 to 5 to set prefered reposit in case of many reposits candidat.
	 *
	 * @param integer $int
	 * @return Reposit
	 */
	public function setPriority($int)
	{
		$this->priority = (int)$int;
		return $this;
	}

	/**
	 * Priority is integer 0 to 5 to set prefered reposit in case of many reposits candidat.
	 *
	 * @return integer
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 *
	 * @param string $url
	 * @return Reposit
	 */
	public function setPath($path)
	{
		$this->path = $path;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getVersionPath()
	{
		return $this->path . '/' . self::$versionDirname;
	}

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getIterationPath()
	{
		return $this->path . '/' . self::$iterationDirname;
	}
	
	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getTrashPath()
	{
		return $this->path . '/' . self::$trashDirname;
	}

	/**
	 * Setter
	 *
	 * @param string $number
	 * @return Reposit
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getSpacename()
	{
		return $this->spacename;
	}

	/**
	 * Setter
	 *
	 * @param string $number
	 * @return Reposit
	 */
	public function setSpacename($name)
	{
		$this->spacename = $name;
		return $this;
	}
	
	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}
	
	
	
	/**
	 *
	 * @param string $desc
	 * @return Reposit
	 */
	public function setDescription($desc)
	{
		$this->description = $desc;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActive($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->active = $bool;
		}
		else {
			return $this->active;
		}
	}

	/**
	 * Check the size and the count of reposit
	 */
	public function check()
	{
		Signal::trigger('MAX_SIZE_REACH', $this);
		Signal::trigger('MAX_COUNT_REACH', $this);
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Reposit
	 */
	public function hydrate(array $properties)
	{
		// Any
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		// This
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['active'])) ? $this->active= (bool)$properties['active'] : null;
		(isset($properties['mode'])) ? $this->mode = $properties['mode'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['priority'])) ? $this->priority = $properties['priority'] : null;
		(isset($properties['maxsize'])) ? $this->maxsize = $properties['maxsize'] : null;
		(isset($properties['maxcount'])) ? $this->maxcount = $properties['maxcount'] : null;
		(isset($properties['path'])) ? $this->path= $properties['path'] : null;
		(isset($properties['spacename'])) ? $this->spacename= $properties['spacename'] : null;
		
		$this->lifeControlHydrate($properties);
		$this->mappedHydrate($properties);

		return $this;
	}
} /* End of class */
