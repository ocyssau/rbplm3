<?php
//%LICENCE_HEADER%

namespace Rbplm\Vault;

/**
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class ExistingDataException extends \Rbplm\Sys\Exception
{
}
