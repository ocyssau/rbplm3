//%LICENCE_HEADER%

namespace Rbplm\Vault;

use Rbplm\Vault\Fslink\FslinkAbstract;



if(!function_exists ('link')){
	/**
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	function link($from, $to){
		$from = str_replace('\\', '/', realpath($from));
		$to = str_replace('\\', '/', $to);
		$cmd =  Ranchbe::getConfig()->cmd->link;
		if(!$cmd) return false;
		$cmd = str_replace('%from%', $from, $cmd);
		$cmd = str_replace('%to%', $to, $cmd);
		system($cmd, $retval);
		//var_dump(!($retval), $cmd, $target, $path);die;
		return !($retval);
	}
}

if(!function_exists ('readlink')){
	/**
	 *
	 * @param string $path
	 * @return string
	 */
	function readlink($path){
		//@todo: implement this function
		if( linkinfo($path) ) return true;
	}
}

/**
 * @brief Hardlink on filesystem
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 *
 */
class Hardlink extends FslinkAbstract
{

	//----------------------------------------------------------
	/** Create a new link
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	public static function create($from, $to){
		if(!Rbplm_Vault_Filesystem::limitDir($to)) return false;
		//if(!Rbplm_Vault_Filesystem::limitDir($from)) return false;
		if(is_file($to)) unlink($to);
		if(link($from, $to)) {
			return new self($to);
		}else{
			return false;
		}
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Fslink/\Rbplm\Vault\Fslink\FslinkAbstract#suppress()
	 */
	public function suppress(){
		Ranchbe::getError()->push(Rbplm_Vault_Error::INFO, array('file'=>$this->_path),
                                              'try to suppress hardlink %file%');
		if(!$this->test()) return false;
		if(!Rbplm_Vault_Filesystem::limitDir($this->_path)) return false;
		if(!unlink($this->_path)){
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array('file'=>$this->_path),
                                        'error during suppressing hardlink %file%');
			return false;
		}
		Ranchbe::getError()->push(Rbplm_Vault_Error::INFO, array('file'=>$this->_path),
                                             'hardlink %file% has been suppressed');
		return true;
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Fslink/\Rbplm\Vault\Fslink\FslinkAbstract#test()
	 */
	public function test(){
		if(!readlink($this->_path)){
			Ranchbe::getError()->push(Rbplm_Vault_Error::INFO, array('file'=>$this->_path),
                                                '%file% is not a hard link');
			return false;
		}else return true;
	}//End of method

} //End of class
