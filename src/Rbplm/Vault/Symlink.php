//%LICENCE_HEADER%

namespace Rbplm\Vault;


/**
 * $Id: Symlink.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Vault/Symlink.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

if(!function_exists ('symlink')){
	/**
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	function symlink($from, $to)
	{
		$from = str_replace('\\', '/', realpath($from));
		$to = str_replace('\\', '/', $to);
		$cmd =  \Ranchbe::getConfig()->cmd->symlink;
		if(!$cmd) return false;
		$cmd = str_replace('%from%', $from, $cmd);
		$cmd = str_replace('%to%', $to, $cmd);
		system($cmd, $retval);
		//var_dump(!($retval), $cmd, $target, $path);die;
		return !($retval);
	}
}

/**
 * @brief Symbolic link on filesystem
 *
 * @author Administrateur
 *
 */
class Symlink extends \Rbplm\Vault\Fslink\FslinkAbstract
{

	/**
	 * Create a new link
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	public static function create($from, $to)
	{
		if( symlink($from, $to) ){
			return new self($to);
		}
		else return false;
	}//End of method

	/**
	 * @see \Rbplm\Vault\Fslink\FslinkAbstract#suppress()
	 */
	public function suppress()
	{
		Ranchbe::getError()->push(Rbplm_Vault_Error::INFO, array('file'=>$this->_path),
                                              'try to suppress symlink %file%');
		if(!$this->test()) return false;
		if(!Rbplm_Vault_Filesystem::limitDir($this->_path)) return false;

		if(!unlink($this->_path)){
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array('file'=>$this->_path),
                                        'error during suppressing symlink %file%');
			return false;
		}

		Ranchbe::getError()->push(Rbplm_Vault_Error::INFO, array('file'=>$this->_path),
                                             'symlink %file% has been suppressed');
		return true;
	}//End of method

	/**
	 * @see \Rbplm\Vault\Fslink\FslinkAbstract#test()
	 */
	public function test()
	{
		if(!is_link($this->_path)){
			Ranchbe::getError()->push(Rbplm_Vault_Error::INFO, array('file'=>$this->_path),
                                               '%file% is not a symbolic link');
			return false;
		}else return true;
	}//End of method

} //End of class
