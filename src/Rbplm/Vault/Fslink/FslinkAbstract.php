//%LICENCE_HEADER%

namespace Rbplm\Vault\Fslink;


/**
 * $Id: Abstract.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Vault/Fslink/Abstract.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


if(!function_exists ('dellink')){
	/**
	 *
	 * @param string $link
	 * @return boolean
	 */
	function dellink($link){
		$cmd =  Ranchbe::getConfig()->cmd->delln;
		if(!$cmd) return false;
		$cmd = str_replace('%path%', $link, $cmd);
		system($cmd, $retval);
		return !($retval);
	}
}


/**
 * @brief super class for fslink
 *
 */
abstract class FslinkAbstract{

	protected $_path;

	//----------------------------------------------------------
	/**
	 *
	 * @param string $path Path to link element
	 * @return void
	 */
	function __construct($path){
		$this->_path = $path;
	}//End of method

	//----------------------------------------------------------
	/** Get path to current link element
	 *
	 * @return string
	 */
	public function getPath(){
		return $this->_path;
	}//End of method

	//----------------------------------------------------------
	/** Create a new link
	 *
	 * @param string $from The link target, it is the original file or source file
	 * @param string $to The link name, it is the new name for file $target
	 * @return boolean
	 */
	abstract public static function create($from, $to);

	//----------------------------------------------------------
	/** Suppress current link only if it is a link
	 *
	 * @return boolean
	 */
	abstract public function suppress();

	//----------------------------------------------------------
	/** Test if the current path is a link
	 *
	 * @return boolean
	 */
	abstract public function test();

} //End of class
