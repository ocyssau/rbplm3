<?php
// %LICENCE_HEADER%
namespace Rbplm\Vault;

// interface
use Rbplm\Dao\MappedInterface;

// traits
use Rbplm\LifeControl;
use Rbplm\Bewitched;
use Rbplm\Mapped;
use Rbplm\Owned;
use Rbplm\Any;
use Rbplm\Uuid;
use Rbplm\Sys\Fsdata;
use Rbplm\Sys\Date as DateTime;

/**
 *
 * @brief A record of a data in the vault.
 *
 * A record is a set of properties and a link to a data on filesystem.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 */
class Record extends Any implements MappedInterface
{
	use Owned, LifeControl, Bewitched, Mapped;

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e951e03bac';

	/**
	 *
	 * @var \Rbplm\Sys\Fsdata
	 */
	protected $fsdata;

	/**
	 * path to the File '/dir/sub_dir'
	 *
	 * @var string
	 */
	public $path;

	/**
	 * extension of the File '.ext'
	 *
	 * @var string
	 */
	public $extension;

	/**
	 * root name of the File 'File'
	 *
	 * @var string
	 */
	public $rootname;

	/**
	 * Real full path and name of file
	 *
	 * @var string
	 */
	public $fullpath;

	/**
	 * Size of the File in octets
	 *
	 * @var integer
	 */
	public $size;

	/**
	 * modification time of the File
	 *
	 * @var integer
	 */
	public $mtime;

	/**
	 * type of File = File, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	 *
	 * @var string
	 */
	public $type;

	/**
	 * md5 code of the File.
	 *
	 * @var string
	 */
	public $md5;

	/**
	 *
	 * @return Record
	 */
	public static function init($name = "")
	{
		$class = get_called_class();
		$obj = new $class();

		$obj->setUid(Uuid::newUid());
		$obj->setName($name);
		$obj->setCreated(new DateTime());
		$obj->ownerId = \Rbplm\People\User::SUPER_USER_ID;

		if ( !$name ) {
			$name = uniqid();
		}

		return $obj;
	}

	/**
	 *
	 * @param Fsdata $fsdata
	 * @return Record
	 */
	public function setFsdata(Fsdata $fsdata)
	{
		$this->fsdata = $fsdata;
		$this->name = $this->fsdata->getName();
		$this->extension = $this->fsdata->getExtension();
		$this->fullpath = $this->fsdata->getFullpath();
		$this->md5 = $this->fsdata->getMd5();
		$this->mtime = $this->fsdata->getMtime();
		$this->path = $this->fsdata->getPath();
		$this->rootname = $this->fsdata->getRootname();
		$this->size = $this->fsdata->getSize();
		$this->type = $this->fsdata->getType();
		return $this;
	}

	/**
	 *
	 * @return Fsdata
	 */
	public function getFsdata()
	{
		if ( $this->fsdata == null && $this->fullpath != '' ) {
			$this->fsdata = new Fsdata($this->fullpath);
		}
		return $this->fsdata;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		/* traits */
		$this->ownedHydrate($properties);
		$this->mappedHydrate($properties);
		$this->lifeControlHydrate($properties);

		/* Any */
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;

		/* This */
		(isset($properties['path'])) ? $this->path = $properties['path'] : null;
		(isset($properties['extension'])) ? $this->extension = $properties['extension'] : null;
		(isset($properties['rootname'])) ? $this->rootname = $properties['rootname'] : null;
		(isset($properties['fullpath'])) ? $this->fullpath = $properties['fullpath'] : null;
		(isset($properties['size'])) ? $this->size = $properties['size'] : null;
		(isset($properties['mtime'])) ? $this->mtime = $properties['mtime'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['md5'])) ? $this->md5 = $properties['md5'] : null;
		return $this;
	}
} /* End of class */
