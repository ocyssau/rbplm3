<?php
//%LICENCE_HEADER%

namespace Rbplm\Vault\Cache\Builder;

use Rbplm\Vault\Exception;

/**
 * @brief Build a cache of data in vault on other emplacement.
 *
 */
class Localfile
{

	const MODE_SYMLINK = 2;
	const MODE_HARDLINK = 4;

	/**
	 *
	 * Reposit
	 * @var \Rbplm\Vault\Cache\Reposit
	 */
	protected $_reposit = false;


	/**
	 *
	 * @param Rbplm_Model_List | \Rbplm\Model\Collection
	 * @return void
	 */
	public function __construct( $list=null )
	{
		if( is_a($list, 'Rbplm_Model_List') ){
		}
		else if( is_a($list, '\Rbplm\Model\Collection') ){
		}
	}

	/**
	 *
	 * @param string	$datapath
	 * @param string	$type
	 * @param string	$name
	 * @param \Rbplm\Vault\Cache\Reposit	$cache
	 * @throws \Rbplm\Vault\Exception
	 * @return boolean
	 */
	public static function build($datapath, $type, $name, \Rbplm\Vault\Cache\Reposit $cache, $mode = self::MODE_SYMLINK)
	{
		if( empty($name) ){
			throw new Exception(sprintf('VAR_NOT_SET_%s','name'));
		}

		$link = $cache->getPath() . '/' . $name;

		if( function_exists('symlink') ){
			return symlink($datapath, $link);
		}
		else{
			throw new Exception(sprintf('FUNCTION_%s_IS_NOT_EXISTING','symlink'));
		}
	}

} //End of class