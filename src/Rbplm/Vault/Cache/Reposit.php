<?php
//%LICENCE_HEADER%

namespace Rbplm\Vault\Cache;

/**
 * @brief A Reposit is a url to a directory or other emplacement.
 * Each Reposit is recorded in database.
 *
 */
class Reposit extends \Rbplm\Vault\Reposit
{
    
    /**
     *
     * @var string
     */
    public static $classId = '569e95d92cc5a';
    
}//End of class
