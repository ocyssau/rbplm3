<?php
//%LICENCE_HEADER%

namespace Rbplm\Vault\Cache;

/**
 * @brief Builder for cache of vaulted datas.
 *
 */
class Builder
{
	
	/**
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 *
	 * @param string	$datapath
	 * @param string	$type
	 * @param string	$name
	 * @param Reposit	$cache
	 * @return boolean
	 */
	public static function build($datapath, $type, $name, Reposit $cache)
	{
	}

}