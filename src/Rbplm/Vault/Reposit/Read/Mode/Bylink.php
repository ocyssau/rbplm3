//%LICENCE_HEADER%

namespace Rbplm\Vault\Reposit\Read\Mode;


/**
 * $Id: Bylink.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Bylink.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Create read cache by create link with vaulted datas.
 *
 */
interface Bylink extends \Rbplm\Vault\Reposit_Read_Mode_Interface
{

	/**
	 * Get the link
	 *
	 * @return \Rbplm\Vault\Fslink\FslinkAbstract
	 */
	public function getLink();

}
