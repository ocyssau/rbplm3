//%LICENCE_HEADER%

namespace Rbplm\Vault\Reposit\Read\Mode;

/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Interface for read objects
 *
 */
interface ModeInterface
{

	/**
	 *
	 * @return \Rbplm\Vault\Reposit_Read
	 */
	public function getReposit();

	/**
	 *
	 * @return \Rbplm\Vault\Recordfile_Abstract
	 */
	public function getRecordfile();

	/**
	 *
	 * @return boolean
	 */
	public function save();

	/**
	 *
	 * @return boolean
	 */
	public function suppress();

}
