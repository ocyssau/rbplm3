//%LICENCE_HEADER%

namespace Rbplm\Vault\Reposit\Read\Mode;


/**
 * $Id: Hardlink.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Hardlink.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */


/**
 * @brief Link between recordfile fsdata in reposit and symbolic link in read reposit
 *
 */
class Hardlink implements \Rbplm\Vault\Reposit_Read_Mode_Bylink
{

	/**
	 *
	 * @var \Rbplm\Vault\Recordfile_Abstract
	 */
	protected $_recordfile = false;

	/**
	 *
	 * @var \Rbplm\Vault\Reposit_Read
	 */
	protected $_reposit = false;

	/**
	 *
	 * @var \Rbplm\Vault\Hardlink
	 */
	protected $_link = false;

	/**
	 *
	 * @param \Rbplm\Vault\Reposit_Read $reposit
	 * @param \Rbplm\Vault\Recordfile_Abstract $recordfile
	 * @return void
	 */
	public function __construct(\Rbplm\Vault\Reposit_Read &$reposit,
								\Rbplm\Vault\Recordfile_Abstract &$recordfile)
	{
		$this->_reposit = $reposit;
		$this->_recordfile = $recordfile;
		$link_path = $this->_reposit->getProperty('url')
							.'/'.$this->_recordfile->getProperty('file_name');
		$this->_link = new \Rbplm\Vault\Hardlink($link_path);
	}//End of method


	/**
	 * Get the reposit
	 *
	 * @return \Rbplm\Vault\Reposit_Read
	 */
	public function getReposit()
	{
		return $this->_reposit;
	}//End of method


	/** Get the recordfile
	 *
	 * @return \Rbplm\Vault\Recordfile_Abstract
	 */
	public function getRecordfile()
	{
		return $this->_recordfile;
	}//End of method


	/** Get the hardlink
	 *
	 * @return \Rbplm\Vault\Hardlink
	 */
	public function getLink()
	{
		return $this->_link;
	}//End of method


	/** Create hardlink in reposit directory
	 *
	 * @return \Rbplm\Vault\Hardlink | false
	 */
	public function save()
	{
		if(!$this->_reposit || !$this->_recordfile) return false;
		if(!$this->_link) return false;
		$target = $this->_recordfile->getProperty('file');
		$this->_link = \Rbplm\Vault\Hardlink::create( $target, $this->_link->getPath() );
		return $this->_link;
	}//End of method


	/**

	 * @see library/Rb/Reposit/Read/Mode/\Rbplm\Vault\Reposit_Read_Mode_Interface#suppress()
	 */
	public function suppress()
	{
		if(!$this->_link) return false;
		return $this->_link->suppress();
	}//End of method

}//End of class
