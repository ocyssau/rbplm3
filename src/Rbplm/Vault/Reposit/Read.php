//%LICENCE_HEADER%

namespace Rbplm\Vault\Reposit;

use Rbplm\Vault\Reposit;
use Rbplm\Sys\Date as DateTime;

/**
 * @brief Read is use to organize data from reposit in directories on filesystem.
 *
 *
 */
class Read extends Reposit
{

    /**
     *
     * @var string
     */
    public static $classId = '569e95a3ec264';


	/**
	 * Registry of constructed objects of this multiton
	 * @var array
	 */
	protected static $_registry = array();

	/**
	 * Id of default parent resource
	 * @var Integer
	 */
	protected static $_defaultResource_id = 8;

	/**
	 * Name of the subdirectory to be created in the default reposit path
	 * @var string
	 */
	protected $subDir = 'main';

	/**
	 * Singleton constructor
	 * If $id is < 0 then forced id to -1
	 * If $id = 0 to create a new object not yet recorded in database
	 *
	 * @param integer $id , id of reposit
	 * @return Read
	 */
	public static function get($id = -1)
	{
		if($id < 0) $id = -1; //forced to value -1
		if($id == 0)
		return new self(0);
		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new self($id);
		}
		return self::$_registry[$id];
	}//End of method

	/**
	 * Get read reposits lchild to container
	 *
	 * @param Rbplm_Vault_Container $container
	 * @return array , array of \Rbplm\Vault\Reposit
	 */
	public static function getReadReposit( $container )
	{
		if( $container->getId() < 1 ){
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(), 'invalid parameter');
			trigger_error('invalid parameter', E_USER_WARNING);
			return false;
		}
		$ret = array();
		$reads = Rbplm_Vault_Container_Relation_Read::get()->getReads ( $container->getId(), array () );
		foreach($reads as $read){
			$ret[] = Read::get( $read['reposit_id'] );
		}
		return $ret;
		/*
		if($container->getProperty('read_id'))
			return Read::get( $container->getProperty('read_id') );
		else return false;
		*/
	}//End of method

	/**
	 * Set the subdir name. Subdir is created in read directory to store elements.
	 *  Default subdir is 'main'. You can change it with this method
	 *
	 * @param string $subdir
	 * @return this
	 */
	public function setSubdir($subdir)
	{
		$this->subDir = str_replace(array('/', '.', '\\'),'', $subdir);
		return $this;
	}//End of method

	/**
	 * Get the subdir name.
	 *  See too Read::setSubdir().
	 *
	 * @return string
	 */
	public function getSubdir()
	{
		return $this->subDir;
	}//End of method

	/**
	 * Rebuild the Read
	 *
	 * Integer mode of rebuild
	 *     1=clean and rebuild
	 *     2=rebuild
	 *     3=clean
	 * @param integer $mode
	 *
	 * @return boolean
	 */
	public function rebuild($mode)
	{
		switch($mode){
			case 1:
				$this->_clean()->_build();
				break;
			case 2:
				$this->_build();
				break;
			case 3:
				$this->_clean();
				break;
		}
		return true;
	}//End of method

	/**
	 * Clean the read content.
	 *  Suppress all elements in the read.
	 *
	 * @return this
	 */
	protected function _clean()
	{
		//get list of files in Read
		$path = $this->getProperty('url');
		$flist = Rbplm_Vault_\Directory::listDir($path , false, false, true);
		foreach($flist as $fsdata){
			$fsdata->suppress();
		}
		return $this;
	}//End of method

	/**
	 * Get the right read_mode object for the current read mode
	 *
	 * @param Read $read
	 * @param \Rbplm\Vault\Recordfile_Abstract $recordfile
	 * @return Read_Mode_Interface | false
	 */
	public static function getReadLink(Read $read, \Rbplm\Vault\Recordfile_Abstract $recordfile)
	{
		switch($read->mode){
			case 1:
				return new Read_Mode_Symlink($read, $recordfile);
				break;
			case 2:
				return new Read_Mode_Copy($read, $recordfile);
				break;
			case 3:
				return new Read_Mode_Hardlink($read, $recordfile);
				break;
			default:
				Ranchbe::getError()->warning( tra('Unknow read mode') );
				return false;
		}
	}

	/**
	 * Build the read content
	 *
	 * @return this
	 */
	protected function _build()
	{
		//Select docfiles of documents in container lchild to current read :
		$query = 'SELECT file_id FROM %space%_doc_files AS docfiles '.
             'JOIN %space%_documents AS documents ON docfiles.document_id = documents.document_id '.
             'JOIN %space%s AS containers ON documents.%space%_id = containers.%space%_id '.
             'JOIN objects_rel AS relations ON containers.%space%_id = relations.parent_id '.
			 'WHERE relations.child_id = '.$this->getId().
             ' AND documents.document_life_stage = 1'.
             ' AND docfiles.file_life_stage = 1';
		$query = str_replace('%space%',$this->getSpaceName(),$query);

		$res = Ranchbe::getDb()->execute($query);
		if($res === false ){
			Ranchbe::getError()->errorDb(Ranchbe::getDb()->ErrorMsg(), $query);
			return false;
		}
		while($row = $res->FetchRow() ){
			$docfile = new Rbplm_Vault_Docfile($this->getSpace(), $row['file_id']);
			switch($this->mode){
				case 1:
					$readlink = new Read_Mode_Symlink($this, $docfile);
					break;
				case 2:
					$readlink = new Read_Mode_Copy($this, $docfile);
					break;
				case 3:
					$readlink = new Read_Mode_Hardlink($this, $docfile);
					break;
				default:
					Ranchbe::getError()->warning( tra('Unknow read mode') );
					return $this;
			}
			$readlink->save();
			unset($docfile);
		}
		return $this;
	}//End of method

}//End of class

