<?php
// %LICENCE_HEADER%
namespace Rbplm\Vault;

use Rbplm\Sys\Fsdata;
use Rbplm\Dao\DaoInterface;

/**
 * @brief Helper to record datas in vault.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 */
class Vault
{

	/**
	 * Previous record use
	 *
	 * @var array of Record
	 */
	protected $lastRecords;

	/**
	 * True if startTransaction is called
	 *
	 * @var boolean
	 */
	protected $inTransaction = false;

	/**
	 *
	 * Reposit
	 *
	 * @var \Rbplm\Vault\Reposit
	 */
	protected $reposit;

	/**
	 *
	 * @var DaoInterface
	 */
	protected $dao;

	/**
	 *
	 * @param Reposit $reposit
	 * @return void
	 */
	public function __construct(Reposit $reposit)
	{
		$this->reposit = $reposit;
	}

	/**
	 */
	public function beginTransaction()
	{
		$this->inTransaction = true;
		$this->lastRecords = array();
	}

	/**
	 */
	public function rollBack()
	{
		if ( $this->inTransaction == true ) {
			foreach( $this->lastRecords as $record ) {
				$record->getFsdata()->delete();
			}
			$this->inTransaction = false;
		}
		else {
			throw new Exception('rollBack called without opened transaction');
		}
	}

	/**
	 */
	public function commit()
	{
		$this->inTransaction = false;
	}

	/**
	 * Create data record of a Record and create the reposit file data on reposit file system.
	 * The method setDao must be call before use this method.
	 *
	 * @param Record $record
	 *        	The record object.
	 * @param string $fsdata
	 *        	Fsdata instance
	 * @param string $name
	 *        	The name of data as create on reposit file system.
	 * @param string $md5
	 *        	Md5 code of data to check if no error on transfert. If false, none integrity control.
	 * @return Record
	 */
	public function record(Record $record, Fsdata $fsdata, $name, $md5 = null, $replace = true)
	{
		if ( $record->getUid() == '' ) {
			$record->newUid();
		}
		$name = trim($name);
		$toPath = $this->reposit->getPath() . '/' . $name;

		if ( $md5 ) {
			if ( $fsdata->getMd5() != $md5 ) {
				throw new Exception(sprintf('TRANSMISSION_ERROR_ON_%s', $toPath));
			}
		}

		/* */
		if ( $this->reposit == false ) {
			throw new Exception(sprintf('VAR_NOT_SET_%s', 'reposit'));
		}

		/* */
		if ( empty($name) ) {
			throw new Exception(sprintf('VAR_NOT_SET_%s', 'name'));
		}

		/* Tets if target is existing */
		/* @todo = if data existing check md5 to send the right exception message */
		if ( $replace == false && (is_file($toPath) || is_dir($toPath)) ) {
			throw new ExistingDataException(sprintf('DATA %s IS EXISTING', $toPath));
		}

		/* */
		$fsdata->copy($toPath, 0755, true);
		$this->lastRecords[] = $record;

		/* */
		$fsdata = new Fsdata($toPath);
		$record->setFsdata($fsdata);
		return $record;
	}

	/**
	 *
	 * @param Record $record
	 * @param integer $iterationId
	 * @throws Exception
	 * @return Record
	 */
	public function depriveToIteration(Record $record, $iterationId)
	{
		if ( $this->reposit == false ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'reposit'));
		}
		$toPath = $this->reposit->getIterationPath() . '/' . $record->rootname . '.' . $iterationId . $record->extension;
		$record->getFsdata()->copy($toPath, 0755, false);
		$fsdata = new Fsdata($toPath);

		$record->setFsdata($fsdata);
		$this->lastRecords[] = $record;
		return $record;
	}

	/**
	 *
	 * @param Record $record
	 * @param integer $versionId
	 * @throws Exception
	 * @return Record
	 */
	public function depriveToVersion(Record $record, $versionId)
	{
		if ( $this->reposit == false ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'reposit'));
		}
		$toPath = $this->reposit->getVersionPath() . '/' . $record->rootname . '.' . $versionId . $record->extension;
		$record->getFsdata()->copy($toPath, 0755, false);
		$fsdata = new Fsdata($toPath);

		$record->setFsdata($fsdata);
		$this->lastRecords[] = $record;

		return $record;
	}

	/**
	 *
	 * @param Record $record
	 * @throws Exception
	 * @return Record With property "previousFsdata" setted with the fsdata before restoration. May be used to delete fsdata.
	 */
	public function restoreRecordToHead(Record $record)
	{
		if ( $record->getFsdata()->isExisting() == false ) {
			throw new ExistingDataException(sprintf('data %s is not existing', $record->fullpath));
		}

		if ( $this->reposit == false ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'reposit'));
		}

		$record->previousFsdata = null;

		/** @var \Rbplm\Vault\Reposit */
		$reposit = $this->reposit;

		$re = '/^(.+)(\.{1}[[:digit:]]+)(\.{1}.+)$/';
		$filename = $record->getName();
		$matches = null;
		preg_match($re, $filename, $matches);
		if ( $matches ) {
			$rootname = trim($matches[1], '.');
			$iterationId = trim($matches[2], '.');
		}
		else {
			$rootname = $record->rootname;
			$iterationId = 1;
		}

		/* construct path without .[version Id]. mark */
		$toPath = $reposit->getPath() . '/' . $rootname . $record->extension;
		while( is_file($toPath) ) {
			$iterationId = $iterationId - 1;
			if ( $iterationId <= 0 ) {
				$toPath = $reposit->getPath() . '/' . $rootname . '.' . uniqid() . $record->extension;
			}
			else {
				$toPath = $reposit->getPath() . '/' . $rootname . '.' . $iterationId . $record->extension;
			}
		}

		/* Copy to head vault */
		$replace = false;
		$mode = 0755;
		$fsdata = $record->getFsdata()->copy($toPath, $mode, $replace);

		/* set previousFsdata */
		$record->previousFsdata = $record->getFsdata();

		/* set new fsdata to record */
		$record->setFsdata($fsdata);
		$this->lastRecords[] = $record;

		return $record;
	}

	/**
	 *
	 * @param Record $record
	 * @param Reposit $archiveReposit
	 * @param integer $versionId If set, add the version id to the file name
	 *
	 * @throws Exception
	 * @return Record
	 */
	public function depriveToArchive(Record $record, Reposit $archiveReposit, $versionId = null)
	{
		if ( $this->reposit == false ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'reposit'));
		}

		if ( $versionId ) {
			$toPath = $archiveReposit->getPath() . '/' . $record->rootname . '.' . $versionId . $record->extension;
		}
		else{
			$toPath = $archiveReposit->getPath() . '/' . $record->name;
		}

		$record->getFsdata()->copy($toPath, 0755, false);
		$record->getFsdata()->delete();

		$fsdata = new Fsdata($toPath);
		$record->setFsdata($fsdata);
		$this->lastRecords[] = $record;
		return $record;
	}

	/**
	 * Test if dataName exist in this vault
	 *
	 * @param string $dataName
	 *        	Name of data in vault to test
	 * @return bool
	 */
	public function exist($dataName)
	{
		$fullpath = $this->reposit->getPath() . '/' . $dataName;
		return (is_file($fullpath) || is_dir($fullpath));
	}

	/**
	 *
	 * @return Reposit
	 *
	 */
	public function getReposit()
	{
		return $this->reposit;
	}

	/**
	 *
	 * @param Reposit $reposit
	 * @return Vault
	 */
	public function setReposit(Reposit $reposit)
	{
		$this->reposit = $reposit;
		return $this;
	}

	/**
	 *
	 * @return array of Record
	 */
	public function getlastRecords()
	{
		return $this->lastRecords;
	}
}