<?php
namespace Rbplm;

/**
 */
class Event
{

	/**
	 *
	 * @var string
	 */
	public $name;

	/**
	 *
	 * @var \stdClass
	 */
	public $sender;

	/**
	 * @var \stdClass
	 */
	public $callback;

	/**
	 * @var \stdClass
	 */
	public $result;

	/**
	 *
	 * @var array
	 */
	public $error;

	/**
	 *
	 * @var array
	 */
	public $children = array();

	/**
	 *
	 * @param string $name
	 * @param \stdClass $callback
	 */
	public function __construct($name, $callback = null)
	{
		$this->name = $name;
		$this->callback = $callback;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasError()
	{
		if ( $this->children ) {
			foreach( $this->children as $child ) {
				if ( $child->hasError() ) {
					return true;
				}
			}
		}
		return (!is_null($this->error));
	}

	/**
	 *
	 * @return array
	 */
	public function getError()
	{
		$errors = array();
		if ( $this->children ) {
			foreach( $this->children as $child ) {
				if ( $child->hasError() ) {
					$errors = array_merge($child->getError(), $errors);
				}
			}
		}
		if ( !is_null($this->error) ) {
			$errors[] = $this->error;
		}
		return $errors;
	}

	/**
	 *
	 * @return string
	 */
	public function getLastError()
	{
		if ( !is_null($this->error) ) {
			return $this->error;
		}
		if ( $this->children ) {
			foreach( $this->children as $child ) {
				if ( $child->hasError() ) {
					$error = $child->getLastError();
					return $error;
				}
			}
		}
		return '';
	}
}
