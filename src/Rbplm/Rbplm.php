<?php
// %LICENCE_HEADER%
namespace Rbplm;

/**
 * @brief Abstract generic type for all Rbplm classes.
 */
class Rbplm
{

	/**
	 *
	 * @var \Zend\Log\Logger
	 */
	protected static $logger;

	/**
	 * Current version of RbPlm librairy
	 * @constant String
	 */
	const VERSION = '%VERSION%';

	/**
	 * Build number of Ranchbe
	 * @constant String
	 */
	const BUILD = '%BUILD%';

	/**
	 * Copyright
	 * @constant String
	 */
	const COPYRIGHT = '&#169;%COPYRIGHT%';

	/**
	 *
	 * @var array
	 */
	protected static $_config = false;

	/**
	 * Associative array where key is name of config of a metamodel
	 *
	 *
	 * @var Array Collection of Rbplm_Meta_Model
	 */
	protected static $_metamodels = array();

	/**
	 */
	public function __construct()
	{}

	/**
	 * Get the version, build and copyright in array
	 *
	 * @return array
	 */
	public static function getVersion()
	{
		return array(
			'version' => self::VERSION,
			'build' => self::BUILD,
			'copyright' => self::COPYRIGHT
		);
	}

	/**
	 * Get the config instance
	 *
	 * @return Array
	 */
	public static function getConfig($key = null)
	{
		if ( $key ) {
			return self::$_config[$key];
		}
		else {
			return self::$_config;
		}
	}

	/**
	 * Set the config
	 *
	 * @param array $config
	 * @return void
	 */
	public static function setConfig($config)
	{
		self::$_config = $config;
	}

	/**
	 * Logger.
	 * Create a new log message. If none logger is find, none exception is throws.
	 *
	 * @return void
	 */
	public static function log($message)
	{
		if ( Rbplm::$logger ) {
			Rbplm::$logger->log($message);
		}
	}

	/**
	 * Setter for the logger instance.
	 * $logger may be a instance of \Zend_Log but it is probably better
	 * to use a instance of \Rbplm\Sys\Logger.
	 *
	 * @param \Zend\Log\Logger $logger
	 * @return void
	 */
	public static function setLogger(\Zend\Log\Logger $logger)
	{
		Rbplm::$logger = $logger;
		return $this;
	}

	/**
	 * Getter for the logger.
	 *
	 * @return \Zend\Log\Logger
	 */
	public static function getLogger()
	{
		return Rbplm::$logger;
	}

	/**
	 * Return the full version and build identifier of the Rbplm Api.
	 *
	 * @return string
	 */
	public static function getApiFullVersion()
	{
		return self::VERSION . '-' . self::BUILD;
	}

	/**
	 * Return the version identifier of Rbplm librairy.
	 *
	 * @return string
	 */
	public static function getApiVersion()
	{
		return self::VERSION;
	}

	/**
	 * Return the Rbplm Api copyright.
	 *
	 * @return string
	 */
	public static function getApiCopyright()
	{
		return self::COPYRIGHT;
	}

	/**
	 * Get configuration parameters for a Dao
	 *
	 * @param string $daoName
	 * @return string
	 */
	public static function getDaoConfiguration($daoName = 'default', $toArray = false)
	{
		if ( !self::getConfig()->dao->$daoName ) {
			$daoName = 'default';
		}
		if ( $toArray ) {
			return self::getConfig()->dao->$daoName->toArray();
		}
		else {
			return self::getConfig()->dao->$daoName;
		}
	}
} /* End of class */
