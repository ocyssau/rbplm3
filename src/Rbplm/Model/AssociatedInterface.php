<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;


/**
 * $Id: AssociatedInterface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Model/AssociatedInterface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief A associated object may be associate to a \Rbplm\AnyObject.
 *
 */
interface AssociatedInterface{

	/**
	 * 
	 * @param \Rbplm\AnyObject $associated
	 * @return void
	 */
	public function setAssociate(\Rbplm\AnyObject $associated);
	
	/**
	 * 
	 * @return \Rbplm\AnyObject
	 */
	public function getAssociate();

}
