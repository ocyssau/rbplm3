<?php
//%LICENCE_HEADER%
namespace Rbplm\Model\Property;

/**
 * Builder for property
 *
 */
class Builder
{

	/**
	 * Build a Property from name, value and unit.
	 * 
	 * @param string $name
	 * @param string $value
	 * @param string $unit
	 * @return \Rbplm\Model\Property
	 */
	public static function build($name, $value, $unit = 0)
	{
		$property = new \Rbplm\Model\Property(3);
		$property[0] = $name;
		$property[1] = $value;
		$property[2] = $unit;
		return $property;
	}
}
