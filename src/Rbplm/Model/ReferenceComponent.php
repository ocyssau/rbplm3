<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;

use Rbplm\AnyObject;
use \Rbplm\Any;


/**
 * 
 * @brief It may be use to as \Rbplm\AnyObject.
 * Many components may reference the same \Rbplm\Any instance.
 * This class is typically use to include a instance of a \Rbplm\Any in many composite structures.
 * 
 * Implement design pattern COMPOSITE OBJECT.
 * And implement design pattern DECORATOR.
 * 
 * 
 */
class ReferenceComponent extends AnyObject
{
	
	/**
	 * @var Any
	 */
	protected $_reference = false;
	
    /**
     * Getter
     * @return Any
     */
    public function getReference(){
    	return $this->_reference;
    }
    
    /**
     * Setter
     * @return Any
     */
    public function setReference(\Rbplm\Any $reference){
    	$this->_reference = $reference;
    	return $this;
    }
    
}
