<?php
//%LICENCE_HEADER%
namespace Rbplm\Model;

/**
 * @brief Iterator for get all parents tree of a component.
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 * @verbatim
 * Methods:
 * 	abstract public mixed current ( void );
 * 	abstract public scalar key ( void );
 * 	abstract public void next ( void );
 * 	abstract public void rewind ( void );
 * 	abstract public boolean valid ( void );
 * 
 * public RecursiveIterator getChildren ( void )
 * public bool hasChildren ( void )
 * @endverbatim
 */
class ComponentParentIterator implements \Iterator
{

	/**
	 * @var \Rbplm\Model\CompositComponentInterface
	 */
	protected $_current;

	protected $_key;

	/**
	 */
	public function __construct(\Rbplm\Model\CompositComponentInterface $component)
	{
		$this->_current = $component;
		$this->_key = 0;
	}

	/**
	 */
	public function current()
	{
		return $this->_current;
	}

	/**
	 */
	public function key()
	{
		return $this->_key;
	}

	/**
	 */
	public function next()
	{
		$this->_key++;
		$this->_current = $this->_current->getParent();
	}

	/**
	 */
	public function rewind()
	{
		return;
	}

	/**
	 */
	public function valid()
	{
		if ( $this->_current->getParent() ) {
			return true;
		}
		else {
			return false;
		}
	}
}
