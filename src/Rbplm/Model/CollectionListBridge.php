<?php
//%LICENCE_HEADER%
namespace Rbplm\Model;

/**
 * Class to create association between \Rbplm\Model\Collection and a \Rbplm\Dao\ListInterface.
 * It use to populate a collection from a list dao.
 * 
 * Example and tests: Rbplm/Model/CollectionListAssocTest.php
 * 
 * Iterator interface methods :
 *	abstract public mixed current ( void )
 *	abstract public scalar key ( void )
 *	abstract public void next ( void )
 *	abstract public void rewind ( void )
 *	abstract public boolean valid ( void )
 * 
 */
class CollectionListBridge implements \Iterator
{

	/**
	 * 
	 * @var \Rbplm\Model\Collection
	 */
	protected $_collection;

	/**
	 * @var \Rbplm\Dao\ListInterface
	 */
	protected $_list;

	/**
	 * 
	 * @var string
	 */
	protected $_key = 0;

	/**
	 * @param \Rbplm\Model\Collection $collection
	 * @param \Rbplm\Dao\ListInterface $List
	 */
	public function __construct(\Rbplm\Model\Collection &$collection, \Rbplm\Dao\ListInterface &$List)
	{
		$this->_collection = $collection;
		$this->_list = $List;
		$this->_collection->rewind();
		$this->_list->rewind();
	}
	
	/**
	 * @see \Iterator::current()
	 * @return void
	 */
	public function current()
	{
		$current = $this->_collection->current();
		if ( !$current ) {
			$current = $this->_list->toObject();
			$this->_collection->add($current);
		}
		return $current;
	}
	
	/**
	 * @see \Iterator::key()
	 * @return string
	 */
	public function key()
	{
		return $this->_key;
	}
	
	/**
	 * @see \Iterator::next()
	 * @return void
	 */
	public function next()
	{
		$this->_list->next();
		$this->_collection->next();
		$this->_key++;
	}
	
	/**
	 * @see \Iterator::rewind()
	 * @return void
	 */
	public function rewind()
	{
		//Rewind only collection, collection is prioritary in current() method
		$this->_collection->rewind();
		$this->_key = 0;
	}
	
	/**
	 * @see \Iterator::valid()
	 * @return boolean
	 */
	public function valid()
	{
		if ( $this->_key < $this->_list->count() ) {
			return true;
		}
		else {
			return false;
		}
	}
}
