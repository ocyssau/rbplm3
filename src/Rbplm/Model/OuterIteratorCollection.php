<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;

/*
OuterIterator extends Iterator {
public Iterator getInnerIterator ( void )
abstract public mixed Iterator::current ( void )
abstract public scalar Iterator::key ( void )
abstract public void Iterator::next ( void )
abstract public void Iterator::rewind ( void )
abstract public boolean Iterator::valid ( void )
}
*/


/**
 * EXPERIMENTAL.
 * 
 * @brief Collection of \Rbplm\LinkableInterface object.
 * 
 * See Php \SplObjectStorage documentation:
 * @link http://php.net/manual/en/class.splobjectstorage.php
 * 
 * Collection implements RecursiveIterator interface and may be iterate on all COMPOSITE OBJECTS tree.
 * Collection implements \Rbplm\LinkableInterface to be link to a \Rbplm\AnyObject.
 * Each item of this collection must be a \Rbplm\LinkableInterface object.
 * 
 * A collection is a \Rbplm\Model\CompositComponentInterface, so it may be add to a other collection and be integrated to COMPOSIT OBJECTS tree.
 * 
 * Example and tests: Rbplm/Model/CollectionTest.php
 * 
 * @see \Rbplm\Model\CompositComponentInterface
 * @see \Rbplm\LinkableInterface
 * 
 * @link http://php.net/manual/en/class.recursiveiterator.php
 * 
 */
class OuterIteratorCollection implements \OuterIterator, \Rbplm\Model\CompositComponentInterface, \Rbplm\LinkableInterface, \RecursiveIterator{
	
	/**
	 * 
	 * @var string
	 */
	protected $_uid;
	
	/**
	 * 
	 * @var string
	 */
	protected $_name;
	
	/**
	 *
	 * Base path of the object.
	 * @var String
	 */
	protected $_basePath = '';
	
	/**
	 * Implement design pattern COMPOSITE OBJECT.
	 * @var \Rbplm\Model\Collection
	 */
	protected $_children;
		
	/**
	 * 
	 * @var \Rbplm\Model\CompositComponentInterface
	 */
	protected $_parent;
	
	/**
	 * @var \RecursiveIterator
	 */
	protected $_innerIterator;
	
	/**
	 * Constructor.
	 * 
	 * @param array	$properties
	 * @param \Rbplm\Model\CompositComponentInterface $parent
	 */
	public function __construct( array $properties = null, \Rbplm\Model\CompositComponentInterface $parent = null )
	{
		if( $properties ){
			foreach($properties as $name=>$value){
				if($name[0] == '_'){
					continue;
				}
				$this->$name = $value;
			}
			$this->uid = $properties['uid'];
			$this->name = $properties['name'];
		}
		if(!$this->uid){
			$this->uid = \Rbplm\Uuid::newUid();
		}
		if(!$this->name){
			$this->name = uniqid('collection');
		}
		if($parent){
			$this->setParent($parent);
		}
	}
	
	
	/**
	 */
	public function getInnerIterator()
	{
		return $this->_innerIterator;
	}
	
	/**
	 */
	public function current()
	{
		return $this->_innerIterator->current();
	}
	
	/**
	 */
	public function key()
	{
		return $this->_innerIterator->key();
	}
	
	
	/**
	 */
	public function next()
	{
		return $this->_innerIterator->next();
	}
	
	
	/**
	 */
	public function rewind()
	{
		return $this->_innerIterator->rewind();
	}
	
	
	/**
	 */
	public function valid()
	{
		return $this->_innerIterator->valid();
	}
	
	
	/**
	 * Implements RecursiveIterator.
	 * Do not confuse with \Rbplm\Model\CompositComponentInterface::getChild
	 * 
	 * @return \Rbplm\Model\Collection | false
	 */
	public function getChildren()
	{
		return $this->_innerIterator->getChildren();
	}
	
	
	/**
	 * 
	 * Implements RecursiveIterator.
	 * Do not confuse with \Rbplm\Model\CompositComponentInterface::hasChild
	 * 
	 * @return boolean
	 */
	public function hasChildren ()
	{
		return $this->_innerIterator->hasChildren();
	}
	
	
	/**
	 *
	 * Setter for children collection
	 * @param \Rbplm\Model\Collection $collection
	 * @return void
	 */
	public function setChild($collection)
	{
		return $this->_innerIterator->setChild($collection);
	} //End of method
	
	
	/**
	 * Getter for the children collection.
	 * @return \Rbplm\Model\Collection
	 */
	public function getChild()
	{
		return $this->_innerIterator->getChild();
	} //End of method
	
	
	/**
	 * Return true if element has childrens
	 * @return boolean
	 */
	public function hasChild()
	{
		return $this->_innerIterator->hasChild();
	} //End of method
	
	
	/**
	 * Add a object to collection.
	 * Alias for attach.
	 * 
	 * @param \Rbplm\LinkableInterface
	 * @param mixed			Additionals datas to associate to object in collection
	 */
	public function add(\Rbplm\LinkableInterface $object, $data = null)
	{
		return $this->_innerIterator->add();
	}
	
	
	/**
	 * Get item object of collection from his uid.
	 * 
	 * @param string $uid
	 * @return \Rbplm\Model\CompositComponentInterface | false
	 */
	public function getByUid($uid)
	{
		foreach($this->_innerIterator as $current){
			if( $current->getUid() == $uid ){
				return $current;
				break;
			}
		}
	}
	
	
	/**
	 * Get item object of collection from his name.
	 * 
	 * @param string $name
	 * @return \Rbplm\Model\CompositComponentInterface | void
	 */
	public function getByName($name)
	{
		foreach($this->_innerIterator as $current){
			if( $current->getName() == $name ){
				return $current;
				break;
			}
		}
		return;
		/* 3x Plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getName() == $name ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	
	/**
	 * Get item object of collection from his index.
	 * 
	 * @param string $name
	 * @return \Rbplm\Model\CompositComponentInterface | false
	 */
	public function getByIndex($index)
	{
		$this->_innerIterator->rewind();
		while($this->_innerIterator->key() != $index){
			$this->_innerIterator->next();
		}
		return $this->_innerIterator->current();
	}
	
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#getBasePath($force)
	 */
	public function getBasePath($force = false) 
	{
		if($force){
			if($this->_parent){
				$this->_basePath = $this->_parent->getPath(true);
			}
		}
		return $this->_basePath;
	}

	
	/**
	 * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#getPath($force)
	 */
	public function getPath( $force = false ) 
	{
		return $this->getBasePath($force) . '/' . $this->getName();
	}
	
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#setBasePath($basePath)
	 */
	public function setBasePath($basePath) 
	{
		$this->_basePath = $basePath;
	}
	
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#getParent()
	 */
    public function getParent()
    {
    	return $this->_parent;
    }
    
	
    /**
     * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#setParent($object)
     */
    public function setParent(\Rbplm\Model\CompositComponentInterface $object)
    {
    	$this->_parent = $object;
		$this->_basePath = $this->_parent->getPath();
		$this->_parent->getChild()->add( $this );
    }
	
    
	/**
	 * @see library/Rbplm/Model/\Rbplm\LinkableInterface#getUid()
	 */
	function getUid()
	{
		return $this->uid;
	}
	
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\LinkableInterface#getName()
	 */
	function getName()
	{
		return $this->name;
	}
	
	
	/**
	 * @param 	string	$name
	 * @return void
	 */
	function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbplm\Model\CompositComponentInterface::addChild()
	 */
	public function addChild($child)
    {}

    
}
