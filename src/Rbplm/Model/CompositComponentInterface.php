<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;

/**
 * @brief Interface to implement to use iteratif iterator.
 * Interface to implement to implement design pattern COMPOSITE OBJECTS.
 *
 */
interface CompositComponentInterface
{
	
	/**
	 * Getter to parent object
	 * @return CompositComponentInterface
	 */
	public function getParent();
	
	/**
	 * Setter to parent object
	 * @return CompositComponentInterface
	 */
	public function setParent($object);
	
	/**
	 * Getter
	 * 
	 * @param	boolean		if true, force to recalculate the path from parent
	 * @return string
	 */
	public function getBasePath($force = false);
    
	/**
	 * Method for get Path
	 * 
	 * @param	boolean		if true, force to recalculate the path from parent
	 * @return string
	 */
	public function getPath( $force = false );
	
	/**
	 * Setter
	 * @param string
	 * @return CompositComponentInterface
	 */
	public function setBasePath($basePath);
	
	/**
	 * Return true if component has one or more childrens.
	 * @return CompositComponentInterface
	 */
	public function addChild($child);
	
	/**
	 * Getter for childrens collection of component.
	 * @return array
	 */
	public function getChildren();

}
