<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;


/**
 * @brief Test class for \Rbplm\Model\Collection.
 * 
 * @include Rbplm/Model/CollectionTest.php
 * 
 */
class CollectionTest extends \Rbplm\Test\Test
{
    /**
     * @var    \Rbplm\Model\Collection
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
		$name = uniqId();
		$this->object = new Collection( array('name'=>$name) );
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }
    
    
    /**
     * 
     */
    function testReference(){
    	$o[0] = new \Rbplm\AnyObject( array('name'=>'o1') );
    	$uid0 = $o[0]->getUid();
		$o[1] = new \Rbplm\AnyObject( array('name'=>'o2') );
		$o[2] = new \Rbplm\AnyObject( array('name'=>'o3') );
		$o[3] = new \Rbplm\AnyObject( array('name'=>'o4') );
		$o[4] = new \Rbplm\AnyObject( array('name'=>'o5') );
		$o[5] = new \Rbplm\AnyObject( array('name'=>'o6') );
		$o[6] = new \Rbplm\AnyObject( array('name'=>'o7') );
		$o[7] = new \Rbplm\AnyObject( array('name'=>'o8') );
		$o[8] = new \Rbplm\AnyObject( array('name'=>'o9') );
		$o[9] = new \Rbplm\AnyObject( array('name'=>'o10') );
    	
		$Collection = new Collection();
    	foreach ($o as $c){
	    	$Collection->add( $c );
    	}
    	
    	assert( $Collection->getByName('o1') == $o[0] );
    	assert( $Collection->getByName('o1')->getName() == 'o1' );
    	assert( $Collection->getByUid($uid0)->getUid() == $uid0 );
    	
    	/*Test reference*/
    	$Collection->getByName('o1')->setName('t1');
    	assert( $Collection->getByName('o1') == null );
    	assert( $Collection->getByName('t1') == $o[0] );
    	assert( $o[0]->getName() == 't1' );
    }
    
    
    /**
     * 
     */
    function testMemory(){
    	$real_usage = false;
    	
    	echo 'Total memory consumption' . CRLF;
    	$mem0 = memory_get_usage($real_usage);
    	echo ($mem0 / (1024*1024)) . 'Mo' . CRLF;
    	
    	$Collection = new Collection();
    	$mem1 = memory_get_usage($real_usage) - $mem0;
    	echo 'Collection constructor memory consumption for 10 components' . CRLF;
    	echo $mem1/1024 . 'Ko' . CRLF;
    	$mem0 = memory_get_usage($real_usage);
    	
    	$o[0] = new \Rbplm\AnyObject( array('name'=>'o1') );
		$o[1] = new \Rbplm\AnyObject( array('name'=>'o2') );
		$o[2] = new \Rbplm\AnyObject( array('name'=>'o3') );
		$o[3] = new \Rbplm\AnyObject( array('name'=>'o4') );
		$o[4] = new \Rbplm\AnyObject( array('name'=>'o5') );
		$o[5] = new \Rbplm\AnyObject( array('name'=>'o6') );
		$o[6] = new \Rbplm\AnyObject( array('name'=>'o7') );
		$o[7] = new \Rbplm\AnyObject( array('name'=>'o8') );
		$o[8] = new \Rbplm\AnyObject( array('name'=>'o9') );
		$o[9] = new \Rbplm\AnyObject( array('name'=>'o10') );
		
    	$mem1 = memory_get_usage($real_usage) - $mem0;
    	echo 'Objects memory consumption' . CRLF;
    	echo $mem1/1024 . 'Ko' . CRLF;
    	$mem0 = memory_get_usage($real_usage);
    	
    	echo 'Collection items memory consumption' . CRLF;
    	foreach ($o as $c){
	    	$Collection->add( $c );
	    	$mem1 = memory_get_usage($real_usage) - $mem0;
	    	echo $mem1/1024 . 'Ko' . CRLF;
    	}
    	
    	echo 'Total memory consumption' . CRLF;
    	$mem0 = memory_get_usage($real_usage);
    	echo ($mem0 / (1024*1024)) . 'Mo' . CRLF;
    	
    	
    	/* Profillings
    	 */
		$startTime = microtime(true);
		$t = $Collection->getByName('o10');
		$executionTime = microtime(true) - $startTime;
		echo "getByName execution time = $executionTime S \n";
		assert( $t === $o[9] );
		assert( $Collection->current() === $o[9] );
		
		$startTime = microtime(true);
		foreach( $Collection as $item ){
    		if( $item->getName() == 'o10' ){
    			$t = $item;
    			break;
    		}
    	}
		$executionTime = microtime(true) - $startTime;
		echo "foreach execution time = $executionTime S \n";
		assert( $t === $o[9] );
    }
    
    
    
	/**
	 * A General example
	 */
	function testExample(){
		$this->setUp();
		$o1 = new \Rbplm\AnyObject( array('name'=>'o1') );
		$o2 = new \Rbplm\AnyObject( array('name'=>'o2') );
		$o3 = new \Rbplm\AnyObject( array('name'=>'o3') );
		$o4 = new \Rbplm\AnyObject( array('name'=>'o4') );
		$o5 = new \Rbplm\AnyObject( array('name'=>'o5') );
		$o6 = new \Rbplm\AnyObject( array('name'=>'o6') );
		
		/*
		 * Add a object to collection and associate datas.
		 * objects of collection may be associate to any type of datas.
		 */
		$data = array(1,2,3);
		$this->object->add($o1, $data );
		assert( $this->object[$o1] == $data);
		
		//if add twice, add only one instance but update datas
		$data = 'string';
		$this->object->add($o1, $data );
		assert( $this->object[$o1] == $data);
		
		
		//Another style
		$data = 'additionals data';
		$this->object[$o2] = $data;
		assert( $this->object[$o2] == $data);
		
		
		/*
		 * Get a object from collection from his uid 
		 */
		$uid = $o1->getUid();
		$ret = $this->object->getByUid($uid);
		assert( $ret == $o1 );
		
		
		/*
		 * Get a object in collection from his index
		 */
		$this->setUp();
		$this->object[$o1] = 1;
		$this->object[$o2] = 2;
		$this->object[$o3] = 3;
		$this->object[$o4] = 4;
		$this->object[$o5] = 5;
		$this->object[$o6] = 6;
		
		
		$ret = $this->object->getByIndex(0);
		assert( $ret === $o1);
		$ret = $this->object->getByIndex(3);
		assert( $ret === $o4);
		$ret = $this->object->getByIndex(1);
		assert( $ret === $o2);
		
		
		/*
		 * Unset or detach
		 */
		$this->object->detach( $o1 );
		assert( $this->object->getByUid($o1->getUid()) == null );
		
		//In another style
		unset( $this->object[$o2] );
		assert( $this->object->getByUid($o2->getUid()) == null );
	
		
		/*
		 * remove all
		 */
		$this->setUp();
		
		$this->object[$o1] = 1;
		$this->object[$o2] = 2;
		$this->object[$o3] = 3;
		$this->object[$o4] = 4;
		$this->object[$o5] = 5;
		$this->object[$o6] = 6;
		
		$toremove = new Collection();
		$toremove->add($o1);
		$toremove->add($o2);
		$toremove->add($o3);
		assert( $this->object->count() == 6 );
		assert( $this->object->getByUid( $o1->getUid() ) == $o1 );
		assert( $this->object->getByUid( $o2->getUid() ) == $o2 );
		assert( $this->object->getByUid( $o3->getUid() ) == $o3 );
		$this->object->removeAll($toremove);
		assert( $this->object->count() == 3 );
		assert( $this->object->getByUid( $o1->getUid() ) == null );
		assert( $this->object->getByUid( $o2->getUid() ) == null );
		assert( $this->object->getByUid( $o3->getUid() ) == null );
		
		
		
		/*
		 * Play and understand index
		 */
		$this->setUp();
		$this->object[$o1] = 1;
		$this->object[$o2] = 2;
		$this->object[$o3] = 3;
		$this->object[$o4] = 4;
		$this->object[$o5] = 5;
		$this->object[$o6] = 6;
		
		//rewind must be call fisrt:
		assert( $this->object->key() === 0);
		assert( $this->object->current() === null);
		$this->object->rewind();
		assert( $this->object->key() === 0);
		assert( $this->object->current() === $o1);
		assert( $this->object->getInfo() === 1);
		$this->object->next();
		assert( $this->object->current() === $o2);
		assert( $this->object->getInfo() === 2);

		
		//In a foreach
		$i=0;
		foreach($this->object as $key=>$obj){
			assert( $this->object->key() === $i );
			assert( $key === $this->object->key() );
			assert( $this->object->getInfo() === $i + 1 );
			$i++;
		}

		
		/*
		 * What happens if same object is add more than one
		 */
		$this->setUp();
		$ret = $this->object->add($o1);
		$ret = $this->object->add($o1);
		$ret = $this->object->add($o1);
		
		//only one inserted
		assert($this->object->count() == 1);
		foreach($this->object as $component){
			assert($component === $o1);
		}
	}
    

	function testGetUid(){
		$ret = $this->object->getUid();
		assert( !empty($ret) );
		assert( is_string($ret) );
	}
	

	function testGetSetName(){
		$name = uniqId();
		$o = new Collection( array('name'=>$name) );
		
		$ret = $this->object->setName($name);
		assert( $ret === null);
		
		$ret = $this->object->getName();
		assert( $ret == $name);
	}


	function testGetChildren(){
		$ret = $this->object->getChildren();
		assert( $ret === false );
	}

	function testHasChildren(){
		$ret = $this->object->hasChildren();
		assert( $ret === false );
	}

	
	function testContains(){
		$name = uniqId();
		$o = new Collection( array('name'=>$name) );
		$this->object->add($o);
		$ret = $this->object->contains($o);
		assert( $ret === true);
		
		$o = new Collection( array('name'=>$name) );
		$ret = $this->object->contains($o);
		assert( $ret === false);
	}
	
	
	/**
	 * Test recursive iterator
	 * 
	 * 
	 * @return void
	 */
    function testRecursiveIterator(){
    	
    	$this->setUp();
    	//require_once('Rbplm/Model/anyobject.php');
    	
    	$names = array('o0', 'o1', 'o2', 'o3', 'o4', 'o5', 'o6');
    	
		$this->object->add( new \Rbplm\AnyObject(array('name'=>$names[0])) );
		$this->object->add( new \Rbplm\AnyObject(array('name'=>$names[1])) );
		$this->object->add( new \Rbplm\AnyObject(array('name'=>$names[2])) );
    	
		$Component = new \Rbplm\AnyObject( array('name'=>$names[3]) );
    	$this->object->add($Component);
		
		$o2 = new Collection();
		$o2->add( new \Rbplm\AnyObject(array('name'=>$names[4])) );
		$o2->add( new \Rbplm\AnyObject(array('name'=>$names[5])) );
		$o2->add( new \Rbplm\AnyObject(array('name'=>$names[6])) );
    	$Component->setChild( $o2 );
    	
    	$recursiveIterator = new \RecursiveIteratorIterator($this->object, \RecursiveIteratorIterator::SELF_FIRST);
    	$recursiveIterator->setMaxDepth(10);
    	
    	$i=0;
    	foreach($recursiveIterator as $node){
    		var_dump( $recursiveIterator->getDepth(), $recursiveIterator->getSubIterator()->getName(), $node->getName() );
    		assert( $node->getName() === $names[$i] );
    		$i++;
    	}
    }
    
	
	/**
	 * Test iterator
	 * 
	 * 
	 * @return void
	 */
    function testIterator(){
    	$this->setUp();
    	
    	$names = array('o0', 'o1', 'o2', 'o3', 'o4', 'o5', 'o6');
    	
		$this->object->add( new \Rbplm\AnyObject(array('name'=>$names[0])) );
		$this->object->add( new \Rbplm\AnyObject(array('name'=>$names[1])) );
		$this->object->add( new \Rbplm\AnyObject(array('name'=>$names[2])) );
		
		$o2 = new Collection();
		$o2->setName( $names[3] );
		$o2->add( new \Rbplm\AnyObject(array('name'=>$names[4])) );
		$o2->add( new \Rbplm\AnyObject(array('name'=>$names[5])) );
		$o2->add( new \Rbplm\AnyObject(array('name'=>$names[6])) );
    	$this->object->add( $o2 );
    	
    	$recursiveIterator = new \RecursiveIteratorIterator($this->object, \RecursiveIteratorIterator::SELF_FIRST);
    	$recursiveIterator->setMaxDepth(10);
    	
    	$i=0;
    	foreach($recursiveIterator as $node){
	    	assert( $node->getName() === $names[$i] );
	    	$i++;
    	}
    }
    

}
