<?php
//%LICENCE_HEADER%
namespace Rbplm\Model;

/**
 * @brief Work in progress...
 *
 * Generate a anyobject class definition from the model.
 * Generate only class child of \Rbplm\AnyObject.
 *
 * Assume that model define this DTYP:
 * 		appName: name of class property. e.g 'parent'
 * 		appGetter: signature of getter function or null. e.g 'setParent($Parent)'
 * 		appSetter: signature of setter function or null. e.g 'getParent()'
 * 		appType: type of property. e.g '\Rbplm\AnyObject'
 * 		appDefault: default value.
 * 		isagregation: boolean, true if property is a agregate link to other anyobject.
 * 		iscomposition: boolean, true if property is a compose link.
 * 		iscollection: boolean, true if property is a collection of links.
 *
 */
class Generator
{

	public $collectionClass = '\Rbplm\Model\LinkCollection';

	/**
	 *
	 * @param string	$className
	 * @param string	$outputDir
	 * @param string	$templateFile
	 * @param \stdClass	$model
	 * @return void
	 * @throws \Rbplm\Sys\Exception
	 */
	public function generate($className, $outputDir, $templateFile = null, $model)
	{

		/*Get application class name*/
		$className = trim($className);
		$inheritClassName = trim($model->getAppInherits($className));

		/*Create output dir*/
		if ( !is_dir($outputDir) ) {
			mkdir($outputDir, 0777, true);
		}

		/*Build path to files*/
		$classFile = str_replace('_', '/', $className) . '.php';
		//require_once($classFile);

		$myTestClass = $className . 'Test';
		$myTestFile = str_replace('_', '/', $myTestClass) . '.php';

		$output = $outputDir . '/' . basename($classFile);

		/*Inherits*/
		$extendedClass = $model->getAppInherits($className);
		$extendedClassFile = str_replace('_', '/', $extendedClass) . '.php';

		/*Build class generator*/
		$class = new \Zend\Code\Generator\ClassGenerator();
		if ( $extendedClass ) {
			$class->setExtendedClass($extendedClass);
		}
		$classDocBlock = new \Zend\Code\Generator\DocBlockGenerator(array(
			'shortDescription' => '@brief Auto generated class ' . $className,
			'longDescription' => 'This is a class generated with \Generator.',
			'tags' => array(
				array(
					'name' => 'see',
					'description' => $myTestFile
				),
				array(
					'name' => 'version',
					'description' => '$Rev: $'
				),
				array(
					'name' => 'license',
					'description' => 'GPL-V3: Rbplm/licence.txt'
				)
			)
		));
		$class->setName($className)->setDocblock($classDocBlock);

		/*Application properties*/
		$properties = $model->getAppProperties($className);
		$inheritsProperties = $model->getAppProperties($inheritClassName);

		if ( !$properties ) {
			throw new \Exception('None properties for class ' . $className . '. Check that this class is existing in model definition');
		}

		/*
		 * Generate getter and setter methods for each properties
		 */
		foreach( $properties as $appPropname => $appProp ) {
			$propName = $appProp['appName'];
			$propTyp = $appProp['appType'];
			$defaultValue = $appProp['appDefault'];
			$getterMethod = $appProp['appGetter'];
			$setterMethod = $appProp['appSetter'];
			$isAgregation = $appProp['isagregation'];
			$isComposition = $appProp['iscomposition'];
			$isCollection = $appProp['iscollection'];

			/*ignore properties begginning with @ char*/
			if ( $propName[0] == '@' ) {
				continue;
			}

			/*Test if property is inherits*/
			if ( $inheritsProperties[$propName] ) {
				continue;
			}

			/*Add property*/
			if ( $class->hasProperty($propName) ) {
				continue;
			}

			if ( $propTyp == 'boolean' ) {
				$propName = '_' . $propName;
				$class->setProperty($this->_getProtectedProperty($propName, $propTyp, $defaultValue));
				$class->setMethod($this->_getBooleanSetterGetter($propName));
				continue;
			}

			if ( $isCollection ) {
				$propName = '_' . $propName;
				$class->setProperty($this->_getCollectionProperty($propName, $propTyp));
				$class->setMethod($this->_getCollectionGetter($propName, $propTyp));
				//$class->setMethod( $this->_getCollectionSetter($propName, $propTyp) );
				continue;
			}

			if ( $isAgregation ) {
				/*If is link add ever setter and getter methods*/
				$propName = '_' . $propName;
				$class->setProperty($this->_getProtectedProperty($propName, $propTyp, $defaultValue));
				$class->setMethod($this->_getAgregationGetter($propName, $propTyp));
				$class->setMethod($this->_getAgregationSetter($propName, $propTyp));

				/*Add a property for store the id of the associated object*/
				$linkIdName = trim($propName, '_') . 'Id';
				if ( !$class->hasProperty($linkIdName) ) {
					$property = new \Zend\Code\Generator\PropertyGenerator();
					$property->setVisibility('protected');
					$property->setName($linkIdName);
					$class->setProperty($property);
				}
				continue;
			}

			if ( $isComposition ) {
				/*If is link add ever setter and getter methods*/
				$propName = '_' . $propName;
				$class->setProperty($this->_getProtectedProperty($propName, $propTyp, $defaultValue));
				$class->setMethod($this->_getCompositionGetter($propName, $propTyp));
				//$class->setMethod( $this->_getCompositionSetter($propName, $propTyp) );
				continue;
			}

			if ( $getterMethod || $setterMethod ) {
				$propName = '_' . $propName;
			}

			$class->setProperty($this->_getProtectedProperty($propName, $propTyp, $defaultValue));

			if ( $getterMethod ) {
				$class->setMethod($this->_getRegularGetter($propName, $propTyp));
			}

			if ( $setterMethod ) {
				$class->setMethod($this->_getRegularSetter($propName, $propTyp));
			}
		}

		//Put result in file
		if ( is_file($output) ) {
			rename($output, $output . uniqid() . '.bck');
			echo 'Output file ' . $output . ' is existing!' . CRLF;
		}

		if ( $templateFile ) {
			$out = file_get_contents($templateFile);
			$out = str_replace('%INHERIT_CLASS_FILE%', $extendedClassFile, $out);
			$out = str_replace('%CLASS_DEFINITION%', $class->generate(), $out);
		}
		else {
			$out = $class->generate();
		}

		file_put_contents($output, $out);
		echo 'See result file ' . $output . CRLF;
	}

	private function _getBooleanSetterGetter($propName)
	{
		$publicPropName = trim($propName, '_');
		$method = new \Zend\Code\Generator\MethodGenerator();
		if ( !strstr($publicPropName, 'is') ) {
			$methodName = 'is' . ucfirst($publicPropName);
		}
		else {
			$methodName = $publicPropName;
		}
		$method->setName($methodName);

		$paramName = 'bool';
		$Parameter = new \Zend\Code\Generator\ParameterGenerator(array(
			'name' => $paramName
		));
		$Parameter->setPosition(1);
		$Parameter->setDefaultValue(null);
		//$Parameter->setType('boolean');
		$method->setParameter($Parameter);

		$body = 'if( is_bool($bool) ){
        	return $this->' . $propName . ' = $bool;
        }
        else{
        	return $this->' . $propName . ';
        }';

		$method->setBody($body);

		/*Tags @param and @return*/
		$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
		$docBlock->setTag(new \Zend\Code\Generator\DocBlock\Tag(array(
			'paramName' => $paramName,
			'datatype' => 'boolean'
		)));
		$docBlock->setTag(new \Zend\Code\Generator\Docblock\Tag(array(
			'datatype' => 'boolean'
		)));
		$method->setDocblock($docBlock);
		return $method;
	}

	/**
	 *
	 * @param string $propName
	 * @param string $propTyp
	 * @param string $methodName
	 */
	private function _getRegularSetter($propName, $propTyp, $methodName = null)
	{
		$publicPropName = trim($propName, '_');
		$method = $this->_getBaseSetter($publicPropName, $propTyp, $methodName);

		$body = '$this->' . $propName . ' = $' . ucfirst($publicPropName) . ";\n";
		$method->setBody($body);
		return $method;
	}

	private function _getAgregationSetter($propName, $propTyp, $methodName = null)
	{
		$publicPropName = trim($propName, '_');
		$paramName = ucfirst($publicPropName);
		$method = $this->_getBaseSetter($publicPropName, $propTyp, $methodName);

		$body = '$this->%s = $%s;' . "\n";
		$body .= '$this->%sId = $this->%s->getUid();' . "\n";
		$body .= '$this->getLinks()->add($this->%s);' . "\n";
		$body = sprintf($body, $propName, $paramName, $publicPropName, $propName, $propName);

		$method->setBody($body);
		return $method;
	}

	/*
	 private function _getCollectionSetter($propName, $propTyp, $methodName = null){
	 $publicPropName = trim($propName, '_');
	 $method = $this->_getBaseSetter( $publicPropName, $propTyp, $methodName );

	 $body = '$this->'.$propName . '['.$propName.'->getUid()] = $' . ucfirst( $publicPropName ) . ";\n";
	 if($isLink){
	 $body .= sprintf('$this->%sId = $this->%s->getUid()', $publicPropName, $propName) . ";\n";
	 $body .= '$this->getLinks()->add($this->' . $propName . ')' . ";\n";
	 }
	 $method->setBody( $body );
	 return $method;
	 }
	 */
	private function _getBaseSetter($propName, $propTyp, $methodName = null)
	{
		$method = new \Zend\Code\Generator\MethodGenerator();
		$publicPropName = trim($propName, '_');
		if ( !$methodName ) {
			$methodName = 'set' . ucfirst($publicPropName);
		}
		$method->setName($methodName);

		$paramName = ucfirst($publicPropName);
		$Parameter = new \Zend\Code\Generator\ParameterGenerator(array(
			'name' => $paramName
		));
		$Parameter->setPosition(1);

		if ( !in_array($propTyp, array(
			'string',
			'int',
			'integer',
			'float',
			'decimal',
			'uuid',
			'timestamp',
			'bool',
			'boolean'
		)) ) {
			$Parameter->setType($propTyp);
		}
		$method->setParameter($Parameter);

		/*Tags @param and @return*/
		$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
		$docBlock->setTag(new \Zend\Code\Generator\Docblock\TagManager(array(
			'paramName' => $paramName,
			'datatype' => $propTyp
		)));
		$method->setDocblock($docBlock);

		return $method;
	}

	/**
	 */
	private function _getBaseGetter($propName, $propTyp, $methodName = null)
	{
		$method = new \Zend\Code\Generator\MethodGenerator();
		$publicPropName = trim($propName, '_');
		if ( !$methodName ) {
			$methodName = 'get' . ucfirst($publicPropName);
		}
		$method->setName($methodName);

		/*Tags @param and @return*/
		$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
		$docBlock->setTag(new \Zend\Code\Generator\DocBlockGenerator(array(
			'datatype' => $propTyp
		)));
		$method->setDocblock($docBlock);
		return $method;
	}

	private function _getRegularGetter($propName, $propTyp, $methodName = null)
	{
		return $this->_getBaseGetter($propName, $propTyp, $methodName)->setBody('return $this->' . $propName . ';');
	}

	private function _getAgregationGetter($propName, $propTyp, $methodName = null)
	{
		$method = $this->_getBaseGetter($propName, $propTyp, $methodName);

		$body = 'if( !$this->%s ){' . "\n";
		$body .= '	throw new \Rbplm\Sys\Exception(sprintf(\'PROPERTY_%s_IS_NOT_SET\'), \Rbplm\Sys\Error::WARNING);' . "\n";
		$body .= '}' . "\n";
		$body .= 'return $this->%s;' . "\n";
		$body = sprintf($body, $propName, $propName, $propName);

		$method->setBody($body);
		return $method;
	}

	private function _getCompositionGetter($propName, $propTyp, $methodName = null)
	{
		$publicPropName = trim($propName, '_');
		$method = $this->_getBaseGetter($propName, $propTyp, $methodName);

		$body = 'if( !$this->%s ){' . "\n";
		$body .= '	$this->%s = new %s($this);' . "\n";
		$body .= '}' . "\n";
		$body .= 'return $this->%s;' . "\n";
		$body = sprintf($body, $propName, $propName, $propTyp, $propName);

		$method->setBody($body);
		return $method;
	}

	private function _getCollectionGetter($propName, $propTyp, $methodName = null)
	{
		$method = $this->_getBaseGetter($propName, $propTyp, $methodName);
		$publicPropName = trim($propName, '_');

		$collectionClass = $this->collectionClass;

		$body = 'if( !$this->%s ){' . "\n";
		$body .= '	$this->%s = new ' . $collectionClass . '( array(\'name\'=>\'%s\'), $this );' . "\n";
		$body .= '	$this->getLinks()->add( $this->%s );' . "\n";
		$body .= '}' . "\n";
		$body .= 'return $this->%s;' . "\n";
		$body = sprintf($body, $propName, $propName, $publicPropName, $propName, $propName);
		$method->setBody($body);
		return $method;
	}

	private function _getProtectedProperty($propName, $propTyp, $defaultValue)
	{
		$property = new \Zend\Code\Generator\PropertyGenerator();
		$property->setVisibility('protected');
		$property->setName($propName);
		if ( $defaultValue ) {
			$property->setDefaultValue($defaultValue);
		}
		/*Tags @param and @return*/
		$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
		$docBlock->setTag(new \Zend\Code\Generator\DocBlock\Tag(array(
			'name' => 'var',
			'description' => $propTyp
		)));
		$property->setDocblock($docBlock);
		return $property;
	}

	private function _getCollectionProperty($propName, $propTyp)
	{
		$property = new \Zend\Code\Generator\PropertyGenerator();
		$property->setVisibility('protected');
		$property->setName($propName);
		/*Tags @param and @return*/
		$docBlock = new \Zend\Code\Generator\DocBlockGenerator();
		$docBlock->setShortDescription('Collection of ' . $propTyp);
		$docBlock->setTag(new \Zend\Code\Generator\DocBlock\Tag(array(
			'name' => 'var',
			'description' => $this->collectionClass
		)));
		$property->setDocblock($docBlock);
		return $property;
	}
}


