<?php
//%LICENCE_HEADER%
namespace Rbplm\Model;

/**
 * Prefer Callback\FilterIterator since PHP5.4
 */
class CollectionCallbackFilter extends \FilterIterator
{

	private $_callback;

	/**
	 * @param Iterator
	 * @param array
	 */
	public function __construct(\Iterator $iterator, $callBack)
	{
		parent::__construct($iterator);
		$this->_callback = $callBack;
	}

	/**
	 * @see \FilterIterator::accept()
	 * @return boolean
	 */
	public function accept()
	{
		return call_user_function($this->_callback, $this->current());
	}
}
