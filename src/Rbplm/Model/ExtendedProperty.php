<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;


/**
 * $Id: anyobject.php 504 2011-06-30 16:04:55Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbplm/Model/anyobject.php $
 * $LastChangedDate: 2011-06-30 18:04:55 +0200 (jeu., 30 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 504 $
 */

/**
 * @brief Extends properties are properties created during execution and she are not declared in class. Type is unknow. And exitence is unknow
 * 
 * 
 * Example and tests :
 * 
 * 
 * Emited signals :
 * 
 * @verbatim
 * @endverbatim
 */
class ExtendedProperty
{
	
	const TYPE_MULTIPLE = 1;
	const TYPE_MULTIPLE = 2;
	const TYPE_MULTIPLE = 4;
	const TYPE_MULTIPLE = 8;
	const TYPE_MULTIPLE = 16;
	const TYPE_MULTIPLE = 32;
	const TYPE_MULTIPLE = 64;
	const TYPE_MULTIPLE = 128;
	const TYPE_MULTIPLE = 256;
	const TYPE_MULTIPLE = 516;
	const TYPE_MULTIPLE = 1024;
	const TYPE_MULTIPLE = 2048;
	const TYPE_MULTIPLE = 4056;
	

} //End of class

