<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;


/**
 *
 */
class CollectionClassFilter extends \FilterIterator implements \ArrayAccess, \Countable
{

	private $_class;

    public function __construct(\Iterator $iterator , $class )
    {
        parent::__construct($iterator);
        $this->_class = $class;
    }

	/**
	 * @see \FilterIterator::accept()
	 * @return boolean
	 */
	public function accept()
	{
		if( is_a($this->current(), $this->_class ) ){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * @see \ArrayAccess::offsetExists()
	 * @param mixed
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
	}

	/**
	 * @see \ArrayAccess::offsetGet()
	 * @param mixed
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		$this->rewind();
		while($this->key() != $offset){
			$this->next();
		}
		return $this->current();
	}

	/**
	 * @see \ArrayAccess::offsetSet()
	 * @param mixed
	 * @param mixed
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		throw new \Exception('UNVAILABLE ON THIS COLLECTION');
	}

	/**
	 * @see \ArrayAccess::offsetUnset()
	 * @param mixed
	 * @return void
	 */
	public function offsetUnset($offset)
	{
		throw new \Exception('UNVAILABLE ON THIS COLLECTION');
	}

	/**
	 * @see \countable::cont()
	 * @param mixed
	 * @param mixed
	 * @return void
	 */
	public function count()
	{
		$i=0;
		$this->rewind();
		while($this->valid()){
			$this->next();
			$i++;
		}
		return $i;
	}


}
