<?php
//%LICENCE_HEADER%

namespace Rbplm\Model;

/**
 * @brief A property is a \SplFixedArray.
 * - Key 0 is name.
 * - Key 1 is value.
 * - Key 2 is unit.
 * 
 * It is too a linkable object and may be add to Rbplm_Model_linkCollection.
 *
 */
class Property extends \SplFixedArray implements \Rbplm\LinkableInterface{
	
	/**
	 * @see \Rbplm\LinkableInterface::getUid()
	 */
	public function getUid()
	{
		return $this->offsetGet(0);
	}
	
	/**
	 * @see \Rbplm\LinkableInterface::getName()
	 */
	public function getName()
	{
		return $this->offsetGet(0);
	}
	
}
