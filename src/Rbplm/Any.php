<?php
// %LICENCE_HEADER%
namespace Rbplm;

use DateTime;
use Rbplm\Sys\Date as RbDate;

/**
 * @brief Generic class for model.
 *
 * A Model is linkable, so it may be attach as link to a other Model
 *
 *
 * Emited signals:
 * - self::SIGNAL_POST_SETNAME
 */
class Any implements LinkableInterface
{

	const SIGNAL_POST_SETNAME = 'setname.post';

	/**
	 * Must be define in children classes
	 *
	 * @var integer
	 */
	static $classId = 'aaaa0000aaaaf';

	/**
	 * Class Id
	 *
	 * @var integer
	 */
	public $cid;

	/**
	 *
	 * @var string
	 */
	protected $uid;

	/**
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * Distinguished name involved in ltree of inherits.
	 *
	 * If set as parentDn+'/id/' => may be useful to apply links of the parent to the children
	 *
	 * @var string
	 *
	 */
	protected $dn;

	/**
	 *
	 * @param array $properties
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			foreach( $properties as $name => $value ) {
				$this->$name = $value;
			}
		}
		$this->cid = static::$classId;
	}

	/**
	 */
	public function __clone()
	{
		$this->newUid();
	}

	/**
	 *
	 * @return array
	 */
	public function __sleep()
	{
		$ignore = [
			'_plugins',
			'dao',
			'factory'
		];
		$toSerialize = array_diff(array_keys(get_object_vars($this)), $ignore);
		return $toSerialize;
	}

	/**
	 *
	 * @param string $dnString
	 */
	public function setDn($dnString)
	{
		$this->dn = $dnString;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDn()
	{
		return $this->dn;
	}

	/**
	 * Export all properties
	 * $select may be a array with name of properties to export.
	 *
	 * @var array $select
	 */
	public function export($select = null)
	{
		$ret = [];
		if ( $select ) {
			foreach( $select as $name ) {
				if ( isset($this->$name) ) {
					$ret[$name] = $this->$name;
				}
			}
		}
		else {
			foreach( $this as $name => $value ) {
				$ret[$name] = $value;
			}
		}
		return $ret;
	}

	/**
	 * Alias for __serialize; implement arrayObject interface
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		$return = [];

		foreach( $this->__sleep() as $name ) {
			$value = $this->$name;
			if ( is_scalar($value) ) {
				$return[$name] = $value;
			}
			elseif ( is_array($value) ) {
				$return[$name] = array();
				foreach( $value as $subkey => $subvalue ) {
					if ( $subvalue instanceof Any ) {
						$return[$name][$subkey] = $subvalue->getUid();
					}
					elseif ( $subvalue instanceof DateTime ) {
						$return[$name][$subkey] = $subvalue->format(RbDate::$dateFormat);
					}
					elseif ( is_scalar($subvalue) ) {
						$return[$name][$subkey] = $subvalue;
					}
					elseif ( is_array($subvalue) ) {
						$return[$name][$subkey] = $subvalue;
					}
				}
			}
			elseif ( $value instanceof DateTime ) {
				$return[$name] = $value->format(RbDate::$dateFormat);
			}
			elseif ( $value instanceof Any ) {
				$return[$name] = $value;
			}
			elseif ( $value === null ) {
				$return[$name] = null;
			}
			else {
				$return[$name] = $value;
			}
		}
		return $return;
	}

	/**
	 * Set the uid
	 *
	 * @param
	 *        	string
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
		return $this;
	}

	/**
	 * Get the uid
	 *
	 * @return String
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 *
	 * @param string $name
	 * @return Any
	 */
	public function newUid()
	{
		$this->uid = Uuid::newUid();
		return $this;
	}

	/**
	 * Set the name
	 *
	 * @param
	 *        	string
	 * @return Any
	 */
	public function setName($string)
	{
		$this->name = $string;
		Signal::trigger(self::SIGNAL_POST_SETNAME, $this);
		return $this;
	}

	/**
	 * Get the name
	 *
	 * @return String
	 */
	public function getName()
	{
		return $this->name;
	}
} /* End of class */
