<?php
namespace Rbplm;

/**
 * Classe de gestion des signaux.
 *
 * Copyright (C) 2011 Nicolas Joseph
 *
 * LICENSE:
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package NanoMvc
 * @author Nicolas Joseph <gege2061@homecomputing.fr>
 * @copyright 2011 Nicolas Joseph
 * @license http://www.opensource.org/licenses/agpl-3.0.html AGPL v3
 * @filesource Modified by Olivier CYSSAU.
 *
 */
class Signal
{

	/**
	 *
	 * @var array
	 */
	protected static $callbackList;

	/**
	 *
	 * @var array
	 */
	protected static $eventList;

	/**
	 *
	 * @param
	 *        	$object
	 * @param
	 *        	$signalName
	 * @return string
	 */
	private static function _getSignalHash($object, $signalName)
	{
		return spl_object_hash($object) . $signalName;
	}

	/**
	 * Connecte une fonction de rappel au signal $name de l'objet $object.
	 *
	 * @param \stdClass $object Object à connecter
	 * @param string $eventName Nom du signal
	 * @param mixed $callback Fonction de rappel
	 *
	 * @return int L'identifiant unique de la connexion. Voir Signal::disconnect
	 */
	public static function connect($object, $eventName, $callback)
	{
		$id = -1;
		$hash = self::_getSignalHash($object, $eventName);

		if ( !isset(self::$eventList[$hash]) ) {
			self::$eventList[$hash] = [];
		}

		$id = uniqid();
		self::$eventList[$hash][$id] = new Event($eventName, $callback);
		return $id;
	}

	/**
	 * Déconnecte le signal portant l'identifiant $id de l'object $object.
	 *
	 * @param \stdClass $object
	 *        	Objet connecté au signal
	 * @param string $name
	 *        	Nom du signal
	 * @param
	 *        	int L'indentifiant du signal
	 */
	public static function disconnect($object, $name, $id)
	{
		if ( self::has($object, $name) ) {
			$hash = self::_getSignalHash($object, $name);
			unset(self::$eventList[$hash][$id]);
		}
		else {
			throw new \Exception(sprintf('UNKNOW_SIGNAL %s', $name));
		}
	}

	/**
	 * Emet le signal $name sur l'object $object.
	 *
	 * @param string $eventName
	 *        	Le nom du signal à envoyé
	 * @param mixed $sender
	 *        	Should be the current object instance
	 * @param \stdClass
	 *        	mixed ... Liste variable d'arguments passée à la fonction de rappel
	 * @return Event a event object where children property is populate whith sub triggered event
	 *
	 */
	public static function trigger($eventName, $sender = null /*, ...*/)
	{
		$hash = self::_getSignalHash($sender, $eventName);

		/* build new event, and associate sender */
		$returnEvent = new Event($eventName, null);
		$returnEvent->sender = $sender;

		/* foreach events valid for the eventname for this sender, prepare and call callbacks */
		if ( isset(self::$eventList[$hash]) ) {
			/* get list of callback to be triggered */
			$events = self::$eventList[$hash];
			$args = func_get_args();

			foreach( $events as $event ) {
				if ( !empty($event->callback) ) {
					$returnEvent->children[] = $event;
					$args[0] = $event;
					$event->sender = $sender;
					call_user_func_array($event->callback, $args);
				}
			}
		}
		return $returnEvent;
	}
}
