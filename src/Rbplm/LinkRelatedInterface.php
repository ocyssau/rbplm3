<?php
//%LICENCE_HEADER%

namespace Rbplm;


/**
 * @brief
 *
 */
interface LinkRelatedInterface
{
	
	/**
	 * Getter for links
	 * @return array
	 */
	public function getLinks();
	
	
	/**
	 * Current object has links?
	 * @return boolean
	 */
	public function hasLinks();
	
	/**
	 * Add link
	 * @param Any
	 * @param boolean If true create link in the added linkded object
	 * @return LinkRelatedInterface
	 */
	public function addLink(Any $object, $bidirectionnal = false);
	
}
