<?php

namespace Rbplm;

/**
 * 
 *
 */
trait LinkRelated
{
	
	/**
	 * Collection of all links with this object.
	 * Current object is call related, links are call link.
	 *
	 * @var array
	 */
	protected $links = array();
	
	/**
	 * @see \Rbplm\LinkRelatedInterface#getLinks()
	 * @return array
	 */
	public function getLinks()
	{
		if( !$this->links ){
			$this->links = array();
		}
		return $this->links;
	} //End of method
	
	/**
	 * @see \Rbplm\LinkRelatedInterface#hasLinks()
	 * @return boolean
	 */
	public function hasLinks()
	{
		if( count($this->links) > 0){
			return true;
		}
		else{
			return true;
		}
	} //End of method
	
	
	/**
	 * (non-PHPdoc)
	 * @see Rbplm.LinkRelatedInterface::addLink()
	 */
	public function addLink(LinkableInterface $link, $bidirectionnal=false)
	{
		$this->links[$link->getUid()] = $link;
		if($bidirectionnal){
			$link->addLink($this, false);
		}
		return $this;
	} //End of method
	
}


