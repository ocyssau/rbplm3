<?php
//%LICENCE_HEADER%

namespace Rbplm\Link;

use Rbplm\Model\Collection as BaseCollection;
use Rbplm\AnyObject;

/**
 * @brief A collection of \Rbplm\LinkRelatedInterface.
 * Link collection implements RecursiveIterator with \Rbplm\AnyObject::getLinks.
 *
 * Example and tests: Rbplm/Model/ComponentTest.php
 *
 *
 */
class Collection extends BaseCollection
{

	/**
	 *
	 * Implements RecursiveIterator
	 * @return Collection
	 */
	public function getChildren ()
	{
		$current = $this->current();
		if( $current != false ){
			if( $current instanceof AnyObject){
				return $current->getLinks();
			}
			if( $current instanceof BaseCollection ){
				return $current;
			}
			else{
				return $current->getChildren();
			}
		}
		else{
			return false;
		}
	}


	/**
	 *
	 * Implements RecursiveIterator
	 *
	 * @return boolean
	 */
	public function hasChildren ()
	{
		$current = $this->current();
		if( $current != false ){
			if( $current instanceof AnyObject){
				return $current->hasLinks();
			}
		    if( $current instanceof BaseCollection ){
				return (boolean) $current->count();
			}
			else{
				return $current->hasChildren();
			}
		}
		else{
			return false;
		}
	}

}
