<?php
/*
 * require_once 'PHPUnit/Framework.php';
 * require_once 'PHPUnit/Framework/MockObject/Matcher/InvokedRecorder.php';
 * require_once 'PHPUnit/Framework/MockObject/Matcher/AnyInvokedCount.php';
 * require_once 'PHPUnit/Framework/SelfDescribing.php';
 * require_once 'PHPUnit/Framework/Test.php';
 * require_once 'PHPUnit/Framework/Assert.php';
 * require_once 'PHPUnit/Framework/TestCase.php';
 */
namespace Rbplm\Test;

use Rbplm\Sys\Exception;

class Test
{

	protected $withDao = null;

	protected $toTest = array();

	protected $profiling = false;

	public $loop = 1;

	function __construct()
	{
		// parent::__construct();
		if ( !defined('CRLF') ) {
			if ( php_sapi_name() == 'cli' ) {
				define('CRLF', "\n");
				ini_set('xdebug.cli_color', 1);
			}
			else {
				define('CRLF', "<br />");
			}
		}

		assert_options(ASSERT_ACTIVE, 1);
		assert_options(ASSERT_WARNING, 1);
		assert_options(ASSERT_BAIL, 1);
		// assert_options( ASSERT_CALLBACK , 'myAssertCallback');
	}

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{}

	static function __autoload($class_name)
	{
		$class_file = str_replace('_', '/', $class_name) . '.php';
		$class_file = str_replace('Rbplm/', 'Rbplm/', $class_file);
		require_once ($class_file);
	}

	static function myAssertCallback($file, $line, $code)
	{
		$str = 'Assertion failed:';
		$str .= 'File ' . $file . CRLF;
		$str .= 'Line ' . $line . CRLF;
		$str .= 'Code ' . $code . CRLF;
		echo $str;
	}

	// To format header before test function execution
	static function header_line($function)
	{
		$header = '';
		$header .= str_repeat('-', 80) . CRLF;
		$header .= str_repeat('-', 80) . CRLF;
		$header .= strtoupper($function) . ": \n";
		$header .= str_repeat('-', 80) . CRLF;
		$header .= str_repeat('-', 80) . CRLF;
		return $header;
	}

	/**
	 *
	 * @param \Rbplm\Model\Collection $collection
	 * @param string $displayProperty
	 *        	name of property used to display node in tree.
	 * @return void
	 */
	protected static function _displayCollectionTree($collection, $displayProperty = 'name')
	{
		/* Walk along the groups tree relation with a iterator */
		$it = new \RecursiveIteratorIterator($collection, \RecursiveIteratorIterator::SELF_FIRST);
		$it->setMaxDepth(100);
		foreach( $it as $node ) {
			echo str_repeat('  ', $it->getDepth() + 1) . $node->$displayProperty . CRLF;
		}
	}

	/**
	 *
	 * @param string $basePath
	 * @return array
	 */
	function getTestsFiles($basePath)
	{
		$testFiles = array();
		$directoryIt = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($basePath));
		foreach( $directoryIt as $file ) {
			if ( substr($file->getFilename(), -8) == 'Test.php' ) {
				$testFiles[] = $file->getRealPath();
			}
		}
		return $testFiles;
	}

	public function withDao($bool = -1)
	{
		$this->withDao = (boolean)$bool;
	}

	public function withProfiling($bool = -1)
	{
		$this->profiling = (boolean)$bool;
	}

	public function add($class)
	{
		$this->toTest[] = $class;
	}

	public function runAllMethods()
	{
		$class = get_class($this);

		echo self::header_line('Start test on :' . $class);
		$withdao = $this->withDao;

		$methods = get_class_methods($class);

		$this->setUp();

		foreach( $methods as $method ) {
			if ( substr($method, 0, 5) == 'Test_' ) {
				if ( $withdao == false && substr($method, 0, 8) == 'Test_Dao' ) {
					continue;
				}
				echo self::header_line($class . '::' . $method);

				if ( $this->profiling == true ) {
					$this->startMem = (memory_get_usage() / 1024);
					$this->startTime = microtime(true);
					$i = 0;
					while( $i < $this->loop ) {
						$this->$method();
						$i++;
					}
					$this->endTime = microtime(true);
					$this->endMem = (memory_get_usage() / 1024);
					$this->executionTime = $this->endTime - $this->startTime;
					$this->executionTimeByUnit = $this->executionTime / $this->loop;
					echo "$this->loop loops en $this->executionTime S \n";
					echo "soit $this->executionTimeByUnit S par loop \n";
					echo "Memoire utilisée avant = $this->startMem Ko \n";
					echo "Memoire utilisée après = $this->endMem Ko \n";
					echo "Memoire consommée = " . ($this->endMem - $this->startMem) . "Ko \n";
					echo "Memoire pic = " . (memory_get_peak_usage() / 1024) . " Ko \n";
				}
				else {
					$this->$method();
				}
			}
		}
		$this->tearDown();
	}

	public function run()
	{
		foreach( $this->toTest as $class ) {
			$o = new $class();
			$o->profiling = $this->profiling;
			$o->loop = $this->loop;
			$o->withDao($this->withDao);
			$o->runAllMethods();
		}
	}

	public static function runOne($class = null, $function = null, $withDao = true, $continue = true, $profiling = false)
	{
		try {
			$o = new $class();
			$o->withDao($withDao);
			$o->withProfiling($profiling);

			if ( !$class && !$function ) {
				throw new exception('You must set the method and class to test');
			}

			if ( method_exists($o, $function) && !$class ) {
				$o->setUp();
				$o->$function();
			}
			else {
				echo self::header_line($class . '::' . $function);
				if ( $function ) {
					$o->setUp();
					$o->$function();
				}
				else {
					$o->runAllMethods();
				}
			}
		}
		catch( Exception $e ) {
			echo 'Error occuring' . CRLF;
			echo 'Message:' . $e->getMessage() . CRLF;
			echo 'File:' . $e->getFile() . CRLF;
			echo 'Line:' . $e->getLine() . CRLF;
			// var_dump( $e->getTrace() );
			var_dump($e->args);
			echo $e . CRLF;
			if ( $continue == false ) {
				die();
			}
		}
		echo self::header_line('ALL TESTS ARE OK');
		echo 'End.' . CRLF;
	}

	public function runAllClasses($libPath)
	{
		echo self::header_line(__FUNCTION__);

		$methods = get_class_methods(__CLASS__);

		foreach( $methods as $method ) {
			$t = explode('_', $method);
			if ( $t[0] == 'Test' ) {
				$this->$method();
			}
		}

		$nameSpace = basename($libPath);

		foreach( $this->getTestsFiles($libPath) as $path ) {
			$class = substr($path, strlen($libPath));
			$class = trim($class, '/');
			$class = trim($class, '\\');
			$class = trim($class, '.php');
			$class = str_replace('/', '\\', $class);
			if ( $class == '\\Test\Test' ) {
				continue;
			}
			$class = '\\' . $nameSpace . '\\' . $class;
			// var_dump($nameSpace, $libPath, $class, $path);
			$Test = new $class();
			$Test->runAllMethods();
		}
	}
}
