<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

use Rbplm\AnyPermanent;
use Rbplm\Ged\AccessCode;
use Rbplm\Geometric\Point;
use Rbplm\People\User;
use Rbplm\Sys\Date as DateTime;


/**
 * @brief This class implement the product_definition_formation and product_definition_formation_with_specified_source entities of STEP PDM SHEMA.
 *
 * The product_definition_formation entity represents the identification of a specific version of the base product identification.
 * A particular product_definition_formation is always related to exactly one product.
 *
 * Each instance of product is required to have an associated instance of product_definition_formation.
 * A single product entity may have more than one associated product_definition_formation.
 * The set of these product_definition_formation entities represents the revision history of the product.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#Product_definition
 *
 *
 * Product definition formation with specified source
 * The product_definition_formation_with_specified_source entity is a subtype of the entity product_definition_formation.
 * This entity adds the attribute make_or_buy to indicate the source of the version - either 'made' or 'bought'.
 * This entity is never used in the context of 'Document as Product', but it may be used in 'Part as Product' for compatibility
 * with AP203. However, this make-versus-buy distinction can be ambiguous in the context of exchange - going down a supply chain,
 * the sender is often 'bought' while the receiver is 'made', while in the other direction, the sender is 'made' and the receiver
 * 'bought'. Because of this ambiguity the use of this entity is not generally recommended.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#product_definition_formation_with_specified_source
 *
 *
 * Example and tests: Rbplm/Pdm/Test.php
 *
 * @verbatim
 * @endverbatim
 *
 * @verbatim
 * STEP EXAMPLE
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_description', #40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, );
 * @endverbatim
 */
class Version extends AnyPermanent
{

	const TYPE_PART = 'part';
	const TYPE_PRODUCT = 'product';
	const TYPE_DRAWING = 'drawing';
	const TYPE_ROOT = 'root';

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e972dd4c2c';

	/**
	 * Part version identification.
	 *
	 * @var string
	 */
	protected $version = 1;

	/**
	 *
	 * @var string
	 */
	protected $lifeStage = 'wip';

	/**
	 *
	 * @var string
	 */
	protected $number;

	/**
	 *
	 * @var string
	 */
	public $description;

	/**
	 * Document associate to this product.
	 *
	 * @var \Rbplm\Ged\Document\Version
	 */
	protected $document;

	/**
	 * Document id associate to this product.
	 *
	 * @var int
	 */
	public $documentId;

	/**
	 * Document uid associate to this product.
	 *
	 * @var string
	 */
	public $documentUid;

	/**
	 * Spacename of the referenced document
	 *
	 * @var string
	 */
	public $spacename;

	/**
	 * The base product for this version.
	 *
	 * @var \Rbplm\Pdm\Product
	 */
	protected $ofProduct;

	/**
	 * The base product id.
	 *
	 * @var int
	 */
	public $ofProductId;

	/**
	 * The base product uid.
	 *
	 * @var string
	 */
	public $ofProductUid;

	/**
	 * Unit grammes
	 *
	 * @var float
	 */
	protected $weight;

	/**
	 * Unit m3
	 *
	 * @var float
	 */
	protected $volume;

	/**
	 * Unit m2
	 *
	 * @var float
	 */
	protected $wetSurface;

	/**
	 * Unit gramme/m3
	 *
	 * @var float
	 */
	protected $density;

	/**
	 * Units m
	 *
	 * @var \SplFixedArray
	 */
	protected $gravityCenter;

	/**
	 * Unit m4
	 *
	 * @var \SplFixedArray
	 */
	protected $inertiaCenter;

	/**
	 *
	 * @var \Rbplm\Pdm\Product\PhysicalProperties
	 */
	protected $physicalProperties;

	/**
	 *
	 * @var array
	 */
	protected $materials = array();

	/**
	 *
	 * @var DateTime
	 */
	protected $locked = null;

	/**
	 *
	 * @var User
	 */
	protected $lockBy = null;

	/** @var integer */
	public $lockById = null;

	/** @var string */
	public $lockByUid = null;

	/**
	 * The make_or_buy attribute contains the source information.
	 *
	 * @var string
	 */
	public $makeOrBuy;

	/**
	 * One of value of constants TYPE_*
	 *
	 * @var string
	 */
	public $type;

	/**
	 * Constructor
	 *
	 * @see \Rbplm\AnyObject::__construct()
	 *
	 * @param array|string $properties
	 * @param \Rbplm\Org\Unit $parent
	 */
	public function __construct($properties = null, $parent = null)
	{
		parent::__construct($properties, $parent);
		if ( !$this->number ) {
			$this->number = $this->uid;
		}
	}

	/**
	 *
	 * @param string $name
	 * @param \Rbplm\Org\Unit $parent
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public static function init($name = null, $parent = null)
	{
		$obj = parent::init($name, $parent);
		return $obj;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbplm\Org\Unit $parent
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public static function initFromDocument($document, $parent = null)
	{
		$obj = parent::init($name, $parent);
		$obj->setNumber($document->getNumber());
		$obj->setName($document->getName());
		$obj->description = $document->description;
		$obj->setDocument($document);

		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);

		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['version']) && $properties['version']) ? $this->version = (int)$properties['version'] : null;
		(isset($properties['lifeStage'])) ? $this->lifeStage = $properties['lifeStage'] : null;
		(isset($properties['makeOrBuy'])) ? $this->makeOrBuy = $properties['makeOrBuy'] : null;
		(isset($properties['documentId'])) ? $this->documentId = $properties['documentId'] : null;
		(isset($properties['documentUid'])) ? $this->documentUid = $properties['documentUid'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		(isset($properties['ofProductId'])) ? $this->ofProductId = $properties['ofProductId'] : null;
		(isset($properties['ofProductUid'])) ? $this->ofProductUid = $properties['ofProductUid'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['weight'])) ? $this->weight = $properties['weight'] : null;
		(isset($properties['volume'])) ? $this->volume = $properties['volume'] : null;
		(isset($properties['wetSurface'])) ? $this->wetSurface = $properties['wetSurface'] : null;
		(isset($properties['density'])) ? $this->density = $properties['density'] : null;
		(isset($properties['gravityCenter'])) ? $this->gravityCenter = new Point($properties['gravityCenter']) : null;
		(isset($properties['inertiaCenter'])) ? $this->inertiaCenter = new Point($properties['inertiaCenter']) : null;
		(isset($properties['materials'])) ? $this->materials = $properties['materials'] : null;

		(isset($properties['lockById'])) ? $this->lockById = $properties['lockById'] : null;
		(isset($properties['lockByUid'])) ? $this->lockByUid = $properties['lockByUid'] : null;

		if ( isset($properties['locked']) ) {
			$date = $properties['locked'];
			if($date instanceof \DateTime){
				$this->locked = $date;
			}
			elseif(is_string($date)){
				$this->locked = new DateTime($properties['locked']);
			}
		}

		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 *
	 * @param string $number
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document $document
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function setDocument(\Rbplm\Ged\Document\Version $document)
	{
		$this->documentId = $document->getId();
		$this->documentUid = $document->getUid();
		$this->document = $document;
		if ( $document->spacename ) {
			$this->spacename = $document->spacename;
		}
		return $this;
	}

	/**
	 * Getter
	 *
	 * @param bool $asId
	 * @return \Rbplm\Ged\Document
	 */
	public function getDocument($asId = false)
	{
		if ( $asId ) {
			return $this->documentId;
		}
		else {
			return $this->document;
		}
	}

	/**
	 * Setter for the base product of this version
	 *
	 * @param \Rbplm\Pdm\Product $product
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function setOfProduct(\Rbplm\Pdm\Product $product)
	{
		$this->ofProductId = $product->getId();
		$this->ofProductUid = $product->getUid();
		$this->ofProduct = $product;
		return $this;
	}

	/**
	 * Getter of the base product of this version
	 *
	 * @param bool $asId
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Product
	 */
	public function getOfProduct($asId = false)
	{
		if ( $asId ) {
			return $this->ofProductId;
		}
		else {
			return $this->ofProduct;
		}
	}

	/**
	 *
	 * @return array
	 */
	public function getMaterials()
	{
		return $this->materials;
	}

	/**
	 *
	 * @param string $materialUid
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function addMaterial($materialUid)
	{
		$this->materials[$materialUid] = $materialUid;
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Pdm\Product\PhysicalProperties
	 */
	public function getPhysicalProperties()
	{
		if ( !isset($this->physicalProperties) ) {
			$this->physicalProperties = new PhysicalProperties();
			$this->physicalProperties->hydrate(array(
				'weight' => $this->weight,
				'volume' => $this->volume,
				'wetSurface' => $this->wetSurface,
				'density' => $this->density
			));
			(isset($this->inertiaCenter)) ? $this->physicalProperties->setInertiaCenter($this->inertiaCenter) : null;
			(isset($this->gravityCenter)) ? $this->physicalProperties->setGravityCenter($this->gravityCenter) : null;
		}
		return $this->physicalProperties;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\PhysicalProperties $physicalProperties
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function setPhysicalProperties($physicalProperties)
	{
		$properties = $physicalProperties->getArrayCopy();
		$this->hydrate(array(
			'weight' => $properties['weight'],
			'volume' => $properties['volume'],
			'wetSurface' => $properties['wetSurface'],
			'density' => $properties['density']
		));
		$this->gravityCenter = $properties['gravityCenter'];
		$this->inertiaCenter = $properties['inertiaCenter'];
		return $this;
	}

	/**
	 * Lock a Document with code.
	 *
	 * The value of the access code permit or deny action:
	 * @see AccessCode
	 *
	 * @param integer $code
	 *        	new access code. value of constants in AccessCode::*
	 * @param User $by
	 * @return \Rbplm\Ged\Document\Version
	 *
	 */
	function lock($code = AccessCode::CHECKOUT, User $by = null)
	{
		if ( $code == $this->accessCode ) {
			return $this;
		}

		$this->accessCode = (int)$code;
		$this->locked = new DateTime();

		if ( $by ) {
			$this->setlockBy($by);
		}

		return $this;
	}

	/**
	 * This method can be used to unlock a Document.
	 *
	 * @return \Rbplm\Ged\Document\Version
	 *
	 */
	public function unlock()
	{
		$this->accessCode = AccessCode::FREE;
		$this->lockBy = null;
		$this->lockById = null;
		$this->lockByUid = null;
		$this->locked = null;
		return $this;
	}

	/**
	 *
	 * @return DateTime|String
	 */
	public function getLocked($format = null)
	{
		if ( !$this->locked instanceof DateTime ) {
			$this->locked = new DateTime();
		}

		if ( $format ) {
			return $this->locked->format($format);
		}
		else {
			return $this->locked;
		}
	}

	/**
	 *
	 * @param boolean $asUid
	 * @return User | string
	 */
	public function getLockBy($asUid = false)
	{
		if ( $asUid == true ) {
			return $this->lockByUid;
		}
		return $this->lockBy;
	}

	/**
	 *
	 * @param User $lockBy
	 * @return Version
	 */
	public function setlockBy(User $lockBy)
	{
		$this->lockBy = $lockBy;
		$this->lockById = $this->lockBy->getId();
		$this->lockByUid = $this->lockBy->getUid();
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLifeStage()
	{
		return $this->lifeStage;
	}

	/**
	 *
	 * @param string $lifeStage
	 * @return Version
	 */
	public function setLifeStage($lifeStage)
	{
		$this->lifeStage = $lifeStage;
		return $this;
	}
} /*End of class */
