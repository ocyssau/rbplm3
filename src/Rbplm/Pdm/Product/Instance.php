<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

use Rbplm\Item;
use Rbplm\Mapped;
use Rbplm\Pdm;
use Rbplm\Dao\MappedInterface;

/**
 * @brief This class implement the product_definition and productusage entities of STEP PDM SHEMA.
 *
 * The product_definition entity represents the identification of a particular view
 * on a version of the product base identification relevant for the requirements of
 * particular life-cycle stages and application domains. View may be based on
 * application domain and/or life-cycle stage (e.g., design, manufacturing). A view collects
 * product data for a specific task. It is possible to have many product_definition views
 * for a part/version combination.
 *
 * The product_definition entity enables the establishment of many important relationships.
 * Product_definition is the central element to link product information to parts,
 * e.g., assembly structure, properties (including shape), and external descriptions of the product via documents.
 *
 * The use of product_definition entities is not strictly required by rules in the PDM Schema, but it is strongly recommended.
 * All product_definition_formation entities should always have at least one associated product_definition,
 * except in the case of supplied product identification and version history information.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:product_Master_Identification#Product_definition
 *
 * STEP EXAMPLE 1
 * #30 = PRODUCTcontext('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_description', #40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITIONcontext('part definition', #20, );
 *
 *
 * STEP EXAMPLE 2
 * /* primary application context and life cycle stage
 * #10 = APPLICATIONcontext('mechanical design');
 * #220 = PRODUCTcontext(, #10, );
 * #230 = PRODUCT_DEFINITIONcontext('part definition', #10, 'design');
 *
 * /* optional AP214 specific characterization for assembly definition
 * #340 = PRODUCT_DEFINITIONcontext_ROLE('part definition type', $);
 * #350 = PRODUCT_DEFINITIONcontext('assembly definition', #10, );
 * #520 = PRODUCT_DEFINITIONcontext_ASSOCIATION(#460, #350, #340);
 *
 * /* type discriminator for part as product
 * #100 = PRODUCT_RELATEDproduct_CATEGORY('part', $, (#380, #440));
 *
 * /* part master for component definition
 * #360 = PRODUCT_RELATEDproduct_CATEGORY('detail', $, (#380));
 * #380 = PRODUCT('g1', 'gasket', $, (#220)); /*base
 * #390 = PRODUCT_DEFINITION_FORMATION('A',,#380); /*version
 * #400 = PRODUCT_DEFINITION('gv1', 'design view on gasket', #390, #230); /*instance
 *
 * /* part master for assembly definition
 * #420 = PRODUCT_RELATEDproduct_CATEGORY('assembly', $, (#440));
 * #440 = PRODUCT('s1', 'sleeve assembly', $, (#220)); /*base
 * #450 = PRODUCT_DEFINITION_FORMATION('A',,#440); /*version
 * #460 = PRODUCT_DEFINITION('sv1', 'design view on sleeve assembly', #450, #230); /*instance
 *
 * /* single usage occurrence of component defn within assembly defn
 * #530 = NEXT_ASSEMBLYusage_OCCURRENCE('gu1', 'single instance usage', 'gasket usage 1', #460, #400, $); /*instance parent-child relation
 * @endverbatim
 *
 *
 * A instance is uniq for a product
 *
 * In Rbplm implementation Instance has relation with Pdm\Usage to define type and properties of the use.
 * Type may be PROMISSORY, or QUANTIFIED. Properties of usage are the quantity and unit of this quantity.
 *
 * Instance bring too the position of the instance in 3D space. Position is a simple \SplFixedArray with 12 entries.
 * The first 3 entries define the U axis.
 * 3 next the V axis.
 * 3 next the W axis.
 * and 3 last a translation vector from absolute origin.
 */
class Instance extends \Rbplm\Any implements MappedInterface
{
	use Item, Mapped;

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e971bb57c0';

	/**
	 * Type of instance
	 * ENUM product|part
	 *
	 * Must be set from productVersion
	 *
	 * @var string
	 */
	public $type = 'product';

	/**
	 * The related product version.
	 * STEP: The formation attribute links to the product_definition_formation of which this represents a view.
	 *
	 * @var \Rbplm\Pdm\Product\Version
	 */
	protected $ofProduct;

	/**
	 * Id of the base product version
	 *
	 * @var int
	 */
	public $ofProductId;

	/**
	 * Uid of the base product version
	 *
	 * @var string
	 */
	public $ofProductUid;

	/**
	 * Number of the base product version
	 * Used to reconciliate productInstance/productVersion
	 *
	 * @var string
	 */
	public $ofProductNumber;

	/**
	 *
	 * @var string
	 */
	protected $number;

	/**
	 *
	 * @var \Rbplm\Pdm\Usage
	 */
	protected $usage;

	/**
	 *
	 * @var \SplFixedArray
	 */
	protected $position;

	/**
	 * Identifiant for nomenclature.
	 *
	 * @var string
	 */
	public $nomenclature;

	/**
	 *
	 * @var string
	 */
	public $description;

	/**
	 * Quantity
	 *
	 * @var integer
	 */
	public $quantity;

	/**
	 * which view of the product the particular represents
	 *
	 * @var \Rbplm\Pdm\Product\Definition\Context
	 */
	protected $context;

	/**
	 *
	 * @var int
	 */
	public $contextId;

	/**
	 *
	 * @var string
	 */
	public $contextUid;

	/**
	 * Constructor
	 *
	 * @param array|string $prop
	 * @param Instance $parent
	 * @param \Rbplm\Pdm\Product\Version $relatedProduct
	 */
	public function __construct(array $properties = null, $parent = null, $relatedProduct = null)
	{
		parent::__construct($properties, $parent);

		if ( !$this->number ) {
			$this->number = $this->uid;
		}

		if ( $relatedProduct ) {
			$this->setProduct($relatedProduct);
		}
	}

	/**
	 *
	 * @param string $name
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj = self::mappedinit($obj, $name);
		$obj->setNumber($name);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function hydrate(array $properties)
	{
		$this->mappedHydrate($properties);
		$this->itemHydrate($properties);
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['ofProductId'])) ? $this->ofProductId = $properties['ofProductId'] : null;
		(isset($properties['ofProductUid'])) ? $this->ofProductUid = $properties['ofProductUid'] : null;
		(isset($properties['ofProductNumber'])) ? $this->ofProductNumber = $properties['ofProductNumber'] : null;
		(isset($properties['contextId'])) ? $this->contextId = $properties['contextId'] : null;
		(isset($properties['contextUid'])) ? $this->contextUid = $properties['contextUid'] : null;
		(isset($properties['nomenclature'])) ? $this->nomenclature = $properties['nomenclature'] : null;
		(isset($properties['quantity'])) ? $this->quantity = (int)$properties['quantity'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		if ( isset($properties['position']) ) {
			if(is_array($properties['position'])){
				$position = $this->getPosition();
				$position->setFromArray($properties['position']);
			}
			elseif($properties['position'] instanceof \Rbplm\Pdm\Product\Position){
				$this->setPosition($properties['position']);
			}
			elseif(is_string($properties['position'])){
				$position = $this->getPosition();
				$position->setFromJson($properties['position']);
			}
		}
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 *
	 * @param string $number
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 * Setter for the related product version
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function setOfProduct(Pdm\Product\Version $product)
	{
		$this->ofProductId = $product->getId();
		$this->ofProductUid = $product->getUid();
		$this->ofProductNumber = $product->getNumber();
		$this->type = $product->type;
		$this->ofProduct = $product;
		return $this;
	}

	/**
	 * Getter of the base product of this version
	 *
	 * @param int $asId
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function getOfProduct($asId = false)
	{
		if ( $asId ) {
			return $this->ofProductId;
		}
		return $this->ofProduct;
	}

	/**
	 * Getter
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Usage
	 */
	public function getUsage()
	{
		if ( !$this->usage ) {
			$this->usage = new Pdm\Usage();
		}
		return $this->usage;
	}

	/**
	 * Getter for position.
	 * Position is a array with 12 dimensions where 3 first defined coordinate of the U axis
	 * of triedre, 3 next the V axis, 3 next the W axis, and 3 last the translation vector :
	 * - index 0,1,2, U axis:
	 * - Ux
	 * - Uy
	 * - Uz
	 * - index 3,4,5, V axis:
	 * - Vx
	 * - Vy
	 * - Vz
	 * - index 6,7,8, W axis:
	 * - Wx
	 * - Wy
	 * - Wz
	 * - index 9,10,11, Translation vector:
	 * - Tx
	 * - Ty
	 * - Tz
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \SplFixedArray 12 dimensions fixed array
	 */
	public function getPosition()
	{
		if ( !$this->position ) {
			$this->position = new Position();
		}
		return $this->position;
	}

	/**
	 * @param Position $position
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function setPosition(Position $position)
	{
		$this->position = $position;
		return $this;
	}

	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Product\Definition\Context $context
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function setContext(Pdm\Product\Definition\Context $context)
	{
		$this->contextId = $context->getId();
		$this->contextUid = $context->getUid();
		$this->context = $context;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Product\Definition\Context
	 */
	public function getContext()
	{
		return $this->context;
	}

} /* End of class */
