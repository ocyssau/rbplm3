<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product\Definition;

/**
 * @brief This class implement the product_definition_context entity of STEP PDM SHEMA.
 *
 * The product_definition_context entity is a subtype of application_context_element
 * All STEP product_definitions must be founded in a product_definition_context to
 * identify the product definition type and life-cycle stage from which the data is viewed.
 *
 * There are two ways to implement the product_definition_context in STEP:
 * * association of a single primary context with a product_definition,
 * * assignment of more than one (additional) context to a product_definition.
 *
 * The value of the attribute product_definition.frame_of_reference is a product_definition_context
 * that represents the 'primary' defining context for a view. In general this defining
 * context is qualified both by type (product_definition_context.name) and by life-cycle
 * (product_definition_context.life_cycle_stage) information. The primary context
 * also has associated application domain information (see application_context).
 *
 * The product_definition_context_association entity allows for multiple additional
 * contexts to be associated with a single product_definition. There is always one
 * required 'primary' context that identifies the defining application domain and
 * life-cycle information. Additional product_definition_context entities identify
 * additional concurrent relevant views on the product_definition. These application_contexts
 * are related to the product definition by product_definition_context_association.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Context_Information#Product_definition_context
 *
 * @verbatim
 * STEP EXAMPLE:
 * #10 = APPLICATION_PROTOCOL_DEFINITION('version 1.2','pdm_schema',2000, #20);
 * #20 = APPLICATION_CONTEXT();
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_desc',#40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #100, 'design');
 * #100 = APPLICATION_CONTEXT('mechanical design');
 * #110 = PRODUCT_DEFINITION_CONTEXT_ASSOCIATION (#80, #130, #120);
 * #120 = PRODUCT_DEFINITION_CONTEXT_ROLE ('additional context', $);
 * #130 = PRODUCT_DEFINITION_CONTEXT('', #100, 'manufacturing');
 * @endverbatim
 *
 *
 *
 *
 * @verbatim
 *
 * @property string $application
 * @property string $lifeCycleStage
 * @property \Rbplm\Pdm\Context\Application $frameOfReference @endverbatim
 *
 *
 */
class Context extends \Rbplm\Org\Unit
{

	/**
	 * Name of the general application domain that defined the data.
	 *
	 * @var string
	 */
	protected $application;

	/**
	 *
	 * Identifies the life-cycle view.
	 *
	 * @var string
	 */
	protected $lifeCycleStage;

	/**
	 * attribute is a pointer to the associated application_context entity.
	 *
	 * @var \Rbplm\Pdm\Context\Application
	 */
	protected $frameOfReference;

	/**
	 *
	 * @param array $properties
	 *        	array(number=>string, marketSegmentType=>string, disciplineType=>string, frameOfReference=>string, application=>string, description=>string, owner=>string, closed=>string, name=>string )
	 * @return void
	 */
	public function __construct($properties, $parent = null)
	{
		parent::__construct($properties, $parent);
	}
} /* End of class */
