<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

use Rbplm\Any;

/**
 * @brief Physicals properties of a product.
 *
 * Example and tests: Rbplm/Pdm/Test.php
 */
class PhysicalProperties extends Any implements \Rbplm\LinkableInterface
{

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e972851e2e';

	/**
	 * Unit grammes
	 *
	 * @var float
	 */
	protected $weight;

	/**
	 * Unit m3
	 *
	 * @var float
	 */
	protected $volume;

	/**
	 * Unit m2
	 *
	 * @var float
	 */
	protected $wetSurface;

	/**
	 * Unit gramme/m3
	 *
	 * @var float
	 */
	protected $density;

	/**
	 * Units m
	 *
	 * @var \SplFixedArray
	 */
	protected $gravityCenter;

	/**
	 * Unit m4
	 *
	 * @var \SplFixedArray
	 */
	protected $inertiaCenter;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return PhysicalProperties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['weight'])) ? $this->weight = $properties['weight'] : null;
		(isset($properties['volume'])) ? $this->volume = $properties['volume'] : null;
		(isset($properties['wetSurface'])) ? $this->wetSurface = $properties['wetSurface'] : null;
		(isset($properties['density'])) ? $this->density = $properties['density'] : null;
		(isset($properties['gravityCenter'])) ? $this->setGravityCenter($properties['gravityCenter']) : null;
		(isset($properties['inertiaCenter'])) ? $this->setInertiaCenter($properties['inertiaCenter']) : null;
		return $this;
	}

	/**
	 * Setter
	 *
	 * @param float $value
	 * @return PhysicalProperties
	 */
	public function setWeight($value)
	{
		$this->weight = $value;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return float
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * Setter
	 *
	 * @param float $value
	 * @return PhysicalProperties
	 */
	public function setDensity($value)
	{
		$this->density = $value;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return float
	 */
	public function getDensity()
	{
		return $this->density;
	}

	/**
	 * Setter
	 *
	 * @param float $value
	 * @return PhysicalProperties
	 */
	public function setVolume($value)
	{
		$this->volume = $value;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return float
	 */
	public function getVolume()
	{
		return $this->volume;
	}

	/**
	 * Setter
	 *
	 * @param float $value
	 * @return PhysicalProperties
	 */
	public function setWetSurface($value)
	{
		$this->wetSurface = $value;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return float
	 */
	public function getWetSurface()
	{
		return $this->wetSurface;
	}

	/**
	 * Setter
	 *
	 * @param SplFixedArray(3) $point
	 * @return PhysicalProperties
	 */
	public function setGravityCenter(\SplFixedArray $point)
	{
		$this->gravityCenter = $point;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return SplFixedArray(3)
	 */
	public function getGravityCenter()
	{
		return $this->gravityCenter;
	}

	/**
	 * Setter
	 *
	 * @param SplFixedArray(3) $point
	 * @return PhysicalProperties
	 */
	public function setInertiaCenter(\SplFixedArray $point)
	{
		$this->inertiaCenter = $point;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return SplFixedArray(3)
	 */
	public function getInertiaCenter()
	{
		return $this->inertiaCenter;
	}
} /* End of class */
