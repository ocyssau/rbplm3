<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm;

/**
 * @brief anyobject is just one element in composite object chain.
 *
 * Implement design pattern COMPOSITE OBJECT.
 *
 * Example and tests: RbplmTest/Model/AnyTest.php
 */
class InstanceFactory
{

	/**
	 *
	 * @param string $name
	 * @return \Rbplm\Any
	 */
	public static function get($class = 'Instance', $name = null)
	{
		$obj = new $class();
		self::init($obj, $name);
		return $obj;
	}

	/**
	 *
	 * @param \Rbplm\Any $obj
	 * @param string $name
	 * @return \Rbplm\Any
	 */
	public static function init($obj, $name = null)
	{
		$obj->newUid();
		if ( !$name ) {
			$name = $obj->getUid();
		}
		$properties = array(
			'name' => $name
		);
		$obj->hydrate($properties);

		return $obj;
	}
} /* End of class */
