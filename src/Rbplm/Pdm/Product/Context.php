<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

/**
 * @brief This class implement the product_context entity of STEP PDM SHEMA.
 *
 * The product_context entity is a subtype of application_context_element.
 * All STEP products must be founded in a product_context to specify the point of view on the application.
 * The product_context entity identifies the engineering discipline's point of view from which the data is being presented.
 * This entity will establish the context perspective and source of requirements for product entities.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Context_Information#Product_context
 *
 * @verbatim
 * STEP EXAMPLE:
 * #10 = APPLICATION_PROTOCOL_DEFINITION('version 1.2','pdm_schema',2000, #20);
 * #20 = APPLICATION_CONTEXT();
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_desc',#40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #100, 'design');
 * #100 = APPLICATION_CONTEXT('mechanical design');
 * #110 = PRODUCT_DEFINITION_CONTEXT_ASSOCIATION (#80, #130, #120);
 * #120 = PRODUCT_DEFINITION_CONTEXT_ROLE ('additional context', $);
 * #130 = PRODUCT_DEFINITION_CONTEXT('', #100, 'manufacturing');
 * @endverbatim
 *
 *
 *
 * @verbatim
 *
 * @property string $application
 * @property string $disciplineType @endverbatim
 * @property string $application
 * @property string $disciplineType @endverbatim
 */
class Context extends \Rbplm\Org\Unit
{

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e9714da9aa';

	/**
	 * kind of consumer preferences associated with a product_concept.
	 *
	 * @var string
	 */
	protected $application;

	/**
	 *
	 * @var frameOfReference In STEP model, the frame_of_reference attribute is a pointer to the associated application_context entity.
	 *      In Rbplm, the element application_context is replace by a scalar value of property self::application.
	 */

	/**
	 * STEP: The discipline_type attribute contains a description of the discipline point of view for a product.
	 *
	 * @var string
	 */
	protected $disciplineType;

	/**
	 *
	 * @param array $properties
	 *        	array(number=>string, marketSegmentType=>string, disciplineType=>string, application=>string, description=>string, owner=>string, closed=>string, name=>string )
	 * @return void
	 */
	public function __construct($properties, $parent = null)
	{
		parent::__construct($properties, $parent);
	}
} /* End of class */
