<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

/**
 *
 *
 */
class File
{
	
	/**
	 * Encode file name into something than can not be reversed.
	 * It use to expose visualisation files to public front end without security exceptions.
	 * 
	 * @param string $name
	 * @param string $extension
	 * @param string $passphrase
	 * @return string
	 */
	public static function encodename($name, $extension, $passphrase)
	{
		return sha1($name.$extension.$passphrase).'.'.$extension;
	}
}
