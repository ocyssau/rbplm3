<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

use SplFixedArray;

/**
 * @brief Physicals properties of a product.
 *
 * Example and tests: Rbplm/Pdm/Test.php
 */
class Position extends SplFixedArray
{

	/**
	 *
	 * @var string
	 */
	public static $classId = '489g458759z4h';

	/**
	 */
	public function __construct()
	{
		parent::__construct(12);
	}

	/**
	 *
	 * @param array $properties
	 * @return \Rbplm\Pdm\Product\Position
	 */
	public function setFromArray(array $properties)
	{
		/*
		 * (isset($properties['ux'])) ? $position[0]=$properties['ux'] : null;
		 * (isset($properties['uy'])) ? $position[1]=$properties['uy'] : null;
		 * (isset($properties['uz'])) ? $position[2]=$properties['uz'] : null;
		 * (isset($properties['vx'])) ? $position[3]=$properties['vx'] : null;
		 * (isset($properties['vy'])) ? $position[4]=$properties['vy'] : null;
		 * (isset($properties['vz'])) ? $position[5]=$properties['vz'] : null;
		 * (isset($properties['wx'])) ? $position[6]=$properties['wx'] : null;
		 * (isset($properties['wy'])) ? $position[7]=$properties['wy'] : null;
		 * (isset($properties['wz'])) ? $position[8]=$properties['wz'] : null;
		 * (isset($properties['tx'])) ? $position[9]=$properties['tx'] : null;
		 * (isset($properties['ty'])) ? $position[10]=$properties['ty'] : null;
		 * (isset($properties['tz'])) ? $position[11]=$properties['tz'] : null;
		 */
		for ($i = 0; $i <= 11; $i++) {
			$this[$i] = array_shift($properties);
		}
		return $this;
	}

	/**
	 * @param string $json
	 * @return \Rbplm\Pdm\Product\Position
	 */
	public function setFromJson($json)
	{
		$array = json_decode($json, true);
		return $this->setFromArray($array);
	}

} /* End of class */
