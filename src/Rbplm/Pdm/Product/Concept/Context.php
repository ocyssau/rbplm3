<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product\Concept;

/**
 * @brief This class implement the product_concept_context entity of STEP PDM SHEMA.
 *
 * The product_concept_context entity is a subtype of
 * application_context_element. It defines the market context in which a
 * product_concept is defined and may include information characterizing the
 * potential customers of the products of the associated product_concept. The
 * application domain is identified by the associated application_context entity.
 *
 * It is recommended to instantiate this entity exactly once in the data set.
 * There is no standard mapping for the name and market_segment_type attributes of a product_concept_context.
 * Neither AP203 nor AP214 define requirements for a product_concept_context.
 * It is therefore recommended to use the default values for the entities
 * and attributes not supported by the preprocessor as described in the 'General information' section of this usage guide.
 *
 * See http://www.wikistep.org/index.php/Product_Concept_Identification
 *
 * @verbatim
 *
 * @property string $marketSegmentType @endverbatim
 *
 *
 *           @verbatim
 *           STEP EXAMPLE:
 *           #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
 *           #1=APPLICATION_CONTEXT();
 *           #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
 *           #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
 *           #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
 *           #7=CONFIGURATION_ITEM('PC-Conf3', 'High End Config US', 'High end PC system for US', #2, $);
 *           #8=APPLICATION_CONTEXT('');
 *           #9=PRODUCT_CONTEXT('', #8, '');
 *           #10=PRODUCT('PC-0023', 'PC system', $, (#9));
 *           #11=PRODUCT_DEFINITION_FORMATION('D', 'housing redesigned', #10);
 *           #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
 *           #13=PRODUCT_DEFINITION('pc_v1', 'design view on base PC', #11, #12);
 *           #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10));
 *           #15=CONFIGURATION_DESIGN(#5, #11);
 *           #16=CONFIGURATION_DESIGN(#6, #11);
 *           #17=CONFIGURATION_DESIGN(#7, #11); *
 *           @endverbatim
 */
class Context extends \Rbplm\Org\Unit
{

	/**
	 * kind of consumer preferences associated with a product_concept.
	 * @var string
	 */
	protected $application;

	/**
	 *
	 * @var frameOfReference In STEP model, the frame_of_reference attribute is a pointer to the associated application_context entity.
	 *      In Rbplm, the element application_context is replace by a scalar value of property self::application.
	 */

	/**
	 *
	 * @var string
	 */
	protected $marketSegmentType;
} /* End of class */

