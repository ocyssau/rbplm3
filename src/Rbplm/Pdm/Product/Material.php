<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Product;

/**
 * @brief Material definition.
 *
 * Example and tests: Rbplm/Pdm/Test.php
 *
 * @verbatim
 * @endverbatim
 */
class Material extends \Rbplm\AnyPermanent
{

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e9722bf42a';

	/**
	 *
	 * @var float
	 */
	protected $density;

	/**
	 *
	 * @var array
	 */
	protected $suppliers;

	/**
	 *
	 * @var array
	 */
	protected $technicalDocuments;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Material
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['density'])) ? $this->density = $properties['density'] : null;
		(isset($properties['suppliers'])) ? $this->suppliers = $properties['suppliers'] : null;
		(isset($properties['technicalDocuments'])) ? $this->technicalDocuments = $properties['technicalDocuments'] : null;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @param string $name
	 */
	public function __get($name)
	{
		return $this->$name;
	}

	/**
	 * Setter
	 *
	 * @param float $value
	 * @return Material
	 */
	public function setDensity($value)
	{
		$this->density = $value;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return float
	 */
	public function getDensity()
	{
		return $this->density;
	}

	/**
	 *
	 * @return array
	 */
	public function getSuppliers()
	{
		if ( !$this->suppliers ) {
			$this->suppliers = array();
		}
		return $this->suppliers;
	}

	/**
	 *
	 * @return Material
	 */
	public function addSupplier($supplier)
	{
		$this->suppliers[] = $supplier;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getTechnicalDocuments()
	{
		if ( !$this->technicalDocuments ) {
			$this->technicalDocuments = array();
		}
		return $this->technicalDocuments;
	}

	/**
	 * param \Rbplm\Ged\Document\Version $td
	 *
	 * @return Material
	 */
	public function addTechnicalDocument($td)
	{
		$this->technicalDocuments[] = array(
			'id' => $td->getId(),
			'uid' => $td->getUid()
		);
		return $this;
	}
} /* End of class */
