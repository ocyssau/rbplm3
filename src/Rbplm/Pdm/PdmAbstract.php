<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm;

use Rbplm\AnyPermanent;

/**
 * @brief Provide generic methods and properties for all classes of pdm.
 *
 * @verbatim
 * @endverbatim
 */
abstract class PdmAbstract extends AnyPermanent
{

	/**
	 *
	 * @var string
	 */
	protected $number;

	/**
	 *
	 * @var string
	 */
	public $description;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		return $this;
	}
}/* End of class */
