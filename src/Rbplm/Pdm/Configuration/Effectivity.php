<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Configuration;

/**
 * @brief This class implement the configuration_effectivity, product_definition_effectivity entity of STEP PDM SHEMA.
 *
 * The entity configuration_effectivity is a subtype of
 * product_definition_effectivity that contains information about the planned
 * usage of components in a product configuration. It defines the valid use of a
 * particular product_definition occurrence at a certain position in the assembly
 * structure for a specified product configuration.
 *
 * The configuration_effectivity entity allows the association of the appropriate
 * versions of constituent parts intended to be used at the defined position in
 * the assembly structure to build a configuration_item. In the context of the
 * PDM Schema, the configuration_effectivity always relates to a particular unit
 * of manufacture of the end items of the given configuration_item. The three
 * ways to instantiate configuration_effectivity are:
 *
 * * serial_numbered_effectivity, where the configuration_effectivity defines the
 * valid use of constituent part occurrences for a serial number range of
 * instances of a product configuration to be manufactured;
 * * dated_effectivity, where the configuration_effectivity defines the valid use
 * of constituent part occurrences for a time range based on dates when instances
 * of the product configuration are manufactured;
 * * lot_effectivity, where the configuration_effectivity defines the valid use
 * of constituent part occurrences for a given lot of instances of a product
 * configuration to be manufactured.
 *
 *
 * The particular usage of the constituent part the effectivity applies to is
 * defined as product_definition_usage referenced by the usage attribute. The
 * product_definition_usage should be instantiated as an assembly_component_usage
 * or one of its subtypes (see Product concept). The attribute
 * related_product_definition specifies the definition of a constituent part, and
 * the attribute relating_ product_definition specifies the definition of the
 * assembly (or sub-assembly) in which the constituent may be used. This assembly
 * definition is part of the design that implements the configuration_item for
 * which the configuration_effectivity applies, i.e., it either is a part
 * definition of the product_definition_formation defining the ('complete')
 * design for the configuration_item, or it is an occurrence in the product
 * structure of that complete design, i.e., is related to that in a tree of
 * assembly_component_usage instances.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Configuration_Composition_Management#Configuration_effectivity_2
 *
 *
 *
 * The product_definition_effectivity entity is a subtype of effectivity.
 * It applies to a particular occurrence of a component or sub-assembly part as used within an assembly.
 * It is the identification of a valid use of a particular product_definition in the context of its participation in a given product_definition_usage.
 * The effectivity applies to a product_definition that is referenced as the related_product_definition by the product_definition_relationship
 * associated to the product_definition_effectivity through the usage attribute.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Configuration_Composition_Management#Product_definition_effectivity
 *
 *
 *
 * Example and tests: Rbplm/Pdm/Test.php
 */
class Effectivity extends \Rbplm\Pdm\PdmAbstract
{

	/**
	 * Value is one of constant \Rbplm\Pdm\Effectivity::EFFECTIVITY_TYPE_*
	 *
	 * @var integer
	 */
	protected $type;

	/**
	 * reference to the associated product_definition_relationship.
	 *
	 * @var \Rbplm\Pdm\Product\Instance
	 */
	protected $usage;

	public $usageId;

	public $usageUid;

	/**
	 * The configuration attribute identifies the configuration_design that
	 * specifies the associated configuration_item for which the
	 * configuration_effectivity applies, and the corresponding
	 * product_definition_formation which defines the design that implements the
	 * configuration_item and thus specifies the upper most node ('entry point') in
	 * its product structure.
	 *
	 * Reference to the configuration_design specifying the configuration_item
	 * identifies the context in which the effectivity applies.
	 *
	 * @var \Rbplm\Pdm\Configuration\Design
	 */
	protected $design;

	public $designId;

	public $designUid;

	/**
	 * Reference to the \Rbplm\Pdm\Effectivity subtype dated, lot or snumbered.
	 *
	 * @var \Rbplm\Pdm\Effectivity
	 */
	protected $effectivity;

	public $effectivityId;

	public $effectivityUid;

	/**
	 * Constructor
	 *
	 * @param array $properties
	 * @param \Rbplm\AnyObject $parent
	 * @param \Rbplm\Pdm\Effectivity $effectivity
	 * @param \Rbplm\Pdm\Product\Instance $usage
	 */
	public function __construct($properties = null, $parent = null, $effectivity = null, $usage = null)
	{
		parent::__construct($properties, $parent);

		if ( $effectivity ) {
			$this->setEffectivity($effectivity);
		}

		if ( $usage ) {
			$this->setUsage($usage);
		}
	}

	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Configuration\Design $design
	 */
	public function setDesign(\Rbplm\Pdm\Configuration\Design $design)
	{
		$this->design = $design;
		$this->designId = $design->getId();
		$this->designUid = $design->getUid();
		return $this;
	}


	/**
	 *
	 * Enter description here ...
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Configuration\Design
	 */
	public function getDesign()
	{
		return $this->design;
	}


	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Effectivity $effectivity
	 */
	public function setEffectivity(\Rbplm\Pdm\Effectivity $effectivity)
	{
		$this->effectivity = $effectivity;
		$this->type = $effectivity->getType();
		return $this;
	}


	/**
	 * Getter
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return_Effectivity
	 */
	public function getEffectivity()
	{
		return $this->effectivity;
	}


	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Product\Instance $usage
	 */
	public function setUsage(\Rbplm\Pdm\Product\Instance $usage)
	{
		$this->usage = $usage;
		return $this;
	}


	/**
	 * Getter
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function getUsage()
	{
		return $this->usage;
	}


	/**
	 *
	 * @see library/Rbplm/Pdm/\Rbplm\Pdm\Effectivity#getType()
	 */
	public function getType()
	{
		return $this->type;
	}
}/* End of class */
