<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Configuration;

/**
 * @brief This class implement the product_concept_context entity of STEP PDM SHEMA.
 *
 * The product_concept_context entity is a subtype of
 * application_context_element. It defines the market context in which a
 * product_concept is defined and may include information characterizing the
 * potential customers of the products of the associated product_concept. The
 * application domain is identified by the associated application_context entity.
 *
 *
 * See http://www.wikistep.org/index.php/Product_Concept_Identification
 *
 * @verbatim
 * 
 * @property string $marketSegmentType @endverbatim
 *          
 *          
 *           @verbatim
 *           STEP EXAMPLE:
 *           #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
 *           #1=APPLICATION_CONTEXT();
 *           #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
 *           #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
 *           #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
 *           #7=CONFIGURATION_ITEM('PC-Conf3', 'High End Config US', 'High end PC system for US', #2, $);
 *           #8=APPLICATION_CONTEXT('');
 *           #9=PRODUCT_CONTEXT('', #8, '');
 *           #10=PRODUCT('PC-0023', 'PC system', $, (#9));
 *           #11=PRODUCT_DEFINITION_FORMATION('D', 'housing redesigned', #10);
 *           #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
 *           #13=PRODUCT_DEFINITION('pc_v1', 'design view on base PC', #11, #12);
 *           #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10));
 *           #15=CONFIGURATION_DESIGN(#5, #11);
 *           #16=CONFIGURATION_DESIGN(#6, #11);
 *           #17=CONFIGURATION_DESIGN(#7, #11); *
 *           @endverbatim
 *          
 */
class Context extends \Rbplm\Pdm\Context\Application
{

	/**
	 * kind of consumer preferences associated with a product_concept.
	 * 
	 * @var string
	 */
	protected $marketSegmentType;

	/**
	 *
	 * @param array $properties
	 *        	array(marketSegmentType=>string)
	 * @return void
	 */
	public function __construct($properties)
	{
		foreach( $properties as $name => $value ) {
			$this->$name = $value;
		}
		$this->uid = $this->newUid();
	}
}/* End of class */

