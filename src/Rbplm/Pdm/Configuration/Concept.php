<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Configuration;

/**
 * @brief This class implement the product_concept, product_concept_context and application_context entity of STEP PDM SHEMA.
 *
 * ------------------------------------------------------------------------------------------------------
 *
 * A product_concept describes a class of similar products that an organization
 * provides to its customers. It represents the idea of a product as identified
 * by potential or actual customer requirements. Therefore, a product_concept may
 * exist before the parts have been defined that implement it.
 *
 * Depending on the kind of industry and products, a product_concept might be
 * offered to the customers in one or many different configurations. If exactly
 * one configuration is defined, the product_concept corresponds to a particular
 * part design. If the product concept is offered in different configurations,
 * each of these configurations is a member of the class of products defined by
 * this product_concept.
 *
 * See http://www.wikistep.org/index.php/Product_Concept_Identification
 *
 * ------------------------------------------------------------------------------------------------------
 *
 * The product_concept_context entity is a subtype of
 * application_context_element. It defines the market context in which a
 * product_concept is defined and may include information characterizing the
 * potential customers of the products of the associated product_concept. The
 * application domain is identified by the associated application_context entity.
 *
 *
 * See http://www.wikistep.org/index.php/Product_Concept_Identification
 *
 * @verbatim
 * STEP EXAMPLE:
 * #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
 * #1=APPLICATION_CONTEXT();
 * #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
 * #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
 * #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
 * #7=CONFIGURATION_ITEM('PC-Conf3', 'High End Config US', 'High end PC system for US', #2, $);
 * #8=APPLICATION_CONTEXT('');
 * #9=PRODUCT_CONTEXT('', #8, '');
 * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
 * #11=PRODUCT_DEFINITION_FORMATION('D', 'housing redesigned', #10);
 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
 * #13=PRODUCT_DEFINITION('pc_v1', 'design view on base PC', #11, #12);
 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10));
 * #15=CONFIGURATION_DESIGN(#5, #11);
 * #16=CONFIGURATION_DESIGN(#6, #11);
 * #17=CONFIGURATION_DESIGN(#7, #11); *
 * @endverbatim
 *
 * ------------------------------------------------------------------------------------------------------
 *
 * The application_context entity identifies the application domain that defined
 * the data. The application_context entity may have an identifier associated with it
 * through the entity id_attribute and its attribute_value attribute. The application_context
 * entity may have a description associated with it through the entity description_attribute
 * via the attribute attribute_value. It is not recommended to instantiate these optional
 * values.
 *
 * In the case of application protocol identification the application domain is optional
 * which provides a basis for the interpretation of all information represented in
 * the product data exchange.
 *
 * In the case of application context information, there exists a 'primary' application
 * context for each product_definition. This is the value of the attribute product_definition
 * it is the frame_of_reference for the 'primary' product_definintion_context
 * (see product_definition_context). This 'primary' application context represents the
 * defining application domain for each product_definition. Additional application
 * domains may be associated with a product_definition through additional product_definition_contexts
 * via the entity product_definition_context_association.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Context_Information#Application_context
 *
 * @verbatim
 * STEP EXAMPLE:
 * #10 = APPLICATION_PROTOCOL_DEFINITION('version 1.2','pdm_schema',2000, #20);
 * #20 = APPLICATION_CONTEXT('mechanical design');
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_desc',#40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, );
 * @endverbatim
 *
 *
 * @verbatim
 *
 * @property string $contextId
 * @property \Rbplm\Pdm\Configuration\Context $context
 * @property string $marketSegmentType
 * @property string $application @endverbatim
 *
 */
class Concept extends \Rbplm\Any
{

	/**
	 * @var string
	 */
	protected $id;

	/**
	 * @var string $_name
	 */
	// protected $_name;

	/**
	 *
	 * @var string
	 */
	protected $description;

	/**
	 * kind of consumer preferences associated with a product_concept.
	 * @var string
	 */
	protected $marketSegmentType;

	/**
	 * The application attribute identifies the name of the general application domain that defined the data.
	 *
	 * @var string
	 */
	protected $application;

	/**
	 * the marketContext STEP property
	 * The market for the products belonging to a product_concept is further
	 * described in the market_context attribute.
	 * All product_concepts must have a
	 * related product_concept_context, which specifies their market context.
	 *
	 * @var \Rbplm\Pdm\Configuration\Context
	 */
	protected $context;

	/**
	 *
	 * @var int
	 */
	public $contextId;

	/**
	 * @param array $properties
	 *        	array(id=>string, name=>string, description=>string, contextId=>string, application=>string, marketSegmentType=>string )
	 */
	public function __construct($properties)
	{
		$this->name = $properties['name'];
		$this->id = $properties['id'];
		$this->description = $properties['description'];
		$this->contextId = $properties['contextId'];
		$this->application = $properties['application'];
		$this->marketSegmentType = $properties['marketSegmentType'];

		$this->uid = $this->newUid();
	}

	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Configuration\Context $context
	 * @return Concept
	 */
	public function setContext(\Rbplm\Pdm\Configuration\Context $context)
	{
		$this->context = $context;
		$this->contextId = $context->getId();
		$this->contextUid = $context->getUid();
		return $this;
	}

	/**
	 * Getter
	 * @param boolean $asId
	 * @return \Rbplm\Pdm\Configuration\Context $context
	 */
	public function getContext($asId = false)
	{
		if ( $asId ) {
			return $this->contextId;
		}
		return $this->context;
	}
} /* End of class */
