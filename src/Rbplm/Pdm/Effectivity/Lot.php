<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Effectivity;

/**
 * @brief Effectivity by lot.
 *
 *
 * Example and tests: Rbplm/Pdm/Test.php
 */
class Lot extends \Rbplm\Pdm\Effectivity
{

	/**
	 *
	 * @var string uuid
	 */
	protected $id;

	/**
	 *
	 * @var integer
	 */
	protected $size;

	/**
	 *
	 * @param array $lotdefinition
	 *        	array(lotId=>string, lotSize=>integer)
	 * @return void
	 */
	function __construct(array $lotdefinition = array())
	{
		$this->id = $lotdefinition['lotId'];
		$this->size = $lotdefinition['lotSize'];
	}

	/**
	 *
	 * @see library/Rbplm/Pdm/\Rbplm\Pdm\Effectivity#getType()
	 */
	public function getType()
	{
		return self::EFFECTIVITY_TYPE_LOT;
	}

	/**
	 *
	 * @return integer
	 */
	public function getSize()
	{
		return $this->size;
	}

	/**
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 *
	 * @param integer $size
	 * @return Lot
	 */
	public function setSize($size)
	{
		$this->size = $size;
		return $this;
	}

	/**
	 *
	 * @param string $id
	 * @return Lot
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @param integer $snum
	 * @param string $lotId
	 * @return boolean
	 *
	 */
	public function check($snum, $lotId)
	{
		if ( $lotId != $this->id ) {
			return false;
		}
		if ( $snum ) {
			if ( $snum > $this->size ) {
				return false;
			}
		}

		return true;
	}
} /* End of class */
