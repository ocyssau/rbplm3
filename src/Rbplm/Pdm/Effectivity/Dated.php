<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Effectivity;

/**
 * @brief Effectivity by date.
 *
 * Example and tests: Rbplm/Pdm/Test.php
 */
class Dated extends \Rbplm\Pdm\Effectivity
{

	/**
	 *
	 * @var \DateTime
	 */
	protected $startDate;

	/**
	 *
	 * @var \DateTime
	 */
	protected $endDate;

	/**
	 *
	 * @param array $bornes
	 *        	array(start=>\DateTime, end=>\DateTime)
	 * @return void
	 */
	function __construct(array $bornes = array())
	{
		$this->startDate = $bornes['start'];
		$this->endDate = $bornes['end'];
	}

	/**
	 *
	 * @see library/Rbplm/Pdm/\Rbplm\Pdm\Effectivity#getType()
	 * @return string
	 */
	public function getType()
	{
		return self::EFFECTIVITY_TYPE_DATED;
	}

	/**
	 *
	 * @return \DateTime
	 */
	public function getStart()
	{
		return $this->startDate;
	}

	/**
	 *
	 * @return \DateTime
	 */
	public function getEnd()
	{
		return $this->endDate;
	}

	/**
	 *
	 * @param \DateTime $date
	 * @return Dated
	 */
	public function setStart(\DateTime $date)
	{
		$this->startDate = $date;
		return $this;
	}

	/**
	 *
	 * @param \DateTime $date
	 * @return Dated
	 */
	public function setEnd(\DateTime $date)
	{
		$this->endDate = $date;
		return $this;
	}

	/**
	 *
	 * Check effectivity of date.
	 * Note that date is compare with eqality on start and end date.
	 *
	 * @todo see internationalization of date. But with ts it must be correct.
	 *
	 * @param \DateTime $now
	 * @return boolean
	 */
	public function check(\DateTime $now)
	{
		if ( $this->startDate ) {
			if ( $now < $this->startDate ) {
				return false;
			}
		}

		if ( $this->endDate ) {
			if ( $now > $this->endDate ) {
				return false;
			}
		}

		return true;
	}
} /* End of class */
