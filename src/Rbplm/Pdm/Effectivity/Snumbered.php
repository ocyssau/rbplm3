<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Effectivity;

/**
 * @brief Effectivity by serial number start and end
 *
 * Example and tests: Rbplm/Pdm/Test.php
 */
class Snumbered extends \Rbplm\Pdm\Effectivity
{

	/**
	 *
	 * @var string
	 */
	protected $startId;

	/**
	 *
	 * @var string
	 */
	protected $endId;

	/**
	 *
	 * @param array $bornes
	 *        	array(start=>integer, end=>integer)
	 * @return void
	 */
	function __construct(array $bornes = array())
	{
		$this->startId = $bornes['start'];
		$this->endId = $bornes['end'];
	}

	/**
	 *
	 * @see library/Rbplm/Pdm/\Rbplm\Pdm\Effectivity#getType()
	 */
	public function getType()
	{
		return self::EFFECTIVITY_TYPE_NUMBERED;
	}

	/**
	 * Get start serial number
	 *
	 * @return string
	 */
	public function getStart()
	{
		return $this->startId;
	}

	/**
	 * Get last serial number
	 *
	 * @return string
	 */
	public function getEnd()
	{
		return $this->endId;
	}

	/**
	 * Set start serial number
	 *
	 * @param string $id
	 * @return Snumbered
	 */
	public function setStart($id)
	{
		$this->startId = $id;
		return $this;
	}

	/**
	 * Set last serial number
	 *
	 * @param string $id
	 * @return Snumbered
	 */
	public function setEnd($id)
	{
		$this->endId = $id;
		return $this;
	}

	/**
	 * Check validity of a serial number.
	 *
	 * @param integer $snum
	 * @return boolean
	 *
	 */
	public function check($snum)
	{
		if ( $this->startId ) {
			if ( $snum < $this->startId ) {
				return false;
			}
		}
		if ( $this->endId ) {
			if ( $snum > $this->endId ) {
				return false;
			}
		}

		return true;
	}
} /* End of class */
