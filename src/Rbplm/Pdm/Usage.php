<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm;

/**
 * @brief Usage define quantity and type of use to apply to a product.
 */
class Usage
{

	/**
	 *
	 * @var string
	 */
	public static $classId = '569e96e35bab7';

	/**
	 * Promissory usage.
	 * A promissory_usage_occurrence the intention to use the constituent in an assembly.
	 * It may also be used when product structure is not completely defined.
	 */
	const TYPE_PROMISSORY = 1;

	/**
	 * Quantified usage.
	 */
	const TYPE_QUANTIFIED = 2;

	/**
	 */
	const UNIT_SCALAR = 'SCALAR';

	/**
	 * One of the constante self::TYPE_*
	 * 
	 * @var integer
	 */
	protected $type;

	/**
	 *
	 * @var float
	 */
	protected $quantity;

	/**
	 *
	 * @var string
	 */
	protected $unit;

	/**
	 *
	 * @param
	 *        	integer One of constant self::TYPE_*
	 */
	public function __construct($type = self::TYPE_PROMISSORY)
	{
		$this->type = $type;
	}

	/**
	 * Set quantity and switch type to self::TYPE_QUANTIFIED
	 * 
	 * @param float $quantity        	
	 * @param string $unit        	
	 */
	public function setQuantity($quantity, $unit = self::UNIT_SCALAR)
	{
		$this->type = self::TYPE_QUANTIFIED;
		$this->quantity = $quantity;
		$this->unit = $unit;
	}

	/**
	 *
	 * @return float
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 *
	 * @return string
	 */
	public function getUnit()
	{
		return $this->unit;
	}

	/**
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}
}

