<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Context;

/*
 * TABLE DEFINITION :
 * DROP TABLE `pdm_context_role_seq`;
 * DROP TABLE `pdm_context_role`;
 *
 * CREATE TABLE `pdm_context_role` (
 * `role_id` int(11) NOT NULL,
 * `name` varchar(128) NULL,
 * `description` varchar(512) NULL,
 * PRIMARY KEY (`role_id`),
 * KEY `INDEX_pdm_context_role_1` (`name`),
 * KEY `INDEX_pdm_context_role_2` (`description`)
 * );
 */

/**
 * This class implement the product_definition_context_role entity of STEP PDM SHEMA.
 *
 * The product_definition_context_role entity provides a role string that is
 * to a product_definition_context_association entity.
 */
class Role extends \Rbplm\Pdm\PdmAbstract
{
}/* End of class */

