<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Context;

/**
 * Context information provides a scope and necessary circumstance for product identification information.
 * It consists of two separate and related areas:
 * - Application Protocol Identification,
 * - Application Context Information.
 *
 * Application Protocol identification is provided by the entity application_protocol_definition. It characterizes the STEP Application Protocol (AP) or similar working draft STEP specification that includes or uses the PDM Schema and provides the scope or general context for the exchange data set. The general context identifies the usage of the information within the scope of the PDM Schema, and may define the application domain which provides a basis for the interpretation of all information represented in the product data exchange. It is generally recommended that application protocol identification and general context information be handled structurally the same for 'Part as Product' and 'Document as Product' as for 'Product Concept'. A single instance of the entity application_context is typically referenced by all product_context and product_concept_context entities.
 * This application_context instance is referenced by a single instance of the entity application_protocol_definition.
 *
 * Application context information identifies the usage of the information within the scope of the PDM Schema.
 * Application context information is divided into application domain information and product definition context information:
 *
 * - The application domain is identified by the application_context entity. Each application domain is represented by an instance of application_context that is referenced by product view definitions (product view and type definition) which belong to the application domain;
 * - The product_definition_context carries information distinguishing the life-cycle stage (e.g., design, manufacturing) relevant to a particular product view definition as well as indication of the type of the definition - e.g., part definition, functional definition.
 * Identification of a valid scope and context of interpretation for the identifier of a product is also required in the PDM Schema. This involves the assignment of an organization to the product, in the role 'id owner' to identify the scope of uniqueness and validity of the product id. *
 */

/*
 * CREATE TABLE `pdm_context_application` (
 * `id` int(11) NOT NULL,
 * `application` varchar(128) NULL,
 * PRIMARY KEY (`id`),
 * KEY `INDEX_pdm_context_application_1` (`application`)
 * );
 */

/**
 * @brief This class implement the application_context entity of STEP PDM SHEMA.
 *
 *
 * The application_context entity identifies the application domain that defined
 * the data. The application_context entity may have an identifier associated with it
 * through the entity id_attribute and its attribute_value attribute. The application_context
 * entity may have a description associated with it through the entity description_attribute
 * via the attribute attribute_value. It is not recommended to instantiate these optional
 * values.
 *
 * In the case of application protocol identification the application domain is optional
 * which provides a basis for the interpretation of all information represented in
 * the product data exchange.
 *
 * In the case of application context information, there exists a 'primary' application
 * context for each product_definition. This is the value of the attribute product_definition
 * it is the frame_of_reference for the 'primary' product_definintion_context
 * (see product_definition_context). This 'primary' application context represents the
 * defining application domain for each product_definition. Additional application
 * domains may be associated with a product_definition through additional product_definition_contexts
 * via the entity product_definition_context_association.
 *
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Context_Information#Application_context
 *
 * @verbatim
 * 
 * @property string $application @endverbatim
 *          
 *           @verbatim
 *           STEP EXAMPLE:
 *           #10 = APPLICATION_PROTOCOL_DEFINITION('version 1.2','pdm_schema',2000, #20);
 *           #20 = APPLICATION_CONTEXT('mechanical design');
 *           #30 = PRODUCT_CONTEXT('', #20, '');
 *           #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 *           #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_desc',#40);
 *           #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 *           #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, );
 *           @endverbatim
 *          
 *          
 */
class Application
{

	/**
	 * The application attribute identifies the name of the general application domain that defined the data.
	 * 
	 * @var string
	 */
	protected $application;

	public function __construct($applicationLabel)
	{
		$this->application = $applicationLabel;
	}
}/* End of class */

