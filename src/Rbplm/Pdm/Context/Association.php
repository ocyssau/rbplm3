<?php
// %LICENCE_HEADER%
namespace Rbplm\Pdm\Context;

/*
 * TABLE DEFINITION :
 * DROP TABLE `pdm_context_association_seq`;
 * DROP TABLE `pdm_context_association`;
 * CREATE TABLE `pdm_context_association` (
 * `link_id` int(11) NOT NULL,
 * `pd_id` int(11) NOT NULL,
 * `act_id` int(11) NOT NULL,
 * `role_id` int(11) NOT NULL default 0,
 * PRIMARY KEY (`link_id`),
 * UNIQUE KEY `UNIQ_pdm_context_association_1` (`pd_id`,`act_id`),
 * KEY `INDEX_pdm_context_association_1` (`pd_id`),
 * KEY `INDEX_pdm_context_association_2` (`act_id`),
 * KEY `INDEX_pdm_context_association_3` (`role_id`)
 * );
 */

/**
 * This class implement the product_definition_context_association entity of STEP PDM SHEMA.
 *
 * The product_definition_context_association entity allows for multiple additional
 * contexts to be associated with a single product_definition.
 *
 * With 'Part as Product', there is always one required 'primary' context that identifies
 * the defining application domain and life-cycle information from which the data
 * is viewed. This 'primary' context is the value of the attribute
 * product_definition.frame_of_reference.
 * Additional product_definition_context entities identify additional concurrent relevant
 * views on the product_definition. These additional contexts are related to the product
 * definition by the entity product_definition_context_association.
 *
 *
 * @property string $pdId
 * @property string $actId
 * @property string $roleId
 * @property \Rbplm\Pdm\Product\Definition $definition
 * @property Rbplm_Pdm_Context_Productdefinition $context
 * @property \Rbplm\Pdm\Context\Role $role
 *
 *
 */
class Association extends \Rbplm\Pdm\PdmAbstract
{

	/**
	 * Id of Rb_Pdm_Product_Definition.
	 *
	 * @var string uuid
	 */
	protected $pdId;

	/**
	 * Id of Rb_Pdm_Context_Productdefinition.
	 *
	 * @var string uuid
	 */
	protected $actId;

	/**
	 * Id of Rb_Pdm_Context_Role.
	 *
	 * @var string uuid
	 */
	protected $roleId;

	/**
	 * Provides a reference to the associated product_definition.
	 *
	 * @var \Rbplm\Pdm\Product\Version
	 */
	protected $definition;

	/**
	 * Pointer to the associated product_definition_context.
	 *
	 * @var \Rbplm\Pdm\Product\Context
	 * 
	 */
	protected $context;

	/**
	 * Gives an optional role indication to the association.
	 *
	 * @var \Rbplm\Pdm\Context\Role
	 */
	protected $role;

	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Product\Version $definition        	
	 */
	public function setDefinition(\Rbplm\Pdm\Product\Version &$definition)
	{
		$this->pdId = $definition->getUid();
		$this->definition = $definition;
	}

	/**
	 *
	 * Getter
	 *
	 * @return \Rbplm\Pdm\Product\Version
	 * @throws \Rbplm\Sys\Exception
	 */
	public function getDefinition()
	{
		if ( !$this->definition ) {
			throw new \Rbplm\Sys\Exception(sprintf('PROPERTY_%s_IS_NOT_SET', 'definition'));
		}
		return $this->definition;
	}

	/**
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Product\Context $context        	
	 */
	public function setContext(\Rbplm\Pdm\Product\Context &$context)
	{
		$this->actId = $context->getUid();
		$this->context = $c;
		ontext;
	}

	/**
	 *
	 * Getter
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Product\Version
	 */
	public function getContext()
	{
		if ( !$this->context ) {
			throw new \Rbplm\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbplm\Sys\Error::ERROR, array(
				'_context'
			));
		}
		return $this->context;
	}

	/**
	 *
	 * Setter
	 *
	 * @param \Rbplm\Pdm\Context\Role $role        	
	 */
	public function setRole(\Rbplm\Pdm\Context\Role &$role)
	{
		$this->roleId = $role->getUid();
		$this->role = $role;
	}

	/**
	 *
	 * Getter
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return \Rbplm\Pdm\Context\Role
	 */
	public function getRole()
	{
		if ( !$this->role ) {
			throw new \Rbplm\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbplm\Sys\Error::ERROR, array(
				'_role'
			));
		}
		return $this->role;
	}
}/* End of class */
