<?php
namespace Rbplm\Plugins\Component;

/**
 *
 * @author olivier
 *
 */
class Dao implements \Rbplm\Plugins\PluginsInterface
{

	/**/
	private $_anyobject = null;

	/**/
	private $_dao = null;

	public function dao(){
		if( !$this->_dao ){
			$this->_dao = \Rbplm\Dao\Factory::get()->getDao($this->_anyobject);
		}
		return $this->_dao;
	}

	public function setanyobject(\Rbplm\AnyObject $anyobject){
		$this->_anyobject = $anyobject;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Plugins\PluginsInterface::setComponent()
	 */
	public function setComponent(\Rbplm\AnyObject $anyobject)
    {}

}
