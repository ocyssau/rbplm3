<?php
namespace Rbplm\Plugins\Model;

use \Rbplm\Plugins\PluginsInterface;

/**
 *
 *
 */
class Dao implements PluginsInterface
{

	private $_anyobject = null;
	private $_dao = null;

	/**
	 *
	 * @return \Rbplm\Dao\DaoInterface
	 */
	public function dao(){
		if( !$this->_dao ){
			$this->_dao = \Rbplm\Dao\Factory::get()->getDao($this->_anyobject);
		}
		return $this->_dao;
	}

	/**
	 *
	 * @param \Rbplm\AnyObject $anyobject
	 */
	public function setanyobject(\Rbplm\AnyObject $anyobject){
		$this->_anyobject = $anyobject;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Plugins\PluginsInterface::setComponent()
	 */
	public function setComponent(\Rbplm\AnyObject $anyobject)
    {}

}
