<?php


namespace Rbplm\Plugins;

interface PluginsInterface
{
	/**
	 * Set anyobject related to this plugin.
	 * @param \Rbplm\AnyObject $anyobject
	 * @return void
	 */
	public function setComponent(\Rbplm\AnyObject $anyobject);
}
