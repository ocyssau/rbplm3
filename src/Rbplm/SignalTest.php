<?php
namespace Rbplm;

class Sender extends \Rbplm\Any
{

	public function init()
	{
		\Rbplm\Signal::trigger('init', $this, 'arg2', 'arg3');
	}

	public function nothing($arg)
	{
		// var_dump($arg);
	}

	public function callback($arg, $arg2, $arg3)
	{
		var_dump($arg, $arg2, $arg3);
	}
}

class SignalTest extends \Rbplm\Test\Test
{

	protected function setUp()
	{}

	function Test_Tutorial()
	{
		$s = new \Rbplm\Sender();
		Signal::connect($s, 'init', array(
			$s,
			'callback'
		));
		$s->init();
	}

	/**
	 * Voir SignatTest
	 */
	function Test_SignalOnAnyObject()
	{
		$component = AnyObject::init('component001')->setParent(Org\Root::singleton());
		Signal::connect($component, Item::SIGNAL_POST_SETPARENT, function ($e, $sender) {
			echo 'Set parent on ' . $sender->getName() . ' parent name : ' . $sender->getParent()->getName() . CRLF;
		});

		Signal::connect($component, Any::SIGNAL_POST_SETNAME, function ($e, $sender) {
			echo 'Set name on ' . $sender->getName() . CRLF;
		});
	}
}

