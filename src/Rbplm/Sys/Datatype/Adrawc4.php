// %LICENCE_HEADER%

namespace Rbplm\Sys\Datatype;

/**
 * @brief A cadds data directory.
 */
class Adrawc4 extends Adraw
{

	/**
	 *
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->zipAdrawExt = '.Adrawc4';
		$this->zipAdrawDatatype = 'zipadrawc4';
		$this->reposit_sub_dir = '__caddsDatas';
		return parent::__construct($path);
	}
}
