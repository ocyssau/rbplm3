// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

/**
 * @brief a cadds data directory.
 */
class Cadds4 extends Cadds
{

	/**
	 *
	 * @param string $path
	 * @return void
	 */
	function __construct($path)
	{
		$this->path = $path;
		$this->mainfile = $this->path . '/_pd';
		$this->file_props = array();
		$this->file_props['file_type'] = 'Cadds4';
		$this->_checkPath();
	}
}
