// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

/**
 * @brief A cadds data directory.
 */
class Adrawnatif extends Cadds
{

	/**
	 * if true, test write operations only
	 *
	 * @var boolean
	 */
	protected $test_mode = false;

	/**
	 *
	 * @param string $path
	 * @return void
	 */
	public function __construct($path)
	{
		$this->path = $path;
		$this->mainfile = $this->path . '/_pd';
		$this->file_props = array();
		$this->file_props['file_type'] = 'adrawc4';
		/* check path to prevent lost of data */
		$this->_checkPath();
	}

	/**
	 */
	public function getProperty($property_name)
	{
		if ( !is_dir($this->path) ) return false;
		switch ($property_name) {

			case 'file_name':
				if ( isset($this->file_props['file_name']) ) return $this->file_props['file_name'];
				return $this->file_props['file_name'] = basename(dirname($this->path)) . '/' . basename($this->path);
				break;

			case 'file_path':
				if ( isset($this->file_props['file_path']) ) return $this->file_props['file_path'];
				return $this->file_props['file_path'] = dirname(dirname($this->path));
				break;

			case 'file_root_name':
				if ( isset($this->file_props['file_root_name']) ) return $this->file_props['file_root_name'];
				return $this->file_props['file_root_name'] = basename(dirname($this->path)) . '/' . basename($this->path);
				break;

			case 'doc_name':
				if ( isset($this->file_props['doc_name']) ) return $this->file_props['doc_name'];
				return $this->file_props['doc_name'] = basename(dirname($this->path)) . '.' . basename($this->path);
				break;

			case 'camu_path':
				if ( isset($this->file_props['camu_path']) ) return $this->file_props['camu_path'];
				return $this->file_props['camu_path'] = dirname($this->path);
				break;

			case 'camu_name':
				if ( isset($this->file_props['camu_name']) ) return $this->file_props['camu_name'];
				return $this->file_props['camu_name'] = dirname($this->getProperty('file_name'));
				break;

			case 'adraw_name':
				if ( isset($this->file_props['adraw_name']) ) return $this->file_props['adraw_name'];
				return $this->file_props['adraw_name'] = basename($this->getProperty('file_name'));
				break;

			default:
				return parent::getProperty($property_name);
				break;
		}
	}

	/**
	 * Move the current cadds part
	 *
	 * @param $dst(string) fullpath
	 *        	to new file
	 * @param $replace(bool) true
	 *        	for replace file
	 */
	public function move($dst, $replace = false)
	{
		return false;
	}

	/**
	 * Copy the current file
	 *
	 * @param string $dst(string)
	 *        	fullpath to new file
	 * @param integer $mode(integer)
	 *        	mode of the new file
	 * @param boolean $replace(bool)
	 *        	true for replace existing file
	 */
	public function copy($dst, $mode = 0755, $replace = true)
	{
		return false;
	}
}

