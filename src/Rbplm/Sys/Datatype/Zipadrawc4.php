<?php
//%LICENCE_HEADER%

namespace Rbplm\Sys\Datatype;

/**
 * @brief A zipped adraw cadds data file
 *
 */
class Zipadrawc4 extends \Rbplm\Sys\Datatype\Zipadraw{

	protected $mainfile; //(string) full path to cadds main file

	/**
	 *
	 * @param $file
	 * @return void
	 */
	public function __construct($file){
		return parent::__construct($file);
	}
}

