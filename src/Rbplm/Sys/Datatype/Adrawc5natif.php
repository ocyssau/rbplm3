//%LICENCE_HEADER%

namespace Rbplm\Sys\Datatype;

/**
 * @brief A cadds data directory.
 *
 *
 */
class Adrawc5 extends Adraw
{

	/**
	 *
	 * @param string $path
	 */
	function __construct($path){
		$this->path = $path;
		$this->mainfile = $this->path.'/_fd';
		$this->file_props = array();
		$this->file_props['file_type'] = 'adrawc5';
		/* check path to prevent lost of data */
		$this->_checkPath();
	}
}

