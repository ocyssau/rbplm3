// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

/**
 * A Cadds data directory
 */
abstract class Cadds extends \Rbplm\Vault\Directory implements DatatypeInterface
{

	/** @var string  Full path to cadds data directory */
	protected $path;

	/** @var string  Full path to cadds main file */
	protected $mainfile;

	/** @var boolean  True if you want return the md5 property of the file */
	public $displayMd5 = false;

	/** @var array  content all properties of the file. */
	public $file_props;

	/** @var string  Sub directory of reposit dir where are stored the Cadds datas*/
	protected $reposit_sub_dir = '__caddsDatas';

	/** @var boolean  True if you want return the md5 property of the file */
	public $displayMd5 = false;

	/**
	 * Get the property of the document by the property name.
	 * init() must be call before
	 */
	public function getProperty($property_name)
	{
		if ( !is_dir($this->path) ) {
			return false;
		}
		switch ($property_name) {
			case 'natif_file_name':
			case 'file_name':
				if ( isset($this->file_props['file_name']) ) return $this->file_props['file_name'];
				return $this->file_props['file_name'] = basename($this->path);
				break;

			case 'file_size':
				if ( isset($this->file_props['file_size']) ) return $this->file_props['file_size'];
				return $this->file_props['file_size'] = Rbplm_Vault_\Directory::dskspace($this->path);
				break;

			case 'file_path':
				if ( isset($this->file_props['file_path']) ) return $this->file_props['file_path'];
				return $this->file_props['file_path'] = dirname($this->path);
				break;

			case 'file_iteration':
				if ( isset($this->file_props['file_iteration']) ) return $this->file_props['file_iteration'];
				return false;
				break;

			case 'file_version':
				if ( isset($this->file_props['file_version']) ) return $this->file_props['file_version'];
				return false;
				break;

			case 'file_mtime':
				if ( isset($this->file_props['file_mtime']) ) return $this->file_props['file_mtime'];
				return $this->file_props['file_mtime'] = filemtime($this->mainfile);
				break;

			case 'file_extension':
				if ( isset($this->file_props['file_extension']) ) return $this->file_props['file_extension'];
				return $this->file_props['file_extension'] = basename($this->mainfile);
				break;

			case 'doc_name':
			case 'file_root_name':
				if ( isset($this->file_props['file_root_name']) ) return $this->file_props['file_root_name'];
				return $this->file_props['file_root_name'] = basename($this->path);
				break;

			case 'file_type':
				return $this->file_props['file_type'];
				break;

			case 'file':
				return $this->file_props['file'] = $this->path;
				break;

			case 'file_md5':
				if ( isset($this->file_props['file_md5']) ) return $this->file_props['file_md5'];
				return $this->file_props['file_md5'] = md5_file($this->mainfile);
				break;

			case 'file_mimetype':
				if ( isset($this->file_props['file_mimetype']) ) return $this->file_props['file_mimetype'];
				$this->getMimeTypeInfos();
				return $this->file_mimetype;
				break;

			case 'visu2D_extension':
				if ( isset($this->file_props['visu2D_extension']) ) return $this->file_props['visu2D_extension'];
				$this->getMimeTypeInfos();
				return $this->visu2D_extension;
				break;

			case 'visu3D_extension':
				if ( isset($this->file_props['visu3D_extension']) ) return $this->file_props['visu3D_extension'];
				$this->getMimeTypeInfos();
				return $this->visu3D_extension;
				break;

			case 'no_read':
				if ( isset($this->file_props['no_read']) ) return $this->file_props['no_read'];
				$this->getMimeTypeInfos();
				return $this->no_read;
				break;

			case 'viewer_driver':
				if ( isset($this->file_props['viewer_driver']) ) return $this->file_props['viewer_driver'];
				$this->getMimeTypeInfos();
				return $this->viewer_driver;
				break;

			case 'reposit_sub_dir':
				return $this->reposit_sub_dir;
				break;
		}
	}

	/**
	 * Return the infos record in the conf/mimes.csv file about the extension of the $filename.
	 *
	 * @param $filename(string) Full
	 *        	path to file.
	 */
	private function getMimeTypeInfos()
	{
		$mimefile = "./conf/mimes.csv";
		$this->getProperty('file_extension');

		$handle = fopen($mimefile, "r");

		while( ($data = fgetcsv($handle, 1000, ";")) !== FALSE ) {
			// $mimetype = $data[0];
			// $description = $data[1];
			// $extension = $data[2];
			// $no_read = $data[3];
			// viewer_driver = $data[4];
			// $visu2D = $data[5]; experimental
			// $visu3D = $data[6]; experimental
			if ( strstr($data[2], $this->file_extension) ) {
				$this->file_props['file_mimetype'] = $data[0];
				$this->file_mimetype = $this->file_props['file_mimetype'];
				$this->file_props['file_mimetype_description'] = $data[1];
				$this->file_mimetype_description = $this->file_props['file_mimetype_description'];
				$this->file_props['no_read'] = $data[3];
				$this->no_read = $this->file_props['no_read'];
				$this->file_props['viewer_driver'] = $data[4]; // Experimental
				$this->viewer_driver = $this->file_props['viewer_driver'];
				// $this->file_props['visu2D_extension'] = $data[5]; //Experimental
				// $this->visu2D_extension = $this->file_props['visu2D_extension'];
				// $this->file_props['visu3D_extension'] = $data[6]; //Experimental
				// $this->visu3D_extension = $this->file_props['visu3D_extension'];
				return $data;
				break;
			}
		}
		fclose($handle);

		return false;
	}

	/**
	 * Get infos about the file or the Cadds part.
	 * Return a array if no errors, else return FALSE.
	 *
	 * @param $displayMd5(bool) true
	 *        	if you want return the md5 code of the file.
	 */
	public function getProperties($displayMd5 = true)
	{
		/* Be careful, never change key name of this array. Else the key name must be existing in all database files table */
		if ( is_dir($this->path) ) {
			$props = array(
				'file_name' => $this->getProperty('file_name'),
				'natif_file_name' => $this->getProperty('natif_file_name'),
				'file_size' => $this->getProperty('file_size'),
				'file_path' => $this->getProperty('file_path'),
				'file_iteration' => $this->getProperty('file_iteration'),
				'file_mtime' => $this->getProperty('file_mtime'),
				'file_extension' => $this->getProperty('file_extension'),
				'file_root_name' => $this->getProperty('file_root_name'),
				'file_type' => $this->getProperty('file_type'),
				'file' => $this->getProperty('file'),
				'reposit_sub_dir' => $this->reposit_sub_dir
			);
			if ( $displayMd5 ) {
				$props['file_md5'] = $this->getProperty('file_md5');
			}

			return $props;
		}
		else {

			return false;
		}
	}

	/**
	 * Return file extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * @param $file(string) File
	 *        	name.
	 */
	public function getExtension()
	{
		return $this->getProperty('file_extension');
	}

	/**
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 *
	 * @param $file(string) File
	 *        	name.
	 */
	public function getRoot()
	{
		return $this->getProperty('file_root_name');
	}

	/**
	 * Move the current Cadds part
	 *
	 * @param string $dst
	 *        	fullpath to new file
	 * @param bool $replace
	 *        	true for replace file
	 */
	public function move($dst, $replace = false)
	{
		if ( !is_dir($this->path) ) {
			Ranchbe::getError()->error(tra('%element% is not a Cadds directory'), array(
				'element' => $this->file
			));
			return false;
		}

		if ( !Rbplm_Vault_Filesystem::limitDir($dst) ) {
			Ranchbe::getError()->error(tra('you have no right access on file %element%'), array(
				'element' => $dst
			));
			return false;
		}

		if ( !is_dir(dirname($dst)) ) {
			Ranchbe::getError()->error(tra('the directory %element% dont exist'), array(
				'element' => dirname($dst)
			));
			return false;
		}

		/* Copy datas */
		if ( $this->copy($dst, 0755, $replace) ) {
			/* delete data */
			$this->delete();
			$this->path = $dst;
			unset($this->file_props['file_name']);
			unset($this->file_props['file_path']);
			unset($this->file_props['file_root_name']);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Copy the current file
	 *
	 * @param $dst(string) fullpath
	 *        	to new file
	 * @param $mode(integer) mode
	 *        	of the new file
	 * @param $replace(bool) true
	 *        	for replace existing file
	 */
	public function copy($dst, $mode = 0755, $replace = true)
	{
		if ( !Rbplm_Vault_Filesystem::limitDir($this->path) ) {
			Ranchbe::getError()->error('copy error : you have no right access on file %file%', array(
				'file' => $this->file
			));
			return false;
		}

		if ( !Rbplm_Vault_Filesystem::limitDir($dst) ) {
			Ranchbe::getError()->error('copy error : you have no right access on file %file%', array(
				'file' => $dst
			));
			return false;
		}

		if ( is_file($dst . '/' . $this->getProperty('file_extension')) && !$replace ) {
			Ranchbe::getError()->error('copy error: file %file% exist', array(
				'file' => $dst . '/' . $this->getProperty('file_extension')
			));
			return false;
		}

		if ( is_dir($this->path) ) {
			if ( Rbplm_Vault_\Directory::dircopy($this->path, $dst, false, true, true, 0755) ) {
				touch($dst . '/' . $this->getProperty('file_extension'), filemtime($this->mainfile));
				chmod($dst . '/' . $this->getProperty('file_extension'), $mode);
				return true;
			}
		}

		Ranchbe::getError()->error('copy error on Cadds part %file%', array(
			'file' => $this->file
		));
		return false;
	}

	/**
	 * Suppress the current Cadds directory
	 * Returns TRUE or FALSE
	 */
	public function delete()
	{
		if ( !$this->putInTrash() ) {
			Ranchbe::getError()->error('suppress of Cadds part %file% failed', array(
				'file' => $this->file
			));
			return false;
		}
		else{
			return true;
		}
	}

	/**
	 * Put Cadds directory in the trash dir
	 * Returns TRUE or FALSE
	 */
	public function putInTrash($verbose = false, $trashDir = DEFAULT_TRASH_DIR)
	{
		return Rbplm_Vault_\Directory::putDirInTrash($this->path, $verbose, true, $trashDir);
	}

	/**
	 *
	 */
	public function downloadFile()
	{
		File_Archive::setOption('zipCompressionLevel', 0);
		$reader = File_Archive::read($this->path, $this->getProperty('file_name'));
		$writer = File_Archive::toArchive($this->getProperty('file_name') . '.zip', File_Archive::toOutput());
		File_Archive::extract($reader, $writer);
		die();
		return true;
	}

	/*
	 * * Copy a upload file to $dstfile
	 * Return true or false.
	 *
	 * Take the parameters of the uploaded file(from array $file) and create the file in the wildpspace.
	 * @param $file(array) :
	 * @param $file[name](string) name of the uploaded file
	 * @param $file[type](string) mime type of the file
	 * @param $file[tmp_name](string) temp name of the file on the server
	 * @param $file[error](integer) error code if error occured during transfert(0=no error)
	 * @param $file[size](integer) size of the file in octets
	 * @param $replace(bool) if true and if the file exist in the wildspace, it will be replaced by the uploaded file.
	 * @param $dstdir(string) Path of the dir to put the file.
	 */
	static function uploadFile($file, $replace, $dstdir)
	{
		return true;
	}

	/**
	 * Check the dataPath of Cadds data
	 * Return true or false.
	 * This method check if the Cadds data is present in wildspace or in reposit_dir under special Cadds directory.
	 * This method is a paranoid function that ensure not delete data directory.
	 */
	protected function _checkPath()
	{
		if ( basename($this->path) == '__versions' || basename($this->path) == 'indices' ) {
			die('Cadds::checkPath : ' . $this->path . ' : this path is not valid: <i>__versions</i> and <i>indices</i> are reserved names. You can not used it for Cadds part name');
		}

		$wildspacePath = \Rbplm\People\CurrentUser::get()->getWildspace()->getPath();
		$wildspacePathRegFormat = str_replace('/', '\/', $wildspacePath);
		/* destruct unused object */
		unset($wildspace);

		/* if path begin by wildspace path */
		if ( preg_match('/^' . $wildspacePathRegFormat . '/', $this->path) ) {
			/* $wildspace/camu or $wildspace/camu/adraw not $wildspace/camu/adraw/other */
			if ( preg_match('/^' . $wildspacePathRegFormat . '\/(([^\/]+)|([^\/]+\/{1}[^\/]+)){1}$/', $this->path) ){
				return true;
			}
		}
		/* if path begin by reposit directory path */
		if ( preg_match('/__caddsDatas/', $this->path) ) {
			/* prevent access directly to __caddsData,__caddsData/versions,__caddsData/versions/[num] directories (same for indices directories) */
			if ( preg_match('/^.+\/__caddsDatas(\/)?((__versions\/[0-9]+\/?){1}|(__indices\/[0-9]+\/?)|(__versions\/?)|(__indices\/?))?$/', $this->path) ){
				die('Cadds::checkPath : ' . $this->path . ' : this path is not valid. You try to modify indices or versions reposit directories');
			}
			if ( preg_match('/^.+\/__caddsDatas\/(([^\/]+\/?){1}|([^\/]+\/{1}[^\/]+){1}|((__versions\/){1}[0-9]+\/{1}[^\/]+(\/{1}[^\/]+)?){1}|((__indices\/){1}[0-9]+\/{1}[^\/]+(\/{1}[^\/]+)?){1}){1}$/', $this->path) ){
				return true;
			}
		}
		/* if path begin by reposit directory path */
		if ( preg_match('/__imported/', $this->path) ) {
			/* prevent access directly to __imported directory */
			if ( preg_match('/^.+\/__imported(\/)?$/', $this->path) ){
				die('Cadds::checkPath : ' . $this->path . ' : this path is not valid. You try to modify __imported reposit directory');
			}
			if ( preg_match('/^.+\/__imported\/(([^\/]+\/?){1}|([^\/]+\/{1}[^\/]+){1}){1}$/', $this->path) ){
				return true;
			}
		}
		die('Cadds::checkPath : ' . $this->path . ' : this path is not valid. Cadds data must be stored in __caddsData of reposit_dir sub directory, in the user wildspace directory or in __imported directory');
	}
} /* End of class */
