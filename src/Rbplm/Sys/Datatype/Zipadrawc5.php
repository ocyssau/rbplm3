<?php
// %LICENCE_HEADER%

namespace Rbplm\Sys\Datatype;

/**
 * @brief A zipped adraw cadds data file
 */
class Zipadrawc5 extends Zipadraw
{

	/**
	 *
	 * @param string $file
	 * @return void
	 */
	public function __construct($file)
	{
		return parent::__construct($file);
	}
}
