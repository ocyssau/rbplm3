<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

use Exception;

/**
 * This class manage the import of files or documents in the containers.
 *
 * Events :
 * - onExtractBegin, //Begin the extraction of archive file
 * - onExtractEnd, //End of extraction
 */
class Package extends File
{

	/**
	 * Uncompress Package
	 *
	 * @param string $targetDir 	Path of the dir where put files of the Package file.
	 * @throw Exception
	 * @return boolean
	 */
	public function unpack($targetDir)
	{
		if ( !is_dir($targetDir) ) {
			throw new Exception(sprintf('%s is not directory', $targetDir));
		}
		if ( !is_writable($targetDir) ) {
			throw new Exception(sprintf('%s is not writable', $targetDir));
		}
		if ( !is_file($this->file) ) {
			throw new Exception(sprintf('%s is not a file', $this->file));
		}
		if ( $this->getExtension() == '.Z' ) {
			if ( exec(UNZIPCMD . " $this->file") ) {
				// @todo: suppress $file from history table
				$this->getImportHistory()->updateImportPackage(array(
					'package_file_name' => $this->file
				), NULL, $this->file);
				$this->file = rtrim($this->file, '.Z');
				$this->file_props = array();
			}
			else {
				throw new Exception(sprintf('cant uncompress this file %s', $this->file));
			}
		}

		if ( substr($this->file, strrpos($this->file, '.')) == '.tar' ) {
			$cmd = 'tar -xvf ' . $this->file . ' -C ' . $targetDir;
			$output = '';
			$return = '';
			exec($cmd, $output, $return);
			if ( !$output ) {
				throw new Exception(sprintf('extraction of %s has failed for unknow error. Archive is maybe empty. Out: %s, Return: %s ', $this->file, $output, $return));
			}
		}
	}
} /* End of class */
