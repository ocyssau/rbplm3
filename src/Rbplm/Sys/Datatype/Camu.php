// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

/**
 * @brief brief a cadds data directory
 */
class Camu extends Cadds
{

	/** @var array fsdata objects of adraws */
	protected $adraws;

	/** @var boolean */
	protected $test_mode = false;

	/**
	 *
	 * @param
	 *        	$path
	 * @return void
	 */
	function __construct($path)
	{
		$this->path = $path;
		$this->mainfile = $this->path . '/_db';
		$this->file_props = array();
		$this->file_props['file_type'] = 'Camu';
		$this->_checkPath(); // check path to prevent lost of data
	}


	/*
	 * suppress the current cadds directory
	 * Returns TRUE or FALSE
	 */
	function delete()
	{
		// Test if they are adraw in this directory...if true, dont suppress it
		if ( $this->getAdraws() ) {
			Ranchbe::getError()->notice('Camu directory %file% contains adraws. Camu directory will be not suppressed.', array(
				'file' => $this->path
			));
		}
		if ( !$this->putInTrash() ) {
			Ranchbe::getError()->error('suppress of Camu directory %element% failed', array(
				'file' => $this->path
			));
			return false;
		}
		else
			return true;
	}


	/*
	 * put cadds directory in the trash dir
	 * Returns TRUE or FALSE
	 */
	function putInTrash($verbose = false, $trashDir = DEFAULT_TRASH_DIR)
	{
		/* Test if they are adraw in this directory...if true, dont suppress it */
		if ( $this->getAdraws() ) {
			Ranchbe::getError()->notice('Camu directory %file% contains adraws. Camu directory will be not suppressed.', array(
				'file' => $this->path
			));
		}
		if ( $this->test_mode ) return true;
		return Rbplm_Vault_\Directory::putDirInTrash($this->path, $verbose, false, $trashDir);
	}


	/*
	 * copy the current file
	 *
	 * @param $mode(integer) mode of the new file
	 * @param $replace(bool) true for replace existing file
	 */
	function copy($dst, $mode = 0755, $replace = true)
	{
		if ( !Rbplm_Vault_Filesystem::limitDir($this->path) ) {
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
				'element' => $this->file
			), 'copy error : you have no right access on file %element%');
			return false;
		}

		if ( !Rbplm_Vault_Filesystem::limitDir($dst) ) {
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
				'element' => $dst
			), 'copy error : you have no right access on file %element%');
			return false;
		}

		if ( is_file($dst . '/' . $this->getProperty('file_extension')) && !$replace ) {
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
				'element' => $dst . '/' . $this->getProperty('file_extension')
			), 'copy error: file %element% exist');
			return false;
		}

		if ( is_file($this->mainfile) ) {
			if ( $this->test_mode ) return true;
			if ( Rbplm_Vault_\Directory::dircopy($this->path, $dst, false, true, false, 0755) ) {
				touch($dst . '/' . $this->getProperty('file_extension'), filemtime($this->mainfile));
				chmod($dst . '/' . $this->getProperty('file_extension'), $mode);
				return true;
			}
		}

		Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
			'element' => $this->file
		), 'copy error on cadds part %element%');
		return false;
	}


	/*
	 * move the current cadds part
	 *
	 * @param $dst(string) fullpath to new file
	 * @param $replace(bool) true for replace file
	 */
	function move($dst, $replace = false)
	{
		if ( !is_dir($this->path) ) {
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
				'element' => $this->file
			), '%element% is not a cadds directory');
			return false;
		}

		if ( !Rbplm_Vault_Filesystem::limitDir($dst) ) {
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
				'element' => $dst
			), 'you have no right access on file %element%');
			return false;
		}

		if ( !is_dir(dirname($dst)) ) {
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(
				'element' => dirname($dst)
			), 'the directory %element% dont exist');
			return false;
		}

		if ( $this->test_mode ) return true;

		// Copy file , and suppress file
		if ( Rbplm_Vault_\Directory::dircopy($this->path, $dst, false, true, false, 0755) ) {
			$this->delete();
			$this->path = $dst;
			unset($this->file_props['file_name']);
			unset($this->file_props['file_path']);
			unset($this->file_props['file_root_name']);
			return true;
		}
		else
			return false;
	}


	/*
	 * get adraw from a Camu directory
	 * Return an array of fsdata object if no errors else return false
	 *
	 * @param $init(bool) if true, re-scan Camu directory, else return existing $adraws array
	 */
	function getAdraws($init = false)
	{
		if ( isset($this->adraws) && !$init ) return $this->adraws;
		if ( !$handle = opendir($this->path) ) {
			print 'failed to open ' . $this->path . '<br>';
			return false;
		}

		while( $item = @readdir($handle) ) { // list adraw of the directory "Camu"
			if ( $item == '.' || $item == '..' ) continue;
			$path = $this->path . '/' . $item;
			if ( is_dir($path) ) {
				$datatype = \Rbplm\Sys\Fsdata::setDataType($path);
				// Check if subdir is a caddspart
				if ( $datatype['type'] == 'adrawc4' || $datatype['type'] == 'adrawc5' ) {
					$this->adraws[] = \Rbplm\Sys\Fsdata::_dataFactory($path);
				}
				else {
					print 'error: presence of a unknow subdir in Camu directory <br />';
				}
			}
		} /* End of while */

		if ( is_array($this->adraws) ) return $this->adraws;
		return false;
	}
} /* End of class */

