// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

/**
 * @brief An Adraw cadds data.
 */
class Adraw implements \Rbplm\Sys\Datatype\DatatypeInterface
{

	/** @var string  Full path to cadds data directory */
	protected $path;

	/** @var string  Full path to cadds main file */
	protected $mainfile;

	/** @var boolean  True if you want return the md5 property of the file */
	public $displayMd5 = false;

	/** @var array  content all properties of the file. */
	public $file_props;

	/** @var string  Sub directory of reposit dir where are stored the Cadds datas*/
	protected $reposit_sub_dir = '__caddsDatas';

	/** @var boolean  True if you want return the md5 property of the file */
	public $displayMd5 = false;

 	/** @var bool if true, test write operations only */
	protected $test_mode = false;

	/** @var datatype object of associated zipfile */
	protected $zipdata;

	/** @var datatype object of natif Adraw data format */
	protected $natifdata;

	/** @var real data on filesystem */
	protected $data;

	/** @var string */
	protected $zipAdrawExt = '.Adraw';

	/** @var string */
	protected $zipAdrawDatatype = 'zipadraw';

 	/** @var  true if a real data exist on filesystem */
	protected $isInit = false;

	/** @var string sub directory of reposit dir where are stored the cadds datas */
	protected $reposit_sub_dir = '__caddsDatas';

	/**
	 *
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->file_props = array();
		$this->file_props['file_type'] = 'adrawc4';
		$this->file = $path;
		$this->path = $this->file;

		/* le $path indique soit un fichier [path]/camu.Adraw.adraw soit un repertoire [path]/camu/Adraw. */

		/* si extension = .Adraw : on a indique un fichier, sinon on suppose que l'on indique un repertoire */
		$this->file_props['file_extension'] = substr($this->file, strrpos($this->file, '.'));
		/* le parametre est cense etre un fichier */
		if ( $this->file_props['file_extension'] == $this->zipAdrawExt ) {
			/* on extrait les proprietes depuis le nom de fichier */
			$this->file_props['file_name'] = basename($this->file);
			$this->file_props['file_path'] = dirname($this->file);
			$this->file_props['file_root_name'] = substr($this->file_props['file_name'], 0, strrpos($this->file_props['file_name'], '.'));
			$this->natif_file_name = str_replace('.', '/', $this->file_props['file_root_name']);
			$this->natif_file = $this->file_props['file_path'] . '/' . $this->natif_file_name;

			/* Le fichier peut-etre existant ou non. S'il n'existe pas, on suppose qu'il existe la donnee cadds native */
			if ( is_file($this->file) ) {
				$this->zipdata = new $this->zipAdrawDatatype($this->file);
				$this->data = $this->zipdata;
				$this->isInit = true;
			}
			/* le fichier n'existe pas, le dossier cadds natif doit exister */
			else {
				if ( is_dir($this->natif_file) ) {
					$this->natifdata = new Adrawnatif($this->natif_file);
					$this->data = $this->natifdata;
					$this->zipfile = $this->file; // Le zip file a le meme nom que le fichier
					$this->isInit = true;
				}
				/* Si ce dossier n'existe pas, ce n'est ni un fichier ni un repertoire on parametre comme si c'est un fichier a créer */
				else {
			    	/* le fichier n'existe pas, mais on fait comme si */
					$this->isInit = true;
					return false;
				}
			}
		}
		/* on a indique un repertoire cadds dans le parametre */
		else {
			/* Si l'on indique un repertoire, celui-ci doit exister */
			if ( is_dir($this->file) ) {
				/* on construit l'Adraw depuis la donnee cadds native */
				$this->natifdata = new Adrawnatif($this->file);

				/* On genere le nom de la donnee zip */
				$this->file_props['file_root_name'] = $this->natifdata->getProperty('camu_name') . '.' . $this->natifdata->getProperty('adraw_name');
				$this->file_props['file_path'] = $this->natifdata->getProperty('file_path');
				$this->file_props['file_name'] = $this->file_props['file_root_name'] . $this->zipAdrawExt;
				$this->zipfile = $this->file_props['file_path'] . '/' . $this->file_props['file_root_name'] . $this->zipAdrawExt;
				/* on genere le nom du fichier natif */
				$this->natif_file = $this->file;
				$this->natif_file_name = $this->natifdata->getProperty('file_name');
				$this->data = $this->natifdata;
				$this->isInit = true;
			}
			else{
				return false;
			}
		}

		return false;
	}

	/**
	 *
	 */
	public function initZipadraw()
	{
		if ( !is_object($this->natifdata) ) return false;
		if ( !$this->isInit ) return false;

		/* Create a zip file of the adrawing */
		if ( $this->test_mode == false ) {
			$zipAdrawFile = $this->zipfile;
			//File_Archive::setOption('zipCompressionLevel', 9);
			//$reader = File_Archive::read($this->natif_file, $this->natif_file_name);
			//$writer = File_Archive::toArchive($zipAdrawFile, File_Archive::toFiles(), 'Zip');
			//File_Archive::extract($reader, $writer);
		}
		/* Init the zipdata */
		$this->zipdata = new $this->zipAdrawDatatype($zipAdrawFile);
	}

	/**
	 *
	 */
	public function unZip()
	{
		if ( $this->test_mode == true ) return;
		$zip = new \ZipArchive();
		if ( $zip->open($this->zipfile) === TRUE ) {
			$zip->extractTo(dirname($this->zipfile));
			$zip->close();
		}
		else {
			$manager->error_stack->push(Rbplm_Vault_Error::ERROR, array(
				'file' => $this->zipfile
			), 'WARNING : can not uncompress file : %file%');
		}
	}

	/**
	 *
	 */
	public function getProperty($property_name)
	{
		if ( !$this->isInit ) return false;

		switch ($property_name) {
			case 'file_name':
				return $this->file_props['file_name'];
				break;

			case 'natif_file_name':
				return $this->natif_file_name;
				break;

			case 'file_path':
				return $this->file_props['file_path'];
				break;

			case 'file_extension':
				// return $this->file_props['file_extension'] = $this->zipAdrawExt;
				return $this->file_props['file_extension'] = $this->data->getProperty('file_extension');
				break;

			case 'doc_name':
			case 'file_root_name':
				return $this->file_props['file_root_name'];
				break;

			case 'file_type':
				return $this->file_props['file_type'];
				break;

			case 'file':
				return $this->file;
				break;

			case 'file_md5': // incoherent with zipdata
				if ( isset($this->file_props['file_md5']) ) return $this->file_props['file_md5'];
				return $this->file_props['file_md5'] = $this->data->getProperty('file_md5');
				// return $this->file_props['file_md5'] = md5_file($this->mainfile);
				break;

			case 'file_size': // incoherent with zipdata
				if ( isset($this->file_props['file_size']) ) return $this->file_props['file_size'];
				return $this->file_props['file_size'] = $this->data->getProperty('file_size');
				break;

			case 'file_mtime':
				if ( isset($this->file_props['file_mtime']) ) return $this->file_props['file_mtime'];
				return $this->file_props['file_mtime'] = $this->data->getProperty('file_mtime');
				// return $this->file_props['file_mtime'] = filemtime($this->mainfile);
				break;

			case 'camu_path':
				return $this->data->getProperty('camu_path');
				break;

			case 'camu_name':
				return $this->data->getProperty('camu_name');
				break;

			case 'adraw_name':
				return $this->data->getProperty('adraw_name');
				break;

			case 'reposit_sub_dir':
				if ( isset($this->reposit_sub_dir) ) return $this->reposit_sub_dir;
				break;

			default:
				return $this->file_props[$property_name];
				break;
		}
	}


	/**
	 * Get infos about the file or the cadds part.
	 * Return a array if no errors, else return FALSE.
	 *
	 * @param $displayMd5(bool) true if you want return the md5 code of the file.
	 */
	public function getProperties($displayMd5 = true)
	{
		if ( !$this->isInit ) return false;
		if ( isset($this->zipdata) ) {
			return $this->zipdata->getProperties($displayMd5);
		}
		$this->file_props = array(
			'file_name' => $this->getProperty('file_name'),
			'natif_file_name' => $this->getProperty('natif_file_name'),
			'file_size' => $this->getProperty('file_size'),
			'file_path' => $this->getProperty('file_path'),
			'file_mtime' => $this->getProperty('file_mtime'),
			'file_extension' => $this->getProperty('file_extension'),
			'file_root_name' => $this->getProperty('file_root_name'),
			'file_type' => $this->getProperty('file_type'),
			'file' => $this->file
		);
		if ( $displayMd5 ) {
			$this->file_props['file_md5'] = $this->getProperty('file_type');
		}
		return $this->file_props;
	}

	/**
	 * Return file extension.(ie: /dir/file.ext, return '.ext')
	 */
	public function getExtension()
	{
		return $this->zipAdrawExt;
	}

	/**
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 *
	 */
	public function getRoot()
	{
		if ( !$this->isInit ) return false;
		return $this->file_props['file_root_name'];
	}

	/**
	 * move the current file
	 *
	 * @param string $dst(string) fullpath to new file
	 * @param boolean $replace(bool) true for replace file
	 */
	public function move($dst, $replace = false)
	{
		if ( !$this->isInit ) return false;

		/* init $this->zipdata, create the zipfile */
		$this->initZipadraw();

		if ( isset($this->zipdata) ) {
			if ( $this->zipdata->move($dst, $replace) ) {
				if ( isset($this->natifdata) ) {
					$this->natifdata->delete();
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * copy the current file
	 *
	 * @param string $dst fullpath to new file
	 * @param integer $mode mode of the new file
	 * @param boolean $replace true for replace existing file
	 */
	public function copy($dst, $mode = 0755, $replace = true)
	{
		if ( !$this->isInit ) return false;

		/* init $this->zipdata, create the zipfile */
		$this->initZipadraw();

		if ( isset($this->zipdata) ) return $this->zipdata->copy($dst, $mode, $replace);
		return false;
	}

	/**
	 * suppress the current attachment file
	 */
	public function delete()
	{
		if ( !$this->isInit ) return false;

		if ( isset($this->zipdata) ) {
			/* supprime le zip... */
			$this->zipdata->delete();
		}

		if ( isset($this->natifdata) ) {
			$this->natifdata->delete();
		}

		return true;
	}

	/**
	 * put file in the trash dir
	 * Returns TRUE or FALSE
	 */
	public function putInTrash($verbose = false, $trashDir = DEFAULT_TRASH_DIR)
	{
		if ( !$this->isInit ) return false;

		if ( isset($this->zipdata) ) $this->zipdata->putInTrash($verbose, $trashDir);

		if ( isset($this->natifdata) ) {
			$this->natifdata->delete();
		}

		return true;
	}

	/**
	 * send data to user navigator
	 * Returns TRUE or FALSE
	 */
	public function downloadFile()
	{
		if ( !$this->isInit ) return false;
		$this->initZipadraw();
		return $this->zipdata->downloadFile();
	}
	public function getMd5()
    {}

	public function getName()
    {}

	public function getMimetype()
    {}

	public function getRootname()
    {}

	public function getSize()
    {}

	public function download()
    {}

	public function getType()
    {}

	public function rename($dst, $replace = false)
    {}

	public function getFullpath()
    {}

	public function getMtime()
    {}

	public function getPath()
    {}


} /* End of class */
