<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

use \Rbplm\Sys\Filesystem;
use \Rbplm\Sys\Trash;
use \Rbplm\Sys\Exception;

/**
 * A file on the filesystem
 */
class File extends Filesystem implements DatatypeInterface
{
	/**
	 *
	 * @var \SplFileInfo
	 */
	protected $_file = false;

	/**
	 * Name of the File 'File.ext'
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * path to the File '/dir/sub_dir'
	 *
	 * @var string
	 */
	protected $path;

	/**
	 * extension of the File '.ext'
	 *
	 * @var string
	 */
	protected $extension;

	/**
	 * root name of the File 'File'
	 *
	 * @var string
	 */
	protected $rootname;

	/**
	 * Real full path and name of file
	 *
	 * @var string
	 */
	protected $fullpath;

	/**
	 * Size of the File in octets
	 *
	 * @var integer
	 */
	protected $size;

	/**
	 * modification time of the File
	 *
	 * @var integer
	 */
	protected $mtime;

	/**
	 * type of File = File, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * md5 code of the File.
	 *
	 * @var string
	 */
	protected $md5;

	/**
	 *
	 * @var string
	 */
	protected $mimetype;

	/**
	 *
	 * @var boolean
	 */
	protected $_no_read;

	/**
	 *
	 * @var string
	 */
	protected $_viewer_driver;

	/**
	 *
	 * @param string $file Path to file
	 */
	public function __construct($file)
	{
		$this->_file = new \SplFileInfo($file);
		$this->type = 'file';
		$this->_init();
	}

	/**
	 */
	public function __clone()
	{}

	/**
	 * @return array
	 */
	public function __sleep()
	{
		$ignore = array(
			'_plugins',
			'dao',
			'factory',
			'_file'
		);
		$toSerialize = array_diff(array_keys(get_object_vars($this)), $ignore);
		return $toSerialize;
	}

	/**
	 * Re-init properties
	 *
	 * @return void
	 */
	private function _init()
	{
		$this->name = $this->_file->getFilename();
		$this->size = $this->_file->getSize();
		$this->mtime = new \DateTime();
		$this->mtime->setTimestamp($this->_file->getMTime());
		$this->extension = self::sGetExtension($this->name);
		$this->rootname = self::sGetRoot($this->name);
		$this->fullpath = $this->_file->getRealPath();
		$this->path = dirname($this->fullpath);
	}

	/**
	 *
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getProperies($displayMd5)
	 *
	 */
	public function getProperties($displayMd5 = true)
	{
		$properties = array(
			'name' => $this->name,
			'size' => $this->size,
			'path' => $this->path,
			'mtime' => $this->mtime,
			'extension' => $this->extension,
			'rootname' => $this->rootname,
			'type' => $this->type,
			'fullpath' => $this->fullpath
		);

		if ( $displayMd5 ) {
			$properties['md5'] = $this->getMd5();
		}

		return $properties;
	}

	/**
	 * Size on filesystem.
	 * Return size in octets.
	 *
	 * @return integer
	 */
	function getSize()
	{
		return $this->size;
	}

	/**
	 * Return data extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * @return string
	 */
	function getExtension()
	{
		return $this->extension;
	}

	/**
	 * Last modification time of data
	 *
	 * @return \DateTime
	 */
	function getMtime()
	{
		return $this->mtime;
	}

	/**
	 *
	 * @return string
	 */
	function getName()
	{
		return $this->name;
	}

	/**
	 *
	 * @return string
	 */
	function getPath()
	{
		return $this->path;
	}

	/**
	 *
	 * @return string
	 */
	function getFullpath()
	{
		return $this->fullpath;
	}

	/**
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 *
	 * @return string
	 *
	 */
	function getRootname()
	{
		return $this->rootname;
	}

	/**
	 *
	 * @return string
	 */
	function getType()
	{
		return $this->type;
	}

	/**
	 *
	 * @return string
	 */
	function getMd5()
	{
		if ( !$this->md5 ) {
			$this->md5 = md5_file($this->fullpath);
		}
		return $this->md5;
	}

	/**
	 *
	 * @return string
	 */
	function getMimetype()
	{
		if(!$this->mimetype){
			$this->mimetype = mime_content_type($this->fullpath);
		}
		return $this->mimetype;
	}

	/**
	 * Move/rename the current file
	 *
	 * @param string $dst
	 *        	fullpath to new file
	 * @param boolean $replace
	 *        	true for replace file
	 * @throws \Rbplm\Sys\Exception
	 * @return File
	 */
	function rename($newName, $replace = false)
	{
		$newName = trim($newName);

		if ( !Filesystem::limitDir($this->fullpath) ) {
			throw new Exception(sprintf('NO ACCESS TO %s', $this->fullpath));
		}

		if ( !Filesystem::limitDir($newName) ) {
			throw new Exception(sprintf('NO ACCESS TO %s', $newName));
		}

		if ( empty($newName) ) {
			throw new Exception(sprintf('NO ACCESS TO %s', $newName));
		}

		if ( is_file($newName) ) {
			$ok = true;
			if ( $replace ) {
				$ok = unlink($newName);
			}
			else {
				$ok = false;
			}
			if ( $ok == false ) {
				throw new Exception(sprintf('%s IS EXISTING', $newName));
			}
		}

		if ( !is_dir(dirname($newName)) ) {
			throw new Exception(sprintf('%s IS NOT EXISTING', $newName));
		}

		if ( !is_writeable($this->fullpath) ) {
			throw new Exception(sprintf('%s IS NOT WRITABLE', $this->fullpath));
		}

		if ( !@rename($this->fullpath, $newName) ) {
			throw new Exception(sprintf('CAN NOT RENAME %s', $this->fullpath));
		}

		$this->_file = new \SplFileInfo($newName);
		$this->_init();
		return $this;
	}

	/**
	 * Copy the current file
	 *
	 * @param string $toFile	Fullpath to new file
	 * @param integer $mode 	Mode of the new file
	 * @param boolean $replace	True for replace existing file
	 * @throws \Rbplm\Sys\Exception
	 * @return File New file object
	 */
	function copy($toFile, $mode = 0755, $replace = true)
	{
		if ( !Filesystem::limitDir($this->fullpath) ) {
			throw new Exception(sprintf('NO ACCESS TO %s', $this->fullpath));
		}

		if ( !Filesystem::limitDir($toFile) ) {
			throw new Exception(sprintf('NO ACCESS TO %s', $toFile));
		}

		if ( $replace == false ) {
			if ( is_file($toFile) ) {
				throw new Exception(sprintf('%s IS EXISTING', $toFile));
			}
		}

		if ( is_dir($toFile) ) {
			throw new Exception(sprintf('%s IS EXISTING OR BAD TYPE', $toFile));
		}

		if ( $this->_file->isFile() ) {
			if ( !@copy($this->fullpath, $toFile) ) {
				throw new Exception(sprintf('COPY ERROR FROM %s TO %s', $this->fullpath, $toFile));
			}
			if ( !@touch($toFile, $this->mtime->getTimestamp()) ) {
				/* @todo: log error */
				//throw new Exception(sprintf('UNABLE TO SET MTIME ON %s, CHECK PERMISSIONS', $toFile));
			}
			if ( !@chmod($toFile, $mode) ) {
				/* @todo: log error */
				//throw new Exception(sprintf('UNABLE TO CHANGE PERMISSION ON %s, CHECK PERMISSION', $toFile));
			}
		}
		else {
			throw new Exception(sprintf('BAD TYPE DATA %s', $this->fullpath));
		}

		$fileCopy = clone ($this);
		$fileCopy->_file = new \SplFileInfo($toFile);
		$fileCopy->_init();
		return $fileCopy;
	}

	/**
	 * Suppress the current file
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @return File
	 */
	function delete()
	{
		if ( is_file($this->fullpath) ) {
			if ( !self::putInTrash($this->fullpath) ) {
				throw new Exception(sprintf('SUPPRESSION ERROR ON DATA %s', $this->fullpath));
			}
		}
		return $this;
	}

	/**
	 * Put file in the trash dir
	 *
	 * @param boolean $verbose
	 * @throws \Rbplm\Sys\Exception
	 * @return boolean
	 */
	function putInTrash($verbose = false)
	{
		$dstfile = Trash::getTrashedFilePath($this->_file);

		/* Copy File to trash */
		try{
			$this->copy($dstfile);
			$fb = @chmod($dstfile, 0755);
			if(!$fb){
				throw new Exception(sprintf('Unable to exec chmod on %s. Check permissions, ranchbe server must be owner of the vault directories and files', $dstfile));
			}
		}
		catch(\Exception $e){
			throw $e;
		}

		if ( !@unlink($this->fullpath) ) {
			throw new Exception(sprintf('ERROR DURING SUPPRESS OF %s', $this->fullpath));
		}

		$this->log(sprintf('File %s has been put in trash', $this->fullpath));
		return true;
	}

	/**
	 * Download current data.
	 *
	 * @throws \Rbplm\Sys\Exception
	 * @param string $name Name used to present on user navigator
	 * @return boolean
	 */
	function download($name=null)
	{
		$mimeType = $this->getMimetype();
		if ( $mimeType == 'no_read' ) {
			return false;
		}
		if(!$name){
			$name = $this->name;
		}
		header("Content-disposition: attachment; filename=\"$name\"");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: \"$name\"\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . ($this->size));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($this->fullpath);
		return true;
	}

	/**
	 * Static method version of getExtension()
	 *
	 * @param string $fileName
	 * @return string
	 */
	static function sGetExtension($fileName)
	{
		return substr($fileName, strrpos($fileName, '.'));
	}

	/**
	 * Static method version of getRoot()
	 *
	 * @param string $fileName
	 * @return string
	 */
	static function sGetRoot($fileName)
	{
		return substr($fileName, 0, strrpos($fileName, '.'));
	}

	/**
	 * Copy a upload File to $dstfile
	 *
	 * @param array $file
	 *        	$file[name](string) name of the uploaded File
	 *        	$file[type](string) mime type of the File
	 *        	$file[tmp_name](string) temp name of the File on the server
	 *        	$file[error](integer) error code if error occured during transfert(0=no error)
	 *        	$file[size](integer) size of the File in octets
	 *
	 * @param boolean $replace
	 *        	if true and if the File exist in the wildspace, it will be replaced by the uploaded File.
	 * @param string $dstdir
	 *        	Path of the dir to put the File.
	 * @return boolean
	 */
	public static function upload($file, $replace, $dstdir)
	{
		$dstfile = $dstdir . '/' . $file['name'];
		if ( !$replace ) { /* Test if File exist in target dir only if replace=false */
			if ( is_file($dstfile) ) {
				throw new Exception(sprintf('%s IS NOT EXISTING', $dstfile));
			}
		}
		if ( !is_file($file['tmp_name']) ) {
			throw new Exception(sprintf('%s IS NOT FILE', $file['tmp_name']));
		}
		if ( copy($file['tmp_name'], $dstfile) ) {
			chmod($dstfile, 0775);
			return true;
		}
		else {
			throw new Exception(sprintf('ERROR DURING COPY OF %s TO %s', $file['tmp_name'], $dstfile));
		}
	}
} /* End of class */

