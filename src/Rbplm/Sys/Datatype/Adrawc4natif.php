//%LICENCE_HEADER%

namespace Rbplm\Sys\Datatype;


/**
 * @brief A cadds data directory.
 *
 */
class Adrawc4 extends \Rbplm\Sys\Datatype\Adraw
{
	/**
	 *
	 * @param string $path
	 */
	function __construct($path)
	{
		$this->path = $path;
		$this->mainfile = $this->path.'/_pd';
		$this->file_props = array();
		$this->file_props['file_type'] = 'adrawc4';
		/* check path to prevent lost of data */
		$this->_checkPath();
	}
}
