<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys\Datatype;

/**
 * @brief A zipped adraw cadds data file
 */
class Zipadraw extends File
{

	/* @var string full path to cadds main file */
	protected $mainfile;

	/* @var string sub directory of reposit dir where are stored the cadds datas */
	protected $reposit_sub_dir = '__caddsDatas';

	/**
	 *
	 * @param
	 *        	$file
	 * @return void
	 */
	public function __construct($file)
	{
		$this->file = $file;
		$this->file_props = array();
		$this->file_props['file_type'] = 'Zipadraw';
		$this->file_type = $this->file_props['file_type'];
		return true;
	}
} /* End of class */

