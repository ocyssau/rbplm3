<?php
//%LICENCE_HEADER%

namespace Rbplm\Sys\Datatype;

/**
 * Represent a file on the filesystem
 *
 */
interface DatatypeInterface
{

	/**
	 * Get infos about the data
	 *
	 * @param boolean	$displayMd5	true if you want return the md5 code of the file.
	 * @return array
	 * @throws \Rbplm\Sys\Exception
	 */
	public function getProperties( $displayMd5 = true);

	/**
	 * Size on filesystem.
	 * Return size in octets.
	 *
	 * @return integer
	 */
	public function getSize();

	/**
	 * Return data extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * @return string
	 */
	public function getExtension();

	/**
	 * Last modification time of data
	 *
	 * @return integer
	 */
	public function getMtime();

	/**
	 *
	 * @return string
	 */
	public function getName();

	/**
	 *
	 * @return string
	 */
	public function getPath();

	/**
	 *
	 * @return string
	 */
	public function getFullpath();

	/**
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 * @return string
	 *
	 */
	public function getRootname();

	/**
	 *
	 * @return string
	 */
	public function getType();

	/**
	 *
	 * @return string
	 */
	public function getMd5();

	/**
	 *
	 * @return string
	 */
	public function getMimetype();

	/**
	 * Move the current file
	 *
	 * @param string	$dst 		fullpath to new file
	 * @param boolean	$replace 	true for replace file
	 * @throws \Rbplm\Sys\Exception
	 */
	public function rename($dst , $replace = false);

	/**
	 * Copy the current file
	 *
	 * @param string 	$dst fullpath to new file
	 * @param integer 	$mode mode of the new file
	 * @param bool 		$replace true for replace existing file
	 * @throws \Rbplm\Sys\Exception
	 */
	public function copy($dst, $mode=0755, $replace=true);

	/**
	 * Suppress the current file
	 *
	 * @throws \Rbplm\Sys\Exception
	 */
	public function delete();

	/**
	 * Put file in the trash dir
	 *
	 * @param boolean	$verbose
	 * @throws \Rbplm\Sys\Exception
	 */
	public function putInTrash($verbose = false);

	/**
	 * Download current data.
	 *
	 * @param string $name Force the file name pushed to user. If not set, pushed the real name.
	 * @throws \Rbplm\Sys\Exception
	 */
	public function download($name = null);

} /* End of class */
