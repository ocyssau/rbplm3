<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys\Filesystem;

/**
 *
 */
class Folder extends \SplFileInfo
{

	/**
	 *
	 * @var \DirectoryIterator
	 */
	protected $iterator;

	/**
	 *
	 * @return \DirectoryIterator
	 */
	public function getIterator()
	{
		if ( !$this->iterator ) {
			$this->iterator = new \DirectoryIterator($this->getPathname());
		}
		return $this->iterator;
	}
}

