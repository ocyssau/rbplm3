<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys\Filesystem;

use Rbplm\Dao\Filter\Op;
use Rbplm\Dao\FilterInterface;
use Rbplm\Sys\Exception;

/**
 * create params to search on Filesystem
 */
class Filter implements FilterInterface
{

	/**
	 * Limit the number of records in the result
	 *
	 * @var integer
	 */
	protected $limit = 0;

	/**
	 * Pagination page to display
	 *
	 * @var integer
	 */
	protected $offset = 0;

	/**
	 * field use for sort sql option
	 *
	 * @var string
	 */
	protected $sortBy = '';

	/**
	 * field use for sort sql option
	 *
	 * @var string
	 */
	protected $sortOrder = '';

	/**
	 *
	 * @var array
	 */
	protected $regex = array();

	/**
	 *
	 * @return void
	 */
	public function __construct()
	{}

	/**
	 *
	 * @param string $what
	 *        	a word to find
	 * @param string $where
	 *        	property name
	 * @param string $op
	 *        	One of constant \Rbplm\Dao\Filter\Op::*
	 * @return void
	 */
	public function andfind($what, $where, $op = Op::CONTAINS)
	{
		return $this->_find($what, $where, $op, 'AND');
	}

	/**
	 *
	 * @param string $what
	 *        	a word to find
	 * @param string $where
	 *        	property name
	 * @param string $op
	 *        	One of constant \Rbplm\Dao\Filter\Op::*
	 * @return void
	 */
	public function orfind($what, $where, $op = Op::CONTAINS)
	{
		return $this->_find($what, $where, $op, 'OR');
	}

	/**
	 *
	 * @param string $what
	 *        	a word to find
	 * @param string $where
	 *        	property name
	 * @param string $op
	 *        	One of constant Rbplm_Search_Op::OP_*
	 * @return void
	 */
	protected function _find($what, $where, $op = Op::CONTAINS, $boolOp = 'AND')
	{
		if ( !$what || !$where ) {
			return;
		}

		$what = preg_quote($what);

		switch ($op) {
			case Op::BEGIN:
				$op = '=';
				$what = '^' . $what;
				break;
			case Op::END:
				$op = '=';
				$what = $what . '$';
				break;
			case Op::CONTAINS:
				$op = '=';
				$what = $what;
				break;
			case Op::NOTCONTAINS:
				$op = '<>';
				$what = '[^' . $what . ']';
				break;
			case Op::NOTEND:
				$op = '<>';
				$what = '' . $what . '';
				break;
			case Op::NOTBEGIN:
				$op = '<>';
				$what = '^[^' . $what . ']';
				break;
			case Op::SUP:
				$op = '>';
				$what = $what;
				break;
			case Op::EQUALSUP:
				$op = '>=';
				$what = $what;
				break;
			case Op::INF:
				$op = '<';
				$what = $what;
				break;
			case Op::EQUALINF:
				$op = '<=';
				$what = $what;
				break;
			case Op::REGEX:
				$op = '=';
				$what = $what;
				break;
			default:
				throw new Exception(sprintf('UNKNOW_OPERATOR %s', $op), \Rbplm\Sys\Error::WARNING);
		}
		$this->regex[] = array(
			$what,
			$boolOp
		);
	}

	/**
	 *
	 * @param string $by
	 * @param string $direction
	 *        	ASC or DESC
	 * @return void
	 */
	public function sort($by, $direction = 'ASC')
	{
		$this->sortBy = $by;
		$this->sortOrder = strtoupper($direction);
	}

	/**
	 *
	 * @param integer $page
	 * @param integer $pageLimit
	 * @return void
	 */
	public function page($page, $pageLimit)
	{
		(!$page) ? $page = 1 : null;
		$this->offset = ($page - 1) * $pageLimit;
		$this->limit = $this->offset + $pageLimit;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.FilterInterface::getOffset()
	 */
	public function getOffset()
	{
		return $this->offset;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.FilterInterface::getLimit()
	 */
	public function getLimit()
	{
		return $this->limit;
	}

	/**
	 *
	 * @see http://www.phpliveregex.com/
	 * @return string
	 */
	public function regexToString()
	{
		$out = '';
		$prefix = '';

		if ( $this->regex ) {
			$i = 0;
			foreach( $this->regex as $reg ) {
				if ( $reg[1] == 'AND' && $i > 0 ) {
					$prefix = '';
				}
				elseif ( $reg[1] == 'OR' && $i > 0 ) {
					$prefix = '|';
				}
				$out .= "$prefix($reg[0])";
				$i++;
			}
		}

		return $out;
	}

	/**
	 *
	 * @param array $select
	 * @return Filter
	 */
	public function select($select)
	{
		if ( $this->options['asapp'] ) {
			$dao = $this->options['dao'];
			$select = array_flip($dao->getTranslator()->toSys(array_flip($select)));
		}
		$this->select = $select;
		return $this;
	}

	/**
	 *
	 * @return array|string
	 */
	public function selectToString()
	{
		if ( $this->select ) {
			return implode(',', $this->select);
		}
		else {
			return '*';
		}
	}

	/**
	 */
	public function orderToString()
	{
		$filter = '';
		if ( $this->options['paginationIsActive'] == true ) {
			if ( $this->sortBy ) {
				$Sorder = ' ORDER BY ' . $this->sortBy . ' ' . $this->sortOrder;
				$filter .= ' ' . $Sorder;
			}
		}
		return $filter;
	}

	/**
	 */
	public function getOrder()
	{
		return $this->sortOrder;
	}

	/**
	 */
	public function getSort($asString = true)
	{
		return $this->sortBy;
	}

	/**
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->regex;
	}
}
