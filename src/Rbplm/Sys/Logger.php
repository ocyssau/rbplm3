<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;

use Zend\Log\Writer\Syslog;

/**
 *
 * @brief Logger for Rbplm. Inherit from \Zend_Log and add singleton pattern.
 * Singleton instanciate logger and set a \Zend_Log_Writer_Syslog writer.
 * In futur version, singleton may add other functionalities to logger.
 *
 * Consult \Zend_Log documentation to learn more about Logger:
 * @see http://framework.zend.com/manual/en/zend.log.html
 */
class Logger extends \Zend\Log\Logger
{

	/**
	 *
	 * @var Syslog
	 */
	public $syslog;

	/**
	 * String added to beginning of the log message
	 *
	 * @var string
	 */
	public $prefix;

	/**
	 *
	 * @var Logger
	 */
	private static $instance;

	/**
	 * Emergency: system is unusable
	 *
	 * @var integer
	 */
	const EMERG = 0;

	/**
	 * Alert: action must be taken immediately
	 *
	 * @var integer
	 */
	const ALERT = 1;

	/**
	 * Critical: critical conditions
	 *
	 * @var integer
	 */
	const CRIT = 2;

	/**
	 * Error: error conditions
	 *
	 * @var integer
	 */
	const ERR = 3;

	/**
	 * Warning: warning conditions
	 *
	 * @var integer
	 */
	const WARN = 4;

	/**
	 * Notice: normal but significant condition
	 */
	const NOTICE = 5;

	/**
	 * Informational: informational messages
	 *
	 * @var integer
	 */
	const INFO = 6;

	/**
	 * Debug: debug messages
	 *
	 * @var integer
	 */
	const DEBUG = 7;

	/**
	 * Singleton method.
	 * Add the writer syslog.
	 *
	 * @return Logger
	 */
	public static function singleton()
	{
		if ( self::$instance === null ) {
			self::$instance = new Logger();
		}
		return self::$instance;
	}

	/**
	 *
	 * {@inheritdoc}
	 * @see \Zend\Log\Logger::log()
	 */
	public function log($message, $priority = Logger::INFO, $extras = array())
	{
		$message = $this->prefix . ' - ' . $message;
		return parent::log($priority, $message, $extras);
	}

	/**
	 *
	 * @param
	 *        	string
	 * @return Logger
	 */
	public function initSyslog($applicationName = 'Rbplm')
	{
		if(!isset($this->syslog)){
			$writer = new Syslog();
			$writer->setApplicationName($applicationName);
			$this->addWriter($writer);
			$this->syslog = $writer;
		}
		return $this;
	}

	/**
	 *
	 * @param string $applicationName
	 */
	public function setApplicationName($applicationName)
	{
		$this->syslog->setApplicationName($applicationName);
	}

	/**
	 *
	 * @param string $applicationName
	 */
	public function setPrefix($string)
	{
		$this->prefix = $string;
	}
}
