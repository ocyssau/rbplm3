<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;


/**
 * @brief Trash for data suppressed of filesystem
 */
class Trash
{

	/**
	 * Path to trashbin
	 *
	 * @var string
	 */
	static $path = '';

	/**
	 * Init trash directory.
	 *
	 * @throws \Rbplm\Sys\Exception
	 */
	public static function init($trashDir)
	{
		self::$path = $trashDir;
		if ( !is_dir($trashDir) ) {
			mkdir($trashDir, 0755, true);
		}
		if ( !is_writable($trashDir) ) {
			throw new Exception(sprintf('CAN_NOT_CREATE_PATH %s', $trashDir));
		}
	}

	/**
	 * Return full path to trashed file.
	 *
	 * @param string $filepath
	 * @throws Exception
	 * @return string
	 */
	public static function getTrashedFilePath($filepath)
	{
		$trashDir = self::$path;
		$basename = basename($filepath);
		$dirname = dirname($filepath);

		/* Add original path to name of dstfile */
		$oriPath = Filesystem::encodePath($dirname);

		/* Check if there is not conflict name in trash dir else rename it */
		$dstfile = $trashDir . '/' . $oriPath . '%&_' . $basename;
		$i = 0;
		if ( is_file($dstfile) ) {
			/* Rename destination dir if exist */
			$renamedstfile = $dstfile;
			$pathParts = pathinfo($renamedstfile);

			while( is_file($renamedstfile) ) {
				$renamedstfile = $pathParts['dirname'] . '/' . $pathParts['filename'] . '(' . $i . ')' . '.' . $pathParts['extension'];
				$i++;
			}
			$dstfile = $renamedstfile;
		}

		return $dstfile;
	}
} /* End of class */

