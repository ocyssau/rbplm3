<?php
namespace Rbplm\Sys;

use DateTime;

/**
 * @brief Library of static method to manage date in Rbplm
 */
class Date extends DateTime
{

	/**
	 *
	 * @var string
	 */
	public static $dateFormat = 'd-m-Y H:i:s';

	/**
	 *
	 * @var string
	 */
	public static $timeZone = 'Europe/Paris';


	/**
	 * @param string|integer $date
	 */
	public function __construct($date = null)
	{
		if ( is_integer($date) ) {
			parent::__construct();
			$this->setTimestamp($date);
		}
		else {
			parent::__construct($date);
		}
	}

	/**
	 */
	public function __toString()
	{
		// $format = 'Y-m-d H:i:s';
		$this->setTimezone(new \DateTimeZone(self::$timeZone));
		return $this->format(self::$dateFormat);
	}
} /* End of class */
