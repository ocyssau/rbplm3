<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;

use Rbplm\Dao\MappedInterface;
use Rbplm\Owned;
use Rbplm\Mapped;
use Rbplm\ArrayObject;
use Rbplm\Sys\Date as DateTime;

/**
 * @brief Message to users.
 *
 * Message are echange between users without use of sendmail or other mail protocol.
 * Its just a internal to the application.
 *
 * Message use \Zend\Mail\Message, and may be use as \Zend\Mail\Message, but, there is probably some methods wich may be dont operate.
 * See the example to know more.
 *
 * Example and tests: RbplmTest/Sys/MessageTest.php
 *
 * @verbatim
 * @endverbatim
 */
class Message extends \Rbplm\Any implements MappedInterface
{

	use Owned, Mapped, ArrayObject;

	/**
	 *
	 * @var string
	 */
	public static $classId = '569b7d17e4ccb';

	/**
	 *
	 * @var boolean
	 */
	protected $isRead = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isReplied = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isFlagged = false;

	/**
	 *
	 * @var integer
	 */
	protected $priority = 3;

	/**
	 * Uniq identifier
	 *
	 * @var string
	 */
	protected $uid;

	/**
	 * One of constante self::TYPE_*
	 *
	 * @var integer
	 */
	protected $type = 1;

	/**
	 *
	 * @var \DateTime
	 */
	protected $created;

	/**
	 * Content of the message
	 *
	 * @var string|object
	 */
	protected $body;
	protected $subject;
	protected $from;
	protected $to=array();
	protected $cc=array();
	protected $bcc=array();

	/**
	 * Message in mailbox to read
	 */
	const TYPE_MAILBOX = 1;

	/**
	 * Message in sendbox
	 */
	const TYPE_SEND = 2;

	/**
	 * Message archive
	 */
	const TYPE_ARCHIVE = 3;

	/**
	 * Constructor
	 *
	 * @param string $type One of constant self::TYPE_*
	 * @return void
	 */
	public function __construct($type)
	{
		$this->cid = static::$classId;
		$this->uid = uniqid();
		$this->type = $type;
	}

	/**
	 * Hydrator.
	 * Load the properties in the object.
	 *
	 * @param array $properties
	 *        	key=property name in model sementic, $value=$property value
	 * @return Message
	 */
	public function hydrate(array $properties)
	{
		$this->ownedHydrate($properties);
		$this->mappedHydrate($properties);

		isset($properties['id']) ? $this->id = $properties['id'] : null;
		isset($properties['uid']) ? $this->uid = $properties['uid'] : null;
		isset($properties['type']) ? $this->type = $properties['type'] : null;

		isset($properties['isRead']) ? $this->isRead = (bool)$properties['isRead'] : null;
		isset($properties['isReplied']) ? $this->isReplied = (bool)$properties['isReplied'] : null;
		isset($properties['isFlagged']) ? $this->isFlagged = (bool)$properties['isFlagged'] : null;
		isset($properties['priority']) ? $this->priority = (int)$properties['priority'] : null;
		
		isset($properties['from']) ? $this->from = $properties['from'] : null;
		isset($properties['to']) ? $this->to = $properties['to'] : null;
		isset($properties['cc']) ? $this->cc = $properties['cc'] : null;
		isset($properties['bcc']) ? $this->bcc = $properties['bcc'] : null;
		
		($properties['body']) ? $this->setBody($properties['body']) : null;
		($properties['subject']) ? $this->setSubject($properties['subject']) : null;
		
		if ( isset($properties['created']) ) {
			$date = $properties['created'];
			if($date instanceof \DateTime){
				$this->setCreated($date);
			}
			elseif(is_string($date)){
				$this->setCreated(new DateTime($date));
			}
		}

		return $this;
	}

	/**
	 *
	 * @param DateTime $date
	 * @return \Rbplm\Any
	 */
	public function setCreated(\DateTime $date)
	{
		$this->created = $date;
		return $this;
	}

	/**
	 *
	 * @return \DateTime
	 */
	public function getCreated($format = null)
	{
		if ( !$this->created instanceof \DateTime ) {
			$this->created = new DateTime();
		}

		if ( $format ) {
			return $this->created->format($format);
		}
		else {
			return $this->created;
		}
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isRead($bool = null)
	{
		if ( !is_null($bool) ) {
			$this->isRead = $bool;
		}
		return $this->isRead;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isReplied($bool = null)
	{
		if ( !is_null($bool) ) {
			$this->isReplied = $bool;
		}
		return $this->isReplied;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isFlagged($bool = null)
	{
		if ( !is_null($bool) ) {
			$this->isFlagged = $bool;
		}
		return $this->isFlagged;
	}

	/**
	 *
	 * @param integer $int
	 * @return Message
	 */
	public function setPriority($int)
	{
		$this->priority = $int;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 *
	 * @param integer $int
	 * @return Message
	 */
	public function setType($int)
	{
		$this->type = $int;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Setter for uniq identifier
	 *
	 * @param string $uid
	 * @return string
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
	}

	/**
	 *
	 * @return string
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * Set (overwrite) From addresses
	 *
	 * @param string $email
	 * @return Message
	 */
	public function setFrom($email)
	{
		$this->from = $email;
		return $this;
	}

	/**
	 * Retrieve list of From senders
	 *
	 * @return string
	 */
	public function getFrom()
	{
		return $this->from;
	}

	/**
	 * Overwrite the address list in the To recipients
	 *
	 * @param array $array
	 * @return Message
	 */
	public function setTo($array)
	{
		$this->to = $array;
		return $this;
	}

	/**
	 * Add one or more addresses to the To recipients
	 *
	 * Appends to the list.
	 *
	 * @param string $email
	 * @return Message
	 */
	public function addTo($email)
	{
		$this->to[] = $email;
		return $this;
	}

	/**
	 * Access the address list of the To header
	 *
	 * @return array
	 */
	public function getTo()
	{
		return $this->to;
	}

	/**
	 * Set (overwrite) CC addresses
	 *
	 * @param array $array
	 * @return Message
	 */
	public function setCc($array)
	{
		$this->cc = $array;
		return $this;
	}

	/**
	 * Add a "Cc" address
	 *
	 * @param string $email
	 * @return Message
	 */
	public function addCc($email)
	{
		$this->cc[] = $email;
		return $this;
	}

	/**
	 * Retrieve list of CC recipients
	 *
	 * @return array
	 */
	public function getCc()
	{
		return $this->cc;
	}

	/**
	 * Set (overwrite) BCC addresses
	 *
	 * @param array $array
	 * @return Message
	 */
	public function setBcc($array)
	{
		$this->bcc = $array;
		return $this;
	}

	/**
	 * Add a "Bcc" address
	 *
	 * @param string $email
	 * @return Message
	 */
	public function addBcc($email)
	{
		$this->bcc[] = $email;
		return $this;
	}

	/**
	 * Retrieve list of BCC recipients
	 *
	 * @return array
	 */
	public function getBcc()
	{
		return $this->bcc;
	}

	/**
	 * Set the message subject header value
	 *
	 * @param string $subject
	 * @return Message
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
		return $this;
	}

	/**
	 * Get the message subject header value
	 *
	 * @return null|string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * Set the message body
	 *
	 * @param null|string|\Zend\Mime\Message|object $body
	 * @throws \InvalidArgumentException
	 * @return Message
	 */
	public function setBody($body)
	{
		$this->body = $body;
		return $this;
	}

	/**
	 * Return the currently set message body
	 *
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * Suppress quote, html tag and php tags from a string
	 */
	public function cleanUserInputText($string)
	{
		/* Suppress quotes if necessary */
		$string = (get_magic_quotes_gpc()) ? stripslashes($string) : $string;
		$string = html_entity_decode($string);
		return $string;
	}
} /* End of class */
