<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;

use Rbplm\Sys\Datatype\DatatypeInterface;
use Rbplm\Rbplm;
use Rbplm\People\CurrentUser;

/**
 * @brief Fsdata create a link between the database record of data (docfile or recordfile)
 * and the real data on the filesystem of the server like a file or a directory.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 *
 * If you try to construct a fsData or create a new fsData from _dataFactory() method, from a path wich is not existing, none exception will be throw,
 * but the mehod isExisting() will be return false.
 */
class Fsdata implements DatatypeInterface
{

	/**
	 * Child object data, cadds, camu...
	 * lchild
	 *
	 * @var \Rbplm\Sys\Datatype\DatatypeInterface
	 */
	protected $data;

	/**
	 *
	 * type of the Fsdata: file, cadds, camu...
	 *
	 * @var string
	 */
	protected $dataType;

	/**
	 *
	 * Full path to data
	 *
	 * @var string
	 */
	protected $dataPath;

	/**
	 *
	 * @param string $dataPath
	 */
	public function __construct($dataPath)
	{
		$this->dataPath = $dataPath;
		$this->data = self::_dataFactory($dataPath);

		if ( !$this->data ) {
			$this->dataType = false;
			Rbplm::log('UNREACHABLE_DATA: ' . $dataPath);
		}
		else {
			$this->dataType = $this->data->getType();
			return true;
		}
	}

	/**
	 */
	public function __clone()
	{}

	/**
	 *
	 * @return array
	 */
	public function __sleep()
	{
		$ignore = array(
			'_plugins',
			'dao',
			'factory',
			'data'
		);
		$toSerialize = array_diff(array_keys(get_object_vars($this)), $ignore);
		return $toSerialize;
	}

	/**
	 * Return true if data is realy on file system
	 *
	 * @return boolean
	 *
	 */
	public function isExisting()
	{
		return (is_file($this->dataPath) || is_dir($this->dataPath));
	}

	/**
	 * Set type of a data.
	 * Return false or a array with "type" and "extension"
	 *
	 * @param string $dataPath
	 * @return array | boolean
	 * @throws \Rbplm\Sys\Exception
	 */
	public static function setDataType($dataPath)
	{
		if ( is_dir($dataPath) ) {
			/* Test for adraw from datatype file */
			if ( is_file($dataPath . '/ranchbe.adraw') ) {
				if ( is_file($dataPath . '/_fd') ) {
					return array(
						'type' => 'adrawc5',
						'extension' => '_fd'
					);
				}
				if ( is_file($dataPath . '/_pd') ) {
					return array(
						'type' => 'adrawc4',
						'extension' => '_pd'
					);
				}
			}
			/* Tests for cadds5 parts and adraw */
			if ( is_file($dataPath . '/_fd') ) {
				if ( is_file($dataPath . '/../_db') ) { // If parent dir is a camu, this part is a adraw
					touch($dataPath . '/ranchbe.adraw');
					return array(
						'type' => 'adrawc5',
						'extension' => '_fd'
					);
				}
				else {
					return array(
						'type' => 'cadds5',
						'extension' => '_fd'
					);
				}
			}
			else if ( is_file($dataPath . '/_pd') ) { // Tests for cadds4 parts and adraw
				if ( is_file($dataPath . '/../_db') ) { // If parent dir is a camu, this part is a adraw
					touch($dataPath . '/ranchbe.adraw');
					return array(
						'type' => 'adrawc4',
						'extension' => '_pd'
					);
				}
				else {
					return array(
						'type' => 'cadds4',
						'extension' => '_pd'
					);
				}
			}
			else if ( is_file("$dataPath/_db") ) { // Tests for camu
				return array(
					'type' => 'camu',
					'extension' => '_db'
				);
			}
			else if ( is_file("$dataPath/_ps") ) { // Tests for ps tree
				return array(
					'type' => 'pstree',
					'extension' => '_ps'
				);
			}
			else {
				return FALSE;
			}
		}
		else if ( is_file($dataPath) ) {
			$extension = substr($dataPath, strrpos($dataPath, '.'));
			switch ($extension) {
				case '.adrawc5':
					return array(
						'type' => 'zipadrawc5',
						'extension' => '.adrawc5'
					);
					break;
				case '.adrawc4':
					return array(
						'type' => 'zipadrawc4',
						'extension' => '.adrawc4'
					);
					break;
				case '.adraw':
					return array(
						'type' => 'zipadraw',
						'extension' => '.adraw'
					);
					break;
				case '.z':
					return array(
						'type' => 'archive',
						'extension' => '.z'
					);
				case '.zip':
					return array(
						'type' => 'archive',
						'extension' => '.zip'
					);
				case '.Z':
					return array(
						'type' => 'archive',
						'extension' => '.Z'
					);
					break;
				default:
					return array(
						'type' => 'file',
						'extension' => $extension
					);
					break;
			}
		}
		/* if data is not on filesystem or is other thing that file or dir */
		else {
			Rbplm::log('UNREACHABLE_DATA: ' . $dataPath);
			$extension = substr($dataPath, strrpos($dataPath, '.'));
			return array(
				'type' => null,
				'extension' => $extension
			);
		}
	}

	/**
	 * Factory for \Rbplm\Vault\Datatype
	 *
	 * @param string $dataPath
	 * @return \Rbplm\Sys\Datatype\DatatypeInterface
	 */
	static function _dataFactory($dataPath)
	{
		$dataType = self::setDataType($dataPath);

		switch ($dataType['type']) {
			/*
			 * case 'cadds5':
			 * return new \Rbplm\Sys\Datatype\Cadds5($dataPath);
			 * break;
			 * case 'cadds4':
			 * return new \Rbplm\Sys\Datatype\Cadds4($dataPath);
			 * break;
			 * case 'adraw':
			 * case 'adrawc4':
			 * case 'adrawc5':
			 * return new \Rbplm\Sys\Datatype\Adraw($dataPath);
			 * break;
			 * case 'zipadraw':
			 * case 'zipadrawc4':
			 * case 'zipadrawc5':
			 * return new \Rbplm\Sys\Datatype\Zipadraw($dataPath);
			 * break;
			 * case 'camu':
			 * return new \Rbplm\Sys\Datatype\Camu($dataPath);
			 * break;
			 * case 'pstree':
			 * return new \Rbplm\Sys\Datatype\Pstree($dataPath);
			 * break;
			 */
			case 'package':
			case 'archive':
				return new \Rbplm\Sys\Datatype\Package($dataPath);
				break;
			case 'file':
				return new \Rbplm\Sys\Datatype\File($dataPath);
				break;
			default:
				return new \Rbplm\Sys\Datatype\File($dataPath);
				break;
		}
	}

	/**
	 * Getter of data
	 *
	 * @return \Rbplm\Sys\Datatype\DatatypeInterface
	 *
	 */
	public function getData()
	{
		if ( isset($this->data) ) {
			return $this->data;
		}
		else {
			return false;
		}
	}

	/**
	 * Setter of data
	 *
	 * @param \Rbplm\Sys\Datatype\DatatypeInterface $data
	 */
	public function setData(Datatype\DatatypeInterface $data)
	{
		$this->data = $data;
		return $this;
	}

	/**
	 * Getter for datatype
	 *
	 * @return string
	 */
	public function getDatatype()
	{
		return $this->dataType;
	}

	/**
	 * Put the file in the wildspace.
	 * The file is put with a default prefix "consult__" to prevent lost of data.
	 *
	 * @param string $addPrefix
	 *        	string to add before filename.
	 * @param boolean $replace
	 * @return boolean
	 */
	public function putInWildspace($addPrefix = NULL, $replace = false)
	{
		$dstfile = CurrentUser::get()->getWildspace()->getPath() . '/' . $addPrefix . $this->getProperty('file_name');
		return $this->copy($dstfile, 0775, $replace);
	}

	/**
	 *
	 * @param bool $displayMd5
	 * @return array
	 */
	public function getProperties($displayMd5 = true)
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		$properties = $this->data->getProperties($displayMd5);
		$properties['dataType'] = $this->dataType;
		return $properties;
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getSize()
	 */
	public function getSize()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getSize();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getExtension()
	 */
	public function getExtension()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getExtension();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getMtime()
	 */
	public function getMtime()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getMtime();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getName()
	 */
	public function getName()
	{
		return $this->data->getName();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getPath()
	 */
	public function getPath()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getPath();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getFullpath()
	 */
	function getFullpath()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getFullpath();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getRootname()
	 */
	function getRootname()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getRootname();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getType()
	 */
	public function getType()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getType();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getMd5()
	 */
	public function getMd5()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getMd5();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#getMimetype()
	 */
	public function getMimetype()
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->getMimetype();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#rename($dst, $replace)
	 */
	public function rename($dst, $replace = false)
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->rename($dst, $replace);
	}

	/**
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#copy($dst, $mode, $replace)
	 * @return Fsdata
	 */
	public function copy($dst, $mode = 0755, $replace = true)
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		$fsdataCopy = clone ($this);
		$fsdataCopy->data = $this->data->copy($dst, $mode, $replace);
		return $fsdataCopy;
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#suppress()
	 */
	public function delete()
	{
		return $this->data->delete();
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#putInTrash($verbose, $trashDir)
	 */
	public function putInTrash($verbose = false)
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		return $this->data->putInTrash($verbose);
	}

	/**
	 *
	 * @see \Rbplm\Sys\Datatype\DatatypeInterface#download()
	 */
	public function download($name=null)
	{
		if ( !$this->data ) {
			throw new \Exception(sprintf('data %s is not found', $this->dataPath));
		}
		if ( $this->data->download($name) ) {
			die();
		}
	}
} /* End of class */

