<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;

use Rbplm\Rbplm;

/**
 * @brief \Directory on filesystem
 */
class Directory extends Filesystem
{

	/**
	 * Path
	 *
	 * @var string
	 */
	protected $path;

	/**
	 *
	 * @var string
	 */
	public static $defaulTrashDir = '/tmp';

	/**
	 *
	 * @param string $path
	 * @return void
	 */
	public function __construct($path = null)
	{
		$this->setPath($path);
	}

	/**
	 * Set the path of the Wildspace
	 *
	 * @param string $path
	 */
	public function init($secure=false)
	{
		$path = $this->path;
		
		/* Check if Reposit dir is existing */
		if ( !is_dir($path) ) {
			try {
				Directory::create($path, 0755, $secure);
			}
			catch( \Exception $e ) {
				throw new Exception(sprintf('Cant create path %s with reason %s', $path, $e->getMessage()));
			}
		}

		$this->path = realpath($path);
		return $this;
	}

	/**
	 * Return the path of the Directory
	 *
	 * @param string $path If set, add path to the current directory
	 * @return string
	 */
	public function getPath($path = null)
	{
		if($path){
			$path = $this->path . '/' . str_replace('..', '', trim(trim($path, '/')));
		}
		else{
			$path = $this->path;
		}
		return $path;
	}

	/**
	 * Set the path of the Directory
	 *
	 * @param string $path
	 */
	public function setPath($path)
	{
		$this->path = $path;
		return $this;
	}

	/**
	 * Delete a file or directory from the directory.
	 *
	 * @param string $dname
	 *        	name of the data to suppress.
	 * @throws Exception
	 */
	public function suppressData($dname)
	{
		$path = $this->path . '/' . $dname;
		$odata = Fsdata::_dataFactory($path);
		if ( $odata ) {
			return $odata->putInTrash();
		}
		else {
			throw new Exception(sprintf('FILE_NOT_FOUND %s', $path));
		}
	}

	/**
	 * A function to copy files from one \Directory to another one,
	 * including subdirectories and nonexisting or newer files.
	 *
	 * @param string $srcdir
	 * @param string $dstdir
	 * @param boolean $verbose
	 * @param boolean $initial
	 * @param boolean $recursive
	 * @param integer $mode
	 * @return boolean
	 */
	public static function copy($srcdir, $dstdir, $verbose = false, $initial = true, $recursive = true, $mode = 0755)
	{
		if ( !Filesystem::limitDir($srcdir) ) {
			throw new Exception(sprintf('NO_ACCESS_TO_%s', $srcdir));
		}

		if ( !is_dir($dstdir) ) {
			mkdir($dstdir, $mode, true);
		}
		if ( $curdir = opendir($srcdir) ) {
			while( $file = readdir($curdir) ) {
				if ( $file != '.' && $file != '..' ) {
					$srcfile = $srcdir . '/' . $file;
					$dstfile = $dstdir . '/' . $file;
					if ( is_file($srcfile) ) {
						if ( $verbose ) {
							Rbplm::Log("Copying '$srcfile' to '$dstfile'...");
						}
						if ( copy($srcfile, $dstfile) ) {
							touch($dstfile, filemtime($srcfile));
							if ( $verbose ) {
								Rbplm::Log("Copying '$srcfile' to '$dstfile'...OK");
							}
						}
						else {
							Rbplm::Log("Error: File '$srcfile' could not be copied!");
							throw new Exception(sprintf('COPY_ERROR_FROM_%s_TO_%s', $srcdir, $dstfile));
						}
					}
					else {
						if ( is_dir($srcfile) && $recursive ) {
							self::dircopy($srcfile, $dstfile, $verbose, false);
						}
					}
				}
			}
			closedir($curdir);
		}
		return true;
	}

	/**
	 * Rename a directory from $from to $to
	 *
	 * @param string $from
	 * @param string $to
	 * @throws Exception
	 */
	public static function rename($from, $to)
	{
		if ( !is_dir($from) ) {
			throw new Exception(sprintf('%s is not a directory'), $from);
		}
		if ( !is_dir($to) || is_file($to) ) {
			throw new Exception(sprintf('%s is existing'), $to);
		}
		if ( !@rename($from, $to) ) {
			throw new Exception(sprintf('Error during rename of %s to %s'), $from, $to);
		}
		Rbplm::Log('Directory ' . $from . ' renamed to ' . $to);
	}

	/**
	 * Create a new \Directory recursively
	 *
	 * @param string $path
	 * @param integer $mode
	 * @param boolean $secure
	 *        	If true, check that \Directory to create is in a authorized \Directory
	 * @throws Exception
	 */
	public static function create($path, $mode = 0755, $secure = true)
	{
		if ( $secure ) {
			if ( !Filesystem::limitDir($path) ) {
				throw new Exception(sprintf('NO ACCESS TO %s', $path));
			}
		}
		
		/* Dont create a directory with same name of a existing file */
		if ( file_exists($path) ) {
			throw new Exception(sprintf('%s IS EXISTING', $path));
		}

		/*
		 * Le "mode" ne fonctionne pas sous windows, ce qui oblige a faire un
		 * chmod par la suite.
		 * bool mkdir ( string pathname [, int mode [, bool recursive [, resource context]]] )
		 *
		 */
		if ( !is_dir($path) ) {
			$ret = mkdir($path, $mode, true);
			if ( $ret === false ) {
				$err = error_get_last();
				throw new Exception(sprintf('CREATION OF %s FAILED: %s', $path, $err['message']));
			}
			$ret = @chmod($path, $mode);
			/* !!chmod ne fonctionne pas avec les fichiers distants!! */
			if ( $ret === false ) {
				Rbplm::Log('Chmod impossible on ' . $path);
			}
			Rbplm::Log('Create directory ' . $path);
		}
		else {
			Rbplm::Log('Directory ' . $path . 'is yet existing ');
		}
	}

	/**
	 * Remove files from one \Directory to another one, including subdirectories and nonexisting or newer files.
	 * This function is a adaptation from the script of "....." visible in PHP help site.
	 *
	 * @param string $path
	 * @param boolean $recursive
	 * @param boolean $verbose
	 * @throws Exception
	 */
	public static function removeDir($path, $recursive = false, $verbose = false)
	{
		if ( !Filesystem::limitDir($path) ) {
			throw new Exception(sprintf('NO_ACCESS_TO_%s', $path));
		}

		/* Add trailing slash to $path if one is not there */
		if ( substr($path, -1, 1) != '/' ) {
			$path .= '/';
		}

		foreach( glob($path . '*') as $file ) {
			if ( is_file($file) === true ) {
				/* Remove each file in this Directory */
				if ( @unlink($file) === false ) {
					$err = error_get_last();
					throw new Exception(sprintf('DELETE FILE %s FAILED: %s', $file, $err['message']));
				}
				else {
					if ( $verbose ) Rbplm::Log("Removed File: $file");
				}
			}
			else if ( is_dir($file) === true && $recursive === true ) {
				/* If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it */
				self::removeDir($file, $recursive, $verbose);
			}
		}

		/* Remove Directory once Files have been removed (If Exists) */
		if ( is_dir($path) ) {
			if ( @rmdir($path) ) {
				if ( $verbose ) Rbplm::Log("Removed Directory: $path");
				return true;
			}
		}

		return false;
	}

	/**
	 * Remove files from one \Directory to another one, including subdirectories and put in a trash.
	 *
	 * @param string $dir
	 * @param boolean $verbose
	 * @param boolean $recursive
	 * @return boolean
	 */
	public static function putDirInTrash($dir, $verbose = false, $recursive = false)
	{
		$trashDir = self::$defaulTrashDir;

		/* Check if directories exists and are writable */
		if ( !is_dir($dir) || !is_dir($trashDir) || !is_writable($trashDir) ) {
			print ' error : ' . $dir . ' don\'t exist or is not writable <br />';
			print ' or    : ' . $trashDir . ' don\'t exist or is not writable <br />';
			return false;
		}

		/* Add original path to name of dstfile */
		$oriPath = self::encodePath(dirname($dir));

		/* Check if there is not conflict name in trash dir else rename it */
		$dstdir = $trashDir . '/' . $oriPath . '%&_' . basename($dir);
		$i = 0;
		if ( is_dir($dstdir) ) {
			/* Rename destination dir if exist */
			$renamedstdir = $dstdir;
			while( is_dir($renamedstdir) ) {
				$renamedstdir = $dstdir . '(' . $i . ')';
				$i++;
			}
			$dstdir = $renamedstdir;
		}

		/* Copy dir to trash */
		try {
			self::dircopy($dir, $dstdir, false, false, true, 0755);
			/* Suppress original dir */
			self::removeDir($dir, $recursive, $verbose);
		}
		catch( \Exception $e ) {
			throw new Exception(sprintf('error copying dir %s to Trash %s'), $dir, $dstdir);
		}
		return true;
	}

	/**
	 *
	 * @param string $path
	 * @param string $regex
	 *        	regular expression to verified by file name
	 * @return \RecursiveDirectoryIterator
	 */
	public static function getIterator($path, $regex = false)
	{
		if ( $regex ) {
			$iterator = new \RegexIterator(new \RecursiveDirectoryIterator($path), '/' . $regex . '/i', \RegexIterator::GET_MATCH);
		}
		else {
			$iterator = new \RecursiveDirectoryIterator($path);
		}
		return $iterator;
	}

	/**
	 * This method can be used to get files in directory.
	 * Return a array of files property if no errors, else return false.
	 *
	 * @param string $path
	 * @param \Rbplm\Sys\Filesystem\Filter $filter
	 * @return array|false
	 */
	public static function getDatas($path, $filter = null)
	{
		$c = self::getCollectionDatas($path, $filter);
		$r = array();
		isset($filter->displayMd5) ? $withMd5 = (bool)$filter->displayMd5 : $withMd5 = true;
		foreach( $c as $f ) {
			$r[] = $f->getProperties($withMd5);
		}
		return $r;
	}

	/**
	 * This method can be used to get files in directory.
	 * Return a collection of \Rbplm\Sys\Datatype\DatatypeInterface objects if no errors, else return false.
	 *
	 * @param string $path
	 * @param \Rbplm\Sys\Filesystem\Filter $filter
	 * @return array|false
	 */
	public static function getCollectionDatas($path, $filter = null)
	{
		if ( empty($path) ) {
			throw new Exception(sprintf('INVALID_PATH %s', $path));
		}

		$sortBy = 'name';
		$sortOrder = 'ASC';
		$limit = 0;
		$offset = 0;
		$regex = null;

		if ( $filter instanceof Filesystem\Filter ) {
			$regex = $filter->regexToString();
			$sortBy = $filter->getSort();
			$sortOrder = $filter->getOrder();
			$offset = $filter->getOffset();
			$limit = $offset + $filter->getLimit();
		}

		try {
			$it = new \DirectoryIterator($path);
			$return = array();
			$count = 0;
			foreach( $it as $file ) {
				if ( $it->isDot() || $it->isDir()) {
					continue;
				}
				if($file->getFilename()=='Thumbs.db'){
					continue;
				}
				if ( $regex ) {
					if ( !preg_match('/' . $regex . '/i', $file->getFilename()) ) {
						continue;
					}
				}
				if ( $offset && $count < $offset ) {
					$count++;
					continue;
				}
				$odata = Fsdata::_dataFactory($file->getPath() . '/' . $file->getFilename());
				if ( $odata ) {
					if ( $sortBy ) {
						/* key used to sort */
						$m = 'get' . ucfirst($sortBy);
						if(!method_exists($odata, $m)){
							$return[] = $odata;
							continue;
						}
						$key = call_user_func([
							$odata,
							'get' . ucfirst($sortBy)
						]);
						/* if it is a date */
						if ( is_a($key, 'DateTime') ) {
							$key = $key->format(\Rbplm\Sys\Date::$dateFormat);
						}
						$key = $key . '.' . $count;
						$return[$key] = $odata;
					}
					else {
						$return[] = $odata;
					}
					$count++;
				}
				if ( $limit && $count > $limit ) {
					break;
				}
			}
		}
		catch( \Exception $e ) {
			throw new Exception(sprintf('INVALID_PATH %s', $path));
		}

		if ( $sortOrder == 'DESC' ) {
			krsort($return);
		}
		else if ( $sortOrder == 'ASC' ) {
			ksort($return);
		}

		return array_values($return);
	}

	/**
	 * Alternative (peu performante) a disk_total_space qui ne comprend pas les chemins
	 * relatifs (sur windows)
	 * source du scripts : http://fr3.php.net/manual/fr/function.disk-total-space.php
	 *
	 * @param string $path
	 * @return integer
	 */
	public static function dskspace($path)
	{
		$s = filesize($path);
		$size = $s;
		if ( is_dir($path) ) {
			$dh = opendir($path);
			while( ($file = readdir($dh)) !== false ) {
				if ( $file != '.' and $file != '..' ) {
					$size += self::dskspace($path . '/' . $file);
				}
			}
			closedir($dh);
		}
		return $size;
	}
} /* End of class */
