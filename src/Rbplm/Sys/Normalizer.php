<?php
namespace Rbplm\Sys;

use Zend\Filter\FilterInterface;

class Normalizer implements FilterInterface
{

	/**
	 * Remove spaces and specials characters from the $string
	 *
	 * @param string $string
	 * @return string
	 */
	static function normalize($string)
	{
		/* Replaces all spaces with hyphens. */
		$string = str_replace(' ', '-', $string);
		/* Removes special chars. */
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		$string = strtolower($string);
		/* Replaces multiple hyphens with single one. */
		//return preg_replace('/-+/', '-', $string);
		return $string;
	}

	/**
	 * Returns the result of filtering $value
	 *
	 * @param  mixed $value
	 * @return mixed
	 */
	public function filter($value)
	{
		return self::normalize($value);
	}
}
