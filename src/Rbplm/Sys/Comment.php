<?php
//%LICENCE_HEADER%

namespace Rbplm\Sys;


/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */


/**
 * @brief Auto generated class Comment
 *
 * This is a class generated with \Rbplm\Model\\Generator.
 *
 * @see Rbplm/Sys/CommentTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Comment
{

    /**
     * @var string
     */
    protected $_uid = null;

    /**
     * @var string
     */
    protected $_commentedId = null;

    /**
     * @var string
     */
    protected $_title = null;

    /**
     * @var string
     */
    protected $_body = null;
    
    
    /**
     * Constructor.
     * 
     * @param array		$properties	Array of properties
     * @code
     * 		$properties = array(
     * 			'title' 		=> 'string', 
     * 			'body'			=> 'string',
     * 			'commentedId'	=> 'string', //Uuid of the object subject of the comment.
     * 		);
     * @endcode
     * 
     * @return void
     */
    public function __construct( $properties = array() )
    {
    	foreach($properties as $key=>$val){
    		$key = '_' . $key;
    		if( isset($key) ){
	    		$this->$key = $val;
    		}
    	}
    	if( !$this->uid ){
	    	$this->uid = \Rbplm\Uuid::newUid();
    	}
    }
    

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $Uid
     * @return void
     */
    public function setUid($Uid)
    {
        $this->uid = $Uid;
        return $this;
    }

    /**
     * @return string
     */
    public function getCommentedId()
    {
        return $this->_commentedId;
    }

    /**
     * @param string $CommentedId
     * @return void
     */
    public function setCommentedId($CommentedId)
    {
        $this->_commentedId = $CommentedId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param string $Title
     * @return void
     */
    public function setTitle($Title)
    {
        $this->_title = $Title;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @param string $Body
     * @return void
     */
    public function setBody($Body)
    {
        $this->_body = $Body;
        return $this;
    }


}

