<?php
//%LICENCE_HEADER%

namespace Rbplm\Sys;

/**
 * @brief Define a package of constants to set error code in Rbplm.
 *
 */
Class Error
{
	const ERROR = E_USER_ERROR;
	const WARNING = E_USER_WARNING;
	const NOTICE = E_USER_NOTICE;

	const BAD_CONNEXION_TYPE = 'BAD CONNEXION TYPE';
	const BAD_PARAMETER_OR_EMPTY = 'BAD PARAMETER OR EMPTY %s';
	const BAD_PARAMETER_TYPE = 'BAD PARAMETER TYPE %s';
	const BAD_UID = 'BAD UID';
	const CAN_NOT_CREATE_PATH = 'CAN NOT CREATE PATH %s';
	const CAN_NOT_BE_LOAD_FROM = 'CAN NOT BE LOAD FROM %s';
	const CAN_NOT_SUPPRESS_ADMIN = 'CAN NOT SUPPRESS ADMIN';
	const CLASS_CAN_NOT_BE_INSTANCIATE = 'CLASS CAN NOT BE INSTANCIATE';
	const CODE_INCLUSION_FAILED = 'CODE INCLUSION FAILED';
	const CONNEXION_IS_NOT_SET = 'CONNEXION IS NOT SET';
	const COPY_ERROR = 'COPY ERROR FROM %s TO %s';
	const DIRECTORY_CREATION_FAILED = 'DIRECTORY CREATION FAILED %s';
	const FS_METAMODEL_NOTFOUND = 'FS METAMODEL NOTFOUND';
	const FILE_NOT_FOUND = 'FILE NOT FOUND %s';
	const FUNCTION_IS_NOT_EXISTING = 'FUNCTION IS NOT EXISTING %s';
	const INVALID_DATA_SIZE = 'INVALID DATA SIZE %s';
	const INVALID_PATH = 'INVALID PATH %s';
	const SUPPRESSION_FAILED = 'SUPPRESSION FAILED';
	const UNABLE_TO_CREATE_DIR = 'UNABLE TO CREATE DIR %s';
	const DIRECTORY_IS_EXISTING = 'DIRECTORY %s IS EXISTING';
	const LIMIT_ACCESS_VIOLATION = 'LIMIT ACCESS VIOLATION';
	const LOCKED_TO_PREVENT_THIS_ACTION = 'LOCKED TO PREVENT THIS ACTION %s';
	const MF_APPFCT_NOT_EXISTING = 'MF APPFCT NOT EXISTING';
	const MF_APPKEY_NOT_EXISTING = 'MF APPKEY NOT EXISTING';
	const MF_SYSATT_NOT_EXISTING = 'MF SYSATT NOT EXISTING';
	const MF_SYSFCT_NOT_EXISTING = 'MF SYSFCT NOT EXISTING';
	const NONE_ASSOC_DOCFILES = 'NONE ASSOC DOCFILES';
	const NONE_FILTER_SET = 'NONE FILTER SET';
	const NONE_VALID_DOCTYPE = 'NONE VALID DOCTYPE';
	const NOT_ON_ROOT_OBJECT = 'NOT ON ROOT OBJECT';
	const NOT_RUNNING_PROCESS = 'NOT RUNNING PROCESS';
	const PROPERTY_IS_NOT_SET = 'PROPERTY %s IS NOT SET';
	const UNABLE_TO_FIND_OBJECT_UID = 'UNABLE TO FIND OBJECT UID';
	const UNABLE_TO_WAKUP_CONNEXION = 'UNABLE TO WAKUP CONNEXION';
	const USER_HAS_NO_RIGHT = 'USER HAS NO RIGHT';
	const VAR_NOT_SET = 'VAR NOT SET';
}

