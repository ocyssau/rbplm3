<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;

/*
 * SPL exception
 *
 * BadFunctionCallException
 * BadMethodCallException
 * DomainException
 * InvalidArgumentException
 * LengthException
 * LogicException
 * OutOfBoundsException
 * OutOfRangeException
 * OverflowException
 * RangeException
 * RuntimeException
 * UnderflowException
 * UnexpectedValueException
 */

/**
 * @brief \Exception for Rbplm.
 */
class Exception extends \Exception
{
}
