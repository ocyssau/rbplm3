<?php
//%LICENCE_HEADER%

namespace Rbplm\Sys\Message;

/**
 * @brief Sent message.
 */
class Sent extends \Rbplm\Sys\Message
{
	public static $classId = '569b7d17esent';
}
