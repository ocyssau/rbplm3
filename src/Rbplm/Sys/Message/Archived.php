<?php
//%LICENCE_HEADER%

namespace Rbplm\Sys\Message;

/**
 * @brief Archived message.
 */
class Archived extends \Rbplm\Sys\Message
{
	public static $classId = '569b7archived';
}
