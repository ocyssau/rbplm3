<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys\Message;

/**
 */
class Factory
{

	/**
	 *
	 * @var string
	 */
	public static $classId = '569factory';

	/**
	 *
	 * @var boolean
	 */
	protected $instance = false;

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		self::$instance = $this;
	}

	/**
	 *
	 * @param
	 *        	$name
	 * @return Factory
	 */
	public static function get($name)
	{
		return self::$instance;
	}

	/**
	 *
	 * @param
	 *        	\Rbplm\Sys\Message
	 * @return \Zend\Mail\Message
	 */
	public static function messageToMail($message)
	{
		$mail = new \Zend\Mail\Message();
		return $mail;
	}

	/**
	 *
	 * @param
	 *        	\Zend\Mail\Message
	 * @return \Rbplm\Sys\Message
	 */
	public static function mailToMessageToMail($mail)
	{
		$message = new \Rbplm\Sys\Message();
		return $message;
	}
}
