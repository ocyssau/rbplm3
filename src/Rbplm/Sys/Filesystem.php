<?php
// %LICENCE_HEADER%
namespace Rbplm\Sys;

/**
 * Filesystem on the server
 */
class Filesystem extends \Rbplm\Rbplm
{

	/**
	 * Array of paths where rbplm is authorized to write.
	 *
	 * @var array
	 */
	private static $_authorizedDir = array();

	/**
	 * Activate or not write limitations.
	 *
	 * @var boolean
	 */
	private static $_isSecure = true;

	/**
	 * Active/unactive the protection against write in not authorized directories
	 */
	static public function isSecure($bool)
	{
		if ( $bool === null ) {
			return self::$_isSecure;
		}
		else {
			return self::$_isSecure = (boolean)$bool;
		}
	}

	/**
	 *
	 * @param string $path
	 * @return void
	 */
	static public function addAuthorized($path)
	{
		$path = (string)$path;
		self::$_authorizedDir[] = $path;
	}

	/**
	 * Check if the request file is in a authorized directory.
	 * Return true if it is authorized else return false.
	 *
	 *
	 * @param string $path
	 *        	Fullpath to test
	 * @return boolean
	 */
	static function limitDir($path)
	{
		if ( self::$_isSecure == false ) {
			return true;
		}

		/* ".." , "//" "/./" forbidden in the path */
		if ( strpos("$path", '..') ) {
			throw new Exception('characters .. forbidden in path');
		}
		if ( strpos("$path", '//') ) {
			throw new Exception('characters // forbidden in path');
		}
		if ( strpos("$path", '/./') ) {
			throw new Exception('characters /./ forbidden in path');
		}
		$path = str_replace('\\', '/', $path);

		/* Check that the path is in a authorized directory */
		foreach( self::$_authorizedDir as $motif ) {
			if ( empty($motif) ) {
				continue;
			}

			$motif = str_replace('\\', '/', $motif);
			$motif = ltrim($motif, './');
			$path = ltrim($path, './');

			/* Check that the system directories are not directly manipulated */
			if ( rtrim($path, '/') == rtrim($motif, '/') ) {
				return false;
			}

			/* Check that the file is in a directory of the system */
			if ( strpos($path, $motif) === 0 ) {
				return true;
			}
		} /* End of foreach */

		throw new Exception(sprintf('LIMIT_ACCESS_VIOLATION %s', $path));
		return false;
	}

	/**
	 * Generation of uniq identifiant
	 *
	 * @return string
	 */
	static function uniqid()
	{
		return md5(uniqid(rand()));
	}

	/**
	 * Encode path
	 */
	static function encodePath($path)
	{
		$path = str_replace('/', '%2F', $path);
		$path = str_replace('.', '%2E', $path);
		$path = str_replace(':', '%3A', $path);
		$path = str_replace('\\', '%2F', $path);
		return $path;
	}

	/**
	 * Decode path
	 */
	static function decodePath($path)
	{
		$path = str_replace('%2F', '/', $path);
		$path = str_replace('%2E', '.', $path);
		$path = str_replace('%3A', ':', $path);
		return $path;
	}
} /* End of class */
