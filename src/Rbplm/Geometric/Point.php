<?php

namespace Rbplm\Geometric;

class Point extends \SplFixedArray
{
	/**
	 * @var string
	 */
	public static $classId = 's4eqgeompoint';

	/**
	 *
	 */
	public function __construct($array)
	{
		parent::__construct(3);
		$this[0]=array_shift($array);
		$this[1]=array_shift($array);
		$this[2]=array_shift($array);
	}

	/**
	 * @return float
	 */
	public function getX()
	{
		return $this[0];
	}

	/**
	 * @return float
	 */
	public function getY()
	{
		return $this[1];
	}

	/**
	 * @return float
	 */
	public function getZ()
	{
		return $this[2];
	}

}
