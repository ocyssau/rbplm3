<?php
namespace Rbplm\Measure;


class Converter
{
	/**
	 *
	 * @param float $value
	 * @param string $fromUnit A value of Weight Class Constants
	 */
	public function weight($value, $fromUnit = Weight::STANDARD)
	{
		if( $unit != Weight::STANDARD ){
			$unite = new Weight($value, $fromUnit);
			return $unite->convertTo(Weight::STANDARD);
		}
		else{
			return $value;
		}
	}

	public function density($value, $fromUnit = Density::STANDARD)
	{
		if( $fromUnit != Density::STANDARD ){
			$unite = new Density($value, $fromUnit);
			return $unite->convertTo(Density::STANDARD);
		}
		else{
			return $value;
		}
	}

	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function volume($value, $fromUnit = Volume::STANDARD)
	{
		if( $fromUnit != Volume::STANDARD ){
			$unite = new Volume($value, $fromUnit);
			return $unite->convertTo(Volume::STANDARD);
		}
		else{
			return $value;
		}
	}

	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function surface($value, $fromUnit = Area::STANDARD)
	{
		if( $fromUnit != Area::STANDARD ){
			$unite = new Area($value, $fromUnit);
			return $unite->convertTo(Area::STANDARD);
		}
		else{
			return $value;
		}
	}

	/**
	 * Setter
	 *
	 * @param SplFixedArray(3) $point
	 * @param string $unit
	 */
	public function point($point, $fromUnit = Length::STANDARD)
	{
		if( $fromUnit != Length::STANDARD ){
			$xunite = new Length($point[0], $fromUnit);
			$point[0] = $xunite->convertTo(Length::STANDARD);

			$yunite = new Length($point[1], $fromUnit);
			$point[1] = $yunite->convertTo(Length::STANDARD);

			$zunite = new Length($point[2], $fromUnit);
			$point[2] = $zunite->convertTo(Length::STANDARD);
		}
		return $point;
	}

	/**
	 * Setter
	 * @param float $x
	 * @param float $y
	 * @param float $z
	 * @param string $unit
	 */
	public function intertiaCenter($point, $fromUnit = 'mm4')
	{
		if( $fromUnit != Length::STANDARD ){
			$xunite = new Length($point[0], $fromUnit);
			$point[0] = $xunite->convertTo(Length::STANDARD);

			$yunite = new Length($point[1], $fromUnit);
			$point[1] = $yunite->convertTo(Length::STANDARD);

			$zunite = new Length($point[2], $fromUnit);
			$point[2] = $zunite->convertTo(Length::STANDARD);
		}
		return $point;
	}

}