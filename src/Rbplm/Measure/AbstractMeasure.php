<?php
namespace Rbplm\Measure;

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category Zend
 * @package Zend_Measure
 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license http://framework.zend.com/license/new-bsd New BSD License
 * @version $Id: Abstract.php 23775 2011-03-01 17:25:24Z ralph $
 */

/**
 * Abstract class for all measurements
 *
 * @category Zend
 * @package Zend_Measure
 * @subpackage Rbplm\Measure\Abstract
 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
abstract class AbstractMeasure
{

	/**
	 * Plain value in standard unit
	 *
	 * @var string $_value
	 */
	protected $_value;

	/**
	 * Original type for this unit
	 *
	 * @var string $_type
	 */
	protected $_type;

	/**
	 * Locale identifier
	 *
	 * @var string $_locale
	 */
	protected $_locale = null;

	/**
	 * Unit types for this measurement
	 */
	protected $_units = array();

	/**
	 * Rbplm\Measure\Abstract is an abstract class for the different measurement types
	 *
	 * @param mixed $value
	 *        	Value as string, integer, real or float
	 * @param int $type
	 *        	OPTIONAL a measure type f.e. Rbplm\Measure\Length::METER
	 * @param string $locale
	 *        	OPTIONAL a Zend_Locale Type
	 * @throws Exception
	 */
	public function __construct($value, $type = null, $locale = null)
	{
		if ( $type === null ) {
			$type = $this->_units['STANDARD'];
		}

		if ( isset($this->_units[$type]) === false ) {
			throw new Exception(sprintf('Type (%s) is unknown', $type));
		}

		$this->setValue($value, $type, $this->_locale);
	}

	/**
	 * Returns the actual set locale
	 *
	 * @return string
	 */
	public function getLocale()
	{
		return $this->_locale;
	}

	/**
	 * Sets a new locale for the value representation
	 *
	 * @param string $locale
	 *        	(Optional) New locale to set
	 * @param boolean $check
	 *        	False, check but don't set; True, set the new locale
	 * @return AbstractMeasure
	 */
	public function setLocale($locale = null, $check = false)
	{
		if ( !$check ) {
			$this->_locale = (string)$locale;
		}
		return $this;
	}

	/**
	 * Returns the internal value
	 *
	 * @param integer $round
	 *        	(Optional) Rounds the value to an given precision,
	 *        	Default is -1 which returns without rounding
	 * @param string $locale
	 *        	(Optional) Locale for number representation
	 * @return integer|string
	 */
	public function getValue($round = -1, $locale = null)
	{
		if ( $round < 0 ) {
			$return = $this->_value;
		}
		else {
			$return = round($this->_value, $round);
		}
		return $return;
	}

	/**
	 * Set a new value
	 *
	 * @param integer|string $value
	 *        	Value as string, integer, real or float
	 * @param string $type
	 *        	OPTIONAL A measure type f.e. Rbplm\Measure\Length::METER
	 * @param string $locale
	 *        	OPTIONAL Locale for parsing numbers
	 * @throws Exception
	 * @return AbstractMeasure
	 */
	public function setValue($value, $type = null, $locale = null)
	{
		$type = $this->_units['STANDARD'];

		if ( empty($this->_units[$type]) ) {
			throw new Exception(sprintf('Type (%s) is unknown', $type));
		}

		$this->_value = $value;
		$this->setType($type);
		return $this;
	}

	/**
	 * Returns the original type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->_type;
	}

	/**
	 * Set a new type, and convert the value
	 *
	 * @param string $type
	 *        	New type to set
	 * @throws Exception
	 * @return AbstractMeasure
	 */
	public function setType($type)
	{
		if ( empty($this->_units[$type]) ) {
			throw new Exception(sprintf('Type (%s) is unknown', $type));
		}

		if ( empty($this->_type) ) {
			$this->_type = $type;
		}
		else {
			// Convert to standard value
			$value = $this->_value;
			if ( is_array($this->_units[$this->getType()][0]) ) {
				foreach( $this->_units[$this->getType()][0] as $key => $found ) {
					switch ($key) {
						case "/":
							if ( $found != 0 ) {
								$value = bcdiv($value, $found, 25);
							}
							break;
						case "+":
							$value = bcadd($value, $found, 25);
							break;
						case "-":
							$value = bcsub($value, $found, 25);
							break;
						default:
							$value = bcmul($value, $found, 25);
							break;
					}
				}
			}
			else {
				$value = bcmul($value, $this->_units[$this->getType()][0], 25);
			}

			/* Convert to expected value */
			if ( is_array($this->_units[$type][0]) ) {
				foreach( array_reverse($this->_units[$type][0]) as $key => $found ) {
					switch ($key) {
						case "/":
							$value = bcmul($value, $found, 25);
							break;
						case "+":
							$value = bcsub($value, $found, 25);
							break;
						case "-":
							$value = bcadd($value, $found, 25);
							break;
						default:
							if ( $found != 0 ) {
								$value = bcdiv($value, $found, 25);
							}
							break;
					}
				}
			}
			else {
				$value = bcdiv($value, $this->_units[$type][0], 25);
			}

			$slength = strlen($value);
			$length = 0;
			for ($i = 1; $i <= $slength; ++$i) {
				if ( $value[$slength - $i] != '0' ) {
					$length = 26 - $i;
					break;
				}
			}

			$this->_value = round($value, $length);
			$this->_type = $type;
		}
		return $this;
	}

	/**
	 * Compare if the value and type is equal
	 *
	 * @param AbstractMeasure $object
	 *        	object to compare
	 * @return boolean
	 */
	public function equals($object)
	{
		if ( (string)$object == $this->toString() ) {
			return true;
		}

		return false;
	}

	/**
	 * Returns a string representation
	 *
	 * @param integer $round
	 *        	(Optional) Runds the value to an given exception
	 * @param string $locale
	 *        	(Optional) Locale to set for the number
	 * @return string
	 */
	public function toString($round = -1, $locale = null)
	{
		if ( $locale === null ) {
			$locale = $this->_locale;
		}

		return $this->getValue($round, $locale) . ' ' . $this->_units[$this->getType()][1];
	}

	/**
	 * Returns a string representation
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->toString();
	}

	/**
	 * Returns the conversion list
	 *
	 * @return array
	 */
	public function getConversionList()
	{
		return $this->_units;
	}

	/**
	 * Alias function for setType returning the converted unit
	 *
	 * @param string $type
	 *        	Constant Type
	 * @param integer $round
	 *        	(Optional) Rounds the value to a given precision
	 * @param string $locale
	 *        	(Optional) Locale to set for the number
	 * @return string
	 */
	public function convertTo($type, $round = 2, $locale = null)
	{
		$this->setType($type);
		return $this->toString($round, $locale);
	}

	/**
	 * Adds an unit to another one
	 *
	 * @param AbstractMeasure $object
	 *        	object of same unit type
	 * @return AbstractMeasure
	 */
	public function add($object)
	{
		$object->setType($this->getType());
		$value = $this->getValue(-1) + $object->getValue(-1);

		$this->setValue($value, $this->getType(), $this->_locale);
		return $this;
	}

	/**
	 * Substracts an unit from another one
	 *
	 * @param AbstractMeasure $object
	 *        	object of same unit type
	 * @return AbstractMeasure
	 */
	public function sub($object)
	{
		$object->setType($this->getType());
		$value = $this->getValue(-1) - $object->getValue(-1);

		$this->setValue($value, $this->getType(), $this->_locale);
		return $this;
	}

	/**
	 * Compares two units
	 *
	 * @param AbstractMeasure $object
	 *        	object of same unit type
	 * @return boolean
	 */
	public function compare($object)
	{
		$object->setType($this->getType());
		$value = $this->getValue(-1) - $object->getValue(-1);

		if ( $value < 0 ) {
			return -1;
		}
		else if ( $value > 0 ) {
			return 1;
		}

		return 0;
	}
}
