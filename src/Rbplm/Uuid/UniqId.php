<?php
// %LICENCE_HEADER%
namespace Rbplm\Uuid;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 *
 * @author Olivier CYSSAU
 *
 */
class UniqId implements GeneratorInterface
{

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid()
	{
		return uniqid();
	}

	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * @param
	 *        	string uuid $uuid
	 * @return string uuid
	 */
	public static function format($uuid)
	{
		return $uuid;
	}
}
