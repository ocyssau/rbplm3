<?php
// %LICENCE_HEADER%
namespace Rbplm\Uuid;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 *
 * Only for windows plateforme.
 *
 * @see http://php.net/manual/fr/com.installation.php
 * @see http://php.net/manual/function.com-create-guid.php
 * @author Olivier CYSSAU
 *
 */
class ComUuid implements GeneratorInterface
{

	protected $uuid;

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid()
	{
		return self::format(com_create_guid());
	}

	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * if $format = 1 [DEFAULT]
	 * A Rbplm Uuid is in lower case without starting'{' and ending '}'.
	 *
	 * if $format = 2
	 * A Rbplm Uuid is in upper case with starting'{' and ending '}'.
	 *
	 * @param
	 *        	string uuid $uuid
	 * @param
	 *        	string integer $format php-uuid|com
	 * @return string uuid
	 */
	public static function format($uuid)
	{
		$uuid = strtolower(trim($uuid, '{}'));
		return $uuid;
	}
}
