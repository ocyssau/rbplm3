<?php
// %LICENCE_HEADER%
namespace Rbplm\Uuid;

/**
 *
 *
 */
interface GeneratorInterface
{

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid();

	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * @param string uuid $uuid
	 * @return string uuid
	 */
	public static function format($uuid);
}
