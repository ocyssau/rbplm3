<?php
// %LICENCE_HEADER%
namespace Rbplm\Uuid;

/**
 * @brief This class enables you to get uuids with a pseudo random function
 *
 * @author Olivier CYSSAU
 *
 */
class EmulateUuid implements GeneratorInterface
{

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public function newUid()
	{
		$chars = md5(uniqid(mt_rand(), true));
		$uuid = substr($chars, 0, 8) . '-';
		$uuid .= substr($chars, 8, 4) . '-';
		$uuid .= substr($chars, 12, 4) . '-';
		$uuid .= substr($chars, 16, 4) . '-';
		$uuid .= substr($chars, 20, 12);
		return $uuid;
	}

	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * if $format = 1 [DEFAULT]
	 * A Rbplm Uuid is in lower case without starting'{' and ending '}'.
	 *
	 * if $format = 2
	 * A Rbplm Uuid is in upper case with starting'{' and ending '}'.
	 *
	 * @param
	 *        	string uuid $uuid
	 * @param
	 *        	string integer $format php-uuid|com
	 * @return string uuid
	 */
	public static function format($uuid)
	{
		$uuid = strtolower(trim($uuid, '{}'));
		return $uuid;
	}
}
