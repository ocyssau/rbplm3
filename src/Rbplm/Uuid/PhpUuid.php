<?php
// %LICENCE_HEADER%
namespace Rbplm\Uuid;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 *
 * Note you need php-uuid installed.
 * @code
 * //On ubuntu, do simply
 * sudo apt-get install php5-uuid
 * @endcode
 *
 * @see http://fr.wikipedia.org/wiki/Universal_Unique_Identifier
 * @author Marius Karthaus
 * @author Eric COSNARD
 * @author Olivier CYSSAU
 *
 */
class PhpUuid implements GeneratorInterface
{

	protected $uuidobject;

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid()
	{
		$uuid = new self();
		return $uuid->v4();
	}

	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 *
	 * if $format = 1 [DEFAULT]
	 * A Rbplm Uuid is in lower case without starting'{' and ending '}'.
	 *
	 * if $format = 2
	 * A Rbplm Uuid is in upper case with starting'{' and ending '}'.
	 *
	 * @param
	 *        	string uuid $uuid
	 * @param
	 *        	string integer $format php-uuid|com
	 * @return string uuid
	 */
	public static function format($uuid)
	{
		$uuid = strtolower(trim($uuid, '{}'));
		return $uuid;
	}

	/**
	 * On long running deamons i've seen a lost resource.
	 * This checks the resource and creates it if needed.
	 */
	protected function create()
	{
		if ( !is_resource($this->uuidobject) ) {
			uuid_create($this->uuidobject);
		}
	}

	/**
	 * Return a type 1 (MAC address and time based) uuid
	 *
	 * @return String
	 */
	protected function v1()
	{
		$this->create();
		$uuidstring = '';
		uuid_make($this->uuidobject, UUID_MAKE_V1);
		uuid_export($this->uuidobject, UUID_FMT_STR, $uuidstring);
		return trim($uuidstring);
	}

	/**
	 * Return a type 4 (random) uuid
	 *
	 * @return String
	 */
	protected function v4()
	{
		$this->create();
		$uuidstring = '';
		uuid_make($this->uuidobject, UUID_MAKE_V4);
		uuid_export($this->uuidobject, UUID_FMT_STR, $uuidstring);
		return trim($uuidstring);
	}

	/**
	 * Return a type 5 (SHA-1 hash) uuid
	 *
	 * @return String
	 */
	protected function v5()
	{
		$this->create();
		$uuidstring = '';
		uuid_make($this->uuidobject, UUID_MAKE_V5);
		uuid_export($this->uuidobject, UUID_FMT_STR, $uuidstring);
		return trim($uuidstring);
	}
}
