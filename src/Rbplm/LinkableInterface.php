<?php
//%LICENCE_HEADER%

namespace Rbplm;

/**
 * @brief If linkable, object may be add to \Rbplm\Model\Collection.
 *
 */
interface LinkableInterface
{
	
	/**
	 * 
	 * Getter
	 * @return String
	 */
	public function getUid();
	
	/**
	 * 
	 * Getter
	 * @return String
	 */
	public function getName();
}
