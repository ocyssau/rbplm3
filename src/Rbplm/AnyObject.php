<?php
// %LICENCE_HEADER%
namespace Rbplm;

use Rbplm\Sys\Date as DateTime;

/**
 * @brief anyobject is just one element in composite object chain.
 *
 * Implement design pattern COMPOSITE OBJECT.
 *
 * Example and tests: RbplmTest/Model/AnyTest.php
 */
class AnyObject extends Any
{
	use LifeControl, Owned, Bewitched;

	/**
	 * Must be define in children classes
	 *
	 * @var integer
	 */
	static $classId = 'aaaa0000bbbbf';

	/**
	 * Array of not declared properties.
	 * Keys are properties name, values are properties values.
	 *
	 * @var array
	 */

	/**
	 *
	 * @param array|string $properties
	 *        	properties or name
	 * @param
	 *        	$parent
	 * @return void
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			$this->hydrate($properties);
		}

		$this->cid = static::$classId;
	}

	/**
	 *
	 * @param string $name
	 * @return AnyObject
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->updated = new DateTime();
		$obj->created = new DateTime();

		if ( !$name ) {
			$name = uniqid();
		}
		$obj->setName($name);

		$obj->ownerId = People\User::SUPER_USER_ID;
		$obj->createById = People\User::SUPER_USER_ID;
		$obj->updateById = People\User::SUPER_USER_ID;

		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		$this->lifeControlHydrate($properties);
		$this->ownedHydrate($properties);
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		return $this;
	}
} /* End of class */

