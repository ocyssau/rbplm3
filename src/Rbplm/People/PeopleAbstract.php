<?php
// %LICENCE_HEADER%
namespace Rbplm\People;

use Rbplm\AnyPermanent;

/**
 * @brief Base class for all people objects.
 *
 * @verbatim
 * @endverbatim
 */
abstract class PeopleAbstract extends AnyPermanent
{

	/**
	 *
	 * @param \Rbplm\People\Group $group
	 * @return \Rbplm\People\PeopleAbstract
	 */
	public function addGroup($group)
	{
		$this->groups[$group->getUid()] = $group;
		return $this;
	}

	/**
	 * Get all groups of the current User
	 *
	 * @return Group
	 */
	public function getGroups()
	{
		return $this->groups;
	}
} /* End of class */
