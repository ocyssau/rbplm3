<?php
//%LICENCE_HEADER%

namespace Rbplm\People;

/**
 * @brief \Exception for Rbplm.
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class Exception extends \Rbplm\Sys\Exception
{
}
