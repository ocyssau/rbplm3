<?php
//%LICENCE_HEADER%

namespace Rbplm\People;

/**
 * @brief Group definition.
 *
 * Example and tests: Rbplm/People/GroupTest.php
 *
 */
class Group extends PeopleAbstract
{

	const SUPER_GROUP_UUID = '99999999-9999-9999-9999-00000000cdef';

	/**
	 * @var integer
	 */
	static $classId = '569e9459a1e71';

	/**
	 *
	 * @var string
	 */
	protected $description;

	/**
	 *
	 * @var array
	 */
	protected $groups;

	/**
	 *
	 * @var array
	 */
	protected $users;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Group
	 */
	public function hydrate( array $properties )
	{
	    /* Any */
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['parentId'])) ? $this->parentId=$properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid=$properties['parentUid'] : null;
		/* Group */
		(isset($properties['description'])) ? $this->description=$properties['description'] : null;
		return $this;
	}


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription( $description )
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return Group
     */
    public function addUser($obj)
    {
    	$this->users[$obj->getUid()] = $obj;
    	return $this;
    }


} /* End of class */
