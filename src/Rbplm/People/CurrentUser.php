<?php
// %LICENCE_HEADER%
namespace Rbplm\People;


/**
 * @brief Current connected user.
 *
 * Example and tests: Rbplm/People/UserTest.php
 */
class CurrentUser extends User
{

	/**
	 * Singleton pattern
	 *
	 * @var User
	 */
	protected static $_instance;

	/**
	 * Constructor must not be directly call, use singleton method instead.
	 */
	function __construct()
	{
		CurrentUser::$_instance = $this;
		//throw new Exception(sprintf('CLASS_%s_CAN_NOT_BE_INSTANCIATE', get_class($this)));
	}

	/**
	 * Set and get user object with current User datas
	 *
	 * this method return always the same instance of class \Rbplm\People\User. Its a singlton method
	 * so you can call it many time in your code without overload server.
	 *
	 * @todo : revoir avec serialisation dans la session de l'objet
	 * @return CurrentUser
	 */
	public static function get()
	{
		if ( !self::$_instance ) {
			$user = new CurrentUser();
			$user->login = 'anonymous';
		}
		return self::$_instance;
	}

	/**
	 * Set the current user instance.
	 *
	 * @param User $user
	 * @return void
	 */
	public static function set(User $user)
	{
		return self::$_instance = $user;
	}
	
}/*End of class */
