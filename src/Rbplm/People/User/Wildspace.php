<?php
// %LICENCE_HEADER%
namespace Rbplm\People\User;

use Rbplm\Sys\Directory;
use Rbplm\Sys\Datatype\File;
use Rbplm\People\User;
use Rbplm\Owned;

/**
 * @brief Wildspace is the user working directory.
 *
 * Wildspace directory must be writable by the user.
 * The files to store in ranchbe must be copy in the Wildspace before.
 *
 * Example and tests: Rbplm/People/UserTest.php
 */
class Wildspace extends Directory
{
	use Owned;

	/**
	 *
	 * @var Wildspace
	 */
	protected static $instances;

	/**
	 *
	 * @var integer
	 */
	static $classId = '569e94e20b878';

	/**
	 * Path to wilspace of user
	 * May contains keywords :
	 * %login%
	 * %uid%
	 * %id%
	 * %firstname%
	 * %lastname%
	 *
	 * @var string
	 */
	public static $basePath;

	/**
	 *
	 * @param User $user
	 * @param String $path
	 * @return void
	 */
	public function __construct(User $user, $path = null)
	{
		/* construct the path to the personnal working directory for the current user. */
		if ( !$path ) {
			$path = str_replace('%login%', $user->getLogin(), self::$basePath);
			$path = str_replace('%id%', $user->getId(), $path);
			$path = str_replace('%uid%', $user->getUid(), $path);
			$path = str_replace('%firstname%', $user->getFirstname(), $path);
			$path = str_replace('%lastname%', $user->getLastname(), $path);
		}
		
		$this->setOwner($user);
		$this->setPath($path);
		self::$instances[$user->getId()] = $this;
		$user->wildspace = $this;
	}

	/**
	 * Get the wildspace of the the user
	 *
	 * @param User $user
	 * @param string $path
	 * @return Wildspace
	 */
	public static function get($user, $path = null)
	{
		if ( !isset(self::$instances[$user->getId()]) ) {
			self::$instances[$user->getId()] = new self($user, $path);
		}
		return self::$instances[$user->getId()];
	}

	/**
	 * Copy a upload file in Wildspace.
	 * Return true or false.
	 *
	 * Take the parameters of the uploaded file(from array $file) and create the file in the wildpspace.
	 *
	 * @param array $file
	 *        	:
	 *        	$file[name](string) name of the uploaded file
	 *        	$file[type](string) mime type of the file
	 *        	$file[tmp_name](string) temp name of the file on the server
	 *        	$file[error](integer) error code if error occured during transfert(0=no error)
	 *        	$file[size](integer) size of the file in octets
	 *
	 * @param boolean $replace
	 *        	if true and if the file exist in the Wildspace, it will be replaced by the uploaded file.
	 *
	 * @return File
	 */
	public function uploadFile($file, $replace = false)
	{
		return File::upload($file, $replace, $this->path);
	}
}
