<?php
// %LICENCE_HEADER%
namespace Rbplm\People\User;

use Rbplm\AnyPermanent;
use Rbplm\Owned;

/**
 *
 * @brief Manage user Preferences.
 *
 * Example and tests: Rbplm/People/UserTest.php
 *
 */
class Preference extends AnyPermanent
{

	use Owned;

	/**
	 *
	 * @var integer
	 */
	static $classId = '569b63eb7cce4';

	/**
	 * Array of default Preferences.
	 *
	 * @var array
	 */
	protected $defaults = array();

	/**
	 * Array result Preferences to apply
	 *
	 * @var array
	 */
	protected $preferences = array();

	/**
	 * True to get prefs from user profile, else set always to default values.
	 *
	 * @var boolean
	 */
	protected $isEnable = true;

	/**
	 * Magic method.
	 * Set the preference value from his name.
	 *
	 * @param
	 *        	string
	 * @param
	 *        	mixed
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->preferences[$name] = $value;
	}

	/**
	 * Magic method.
	 * Get the preference value from his name.
	 *
	 * @param
	 *        	string
	 * @return string
	 */
	public function __get($name)
	{
		if ( !$this->isEnable ) {
			return $this->defaults[$name];
		}
		else {
			return $this->preferences[$name];
		}
	}

	/**
	 *
	 * @param
	 *        	array Default preferences
	 */
	public function __construct($default = array())
	{
		parent::__construct();

		if ( $default ) {
			$this->defaults = $default;
		}
		else {
			$this->defaults = array(
				'cssSheet' => 'default',
				'lang' => 'default',
				'longDateFormat' => 'default',
				'shortDateFormat' => 'default',
				'dateInputMethod' => 'default',
				'hourFormat' => 'default',
				'timeZone' => 'default',
				'maxRecord' => 50
			);
		}
		$this->preferences = $this->defaults;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Preference
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['cssSheet'])) ? $this->preferences['cssSheet'] = $properties['cssSheet'] : null;
		(isset($properties['lang'])) ? $this->preferences['lang'] = $properties['lang'] : null;
		(isset($properties['longDateFormat'])) ? $this->preferences['longDateFormat'] = $properties['longDateFormat'] : null;
		(isset($properties['shortDateFormat'])) ? $this->preferences['shortDateFormat'] = $properties['shortDateFormat'] : null;
		(isset($properties['dateInputMethod'])) ? $this->preferences['dateInputMethod'] = $properties['dateInputMethod'] : null;
		(isset($properties['hourFormat'])) ? $this->preferences['hourFormat'] = $properties['hourFormat'] : null;
		(isset($properties['timeZone'])) ? $this->preferences['timeZone'] = $properties['timeZone'] : null;
		(isset($properties['maxRecord'])) ? $this->preferences['maxRecord'] = $properties['maxRecord'] : null;
		return $this;
	}

	/**
	 * method of Serializable interface
	 *
	 * @return array
	 */
	public function __serialize()
	{
		$return = array_merge($this->preferences, array(
			'id' => $this->id,
			'uid' => $this->uid,
			'cid' => $this->cid,
			'name' => $this->name,
			'ownerId' => $this->ownerId
		));
		return $return;
	}

	/**
	 * Enable the load of the user preferences.
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isEnable($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isEnable = $bool;
		}
		else {
			return $this->isEnable;
		}
	}

	/**
	 * Get all Preferences of current user.
	 *
	 * @return array
	 */
	public function getPreferences()
	{
		if ( !$this->isEnable ) {
			return $this->defaults;
		}
		else {
			return $this->preferences;
		}
	}

	/**
	 */
	public function setPreference($name, $value)
	{
		$this->preferences[$name] = $value;
		return $this;
	}

	/**
	 */
	public function setPreferences(array $prefs)
	{
		$this->preferences = $prefs;
		return $this;
	}
} /* End of class */
