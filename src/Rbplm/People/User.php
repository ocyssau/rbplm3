<?php
// %LICENCE_HEADER%
namespace Rbplm\People;

use DateTime;

/**
 * User definition.
 *
 * Example and tests: Rbplm/People/UserTest.php
 *
 * @property People\Wildspace $wildspace
 * @property People\Preference $preference
 */
class User extends PeopleAbstract implements UserInterface
{

	static $classId = '569e94984ded3';

	const SUPER_USER_UUID = '99999999-9999-9999-9999-00000000abcd';

	const ANONYMOUS_USER_UUID = '99999999-9999-9999-9999-99999999ffff';

	const SUPER_USER_ID = 1;

	const ANONYMOUS_USER_ID = 2;
	
	const CONN_AUTO_DETECT = 'auto';
	
	const CONN_FROM_LOCAL = 'local';
	
	const CONN_FROM_DISTANT = 'distant';
	
	/**
	 *
	 * @var boolean
	 */
	protected $active = null;

	/**
	 *
	 * @var DateTime
	 */
	protected $lastLogin = null;

	/**
	 *
	 * @var string
	 */
	protected $login = null;

	/**
	 *
	 * @var string
	 */
	protected $firstname = null;

	/**
	 *
	 * @var string
	 */
	protected $lastname = null;

	/**
	 *
	 * @var string
	 */
	protected $password = null;

	/**
	 *
	 * @var string
	 */
	protected $mail = null;

	/**
	 *
	 * @var enum('db', 'ldap')
	 */
	public $authFrom = 'db';

	/**
	 * Option setted in user definition
	 * 
	 * @var enum('auto', 'local', 'distant')
	 */
	public $connFrom = 'auto';
	
	/**
	 *
	 * @var array
	 */
	protected $extends = [];
	
	/**
	 *
	 * @param array $properties
	 * @param \Rbplm\Any $parent
	 * @return void
	 */
	public function __construct(array $properties = null, $parent = null)
	{
		parent::__construct($properties, $parent);
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return User
	 */
	public function hydrate(array $properties)
	{
		// Any
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		
		// People
		(isset($properties['firstname'])) ? $this->firstname = $properties['firstname'] : null;
		(isset($properties['lastname'])) ? $this->lastname = $properties['lastname'] : null;
		(isset($properties['mail'])) ? $this->mail = $properties['mail'] : null;
		(isset($properties['password'])) ? $this->password = $properties['password'] : null;
		(isset($properties['login'])) ? $this->login = $properties['login'] : null;
		(isset($properties['authFrom'])) ? $this->authFrom = $properties['authFrom'] : null;
		(isset($properties['connFrom'])) ? $this->connFrom = $properties['connFrom'] : null;
		(isset($properties['active'])) ? $this->active = $properties['active'] : null;
		(isset($properties['isActive'])) ? $this->active = $properties['isActive'] : null;
		(isset($properties['extends'])) ? $this->extends = $properties['extends'] : null;
		
		//Date
		(isset($properties['lastLogin'])) ? $this->setLastLogin(new DateTime($properties['lastLogin'])) : null;
		
		return $this;
	}
	
	/**
	 * @param string $name
	 * @param string $value
	 * @return \Rbplm\People\User
	 */
	public function setExtend($name, $value)
	{
		$this->extends[$name] = $value;
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getExtend($name)
	{
		isset($this->extends[$name]) ? $r = $this->extends[$name] : $r = null;
		return $r;
	}
	
	/**
	 * @return array
	 */
	public function getExtends()
	{
		return $this->extends;
	}
	
	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActive($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->active = $bool;
		}
		else {
			return $this->active;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getLogin()
	{
		return $this->login;
	}

	/**
	 * Set the User name
	 *
	 * @param string $login
	 * @return User
	 */
	public function setLogin($login)
	{
		$this->login = trim($login);
		return $this;
	}

	/**
	 * @param DateTime $date
	 * @return User
	 */
	public function setLastLogin(DateTime $date = null)
	{
		$this->lastLogin = $date;
		return $this;
	}

	/**
	 *
	 * @param string $format
	 *        	[optional]
	 * @return DateTime
	 */
	public function getLastLogin($format = null)
	{
		if ( !$this->lastLogin instanceof DateTime ) {
			$this->lastLogin = new DateTime();
		}
		
		if ( $format ) {
			return $this->lastLogin->format($format);
		}
		else {
			return $this->lastLogin;
		}
	}

	/**
	 * @param string $string 'ldap'|'db'
	 * @return User
	 */
	public function setAuthFrom($string)
	{
		$this->authFrom = $string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 *
	 * @param string $password
	 * @return User
	 */
	public function setPassword($password)
	{
		$this->password = trim($password);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getMail()
	{
		return $this->mail;
	}

	/**
	 *
	 * @param string $mail
	 * @return User
	 */
	public function setMail($mail)
	{
		$this->mail = trim($mail);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getFullName()
	{
		return $this->firstname . ' ' . $this->lastname;
	}

	/**
	 *
	 * @return string
	 */
	public function getFirstname()
	{
		return $this->firstname;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setFirstname($string)
	{
		$this->firstname = $string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLastname()
	{
		return $this->lastname;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setLastname($string)
	{
		$this->lastname = $string;
		return $this;
	}
} /*End of class*/
