<?php
// %LICENCE_HEADER%
namespace Rbplm\People;

/**
 * @brief user definition.
 *
 * Example and tests: Rbplm/People/UserTest.php
 */
interface UserInterface
{

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActive($bool = null);

	/**
	 *
	 * @return string
	 */
	public function getLogin();

	/**
	 * Set the User name
	 *
	 * @param string $Login
	 * @return UserInterface
	 */
	public function setLogin($login);

	/**
	 *
	 * @param \DateTime $date
	 * @return User
	 */
	public function setLastLogin(\DateTime $date);

	/**
	 *
	 * @param string $format
	 *        	[optional]
	 * @return \DateTime
	 */
	public function getLastLogin($format = null);

	/**
	 *
	 * @return string
	 */
	public function getPassword();

	/**
	 *
	 * @param string $password
	 * @return UserInterface
	 */
	public function setPassword($password);

	/**
	 * Set group of the current User
	 *
	 * @return UserInterface
	 */
	public function addGroup($group);

	/**
	 * Get all groups of the current User
	 *
	 * @return Group
	 */
	public function getGroups();
} /*End of class*/
