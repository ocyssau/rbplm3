#!/usr/bin/php
<?php

//namespace Rbplmcli;

use Rbplm\Dao\Connexion As Connexion;
use Rbplm\Dao\Factory As DaoFactory;
use Rbplm\Dao\Schemas\SqlExtractor;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Pg\ClassDao;

use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Sys\Trash;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;

use Rbplm\AnyObject;
use Rbplm\Org;

/**
 *
 */
function rbplmcli_displayhelp()
{
	$display = 'Utilisation :

	php tests.php
	[-c <class>]
	[-m <method>]
	[-a]
	[--nodao,]
	[--croot,]
	[-i]
	[-lf]
	[-lc]
	[--doctype]
	[-help, -h, -?]

	Avec:
	-c <class>
	: Le nom de la classe de test a executer. Il peut y avoir plusieur option -c

	-m <method>
	: La methode a executer, ou si omis, execute tous les tests de la classe.

	-a
	: Execute tous les tests, annule l\'option -c

	-t
	: Execute mes tests, annule l\'option -c
	
	-p
	: Active le profiling des tests

	--nodao
	: Ignore les methodes testDao*

	--croot
	: Creer le noeud root

	-i
	: initialise les données de test.

	--initdb
	: initialise les données de test.

	--lf
	: Affiche la liste des functions de test disponibles

	--lc
	: Affiche la liste des classes de test disponibles

	-s
	: Extrait les scripts sql depuis les classes

	-h
	: Affiche cette aide.

	Examples:
	Execute tous les tests de la classe \Rbplm\AnyObjectTest et Rbplm\Org\Test:
	php tests.php -c \Rbplm\AnyObjectTest -c Rbplm\Org\Test

	Execute la methode \Rbplm\AnyObjectTest::testDao
	php tests.php -c \Rbplm\AnyObjectTest -m testDao

	Avec les options -help, -h vous obtiendrez cette aide

	-t : Execute la function test <fonction> parmis les fonctions suivantes :';
	echo $display;
	rbplmcli_displaylisttest();
}

function rbplmcli_displaylisttest()
{
	echo $display='

	';
}

/**
 *
 */
function rbplmcli_run()
{
    chdir(__DIR__);
    
    /*
	set_include_path(get_include_path() . PATH_SEPARATOR . realpath(__DIR__ . '/vendor/'));
	spl_autoload_register(function ($class) {
		include (str_replace('\\', '/', $class . '.php'));
	});
	*/
    
	error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
	ini_set ( 'display_errors', 1 );
	ini_set ( 'display_startup_errors', 1 );

	ini_set('xdebug.collect_assignments', 1);
	ini_set('xdebug.collect_params', 4);
	ini_set('xdebug.collect_return', 1);
	ini_set('xdebug.collect_vars', 1);
	ini_set('xdebug.var_display_max_children', 1000);
	ini_set('xdebug.var_display_max_data', 5000);
	ini_set('xdebug.var_display_max_depth', 10);

	$loader = include 'vendor/autoload.php';
	$loader->set('Rbplm', realpath('./module/Rbplm/src'));
	$loader->set('Rba', realpath('./vendor/'));
	
	/*
	ASSERT_ACTIVE 	assert.active 	1 	active l'évaluation de la fonction assert()
	ASSERT_WARNING 	assert.warning 	1 	génére une alerte PHP pour chaque assertion fausse
	ASSERT_BAIL 	assert.bail 	0 	termine l'exécution en cas d'assertion fausse
	ASSERT_QUIET_EVAL 	assert.quiet_eval 	0 	désactive le rapport d'erreur durant l'évaluation d'une assertion
	ASSERT_CALLBACK 	assert.callback 	(NULL) 	fonction de rappel utilisateur, pour le traitement des assertions fausses
	*/
	assert_options( ASSERT_ACTIVE, 1);
	assert_options( ASSERT_WARNING, 1 );
	assert_options( ASSERT_BAIL, 0 );
	//assert_options( ASSERT_CALLBACK , 'myAssertCallback');

	/*
	 ob_start();
	phpinfo();
	file_put_contents('.phpinfos.out.txt', ob_get_contents());
	*/

	echo 'include path: ' . get_include_path ()  . PHP_EOL;
	
	$zver = new \Zend\Version\Version();
	echo 'ZEND VERSION : ' . $zver::VERSION . "\n";
	
	//For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
	$i=1;
	while($ENV['arg'.$i]){
		$argv[$i] = $ENV['arg'.$i];
		$i++;
	}

	$shortopts = '';
	$shortopts .= "c:"; // La classe <class> de test a lancer 
	$shortopts .= "m:"; // La methode <method> a executer
	$shortopts .= "a";  // Execute tous les tests
	$shortopts .= "h";  // affiche l'aide
	$shortopts .= "t:"; // call test function <function>
	$shortopts .= "i"; //initialise les données de test
	$shortopts .= "s"; //Extrait les scripts sql depuis les classes
	$shortopts .= "p"; //Active le profiling des tests
	
	$longopts  = array(
		'initdb',
		'nodao',
		'croot',
		'lc', //Affiche la liste des classes de test disponibles
		'lf:',//Affiche la liste des methode de la <classe> de test
	);

	$options = getopt($shortopts, $longopts);
	//var_dump($options);	die;
	
	if( isset($options['h']) ){
		return rbplmcli_displayhelp();
	}

	/**
	 * execute la fonction test spécifiée
	 */
	if ( isset($options['t']) ) {
		rbplmcli_optiont($options);
	}

	/**
	 * liste les fonctions de la classe spécifiée
	 */
	if(isset($options['lf'])){
		rbplmcli_optionlf($options);
	}

	/**
	 * liste les classes de tests
	 */
	if(isset($options['lc'])){
		rbplmcli_optionlc($options);
	}

	/**
	 * execute tous les test
	 */
	if ( isset($options['a']) ) {
		rbplmcli_optiona($options);
	}

	/**
	 *
	 */
	if ( isset($options['croot']) ) {
		rbplmcli_optioncroot($options);
	}

	if ( isset($options['initdb']) ) {
		rbplmcli_optioninitdb($options);
	}

	if ( isset($options['c']) ) {
		rbplmcli_optionc($options);
	}

	/**
	 * Extrait les scripts SQL depuis les classes DAO
	 */
	if ( isset($options['s']) ) {
		rbplmcli_options($options);
	}
}

/**
 * execute la fonction test spécifiée
 */
function rbplmcli_optiont($options)
{
	$suffix = $options['t'];
	if($suffix==''){
		$suffix='my';
	}
	call_user_func('rbplmcli_test_'.$suffix, array());
}

/**
 * execute tous les test
 */
function rbplmcli_optiona($options)
{
    rbplmcli_boot();
    
	$Test = new \Rbplm\Test\Test();
	
	if ( isset($options['nodao']) ) {
	    $Test->withDao(false);
	}
	else{
	    $Test->withDao(true);
	}
	
	if ( isset($options['p']) ) {
	    $Test->withProfiling(true);
	}
	else{
	    $Test->withProfiling(false);
	}
	
	$libPath=__DIR__.'/module/Rbplm/src/Rbplm';
	return $Test->runAllClasses($libPath);
}

/**
 * liste les fonctions de la classe spécifiée
 */
function rbplmcli_optionlf($options)
{
	echo 'List of test function:' .CRLF;
	$class=$options['lf'];
	
	$methods = get_class_methods($class);
	foreach($methods as $method){
		$t = explode('_', $method);
		if($t[0] == 'Test'){
			echo $method . CRLF;
		}
	}
	return;
}

/**
 * liste les classes de tests
 */
function rbplmcli_optionlc($options)
{
	echo 'List of test class:' . CRLF;
	$Test = new \Rbplm\Test\Test();
	$RbplmLibPath = realpath(__DIR__.'/module/Rbplm/src/Rbplm');
	
	var_dump($RbplmLibPath);
	foreach($Test->getTestsFiles($RbplmLibPath) as $path){
		$class = substr( $path, strlen($RbplmLibPath) );
		$class = trim($class, '/');
		$class = trim($class, '.php');
		$class = str_replace('/', '\\', $class);
		if($class == 'Test\Test'){
			continue;
		}
		$class = '\Rbplm\\'.$class;
		echo $class . CRLF;
	}
	return;
}

/**
 * create root node
 */
function rbplmcli_optioncroot($options)
{
	//$Schema = new \Rbplm\Dao\Schema( Connexion::get() );
	//$Schema->createUsers();
	//$Schema->createRootNode();
	echo "please, import data directly from SQL scripts" . PHP_EOL;
}

/**
 * Initialise la base de données
 */
function rbplmcli_optioninitdb($options)
{
    rbplmcli_boot();
    
	$RbplmLibPath = realpath(__DIR__.'/module/Rbplm/src/Rbplm');
	$Schema = new \Rbplm\Dao\Schema( Connexion::get() );
	$Schema->sqlInScripts = array(
		'sequence.sql',
		'create.sql',
		'insert.sql',
	);
	$sqlCompiledFile=$Schema->compileSchema($RbplmLibPath);
	$Schema->createBdd(file_get_contents($sqlCompiledFile));
}

/**
 * Execute les test de la classe ou des classes spécifiées
 */
function rbplmcli_optionc($options)
{
    rbplmcli_boot();
    
	$method = $options['m'];

	if ( isset($options['nodao']) ) {
		$withDao = false;
	}
	else{
		$withDao = true;
	}
	
	if ( isset($options['p']) ) {
	    $withProfiling=true;
	}
	else{
	    $withProfiling=false;
	}

	if(is_array($options['c'])){
		foreach($options['c'] as $class){
			\Rbplm\Test\Test::runOne($class, $method, $withDao, $continue=false, $withProfiling);
		}
	}
	else{
		\Rbplm\Test\Test::runOne($options['c'], $method, $withDao, $continue=false, $withProfiling);
	}
}

/**
 * Extrait les scripts SQL depuis les classes DAO
 */
function rbplmcli_options($options)
{
	$RbplmPath = realpath(__DIR__.'/module/Rbplm/src/Rbplm');
	$Extractor = new SqlExtractor($RbplmPath.'/Dao/Schemas/pgsql/extracted');
	$Extractor->parse($RbplmPath);
	echo 'See result in ' . $Extractor->resultPath . CRLF;
}

/**
 * Initialize
 */
function rbplmcli_boot()
{
    //Bootstrap
    $config = include(__DIR__.'/config/autoload/local.php');
    $daoMap = include(__DIR__.'/config/dist/objectdaomap.config.php');
    
    var_dump($config, $daoMap);
    
    Rbplm\Uuid::init();
    
    Connexion::setConfig($config['rbplm']['db']);
    DaoFactory::setMap($daoMap);
    
    Trash::init(realpath(__DIR__.'/data/trash'));
    
    $user = new User();
    $user->setLogin('Test')->setName('TesterFromCli');
    CurrentUser::set($user);
    
    Trash::init(realpath(__DIR__.'/data/trash'));
}

/**
 * Initialize
 */
function rbplmcli_test_connexion()
{
    rbplmcli_boot();

	$user = new User();
	DaoFactory::get()->getDao($user)->loadFromName($user, 'admin');
	CurrentUser::set($user);
}

/**
 * 
 */
function rbplmcli_test_my()
{
	//initialize
    rbplmcli_boot();
    
	$Test = new \Rbplm\Test\Test();
	$Test->withProfiling(true);
	$Test->loop = 1;
	$Test->withDao(true);
	$Test->add('\Rbplm\Ged\Docfile\VersionTest');
	$Test->add('\Rbplm\Ged\Document\VersionTest');

	$Test->run();
}

/**
 *
 */
function rbplmcli_test_instanceof()
{
    echo '$object is instance of AnyObject'."\n";
    $object=new AnyObject();
    assert($object instanceof AnyObject);
    
    $object=new Org\Unit();
    
    echo '$object is instance of Org\Unit'."\n";
    assert($object instanceof Org\Unit);
    
    echo '$object is instance of AnyObject'."\n";
    assert($object instanceof AnyObject);
}

/**
 *
 */
function rbplmcli_test_date()
{
    $tz = new DateTimeZone('Europe/Paris');
    $date = new DateTime('now',$tz);
    echo $date->format('Y-m-d H:i:s') . "\n";
    echo $date->getTimestamp() . "\n";
    
    $date = new DateTime('05 oct 2050',$tz);
    echo $date->format('Y-m-d') . "\n";
    echo $date->getTimestamp() . "\n";
    
    $date = new datetime();
    $dateTs = $date->setTimeStamp(time());
    echo $date->format('Y-m-d') . "\n";
}

/**
 * 
 * @throws \Exception
 * @throws \Rbplm\Sys\Exception
 * @throws \Rbplm\Dao\Exception
 */
function rbplmcli_test_exception()
{
	try{
		throw new \Exception('Sys exception message avec un %mot1%');
	}
	catch(\Rbplm\Sys\Exception $e){
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	catch(\Exception $e){
	}
	
	try{
		throw new \Rbplm\Sys\Exception('Sys exception message avec un %mot1%', 5000, array('mot1'=>'remplacepar'));
	}
	catch(\Rbplm\Sys\Exception $e){
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	catch(\Rbplm\Sys\Exception $e){
		var_dump($e->getMessage());
	}
	
	try{
		throw new \Rbplm\Sys\Exception('Sys exception message avec un %mot1%', 5000, array('mot1'=>'remplacepar'));
	}
	catch(\Exception $e){
		echo 'attrape SysException depuis \Exception'.CRLF;
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	
	
	try{
		throw new \Rbplm\Dao\Exception('Sys exception message avec un %mot1%', 5000, array('mot1'=>'remplacepar'));
	}
	catch(\Rbplm\Sys\Exception $e){
		echo 'attrape DaoException depuis SysException'.CRLF;
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	
}

/**
 *
 */
function rbplmcli_test_factory()
{
    rbplmcli_boot();
    $dao = \Rbplm\Dao\Factory::getDao(110);
    var_dump($dao);
}

rbplmcli_run();
