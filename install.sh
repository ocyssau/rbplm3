#!/bin/bash
sudo apt-get install curl apache2 php5 postgresql php5-pgsql postgresql-contrib pgadmin3

curl -s https://getcomposer.org/installer | php --
php composer.phar self-update
php composer.phar install
cp vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist config/autoload/zdt.local.php

sudo -u postgres dropdb --interactive rbplm
sudo -u postgres dropdb --interactive rbplm-test
sudo -u postgres createuser -P -e --createrole rbplm
sudo -u postgres createdb rbplm --owner rbplm
sudo -u postgres createdb rbplm-test --owner rbplm
sudo -u postgres psql -d rbplm <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF
sudo -u postgres psql -d rbplm-test <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF

php ./vendor/Rbplm/rbplm.cli.php --initdb

echo "Copier et editer le fichier de config ./config/autoload/local.php.dist -> local.php"
cp ./config/autoload/local.php.dist ./config/autoload/local.php
gedit ./config/autoload/local.php
