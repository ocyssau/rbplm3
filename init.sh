#!/bin/bash
sudo -u postgres dropdb --interactive rbplm
sudo -u postgres createuser -P -e --createrole rbplm
sudo -u postgres createdb rbplm --owner rbplm
sudo -u postgres psql -d rbplm <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF

php ./vendor/Rbh/test.cli.php --initdb
curl -sS https://getcomposer.org/installer | php
