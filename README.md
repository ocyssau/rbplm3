ZendSkeletonApplication
=======================

Introduction
------------
This is a simple, skeleton application using the ZF2 MVC layer and module
systems. This application is meant to be used as a starting place for those
looking to get their feet wet with ZF2.

Installation
------------

Using Composer (recommended)
----------------------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies using the `create-project` command:

    curl -s https://getcomposer.org/installer | php --
    php composer.phar create-project -sdev --repository-url="https://packages.zendframework.com" zendframework/skeleton-application path/to/install

Alternately, clone the repository and manually invoke `composer` using the shipped
`composer.phar`:

    cd my/project/dir
    git clone git://github.com/zendframework/ZendSkeletonApplication.git
    cd ZendSkeletonApplication
    php composer.phar self-update
    php composer.phar install

(The `self-update` directive is to ensure you have an up-to-date `composer.phar`
available.)

Another alternative for downloading the project is to grab it via `curl`, and
then pass it to `tar`:

    cd my/project/dir
    curl -#L https://github.com/zendframework/ZendSkeletonApplication/tarball/master | tar xz --strip-components=1

You would then invoke `composer` to install dependencies per the previous
example.

Using Git submodules
--------------------
Alternatively, you can install using native git submodules:

    git clone git://github.com/zendframework/ZendSkeletonApplication.git --recursive


Ubuntu server setup
-------------------
    	sudo apt-get install apache2 php5 zend-framework postgresql php5-pgsql postgresql-contrib pgadmin3
    	sudo apt-get install libapache2-mod-php5
    	sudo a2enmod rewrite
    	sudo a2enmod session
    	sudo a2ensite monsite
		cd /var
		wget (mettre ici le lien de mon package)
		tar -xvzf ZendFramework-x.x.x.tar.gz
		mv ZendFramework-1.0.0 Zend
    	sudo gedit /etc/php5/conf.d/zend-framework.ini
    		include_path = ".:/usr/share/php:/usr/share/pear:/usr/share/php/libzend-framework-php"
    	sudo service apache2 reload


Postgres server setup
---------------------

			Création de la base de données avec postgresql 9.1:
			<programlisting language="shell"><![CDATA[
			$ sudo -s -u postgres 
				Password: 
			$ psql
			postgres=# CREATE USER rbplm;
			postgres=# ALTER USER rbplm WITH ENCRYPTED PASSWORD 'rbplm';
			postgres=# ALTER ROLE rbplm WITH CREATEDB;
			postgres=# CREATE DATABASE rbplm OWNER rbplm;
			postgres=# GRANT ALL ON DATABASE rbplm TO rbplm;
			postgres=# \connect rbplm
			postgres=# CREATE LANGUAGE plpgsql;
			postgres=# CREATE EXTENSION ltree;
			\q
			$ exit

OU 

			sudo -u postgres dropdb --interactive rbplm
			sudo -u postgres createuser -P -e --createdb --createrole rbplm
			sudo -u postgres createdb rbh-test --owner rbplm
			sudo -u postgres psql -d rbplm <<EOF
			\x
			CREATE LANGUAGE plpgsql;
			CREATE EXTENSION ltree;
			EOF

OU utilisez le script bash
init.sh

//creer les tables:
$ cd [RBPLM_APP_DIR]/vendor/Rbh
$ php ./test.cli.php --initdb




<para>
	Télécharger et décompresser la librairie, vous devez ajouter le dossier "/library" de la distribution en début de votre chemin d'inclusion ("include_path").
	Zend framework sera aussi nécessaire, il devra donc être disponile dans le "include_path".
	Vous pouvez bien entendu aussi déplacer la librairie à tout autre position (partagée ou non) dans votre arborescence de fichiers.
</para>

		<para>
			Il ne reste plus qu'a créer la base de données, puis y importer les scripts <programlisting language="php"><![CDATA[/Rbplm/Dao/Schema/pgsql/compiled]]></programlisting> dans l'ordre suivant :
				create.sql
				insert.sql
				alter.sql
				trigger.sql
				foreignKey.sql
				view.sql
			Vous pourriez aussi inclure directement le scripts schema.xx.sql ou xx est le numéro de version du schéma.
		</para>
    
        Une fois que vous avez téléchargé et
        décompressé la librairie, vous devez ajouter le dossier "/library" de la distribution en
        début de votre chemin d'inclusion ("<programlisting language="php"><![CDATA[include_path]]></programlisting>"). Vous pouvez bien entendu
        aussi déplacer la librairie à tout autre position (partagée ou non) dans votre arborescence
        de fichiers.
        <itemizedlist>
            <listitem>
                <para>
                    <ulink url="http://www.rbplm.com/download/latest">Téléchargement de
                    la dernière version stable&#160;:</ulink> Cette version, disponible à la fois au
                    format <programlisting language="php"><![CDATA[.zip]]></programlisting> et au format <programlisting language="php"><![CDATA[.tar.gz]]></programlisting>, est un bon choix
                    pour ceux qui débutent avec Rbplm.
                </para>
            </listitem>

            <listitem>
                <para>
                    Utilisation d'un client <ulink
                    url="http://subversion.tigris.org">Subversion</ulink> (SVN)&#160;: Rbplm
                    st un logiciel open-source, et le référentiel Subversion utilisé
                    pour son développement est disponible publiquement. Considérer l'utilisation de
                    SVN pour récupérer Rbplm si vous utilisez déjà SVN pour vos propres
                    développements, si vous voulez contribuer à l'évolution du framework, ou si vous
                    désirez mettre à jour votre version du framework plus souvent que les sorties
                    stables.
                </para>

                <para>
                    <ulink url="">L'exportation</ulink>
                    est utile si vous souhaitez obtenir une révision particulière du framework sans
                    les dossiers <programlisting language="php"><![CDATA[.svn]]></programlisting> créé dans une copie de travail.
                </para>

                <para>
                    <ulink url="">L'extraction
                    d'une copie de travail</ulink> est intéressante si vous contribuez à Zend
                    Framework, et une copie de travail peut être mise à jour à n'importe quel moment
                    avec <ulink
                    url=""><programlisting language="php"><![CDATA[svn
                    update]]></programlisting></ulink> et les changements peuvent être livrés au référentiel SVN
                    avec la commande <ulink
                    url=""><programlisting language="php"><![CDATA[svn
                    commit]]></programlisting></ulink>.
                </para>

                <para>
                    Une <ulink url="">définition
                    externe</ulink> est très pratique pour les développeurs utilisant déjà SVN pour
                    gérer les copies de travail de leurs applications.
                </para>

                <para>
                    L'URL du tronc du référentiel SVN de Zend Framework est :
                    <ulink url=""></ulink>
                </para>
            </listitem>
        </itemizedlist>
    </para>

    <para>
        Une fois votre copie de Rbplm disponible, votre application nécessite d'avoir
        accès aux classes du framework. Bien qu'il y ait
        <ulink url="http://www.php.net/manual/fr/configuration.changes.php">plusieurs manières de réaliser
        ceci</ulink>, votre
        <ulink url="http://www.php.net/manual/fr/ini.core.php#ini.include-path"><programlisting language="php"><![CDATA[include_path]]></programlisting></ulink>
        de PHP doit contenir le chemin vers la bibliothèque de Zend Framework et Rbplm.
    </para>




Web Server Setup
----------------

### PHP CLI Server

The simplest way to get started if you are using PHP 5.4 or above is to start the internal PHP cli-server in the root directory:

    php -S 0.0.0.0:8080 -t public/ public/index.php

This will start the cli-server on port 8080, and bind it to all network
interfaces.

**Note: ** The built-in CLI server is *for development only*.

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName rbplm
        DocumentRoot /path/to/rbplm/public
        SetEnv APPLICATION_ENV "development"
        <Directory /path/to/rbplm/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
